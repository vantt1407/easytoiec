<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DatabaseDrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:drop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        try {
            $schemaName = config("database.connections.mysql.database");
            $query = "DROP IF EXISTS DATABASE `$schemaName` ;";
            DB::statement($query);
            $this->info("Drop $schemaName database successfully");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}

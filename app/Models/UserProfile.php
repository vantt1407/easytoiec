<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    public $primaryKey = 'user_profile_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_profile_phone', 'user_profile_full_name','user_profile_address','user_id'
    ];


    public function user()
    {
        return $this->hasOne(\App\Models\User::class, 'user_id', 'user_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vocabulary extends Model
{
    use SoftDeletes;

    public $primaryKey = 'vocabulary_id';

    protected $appends = ['action'];

    protected $fillable = [
        'vocabulary_word', 'antonym_id', 'topic_id', 'vocabulary_type_id', 'vocabulary_vi_translate', 'vocabulary_audio', 'vocabulary_image', 'vocabulary_pronounce',

    ];

    public function topic()
    {
        return $this->hasOne(\App\Models\Topic::class, 'topic_id', 'topic_id');
    }

    public function antonym()
    {
        return $this->hasOne(\App\Models\Vocabulary::class, 'antonym_id', 'vocabulary_id');
    }

    public function dataType()
    {
        return $this->hasOne(\App\Models\VocabularyType::class, 'vocabulary_type_id', 'vocabulary_type_id');
    }

    public function getActionAttribute($data)
    {
        return '<a href="' . route('vocabulary.edit', $this->vocabulary_id) . '" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa" title="" aria-expanded="false">
                    <i class="uil-pen text-warning"></i>
               </a>';
    }
}

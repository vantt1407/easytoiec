<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LessonPartQuestion extends Model
{
   
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'lesson_part_id','question_id'
    ];
}

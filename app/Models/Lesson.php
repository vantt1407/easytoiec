<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    
    public $primaryKey = 'lesson_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'level_id', 'lesson_title','lesson_avatar','lesson_long_description', 'lesson_description','lesson_slug','created_by'
    ];

    public function lessonParts()
    {
        return $this->hasMany(\App\Models\LessonPart::class, 'lesson_id', 'lesson_id');
    }

}

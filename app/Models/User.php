<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Providers\AuthServiceProvider;
use App\Models\Role; 
use Illuminate\Database\Eloquent\SoftDeletes; 

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    public $primaryKey = 'user_id';

    // protected $dateFormat = 'Y-m-d';

    public static function boot()
    {
        parent::boot();
        
        static::creating(function ($model) {
            $userRole = Role::firstWhere('role_name', AuthServiceProvider::USER_ROLE);
            $model->role_id = $userRole->role_id;
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        // 'created_at' => 'date',
    ];


    public function role()
    {
        return $this->hasOne(\App\Models\Role::class, 'role_id', 'role_id');
    }

    public function profile()
    {
        return $this->hasOne(\App\Models\UserProfile::class, 'user_id', 'user_id');
    }

    public function userTests() {

        return $this->hasMany(\App\Models\UserTest::class, 'user_id', 'user_id');
        
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{   
    use SoftDeletes;
    
    public $primaryKey = 'topic_id';


    protected $fillable = [
        'topic_name', 'topic_avatar', 'topic_slug', 'topic_description'
    ];

    public function vocabularies()
    {
        return $this->hasMany(\App\Models\Vocabulary::class,'topic_id','topic_id');
    }

}

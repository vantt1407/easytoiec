<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class QuestionGroup extends Model
{
    public $primaryKey = 'question_group_id';
    
    protected $appends = ['action','check'];

    protected $fillable = [
     'part_id', 'question_group_question','question_group_image', 'question_group_audio'
    ];

    public function test()
    {
        return $this->belongsToMany(\App\Models\Test::class,'test_questions','test_id','question_group_id');
    }

    public function questions()
    {
        return $this->hasMany(\App\Models\Question::class,'question_group_id','question_group_id');
    }

    public function getActionAttribute()
    {
        return '<a href="' . route('question-group.edit', $this->question_group_id) . '" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa" title="" aria-expanded="false">
                    <i class="uil-pen text-warning"></i>
               </a>';
    }

    public function getCheckAttribute()
    {
        return '<input class="skin-square check" type="checkbox" value="'.$this->question_group_id.'">';
    }

    public function part() {
        return $this->hasOne(\App\Models\Part::class,'part_id','part_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Part extends Model
{
    use SoftDeletes;

    public $primaryKey = 'part_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'part_name','part_slug','part_description', 'part_avatar','part_type_id'
    ];

    public function partType()
    {
        return $this->hasOne(\App\Models\PartType::class, 'part_type_id', 'part_type_id');
    }

    public function questions()
    {
        return $this->hasMany(\App\Models\Question::class,'part_id','part_id');
    }


    
}

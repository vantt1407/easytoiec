<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LessonPart extends Model
{
    public $primaryKey = "lesson_part_id";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lesson_id','lesson_part_title', 'lesson_part_slug','lesson_part_content','lesson_video'
    ];


    public function questions()
    {
        return $this->belongsToMany(\App\Models\Question::class, 'lesson_part_questions', 'lesson_part_id', 'question_id');
    }
    

    public function questionGroups()
    {
        return $this->belongsToMany(\App\Models\QuestionGroup::class, 'lesson_part_questions', 'lesson_part_id', 'question_group_id');
    }
}

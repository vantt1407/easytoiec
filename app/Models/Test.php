<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{

    // use SoftDeletes;

    public $primaryKey = 'test_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'test_name',
        'test_time',
        'test_slug' 
    ];


    public function question_groups() {

        return $this->belongsToMany(\App\Models\QuestionGroup::class, 'test_questions', 'test_id', 'question_group_id');

    }


}

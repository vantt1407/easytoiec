<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $primaryKey = 'answer_id';


    protected $fillable = [
        'answer_option',
        'answer_is_correct',
        'question_id'
    ];

}

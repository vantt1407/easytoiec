<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Practice extends Model
{
    
    use SoftDeletes;

    public $primaryKey = 'practice_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'practice_name',
        'practice_slug',
        'level_id' 
    ];


    public function questions() {

        return $this->belongsToMany(\App\Models\Question::class, 'practice_questions', 'practice_id', 'question_id');

    }
}

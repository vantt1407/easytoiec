<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTest extends Model
{
    

    public function userTestAnswers()
    {
       return $this->hasMany(\App\Models\UserTestAnswer::class,'user_test_id','id');
    }

    public function test()
    {
        return $this->hasOne(\App\Models\Test::class,'test_id','test_id');
    }
}

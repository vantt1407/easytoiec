<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;
    //

    public $primaryKey = 'article_id';


    protected $fillable = [
        'article_avatar',
        'article_title',
        'article_slug',
        'article_content',
        'article_view_count',
        'article_category_id',
        'created_by',
        'article_publish'
    ];

    public function category()
    {
        return $this->hasOne(\App\Models\ArticleCategory::class, 'article_category_id', 'article_category_id');
    }
    public function author()
    {
        return $this->hasOne(\App\Models\User::class,'user_id','created_by');
    }
}

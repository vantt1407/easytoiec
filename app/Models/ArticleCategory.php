<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleCategory extends Model
{
    use SoftDeletes;

    public $primaryKey = 'article_category_id';


    protected $fillable = [
        'article_category_name','article_category_slug', 'article_category_parent_id'
    ];

    public function articles() {
        return $this->hasMany(\App\Models\Article::class,'article_category_id','article_category_id');
    }
}

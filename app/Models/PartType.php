<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartType extends Model
{
    public $primaryKey = 'part_type_id';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VocabularyType extends Model
{
    public $primaryKey = 'vocabulary_type_id';


    protected $fillable = [
        'vocabulary_type',  'vocabulary_symbol',

    ];

    public function vocabularies()
    {
        return $this->hasMany(\App\Models\Vocabulary::class, 'vocabulary_type_id', 'vocabulary_type_id');
    }
}

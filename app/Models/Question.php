<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    // use SoftDeletes;
    
    public $primaryKey = 'question_id';
    
    protected $appends = ['action','check'];

    protected $fillable = [
        'question', 'question_scores', 'question_audio', 'question_image','part_id','question_time','question_group_id'
    ];

    public function questionGroup()
    {
        return $this->hasOne(\App\Models\QuestionGroup::class,'question_group_id','question_group_id');
    }

    public function answers() {
        return $this->hasMany(\App\Models\Answer::class,'question_id','question_id');
    }

    public function practice()
    {
        return $this->hasOne(\App\Models\Practice::class,'practice_questions', 'practice_id', 'question_id');
    }

    public function part() {
        return $this->hasOne(\App\Models\Part::class,'part_id','part_id');
    }

    protected $casts = [
        'question_time' => 'integer'
    ];

    public function getActionAttribute()
    {
        return '<a href="' . route('question.edit', $this->question_id) . '" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa" title="" aria-expanded="false">
                    <i class="uil-pen text-warning"></i>
               </a>';
    }

    public function getCheckAttribute()
    {
        return '<input class="skin-square check" type="checkbox" value="'.$this->question_id.'">';
    }
}

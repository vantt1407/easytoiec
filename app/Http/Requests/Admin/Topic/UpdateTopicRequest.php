<?php

namespace App\Http\Requests\Admin\Topic;

use Illuminate\Foundation\Http\FormRequest;

class CreateTopicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'topic_avatar' => 'required',
            'topic_name' => 'required|max:200',
            'topic_description' => 'required|max:1500',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'topic_avatar.required' => 'Ảnh đại diện không được bỏ trống !',
            'topic_name.required' => 'Tên chủ đề không được bỏ trống !',
            'topic_name.max' => 'Tên chủ đề quá dài, tối đa 200 ký tự !',
            'topic_description.required'  => 'Mô tả ngắn không được bỏ trống !',
            'topic_description.max' => 'Mô tả quá dài, tối đa 1500 ký tự !'
        ];
    }
}

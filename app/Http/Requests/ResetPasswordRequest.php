<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|string|min:6',
            'confirm_password' =>'same:password'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.required'  => 'Mật khẩu không được bỏ trống !',
            'password.string'  => 'Mật khẩu không hợp lệ !',
            'password.min'  => 'Mật khẩu quá ngắn, tối thiểu 6 kí tự !',
            'confirm_password.same'  => 'Mật khẩu nhập lại không khớp !',
        ];
    }
}

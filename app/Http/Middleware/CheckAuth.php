<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $nameRoute = $request->route()->getName();

        if( $nameRoute  != 'test_detail' || $nameRoute  != 'submit_test') {

            Session::forget('data_1');
            Session::forget('data_2');
            Session::forget('data_3');
            Session::forget('data_4');
            Session::forget('data_5');
            Session::forget('data_6');
            Session::forget('data_7');
            Session::put('remove_time',true);
        }
        if (Auth::check()) {



            return $next($request);
        }
        return redirect()->route('get_login'); 
    }
}

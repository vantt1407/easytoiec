<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Mail\RegisterUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Mail;
use App\Models\UserProfile;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showView()
    {
        return view('auth.register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function register(RegisterRequest $request)
    {
        try {

        
            $credentials = $request->only('email', 'password');
            
            $credentials['password'] = Hash::make($credentials['password']);

            $user = User::create($credentials);

            UserProfile::create([
                'user_profile_avatar' => 'assets/images/no-avt.png',
                'user_profile_full_name' => '',
                'user_id' => $user->user_id,
                'user_profile_address' => '',
                'user_profile_phone' => ''
            ]);

            $request->session()->flash('register_success','Đăng kí thành công, truy cập email để xác thực !');

            $url = route('put_verify',$credentials['email']);

            $subject = 'Xác nhận email !';

            Mail::queue(new RegisterUser($credentials['email'],$url,$subject) );

            return redirect()->route('get_login')->withInput();

        } catch (\Exception $e) {
            throw $e;
            $error = ['auth_fail' => "Đăng kí thất bại !"];
            return redirect()->back()->withInput()->withErrors($error);
        }
    }

}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\AuthServiceProvider;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Support\Str;
use App\Models\AccessUserInformation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLogin()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        $error = ['auth_fail' => "Sai tài khoản hoặc mật khẩu !"];

        try {

            $credentials = $request->only('email', 'password');

            $remember = $request->remember;

            if (Auth::attempt($credentials, $remember)) {

                $user = Auth::user();

                if (date('d', $user->last_login / 1000) != date('d', strtotime(now())))

                    $this->saveLastLogin($user->user_id);

                $role =  $user->role->role_name;

                if ($role === AuthServiceProvider::USER_ROLE)

                    return redirect()->route('get_dashboard');

                return redirect()->route('get_admin_dashboard');
                
            } else {

                return redirect()->back()->withInput()->withErrors($error);
            }
        } catch (\Exception $e) {

            //throw $e;

            return redirect()->back()->withInput()->withErrors($error);
        }
    }


    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->flush();

        return redirect()->route('home');
    }




    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($service)
    {

        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($service)
    {

        $userSocial = Socialite::driver($service)->user();


        $user = User::where('email', $userSocial->email)->first();

        if (!$user) {
            $user = new User;
            $user->email = $userSocial->email;
            $user->password = bcrypt(Str::random(6));
            $user->last_login = 0;
            $user->email_verified_at = now();
            $user->save();

            try {
                $profile = new  UserProfile();
                $profile->user_id = $user->user_id;
                $profile->user_profile_avatar  = $userSocial->avatar_original;
                $profile->user_profile_phone =  "";
                $profile->user_profile_full_name =  $userSocial->name;
                $profile->user_profile_address =  "";
                $profile->save();
            } catch (\Exception $e) {
            }
        }
        Auth::login($user, true);
        
        if (date('d', (int)($user->last_login / 1000)) != date('d', strtotime(now())))

            $this->saveLastLogin($user->user_id);

        return redirect()->route('get_dashboard');
    }

    private function saveLastLogin($userId)
    {   
        $time =strtotime(now()) * 1000;
        $user = User::findOrFail($userId);
        $user->last_login =  $time;
        $user->save();
        
        $total = DB::select("SELECT count(user_id) as total_in_day
        FROM users 
        WHERE DATE(FROM_UNIXTIME(last_login/1000)) = CURDATE()");

        $total = $total[0] ? $total[0]->total_in_day : 0;

        $daily = AccessUserInformation::firstWhere('created_at',Carbon::today());
        if( $daily) {
            $daily->total_in_day= $total;
            $daily->save();
        }else {
            $daily = new AccessUserInformation();
            $daily->total_in_day= $total;
            $daily->save();
        }
       
    }
}

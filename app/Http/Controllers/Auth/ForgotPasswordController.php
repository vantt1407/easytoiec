<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Support\Str;
use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ForgotPasswordRequest;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    // use SendsPasswordResetEmails;

    public function showView()
    {

        return view('auth.passwords.email');
    }


    public function requestForgotPassword(ForgotPasswordRequest $request)
    {
        try {
            $email = $request->email;
            $user = User::firstWhere('email', $email);
            if ($user) {
                $token = Hash::make(Str::random(5));
                $status = false;
                $old = DB::table('password_resets')->where('email', $email)->first();
                if (!$old->token) {
                    $status = DB::table('password_resets')->insert(['email' => $email, 'token' => $token]);
                } else {
                    $token  = $old->token;
                    $status = true;
                }
                if ($status) {

                    $resetUrl = route('get_reset', ['email' => $email])."?token=$token";

                    Mail::queue(new ForgotPassword($email, $resetUrl, 'Đổi mật khẩu'));
                } else {
                    throw new \Exception('Có lỗi xảy ra, thử lại sau !');
                }
                $request->session()->flash('request_success', 'Yêu cầu lấy lại mật khẩu thành công !');
                return redirect()->back();
            } else {
                $error = ['email' => "Địa chỉ email không tồn tại !"];
                return redirect()->back()->withInput()->withErrors($error);
            }
        } catch (\Exception $e) {
            // throw $e;
            $error = ['email' => "Có lỗi xảy ra, thử lại sau !"];
            return redirect()->back()->withInput()->withErrors($error);
        }
    }
}

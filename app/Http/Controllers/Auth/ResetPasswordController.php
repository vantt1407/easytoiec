<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ResetPasswordRequest;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    public function showView(Request $request, $email)
    {
        $token = $request->token;

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $email]
        );
    }

    public function reset(ResetPasswordRequest $request)
    {
        try {
            $email =  $request->email;
            $token =  $request->token;
            $password = $request->password;
            $userReset = DB::table('password_resets')->where(['email' => $email, 'token' => $token])->first();
            if ($userReset) {
                $status = DB::table('users')
                    ->where('email', $email)
                    ->update(['password' => Hash::make($password)]);
                DB::table('password_resets')->where(['email' => $email, 'token' => $token])->delete();
                $request->session()->flash('reset_success', 'Thay đổi mật khẩu thành công !');
                return $status ? redirect()->route('get_login') : abort(500);
            } else {
                abort(404);
            }
        } catch (\Exception $e) {
            abort(500);
        }
    }
}

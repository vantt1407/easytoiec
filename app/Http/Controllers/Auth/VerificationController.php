<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }


    public function verifyEmail($email, Request $request)
    {
        try {

            $user = User::firstWhere([['email', '=', $email], ['email_verified_at', '=', null]]);

            if ($user) {

                $status = DB::table('users')
                    ->where('user_id', $user->user_id)
                    ->update(['email_verified_at' => now()]);

                $request->session()->flash('email_verified_success', 'Xác thực email thành công !');

                return  $status ? redirect()->route('get_login') :  abort(500);
            } else {
                abort(404);
            }
        } catch (\Exception $e) {
            abort(500);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showHome()
    {
        $config = Setting::find(1);
        
        return view('member.home',compact('config'));
    }
}

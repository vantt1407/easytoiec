<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Answer;
use App\Models\Question;
use App\Models\QuestionGroup;
use HungCP\PhpSimpleHtmlDom\HtmlDomParser;
use Illuminate\Support\Facades\DB;

class CrawlController extends Controller
{

    public function showView()
    {
        return view('crawl');
    }


    public function showViewV2()
    {

        return view('crawl.index');
    }





    public function handle(Request $request)
    {
        for ($j = 10; $j <= 10; $j++) {

            $this->handlePart1("/Users/macbookpro/Desktop/easytoiec/public/crawl/toeic24h/new/de{$j}/part1/index.html");

            $this->handlePart2("/Users/macbookpro/Desktop/easytoiec/public/crawl/toeic24h/new/de{$j}/part2/index.html");

            for ($i = 1; $i <= 13; $i++) {
                $this->handlePart3($i, "/Users/macbookpro/Desktop/easytoiec/public/crawl/toeic24h/new/de{$j}/part3/g{$i}.html");
            }

            for ($i = 1; $i <= 10; $i++) {
                $this->handlePart4("/Users/macbookpro/Desktop/easytoiec/public/crawl/toeic24h/new/de{$j}/part4/g{$i}.html");
            }

            $this->handlePart5("/Users/macbookpro/Desktop/easytoiec/public/crawl/toeic24h/new/de{$j}/part5/index.html");

            for ($i = 1; $i <= 4; $i++) {
                $this->handlePart6("/Users/macbookpro/Desktop/easytoiec/public/crawl/toeic24h/new/de{$j}/part6/g{$i}.html");
            }

            for ($i = 1; $i <= 16; $i++) {
                $this->handlePart7("/Users/macbookpro/Desktop/easytoiec/public/crawl/toeic24h/new/de{$j}/part7/g{$i}.html");
            }
        }
    }


    private function handlePart1($link)
    {
        try {


            $str2 = file_get_contents($link);
        } catch (\Exception $e) {
            return;
        }
        $dom = HtmlDomParser::str_get_html($str2);

        // $questionGroup = $dom->find(".question-content .text-justify b",0);



        $audio = $dom->find("audio source");

        $imgs = $dom->find("img");

        $questions = $dom->find("form p b");


        $listImg = [];

        $listAudioQuestion = [];

        $listQuestion = [];

        $listAnswer = [];

        foreach ($imgs as $src) {


            $listImg[] = $src->src;
        }

        foreach ($audio as $src) {

            $listAudioQuestion[] = $src->src;
        }



        $pattern = '/^[0-9]{1}.\ /';

        foreach ($questions as $q) {
            $listQuestion[] = preg_replace($pattern, '', $q->plaintext);
        }

        $answers = $dom->find(".answer-list");
        foreach ($answers as $a) {
            $ai = $a->find('label');
            $ast = [];
            foreach ($ai as $key =>  $i) {
                $ast[$key]['name'] = $i->plaintext;
                $ast[$key]['is_correct'] = !empty($i->find('i', 1));
            }
            $listAnswer[] = $ast;
        }

        // dd( $listImg , $listAudioQuestion , $listQuestion, $listAnswer);

        $this->insertDataPart1($listQuestion, $listAnswer, $listAudioQuestion, $listImg);
    }
    private function handlePart2($link)
    {
        $str2 = file_get_contents($link);

        $dom = HtmlDomParser::str_get_html($str2);

        //$questionGroup = $dom->find(".question-content .text-justify b",0);



        $audio = $dom->find("audio source");

        $questions = $dom->find(".col-md-5 .answer p b");



        $listAudioQuestion = [];

        $listQuestion = [];

        $listAnswer = [];

        foreach ($audio as $src) {

            $listAudioQuestion[] = $src->src;
        }

        $pattern = '/^[0-9]{1}.\ ||^[0-9]{2}.\ /';

        foreach ($questions as $q) {
            $listQuestion[] = preg_replace($pattern, '', $q->plaintext);
        }

        $answers = $dom->find(".col-md-5 .answer .answer-list");
        foreach ($answers as $a) {
            $ai = $a->find('label');
            $ast = [];
            foreach ($ai as $key =>  $i) {
                $ast[$key]['name'] = $i->plaintext;
                $ast[$key]['is_correct'] = !empty($i->find('i', 1));
            }
            $listAnswer[] = $ast;
        }

        // dd($listQuestion, $listAnswer, $listAudioQuestion);

        $this->insertDataPart2($listQuestion, $listAnswer, $listAudioQuestion);

        return "OK!";
    }
    private function handlePart3($i, $link)
    {
        try {
            $str3 = file_get_contents($link);

            $dom = HtmlDomParser::str_get_html($str3);

            $listDescription = [];

            $listAudio = [];

            $listQuestion = [];

            $listAnswer = [];

            $imgs = $dom->find("img");
            $listImg = [];
            foreach ($imgs as $src) {
                $listImg['i' . $i] = $src->src;
            }

            $ds = $dom->find('form div.text-justify');
            $ad =  $dom->find('audio source');
            $qs =  $dom->find('.answer p b');
            foreach ($ds as $item) {

                $listDescription[] = $item->outertext;
            }

            foreach ($ad  as $item) {
                $listAudio[] = $item->src;
            }
            $pattern = '/^[0-9]{2}.\ /';
            foreach ($qs  as $item) {
                $listQuestion[] =  preg_replace($pattern, '',  $item->plaintext);
            }


            $answers = $dom->find(".answer .answer-list");
            foreach ($answers as $a) {
                $ai = $a->find('label');
                $ast = [];
                foreach ($ai as $key =>  $i) {
                    $ast[$key]['name'] = $i->plaintext;
                    $ast[$key]['is_correct'] = !empty($i->find('i', 1));
                }
                $listAnswer[] = $ast;
            }
            // dd($listImg,$listDescription, $listAudio, $listQuestion, $listAnswer,$listImg,$i);

            $this->insertDataPart3($listDescription, $listAudio, $listQuestion, $listAnswer, $listImg, $i);
        } catch (\Exception $e) {
            return;
        }
    }

    private function handlePart4($link)
    {
        try {
            $str4 = file_get_contents($link);
        } catch (\Exception $e) {
            return;
        }


        $dom = HtmlDomParser::str_get_html($str4);

        $listDescription = [];

        $listAudio = [];

        $listQuestion = [];

        $listAnswer = [];

        $ds = $dom->find('form div.text-justify');
        $ad =  $dom->find('audio source');
        $qs =  $dom->find('.answer p b');
        foreach ($ds as $item) {

            $listDescription[] = $item->outertext;
        }

        foreach ($ad  as $item) {
            $listAudio[] = $item->src;
        }
        $pattern = '/^[0-9]{2}.\ /';
        foreach ($qs  as $item) {
            $listQuestion[] =  preg_replace($pattern, '',  $item->plaintext);
        }


        $answers = $dom->find(".answer .answer-list");
        foreach ($answers as $a) {
            $ai = $a->find('label');
            $ast = [];
            foreach ($ai as $key =>  $i) {
                $ast[$key]['name'] = $i->plaintext;
                $ast[$key]['is_correct'] = !empty($i->find('i', 1));
            }
            $listAnswer[] = $ast;
        }

        // dd($listDescription, $listAudio, $listQuestion, $listAnswer);

        $this->insertDataPart4($listDescription, $listAudio, $listQuestion, $listAnswer);
    }

    private function handlePart5($link)
    {
        try {
            $str5 = file_get_contents($link);
        } catch (\Exception $e) {
            return;
        }


        $dom = HtmlDomParser::str_get_html($str5);

        $questionGroup = "Mark your answer on your answer sheet:";

        $questions = $dom->find("form .answer p b");

        // $listAudioQuestion = [];

        $listQuestion = [];

        $listAnswer = [];

        $pattern = '/^[0-9]{3}.\ /';

        foreach ($questions as $q) {
            $listQuestion[] = preg_replace($pattern, '', $q->plaintext);
        }

        $answers = $dom->find("form .answer .answer-list");

        foreach ($answers as $a) {
            $ai = $a->find('label');
            $ast = [];
            foreach ($ai as $key =>  $i) {
                $ast[$key]['name'] = $i->plaintext;
                $ast[$key]['is_correct'] = !empty($i->find('i', 1));
            }
            $listAnswer[] = $ast;
        }
        // dd($listQuestion, $listAnswer, $questionGroup);
        $this->insertDataPart5($listQuestion, $listAnswer, $questionGroup);

        return "OK!";
    }

    private function handlePart6($link)
    {
        try {
            $str3 = file_get_contents($link);
        } catch (\Exception $e) {
            return;
        }
        $dom = HtmlDomParser::str_get_html($str3);

        $listDescription = [];

        $listAudio = [];

        $listQuestion = [];

        $listAnswer = [];

        $ds = $dom->find('.question-content .ct');

        $qs =  $dom->find('.answer p b');
        foreach ($ds as $item) {

            $listDescription[] = $item->outertext;
        }

        $pattern = '/^[0-9]{3}.\ /';
        foreach ($qs  as $item) {
            $listQuestion[] =  preg_replace($pattern, '',  $item->plaintext);
        }


        $answers = $dom->find(".answer .answer-list");
        foreach ($answers as $a) {
            $ai = $a->find('label');
            $ast = [];
            foreach ($ai as $key =>  $i) {
                $ast[$key]['name'] = $i->plaintext;
                $ast[$key]['is_correct'] = !empty($i->find('i', 1));
            }
            $listAnswer[] = $ast;
        }
        // dd($listDescription, $listQuestion, $listAnswer);
        $this->insertDataPart6($listDescription, $listQuestion, $listAnswer);
    }
    private function handlePart7($link)
    {
        try {
            $str3 = file_get_contents($link);
        } catch (\Exception $e) {
            return;
        }
        $dom = HtmlDomParser::str_get_html($str3);

        $listDescription = [];

        $listImg = [];

        $listQuestion = [];

        $listAnswer = [];

        $ds = $dom->find('div.question-content .text-justify');

        $qs =  $dom->find('.answer p b');

        $imgs = $dom->find('img');

        foreach ($imgs as $src) {


            $listImg[] = $src->src;
        }
        foreach ($ds as $item) {

            $listDescription[] = $item->outertext;
        }

        $pattern = '/^[0-9]{3}.\ /';
        foreach ($qs  as $item) {
            $listQuestion[] =  preg_replace($pattern, '',  $item->plaintext);
        }


        $answers = $dom->find(".answer .answer-list");
        foreach ($answers as $a) {
            $ai = $a->find('label');
            $ast = [];
            foreach ($ai as $key =>  $i) {
                $ast[$key]['name'] = $i->plaintext;
                $ast[$key]['is_correct'] = !empty($i->find('i', 1));
            }
            $listAnswer[] = $ast;
        }

        // dd($listDescription, $listQuestion, $listAnswer, $listImg);
        $this->insertDataPart7($listDescription, $listQuestion, $listAnswer, $listImg);
    }

    private function insertDataPart1($listQuestion, $listAnswer, $listAudioQuestion, $listImg)
    {
        try {
            DB::beginTransaction();

            $qg =  QuestionGroup::create([
                'question_group_question' => 'Mark your answer on your answer sheet',
                'part_id' => 1,
                'question_group_image' =>  null,
                'question_group_audio' => null
            ]);

            $question_group_id = $qg->question_group_id;
            foreach ($listQuestion as $k => $i) {

                $question = Question::create([

                    'question' => $i,
                    'question_scores' => 3,
                    'question_audio' => $listAudioQuestion[$k],
                    'question_image' => $listImg[$k],
                    'part_id' => 1,
                    'question_group_id' =>  $question_group_id
                ]);

                $q_id =  $question->question_id;

                foreach ($listAnswer[$k] as  $ai) {
                    Answer::create([
                        'answer_option' => $ai['name'],
                        'answer_is_correct' => $ai['is_correct'],
                        'question_id' =>  $q_id
                    ]);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
    private function insertDataPart2($listQuestion, $listAnswer, $listAudioQuestion)
    {
        try {
            DB::beginTransaction();

            $qg =  QuestionGroup::create([
                'question_group_question' => 'Mark your answer on your answer sheet',
                'part_id' => 2,
                'question_group_image' =>  null,
                'question_group_audio' => null
            ]);

            $question_group_id = $qg->question_group_id;

            foreach ($listQuestion as $k => $i) {

                $question = Question::create([

                    'question' => $i,
                    'question_scores' => 3,
                    'question_audio' => $listAudioQuestion[$k],
                    'question_image' => null,
                    'part_id' => 2,
                    'question_group_id' => $question_group_id
                ]);

                $q_id =  $question->question_id;

                foreach ($listAnswer[$k] as  $ai) {
                    Answer::create([
                        'answer_option' => $ai['name'],
                        'answer_is_correct' => $ai['is_correct'],
                        'question_id' =>  $q_id
                    ]);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
    private function insertDataPart3($listDescription, $listAudio, $listQuestion, $listAnswer, $listImg, $i)
    {

        try {
            DB::beginTransaction();


            foreach ($listDescription as $k => $item) {
                $qg =  QuestionGroup::create([
                    'question_group_question' => $item,
                    'part_id' => 3,
                    'question_group_image' => isset($listImg['i' . $i]) ? $listImg['i' . $i] : null,
                    'question_group_audio' => $listAudio[$k]
                ]);

                $question_group_id = $qg->question_group_id;

                foreach ($listQuestion as $k => $i) {

                    $question = Question::create([

                        'question' => $i,
                        'question_scores' => 5,
                        'question_audio' => null,
                        'question_image' => null,
                        'part_id' => 3,
                        'question_group_id' => $question_group_id
                    ]);

                    $q_id =  $question->question_id;

                    foreach ($listAnswer[$k] as $ii => $ai) {
                        Answer::create([
                            'answer_option' => $ai['name'],
                            'answer_is_correct' => $ai['is_correct'],
                            'question_id' =>  $q_id
                        ]);
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    private function insertDataPart4($listDescription, $listAudio, $listQuestion, $listAnswer)
    {
        try {
            DB::beginTransaction();


            foreach ($listDescription as $k => $item) {
                $qg =  QuestionGroup::create([
                    'question_group_question' => $item,
                    'question_group_image' => null,
                    'part_id' => 4,
                    'question_group_audio' => $listAudio[$k]
                ]);

                $question_group_id = $qg->question_group_id;

                foreach ($listQuestion as $k => $i) {

                    $question = Question::create([

                        'question' => $i,
                        'question_scores' => 5,
                        'question_audio' => null,
                        'question_image' => null,
                        'part_id' => 4,
                        // 'question_time' => 20000,
                        'question_group_id' => $question_group_id
                    ]);

                    $q_id =  $question->question_id;

                    foreach ($listAnswer[$k] as  $ai) {
                        Answer::create([
                            'answer_option' => $ai['name'],
                            'answer_is_correct' => $ai['is_correct'],
                            'question_id' =>  $q_id
                        ]);
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    private function insertDataPart5($listQuestion, $listAnswer, $questionGroup)
    {
        try {
            DB::beginTransaction();

            $qg =  QuestionGroup::create([
                'question_group_question' => $questionGroup,
                'question_group_image' => null,
                'question_group_audio' => null,
                'part_id' => 5,
            ]);
            foreach ($listQuestion as $k => $i) {

                $question = Question::create([

                    'question' => $i,
                    'question_scores' => 3,
                    'question_audio' => null,
                    'question_image' => null,
                    'part_id' => 5,
                    'question_group_id' => $qg->question_group_id
                ]);

                $q_id =  $question->question_id;

                foreach ($listAnswer[$k] as  $ai) {
                    Answer::create([
                        'answer_option' => $ai['name'],
                        'answer_is_correct' => $ai['is_correct'],
                        'question_id' =>  $q_id
                    ]);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
    private function insertDataPart6($listDescription, $listQuestion, $listAnswer)
    {

        try {
            DB::beginTransaction();


            foreach ($listDescription as $k => $item) {
                $qg =  QuestionGroup::create([
                    'question_group_question' => $item,
                    'question_group_image' => null,
                    'question_group_audio' => null,
                    'part_id' => 6,
                ]);

                $question_group_id = $qg->question_group_id;

                foreach ($listQuestion as $k => $i) {

                    $question = Question::create([

                        'question' => $i,
                        'question_scores' => 5,
                        'question_audio' => null,
                        'question_image' => null,
                        'part_id' => 6,
                        // 'question_time' => 20000,
                        'question_group_id' => $question_group_id
                    ]);

                    $q_id =  $question->question_id;

                    foreach ($listAnswer[$k] as  $ai) {
                        Answer::create([
                            'answer_option' => $ai['name'],
                            'answer_is_correct' => $ai['is_correct'],
                            'question_id' =>  $q_id
                        ]);
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    private function insertDataPart7($listDescription, $listQuestion, $listAnswer, $listImg)
    {
        try {
            DB::beginTransaction();


            foreach ($listDescription as $k => $item) {
                $qg =  QuestionGroup::create([
                    'question_group_question' => $item,
                    'question_group_image' =>  isset($listImg[$k]) ? $listImg[$k] : null,
                    'question_group_audio' => null,
                    'part_id' => 7,
                ]);

                $question_group_id = $qg->question_group_id;

                foreach ($listQuestion as $k => $i) {

                    $question = Question::create([

                        'question' => $i,
                        'question_scores' => 5,
                        'question_audio' => null,
                        'question_image' => null,
                        'part_id' => 7,
                        // 'question_time' => 20000,
                        'question_group_id' => $question_group_id
                    ]);

                    $q_id =  $question->question_id;

                    foreach ($listAnswer[$k] as  $ai) {
                        Answer::create([
                            'answer_option' => $ai['name'],
                            'answer_is_correct' => $ai['is_correct'],
                            'question_id' =>  $q_id
                        ]);
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
        }
    }


    public function handleData()
    {
        $list = QuestionGroup::all();

        foreach ($list as $item) {
            if (!empty($item->question_group_image)) {

                $contentImage = file_get_contents($item->question_group_image);
                // $name = 
            }


            $contentAudio = file_get_contents($item->question_group_audio);


            // Storage::put($name, $contents);
        }
    }
}

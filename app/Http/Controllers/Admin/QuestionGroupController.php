<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuestionGroup;
use Illuminate\Support\Facades\DB;

class QuestionGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $group = $request->group;
        if( $group) {
            $group =  explode(',',  $group );
        }
        $start = $params["start"];
        $limit = $params["length"];
        $keyword = $params["search"]["value"];
        $totalData = QuestionGroup::when($group,function($query) use ($group){
            $query->whereIn('question_group_id',$group);
           })->count();
        $totalFiltered  = 0;
        $dir = $request->input('order.0.dir');
        $fields = [
            '',
            '',
            'question_group_question',
            'question_group_image',
            'question_group_audio'
        ];
     
        $indexOrder = intval($request->input('order.0.column'));
        
        $indexOrder =  $indexOrder  == 1 ?  2 : $indexOrder;
        $order = $fields[$indexOrder];
      
        if (empty($keyword)) {

            $list = QuestionGroup::when($group,function($query) use ($group){
                $query->whereIn('question_group_id',$group);
            })->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

        } else {

            $list =  QuestionGroup::where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                    ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })->when($group,function($query) use ($group){
                $query->whereIn('question_group_id',$group);
               })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = QuestionGroup::where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                         ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })->when($group,function($query) use ($group){
                $query->whereIn('question_group_id',$group);
               })->count();
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => $totalData,
            "recordsFiltered" => intval($totalFiltered) ? intval($totalFiltered) :  $totalData,
            "data"            => $list
        ];

        return  response()->json($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $group = QuestionGroup::findOrFail($id);
            
            return  view('admin.group_question.edit',compact('group'));

        } catch (\Exception $e) {
           abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            
            $data = $request->except('_token', '_method');

            QuestionGroup::where('question_group_audio',$id)
            ->update($data);
            DB::commit();
            $request->session()->flash('success', "Cập nhật thành công !");
            return redirect()->route('question-group.list');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', "Cập nhật thất bại !");
            return redirect()->route('question-group.list');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $flagNotDel = false;
            $msg = "";
            $ids = explode(',' , $request->ids);
            foreach ($ids as $k => $id) {

                $part = QuestionGroup::findOrFail($id);
                
                if($part->questions->count() >0) {
                    unset($ids[$k]);
                    $flagNotDel = true;
                }
            }

            QuestionGroup::destroy($ids);

            if($flagNotDel)
            $msg = ". Các nhóm chứa câu hỏi sẽ không xóa được !";
            $request->session()->flash('success', "Xoá thành công".$msg);
            return redirect()->route('question-group.list');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Xoá thất bại !");
            return redirect()->route('question-group.list');
        }
    }

    public function showList()
    {
        return view('admin.group_question.list');
    }
}

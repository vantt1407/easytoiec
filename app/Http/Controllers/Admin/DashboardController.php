<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Practice;
use App\Models\Article;
use App\Models\Topic;
use App\Models\User;
use App\Models\Test;

class DashboardController extends Controller
{
    public function showView()
    {
       
        $currentDay = date("Y-m-d", strtotime(now()));  
        $firstDay = date("Y-m-d", strtotime('sunday last week'));  
        $lastDay = date("Y-m-d", strtotime('sunday this week'));  
        $firstMonth= date("Y-m", strtotime(now()));  
        $lastMonth= date('Y-m', strtotime(now()));

        $userDaily =  DB::select("SELECT sum(total_in_day) as total_daily FROM 
        access_user_information where  DATE_FORMAT(created_at,'%Y-%m-%d') = ?",[  $currentDay ]);
        
        $userWeekly = DB::select("SELECT sum(total_in_day) as total_weekly FROM 
        access_user_information where DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN ? AND  ? ",[$firstDay, $lastDay]);

        $userMonthly = DB::select("SELECT sum(total_in_day) as total_monthly FROM 
        access_user_information where  DATE_FORMAT(created_at,'%Y-%m') BETWEEN ? AND  ? ",[$firstMonth, $lastMonth]);

   
            $userDaily =  $userDaily[0]->total_daily;
            $userWeekly = $userWeekly[0]->total_weekly;
            $userMonthly = $userMonthly[0]->total_monthly;

        $totalUser = User::count();
        $totalTest = Test::count();
        $totalPractice = Practice::count();
        $totalArticle =Article::count();
        $totalTopic = Topic::count();
        
        return view('admin.dashboard')->with([

            'userDaily' =>  $userDaily,
            'userWeekly' => $userWeekly,
            'userMonthly' => $userMonthly,
            'totalUser' => $totalUser,
            'totalTest' => $totalTest,
            'totalPractice' => $totalPractice,
            'totalArticle' => $totalArticle,
            'totalTopic' => $totalTopic
        ]);
    }
}

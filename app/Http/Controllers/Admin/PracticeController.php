<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Practice;
use Illuminate\Support\Facades\DB;
use App\Models\Level;
use Illuminate\Support\Str;

class PracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = strip_tags($request->keyword);
        $practiceList = Practice::where('practice_name','LIKE','%'.$keyword.'%')->paginate(10);
        return view('admin.practice.list',compact('practiceList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $levels = Level::all();

        return view('admin.practice.create',compact('levels'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        DB::beginTransaction();
       
        try {
            
            $practice = new Practice();
            $practice->practice_name = $data['practice_name'];
            $practice->practice_slug  = Str::slug($data['practice_name']);
            $practice->level_id = $data['level_id'];

            $practice->save();

            $practice->questions()->attach($data['question_id']);
            DB::commit();
            
            $request->session()->flash('success', "Thêm bài luyện tập thành công !");

            return redirect()->route('practice.index');
            
        } catch (\Exception $e) {
            
           
            DB::rollback();
            $request->session()->flash('error', "Thêm bài luyện tập thất bại !");
            return redirect()->route('practice.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
             $levels = Level::all();

             $practice =  Practice::findOrFail($id);

             return view('admin.practice.edit',compact('levels','practice'));
             
        } catch (\Exception $e) {
            abort(404);
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
        DB::beginTransaction();

        try {

            $practice =  Practice::findOrFail($id);
            $practice->practice_name = $data['practice_name'];
            $practice->practice_slug  = Str::slug($data['practice_name']);
            $practice->level_id = $data['level_id'];
            $practice->save();
            $practice->questions()->sync($data['question_id']);
            DB::commit();
            
            $request->session()->flash('success', "Cập nhật bài luyện tập thành công !");
            return redirect()->route('practice.index');
        } catch (\Exception $e) {
            
            DB::rollback();

            $request->session()->flash('error', "Cập nhật bài luyện tập thất bại !");
            return redirect()->route('practice.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
          
            $ids = explode(',' , $request->ids);

            foreach ($ids as $k => $id) {

                $practice = Practice::findOrFail($id);
                if($practice->questions)
                    $practice->questions->detach();
            }

            Practice::destroy($ids);
            $request->session()->flash('success', "Xoá thành công");

            return redirect()->route('practice.index');
        } catch (\Exception $e) {

            $request->session()->flash('error', "Xoá thất bại !");
            return redirect()->route('practice.index');
        }
    }
}

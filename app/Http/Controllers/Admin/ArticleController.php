<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArticleCategory;
use App\Models\Article;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->keyword ?? null;
        $categoryId =  $request->category_id ?? null;
        $articleList  = Article::when($keyword, function ($query, $keyword) {
            return $query->where(
                'article_title',
                'like',
                "%$keyword%"
            )->orWhere(
                'article_content',
                'like',
                "%$keyword%"
            );
        })->when($categoryId, function ($query, $categoryId) {
            return $query->where('article_category_id', $categoryId);
        })->paginate(10);

        $categories = ArticleCategory::all();

        return view('admin.article.list')->with([
            'categories' => $categories,
            'articleList' => $articleList
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = ArticleCategory::all();

        return view('admin.article.create')->with([
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $data = $request->except('_token');
            $data['article_slug'] = Str::slug($data['article_title']);
            $data['created_by'] = Auth::id();
            $data['article_view_count'] = 0;
            Article::create($data);
            $request->session()->flash('success', "Thêm bài viết thành công !");
            return redirect()->route('article.index');
        } catch (\Exception $e) {
            throw $e;
            $request->session()->flash('error', "Thêm bài viết thất bại !");
            return redirect()->route('article.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = ArticleCategory::all();
        $article = Article::findOrFail($id);
        return view('admin.article.edit')->with([
            'categories' => $categories,
            'article' => $article
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->except('_token', '_method');
            $data['article_slug'] = Str::slug($data['article_title']);
            $data['created_by'] = Auth::id();
            Article::where('article_id', $id)
                ->update($data);
            $request->session()->flash('success', "Cập nhật bài viết thành công !");
            return redirect()->route('article.index');
        } catch (\Exception $e) {
            throw $e;
            $request->session()->flash('error', "Cập nhật bài viết thất bại !");
            return redirect()->route('article.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $ids = explode(',', $request->ids);
            Article::destroy($ids);
            $request->session()->flash('success', "Xoá bài viết thành công !");
            return redirect()->route('article.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Xoá bài viết thất bại !");
            return redirect()->route('article.index');
        }
    }
}






				
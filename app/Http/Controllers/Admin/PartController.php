<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Part;
use Illuminate\Support\Str;
use App\Models\PartType;

class PartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partList = Part::paginate(10);

        return view('admin.part.list')->with([
            'partList' => $partList
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $types = PartType::all();
        $count = Part::count();
        if($count >= 7) {
            $request->session()->flash('total_error', "Đã đủ 7 phần !");
            return redirect()->back();
        }
        return view('admin.part.create',compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->except('_token');
            $data['part_slug'] = Str::slug($data['part_name']);
            // dd($data);
            Part::create($data);
            $request->session()->flash('success', "Thêm phần thi thành công !");
            return redirect()->route('part.index');

        } catch (\Exception $e){
            //throw $e;
            $request->session()->flash('error', "Thêm phần thi thất bại !");
            return redirect()->route('part.index');
        }
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $part = Part::findOrFail($id);
        $types = PartType::all();
        return view('admin.part.edit',compact('part','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->except('_token','_method');
            $data['part_slug'] = Str::slug($data['part_name']);
            Part::where('part_id',$id)->update($data);
            $request->session()->flash('success', "Cập nhật phần thi thành công !");
            return redirect()->route('part.index');

        } catch (\Exception $e){
            throw $e;
            $request->session()->flash('error', "Cập nhật phần thi thất bại !");
            return redirect()->route('part.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $flagNotDel = false;
            $msg = "";
            $ids = explode(',' , $request->ids);
            foreach ($ids as $k => $id) {

                $part = Part::findOrFail($id);
                
                if($part->questions->count() >0) {
                    unset($ids[$k]);
                    $flagNotDel = true;
                }
            }
            Part::destroy($ids);
            if($flagNotDel)
            $msg = ". Các phần chứa câu hỏi sẽ không xóa được !";
            $request->session()->flash('success', "Xoá thành công".$msg);
            return redirect()->route('part.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Xoá thất bại !");
            return redirect()->route('part.index');
        }
    }
}

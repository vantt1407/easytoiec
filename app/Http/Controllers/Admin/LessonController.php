<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Lesson;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Models\Level;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.lesson.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = Level::all();
        return view('admin.lesson.create',compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->except('_token');
            $data['lesson_slug'] = Str::slug($data['lesson_title']);
            $data['created_by'] = Auth::id();
            $lesson =  Lesson::create($data);
            $request->session()->flash('success', "Tạo mới thành công !");
            return redirect()->route('lesson.edit', $lesson->lesson_id);
        } catch (\Exception $e) {
            $request->session()->flash('error', "Tạo mới thất bại !");
            return redirect()->route('lesson.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lesson = Lesson::findOrFail($id);
        $levels = Level::all();
        return view('admin.lesson.edit',compact('lesson','levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->except('_token','_method');
            $data['lesson_slug'] = Str::slug($data['lesson_title']);
            Lesson::where('lesson_id',$id)->update($data);
            $request->session()->flash('success', "Cập nhật thành công !");
            return redirect()->route('lesson.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Cập nhật thất bại !");
            return redirect()->route('lesson.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getListLesson(Request $request)
    {

        try {

            $params = $request->all();
            $start = $params["start"];
            $limit = $params["length"];
            $keyword = $params["search"]["value"];
            
            $totalData = Lesson::count();
            $totalFiltered  = 0;
            $dir = $request->input('order.0.dir');
            $fields = [
                'lesson_title'
            ];
            $indexOrder = intval($request->input('order.0.column'));
            // $indexOrder =  $indexOrder > 0 ?  $indexOrder - 2 : $indexOrder;
            $order = $fields[$indexOrder];

            if (empty($keyword)) {
              
                $list = Lesson::withCount('lessonParts')->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                
                $list = Lesson::withCount('lessonParts')->where(function ($query)  use ($fields, $keyword) {
                    foreach ($fields as $field) {
                        $query->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();

                $totalFiltered = Lesson::where(function ($query)  use ($fields, $keyword) {
                    foreach ($fields as $field) {
                        $query->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                })->count();
            }

          
           $list = array_map(function($item) {
                 $item['action'] = '<a href="'.route('lesson.edit',$item['lesson_id'] ).'" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa" title="" aria-expanded="false">
                                        <i class="uil-pen text-warning"></i>
                                    </a>';
                return $item;
            },$list->toArray());
           
            $json_data = [
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => $totalData,
                "recordsFiltered" => intval($totalFiltered) ? intval($totalFiltered) :  $totalData,
                "data"            => $list
            ];

            return json_encode($json_data);
        } catch (\Exception $e) {
            throw $e;
        }
    }



    public function getListLessonPartQuestion(Request $request)
    {
        try {
        } catch (\Exception $e) {
        }
    }
}

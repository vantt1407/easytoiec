<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\QuestionGroup;
use App\Models\Part;
use App\Models\Answer;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.question.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partList = Part::all();
        return view('admin.question.create')->with([
            'partList' => $partList
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createGroup()
    {
        $partList = Part::all();
        $listRawQuestion = Session::get("raw_transaction");
        $listRawQuestion =  $listRawQuestion ?  $listRawQuestion : [];
        return view('admin.question.create')->with([
            'partList' => $partList,
            'createGroup' => true,
            'listRawQuestion' => $listRawQuestion
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        try {
           

            DB::beginTransaction();

            if (empty($data['question_group_image'])) {

                $question = Question::create([
                    'question' => $data['question'],
                    'question_scores' => $data['question_scores'],
                    'question_audio' => $data['question_audio'],
                    'question_image' => $data['question_image'],
                    'part_id' => $data['part_id'],
                    'question_group_id' => null
                ]);

                $qcId =  $question->question_id;
                    foreach ($data['answer_option'] as $a) {
                         Answer::create([
                            'answer_option' => $a,
                            'answer_is_correct' => $a === $data['is_corect'],
                            'question_id' => $qcId
                        ]);
                    }
            } else {
                $listRawQuestion =  Session::get('raw_transaction');
                $qg = QuestionGroup::create([
                    'part_id' => $data['part_id'],
                    'question_group_question' => $data['question_group_question'],
                    'question_group_audio' => $data['question_group_audio'],
                    'question_group_image' => $data['question_group_image']
                ]);
                $qgId = $qg->question_group_id;
                foreach ($listRawQuestion as $q) {
                    $qc = Question::create([
                        'question' => $q['modal_question'],
                        'question_scores' =>  $q['modal_question_scores'],
                        'question_audio' =>  $q['modal_question_audio'],
                        'question_image' =>  $q['modal_question_image'],
                        'part_id' => $data['part_id'],
                        'question_group_id' => $qgId

                    ]);
                    $qcId =  $qc->question_id;
                    foreach ($q['answer_option_modal'] as $a) {
                        $ac = Answer::create([
                            'answer_option' => $a,
                            'answer_is_correct' => $a === $q['is_correct_modal'],
                            'question_id' => $qcId
                        ]);
                    }
                }
            }
            DB::commit();
            $request->session()->flash('success', "Thêm câu hỏi thành công !");

            if( isset($data['callback']) ) {
                return redirect(url()->previous());
            }
            return redirect()->route('question.create.group');
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', "Thêm câu hỏi thất bại !");

            if( isset($data['callback']) ) {
                return redirect(url()->previous());
            }
            return redirect()->route('question.create.group');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        
        try {

            $group_id = $request->group;

            $question = Question::findOrFail($id);


            $partList = Part::all();

            if( $group_id ) {

                if($question->question_group_id != $group_id ) {

                    abort(404);

                } else {
                    $callback = url()->previous();
                    return view('admin.question.edit')->with([
                        'partList' => $partList,
                        'question' => $question,
                        'group_id' => $group_id,
                        'callback' => $callback,
                    ]);
                }

            } else {
               
                return view('admin.question.edit')->with([
                    'partList' => $partList,
                    'question' => $question,
                    ''
                ]);
            }
            

        } catch (\Exception $e) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
        try {
           
        
            DB::beginTransaction();

            if (empty($data['question_group_image'])) {

                Question::where('question_id',$id)->update([
                    'question' => $data['question'],
                    'question_scores' => $data['question_scores'],
                    'question_audio' => $data['question_audio'],
                    'question_image' => $data['question_image'],
                    'part_id' => $data['part_id'],
                    'question_group_id' => $data['question_group_id']
                ]);

                    $answers = $data['answer_option'];
                    $answerIds =  $data['answer_id'];
                    $isCorrect = $data['is_corect'];
                    Answer::destroy($answerIds);

                    foreach ($answers as $a) {
                        Answer::create([
                            'answer_option' => $a,
                            'answer_is_correct' => $a === $isCorrect,
                            'question_id' => $id
                        ]);
                    }

            } else {
                $listRawQuestion =  Session::get('raw_transaction');
                $qg = QuestionGroup::create([
                    'question_group_question' => $data['question_group_question'],
                    'question_group_audio' => $data['question_group_audio'],
                    'question_group_image' => $data['question_group_image'],
                    'part_id' => $data['part_id'],
                ]);
                $qgId = $qg->question_group_id;
                foreach ($listRawQuestion as $q) {
                    $qc = Question::create([
                        'question' => $q['modal_question'],
                        'question_scores' =>  $q['modal_question_scores'],
                        'question_audio' =>  $q['modal_question_audio'],
                        'question_image' =>  $q['modal_question_image'],
                        'part_id' => $data['part_id'],
                        'question_group_id' => $qgId

                    ]);
                    $qcId =  $qc->question_id;
                    foreach ($q['answer_option_modal'] as $a) {
                        $ac = Answer::create([
                            'answer_option' => $a,
                            'answer_is_correct' => $a === $q['is_correct_modal'],
                            'question_id' => $qcId
                        ]);
                    }
                }
            }
            DB::commit();

            $request->session()->flash('success', "Cập nhật câu hỏi thành công !");
            if( isset($data['callback']) ) {
                return redirect($data['callback']);
            }
            return redirect()->route('question.index');
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
            $request->session()->flash('error', "Cập nhật câu hỏi thất bại !");
            if( isset($data['callback']) ) {
                return redirect($data['callback']);
            }
            return redirect()->route('question.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $ids = explode(',' , $request->ids);
             Question::destroy($ids);
            return "Xoá thành công !";
        } catch (\Exception $e) {
            return "Xoá thất bại !";
        }
    }


    public function getQuestionsAjax(Request $request)
    {

        $params = $request->all();
        $start = $params["start"];
        $limit = $params["length"];
        $keyword = $params["search"]["value"];
        $totalData = Question::whereNotNull('question_group_id')->count();
        $totalFiltered  = 0;
        $dir = $request->input('order.0.dir');
        $fields = [
            '',
            '',
            'question',
            'part_name'
        ];
     
        $indexOrder = intval($request->input('order.0.column'));
        
        $indexOrder =  $indexOrder  == 1 ?  2 : $indexOrder;
        $order = $fields[$indexOrder];
      
        if (empty($keyword)) {

            $list = Question::with('answers', 'part')
                ->join('parts','parts.part_id','=','questions.part_id')
                ->whereNotNull('question_group_id')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

        } else {

            $list =  Question::with('answers', 'part')

            ->where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                    ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })  ->join('parts','parts.part_id','=','questions.part_id')
                ->whereNotNull('question_group_id')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Question::where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                         ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })->join('parts','parts.part_id','=','questions.part_id')
            ->whereNotNull('question_group_id')->count();
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => $totalData,
            "recordsFiltered" => intval($totalFiltered) ? intval($totalFiltered) :  $totalData,
            "data"            => $list
        ];

        return  response()->json($json_data);
    }


    public function getQuestionsAjaxV2(Request $request)
    {

        $params = $request->all();
        $start = $params["start"];
        $limit = $params["length"];
        $keyword = $params["search"]["value"];
        $totalData = Question::whereNull('question_group_id')->count();
        $totalFiltered  = 0;
        $dir = $request->input('order.0.dir');
        $fields = [
            '',
            '',
            'question',
            'part_name'
        ];
     
        $indexOrder = intval($request->input('order.0.column'));
        
        $indexOrder =  $indexOrder  == 1 ?  2 : $indexOrder;
        $order = $fields[$indexOrder];
      
        if (empty($keyword)) {

            $list = Question::with('answers', 'part')
                ->join('parts','parts.part_id','=','questions.part_id')
                ->whereNull('question_group_id')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

        } else {

            $list =  Question::with('answers', 'part')

            ->where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                    ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })  ->join('parts','parts.part_id','=','questions.part_id')
                ->whereNull('question_group_id')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Question::where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                         ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })->join('parts','parts.part_id','=','questions.part_id')
            ->whereNull('question_group_id')->count();
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => $totalData,
            "recordsFiltered" => intval($totalFiltered) ? intval($totalFiltered) :  $totalData,
            "data"            => $list
        ];

        return  response()->json($json_data);
    }


    public function getQuestionsAjaxPractice(Request $request)
    {

        $params = $request->all();
        $start = $params["start"];
        $limit = $params["length"];
        $keyword = $params["search"]["value"];
       
        $totalFiltered  = 0;
        $dir = $request->input('order.0.dir');
        $fields = [
            '',
            '',
            'question',
            'part_name'
        ];
        
        $listEq = DB::select('select question_id from practice_questions');
        $ids = [];
            foreach ($listEq as $it) {
                $ids[] = $it->question_id;
        }
        $totalData = Question::whereNull('question_group_id')->whereNotIn('question_id',  $ids )->count();
        $indexOrder = intval($request->input('order.0.column'));
        $indexOrder =  $indexOrder  == 1 ?  2 : $indexOrder;
        $order = $fields[$indexOrder];
      
        if (empty($keyword)) {
            $list = Question::with('answers', 'part')
                ->join('parts','parts.part_id','=','questions.part_id')
                ->whereNull('question_group_id')
                ->whereNotIn('question_id',  $ids )
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {

            $list =  Question::with('answers', 'part')
            ->where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                    ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })  ->join('parts','parts.part_id','=','questions.part_id')
                ->whereNull('question_group_id')
                ->whereNotIn('question_id',  $ids )
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Question::where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                         ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })->join('parts','parts.part_id','=','questions.part_id')
            ->whereNull('question_group_id')->whereNotIn('question_id',  $ids )->count();
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => $totalData,
            "recordsFiltered" => intval($totalFiltered) ? intval($totalFiltered) :  $totalData,
            "data"            => $list
        ];

        return  response()->json($json_data);
    }

    public function getQuestionsAjaxLesson(Request $request)
    {

        $params = $request->all();
        $start = $params["start"];
        $limit = $params["length"];
        $keyword = $params["search"]["value"];
       
        $totalFiltered  = 0;
        $dir = $request->input('order.0.dir');
        $fields = [
            '',
            '',
            'question',
            'part_name'
        ];
        
        $listEq = DB::select('select question_id from lesson_part_questions');
        $ids = [];
            foreach ($listEq as $it) {
                $ids[] = $it->question_id;
        }
        $totalData = Question::whereNull('question_group_id')->whereNotIn('question_id',  $ids )->count();
        $indexOrder = intval($request->input('order.0.column'));
        $indexOrder =  $indexOrder  == 1 ?  2 : $indexOrder;
        $order = $fields[$indexOrder];
      
        if (empty($keyword)) {
            $list = Question::with('answers', 'part')
                ->join('parts','parts.part_id','=','questions.part_id')
                ->whereNull('question_group_id')
                ->whereNotIn('question_id',  $ids )
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {

            $list =  Question::with('answers', 'part')
            ->where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                    ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })  ->join('parts','parts.part_id','=','questions.part_id')
                ->whereNull('question_group_id')
                ->whereNotIn('question_id',  $ids )
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Question::where(function ($query)  use ($fields, $keyword) {
                foreach ($fields as $field) {
                    if( !empty( $field) ) {
                    $query
                         ->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                }
            })->join('parts','parts.part_id','=','questions.part_id')
            ->whereNull('question_group_id')->whereNotIn('question_id',  $ids )->count();
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => $totalData,
            "recordsFiltered" => intval($totalFiltered) ? intval($totalFiltered) :  $totalData,
            "data"            => $list
        ];

        return  response()->json($json_data);
    }
    public function storeRawDataQuestionGroup(Request $request)
    {

        $data = $request->except("_token");
        Session::push('raw_transaction', $data);
        $request->session()->flash('success', "Thêm bản tạm thành công !");
        return redirect()->route('question.create.group');
    }


    public function showRawDataQuestionGroup($index)
    {
        $partList = Part::all();
        $item = Session::get('raw_transaction.' . $index);

        $listRawQuestion = Session::get("raw_transaction");
        // dd($listRawQuestion);
        return view('admin.question.create')->with([
            'partList' => $partList,
            'createGroup' => true,
            'index' => $index,
            'q' =>  $item,
            'listRawQuestion' => $listRawQuestion
        ]);
    }

    public function updateRawDataQuestionGroup(Request $request, $index)
    {

        $data = $request->except("_token");
        $list = Session::get('raw_transaction', []);

        foreach ($list as $key => $item) {

            if ($key == $index) {
                $list[$key] = $data;
            }
        }

        Session::put('raw_transaction', $list);

        $request->session()->flash('success', "Sửa bản tạm thành công !");

        return redirect()->route('question.create.group');
    }

    public function deleteRawQuestion(Request $request, $index)
    {
        Session::forget('raw_transaction.' . $index);
        $request->session()->flash('success', "Xóa bản tạm thành công !");
        return redirect()->route('question.create.group');
    }


}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article; 
use App\Models\ArticleCategory;

class ValidateController extends Controller
{
    function jqueryValidateArticleNameIsset(Request $request) {

        if(!$request->wantsJson()) {
            abort(404);
        }


        $count  = Article::firstWhere('article_title',$request->article_title);
        if($count) return false;
        return true;
    }

    function jqueryValidateCategoryNameIsset(Request $request) {

        if(!$request->wantsJson()) {
            abort(404);
        }


        $count  = ArticleCategory::firstWhere('article_category_name',$request->article_category_name);
        if($count) return false;
        return true;
    }
}

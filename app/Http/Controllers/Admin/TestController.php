<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Test;
use App\Models\Part;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\QuestionGroup;
use App\Models\Question;
use App\Models\Level;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $keyword = $request->keyword;

        $testList =  Test::when($keyword, function ($query) use ($keyword) {

            $query->where('test_name', 'LIKE', '%' . $keyword . '%');
        })
            ->paginate(5);
        return view('admin.test.list', compact('testList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parts = Part::all();
        $levels = Level::all();
        return view('admin.test.create', compact('parts', 'levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data =  $request->except('_token');
        $part_1 = json_decode($data['part_1'], true);
        $part_2 = json_decode($data['part_2'], true);
        $part_3 = json_decode($data['part_3'], true);
        $part_4 = json_decode($data['part_4'], true);
        $part_5 = json_decode($data['part_5'], true);
        $part_6 = json_decode($data['part_6'], true);
        $part_7 = json_decode($data['part_7'], true);
        $groupQuestion =  array_merge($part_1, $part_2, $part_3, $part_4, $part_5, $part_6, $part_7);
        DB::beginTransaction();
        try {

            $test = Test::create([
                'test_name' => $data['test_name'],
                'test_time' => $data['test_time'],
                'test_slug' => Str::slug($data['test_name']),
                'level_id' => $data['level_id']
            ]);
            $test->question_groups()->attach($groupQuestion);
            DB::commit();
            return response()->json("Thêm mới thành công !");
        } catch (\Exception $e) {
            // throw $e;
            DB::rollback();
            return response()->json("Thêm mới thất bại !");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $parts = Part::all();
            $levels = Level::all();
            $test = Test::findOrFail($id);
            $qg = $test->question_groups;
            $part1 = [];
            $part2 = [];
            $part3 = [];
            $part4 = [];
            $part5 = [];
            $part6 = [];
            $part7 = [];
            foreach ($qg as $item) {

                switch ($item->part_id) {
                    case 1:
                        $part1[] = $item->question_group_id;
                        break;
                    case 2:
                        $part2[] = $item->question_group_id;
                        break;
                    case 3:
                        $part3[] = $item->question_group_id;
                        break;
                    case 4:
                        $part4[] = $item->question_group_id;
                        break;
                    case 5:
                        $part5[] = $item->question_group_id;
                        break;
                    case 6:
                        $part6[] = $item->question_group_id;
                        break;
                    case 7:
                        $part7[] = $item->question_group_id;
                        break;
                    default:

                        break;
                }
            }

            return view('admin.test.edit', compact('parts', 'levels', 'test', 'part1', 'part2', 'part3', 'part4', 'part5', 'part6', 'part7'));
        } catch (\Exception $e) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  $request->except('_token','_method');
        $part_1 = json_decode($data['part_1'], true);
        $part_2 = json_decode($data['part_2'], true);
        $part_3 = json_decode($data['part_3'], true);
        $part_4 = json_decode($data['part_4'], true);
        $part_5 = json_decode($data['part_5'], true);
        $part_6 = json_decode($data['part_6'], true);
        $part_7 = json_decode($data['part_7'], true);
        $groupQuestion =  array_merge($part_1, $part_2, $part_3, $part_4, $part_5, $part_6, $part_7);
        DB::beginTransaction();
        try {

           Test::where('test_id',$id)->update([
                'test_name' => $data['test_name'],
                'test_time' => $data['test_time'],
                'test_slug' => Str::slug($data['test_name']),
                'level_id' => $data['level_id']
            ]);
            $test = Test::findOrFail($id);

            $test->question_groups()->sync($groupQuestion);
            DB::commit();
            return response()->json("Cập nhật thành công !");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json("Cập nhật thất bại !");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {

            $ids = explode(',', $request->ids);

            foreach ($ids as $k => $id) {
                $test = Test::findOrFail($id);
                if ($test->question_groups)
                    $test->question_groups->detach();
            }

            Test::destroy($ids);


            $request->session()->flash('success', "Xoá thành công !");
            return redirect()->route('test.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Xoá thất bại !");
            return redirect()->route('test.index');
        }
    }


    public function randomGroupQuestion(Request $request)
    {

        try {

            $data = $request->except('_token');

            $randomConfig = config('random.test');

            $randomConfig =  $randomConfig[$data['time']];

            $totalGroupPart = $randomConfig['part' . $data['part_id']];

            $part = DB::select("SELECT question_group_id FROM question_groups
                where question_groups.question_group_id not in (select question_group_id from test_questions )
                and part_id = ?
                ORDER BY RAND()
                limit ?", [$data['part_id'], $totalGroupPart]);

            $ids = [];
            foreach ($part as $item) {
                $ids[] = $item->question_group_id;
            }
            return response()->json($ids);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function listQuestionShow($ids)
    {
        try {
        } catch (\Exception $e) {

            return collect([]);
        }
    }
}

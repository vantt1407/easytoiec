<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LessonPart;
use App\Models\Part;
use App\Models\Setting;
use App\Models\Question;
use Illuminate\Support\Str;

class LessonPartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $lesson_id = $request->lesson;
 
        return view('admin/lesson/lesson_part_question/create', compact('lesson_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->except('_token');
           
            $lessonPart = LessonPart::create([
                'lesson_id' => $data['lesson_id'],
                'lesson_part_title' =>  $data['lesson_part_title'],
                'lesson_part_slug' =>  Str::slug($data['lesson_part_title']),
                'lesson_part_content'=>  $data['lesson_part_content'],
                'lesson_video'=>  $data['lesson_video'],
            ]);
            if(!empty($data['question_id']))
             $lessonPart->questions()->attach($data['question_id']);

             $request->session()->flash('success', "Thêm mới thành công !");
             return redirect()->route('lesson.edit',$data['lesson_id']);
        } catch (\Exception $e) {
            throw $e;
            $request->session()->flash('error', "Thêm mới thất bại !");
            return redirect()->route('lesson.edit',$data['lesson_id']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,$id)
    {
       try {
           
        $lesson_id = $request->lesson;
        $lessonPart = LessonPart::findOrFail($id);

        return view('admin/lesson/lesson_part_question/edit', compact('lesson_id','lessonPart'));
       } catch (\Exception $e) {
          abort(404);
       }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->except('_token','_method');
            $lessonPart = LessonPart::findOrFail($id);
            LessonPart::where('lesson_part_id',$id)->update([
                'lesson_id' => $data['lesson_id'],
                'lesson_part_title' =>  $data['lesson_part_title'],
                'lesson_part_slug' =>  Str::slug($data['lesson_part_title']),
                'lesson_part_content'=>  $data['lesson_part_content'],
                'lesson_video'=>  $data['lesson_video']
            ]);
           
            if(!empty($data['question_id']))
             $lessonPart->questions()->sync($data['question_id']);

             $request->session()->flash('success', "Cập nhật thành công !");
             return redirect()->route('lesson.edit',$data['lesson_id']);
        } catch (\Exception $e) {
            throw $e;
            $request->session()->flash('error', "Cập nhật thất bại !");
            return redirect()->route('lesson.edit',$data['lesson_id']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getListLessonPart(Request $request)
    {
        try {

            $id =  $request->lesson;
            $params = $request->all();
            $start = $params["start"];
            $limit = $params["length"];
            $keyword = $params["search"]["value"];
            $totalData = LessonPart::count();
            $totalFiltered  = 0;
            $dir = $request->input('order.0.dir');
            $fields = [
                'lesson_part_title'
            ];
            $indexOrder = intval($request->input('order.0.column'));
            // $indexOrder =  $indexOrder > 0 ?  $indexOrder - 2 : $indexOrder;
            $order = $fields[$indexOrder];

            if (empty($keyword)) {
                $list = LessonPart::where('lesson_id', $id)->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $list =  LessonPart::where(function ($query)  use ($fields, $keyword, $id) {
                    $query->where('lesson_id', $id);
                    foreach ($fields as $field) {
                        $query->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();

                $totalFiltered = LessonPart::where(function ($query)  use ($fields, $keyword, $id) {
                    $query->where('lesson_id', $id);
                    foreach ($fields as $field) {
                        $query->orWhere($field, 'LIKE', "%{$keyword}%");
                    }
                })->count();
            }


            $list = array_map(function ($item) use ($id) {
                $item['action'] = '<a href="' . route('lesson-part.edit', $item['lesson_part_id']) . '?lesson='.$id.'" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa" title="" aria-expanded="false">
                <i class="uil-pen text-warning"></i>
                </a>';
                return $item;
            }, $list->toArray());

            $json_data = [
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => $totalData,
                "recordsFiltered" => intval($totalFiltered) ? intval($totalFiltered) :  $totalData,
                "data"            => $list
            ];

            return json_encode($json_data);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    public function randomQuestion(Request $request)
    {
        try {
            $ids =  $request->ids;
            $idsArr =  \json_decode($ids,true);
            $list = Question::with('answers','part')->whereIn('question_id',$idsArr)->get();
            return response()->json($list);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vocabulary;
use App\Models\Topic;
use App\Models\VocabularyType;

class VocabularyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.vocabulary.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Topic::all();
        $types = VocabularyType::all();

        return view('admin.vocabulary.create')->with([
            'categories' => $categories,
            'types' => $types
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->except('_token');

            $antonymData = [];

            foreach ($data as $key => $value) {
                if( preg_match("/^_/",$key) && $value ) {
                    $antonymData['vocabulary'. $key] =  $value;
                    unset($data[$key]);
                }
            }
            $word = Vocabulary::create( $data );
            if( count($antonymData ) > 0 ) {
                $antonymData['topic_id'] =  $data['topic_id'];
                $antony = Vocabulary::create($antonymData);
                $antony->antonym_id = $word->vocabulary_id;
                $word->antonym_id = $antony->vocabulary_id;
                $antony->save();
                $word ->save();
            }
            $request->session()->flash('success', "Thêm mới từ vựng thành công !");
            return  redirect()->route('vocabulary.index');

        } catch (\Exception $e) {
            $request->session()->flash('error', "Thêm mới từ vựng thất bại !");
            return  redirect()->route('vocabulary.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            
        $categories = Topic::all();
        $types = VocabularyType::all();
        $v = Vocabulary::findOrFail($id);
        $av = $v->antonym;
        return view('admin.vocabulary.edit')->with([
            'categories' => $categories,
            'types' => $types,
            'vocabulary' => $v,
            'av' => $av
        ]);
        } catch (\Exception $e) {
           abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $v = Vocabulary::findOrFail($id);
            $antony = $v->antony;
            $data = $request->except('_token','_method');
            $antonymData = [];
            foreach ($data as $key => $value) {
                if( preg_match("/^_/",$key) ) {
                    if($value) {
                        $antonymData['vocabulary'. $key] =  $value;
                    }
                    unset($data[$key]);
                } 
            }
           
            $word = Vocabulary::where('vocabulary_id',$id)->update( $data );
            if( count($antonymData ) > 0 ) {
                $antonymData['topic_id'] =  $data['topic_id'];
                $antony =  $antony ?  $antony : Vocabulary::create($antonymData);
                $antony->antonym_id = $id;
                $v->antonym_id = $antony->vocabulary_id;
                $antony->save();
                $v->save();
            }
            $request->session()->flash('success', "Cập nhật từ vựng thành công !");
            return  redirect()->route('vocabulary.index');

        } catch (\Exception $e) {
            // throw $e;
            $request->session()->flash('error', "Cập nhật từ vựng thất bại !");
            return  redirect()->route('vocabulary.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        try {
            $ids = explode(',' , $request->ids);
             Vocabulary::destroy($ids);
            return "Xoá thành công !";
        } catch (\Exception $e) {
            return "Xoá thất bại !";
        }
    }

    public function listVocabulary(Request $request)
    {

        $params = $request->all();
        $start = $params["start"];
        $limit = $params["length"];
        $keyword = $params["search"]["value"];
        $totalData = Vocabulary::count();
        $totalFiltered  = 0;
        $dir = $request->input('order.0.dir');
        $fields = [
            'vocabulary_word',
            'vocabulary_vi_translate',
            'vocabulary_pronounce'
        ];
        $indexOrder = intval($request->input('order.0.column'));
        $indexOrder =  $indexOrder > 0 ?  $indexOrder-2 : $indexOrder;
        $order = $fields[$indexOrder];

        if(empty( $keyword ) )
        {
            $list = Vocabulary::with('topic','antonym')->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $list =  Vocabulary::with('topic','antonym')
                ->where( function($query)  use ($fields,$keyword)  {
                    foreach($fields as $field) {

                            $query->orWhere( $field ,'LIKE',"%{$keyword}%");

                    }
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Vocabulary::where(function($query)  use ($fields,$keyword)  {
                foreach($fields as $field) {

                        $query->orWhere( $field ,'LIKE',"%{$keyword}%");

                }
            })->count();
        }
        
        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => $totalData,
            "recordsFiltered" => intval($totalFiltered) ? intval($totalFiltered) :  $totalData,
            "data"            => $list
        ];

        return json_encode($json_data);
    }
}

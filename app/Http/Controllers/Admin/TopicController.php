<?php

namespace App\Http\Controllers\Admin;

use App\Models\Topic;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Topic\CreateTopicRequest;
use App\Http\Requests\Admin\Topic\UpdateTopicRequest;


class TopicController extends Controller
{
    private $topicList;
    private $showFormCreate;
    private $showFormView;

    public function __construct()
    {

        $this->topicList = collect(null);
        $this->showFormCreate = false;
        $this->showFormView = false;
        $this->disableInput = false;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->keyword;
        $this->topicList  =  Topic::when($keyword, function ($query) use($keyword) {

            $query->where('topic_name','LIKE','%'.$keyword.'%');

        })
        ->paginate(10);

        return view('admin.topic.list')->with(['topicList' => $this->topicList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->showFormCreate = true;
        return  view('admin.topic.list')->with([
            'topicList' =>  $this->topicList,
            'showFormCreate' => $this->showFormCreate
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTopicRequest $request)
    {
        try {
            $data = $request->except('_token');
            $data['topic_slug'] = Str::slug($data['topic_name'], '-');
            Topic::create($data);
            $request->session()->flash('success', "Thêm mới thành công !");
            return redirect()->route('topic.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Thêm mới thất bại !");
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $topic = Topic::findOrFail($id);
            $this->showFormView = true;
            $this->disableInput = true;
            return  view('admin.topic.list')->with([
                'topicList' =>  $this->topicList,
                'showFormView' => $this->showFormView,
                'disableInput' => $this->disableInput,
                'topic' => $topic
            ]);
        } catch (\Exception $e) {
            // throw $e;
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {
            $topic = Topic::findOrFail($id);
            $this->showFormView = true;
            return  view('admin.topic.list')->with([
                'topicList' =>  $this->topicList,
                'showFormView' => $this->showFormView,
                'topic' => $topic
            ]);
        } catch (\Exception $e) {
            // throw $e;
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopicRequest $request, $id)
    {
        try {
            $data = $request->except('_token', '_method');
            
            $data['topic_slug'] = Str::slug($data['topic_name'], '-');
            Topic::where('topic_id', $id)
                ->update($data);

            $request->session()->flash('success', "Cập nhật thành công !");
            return redirect()->route('topic.index');

        } catch (\Exception $e) {

            $request->session()->flash('error', "Cập nhật thất bại !");

            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $flagNotDel = false;
            $msg = "";
            $ids = explode(',' , $request->ids);
            foreach ($ids as $k => $id) {
                // Topic::withCount('vocabularies')
                $topic = Topic::findOrFail($id);
                if($topic->vocabularies->count() >0) {
                    unset($ids[$k]);
                    $flagNotDel = true;
                }
            }
            Topic::destroy($ids);
            if($flagNotDel)
            $msg = ". Các topic chứa từ vựng sẽ không xóa được !";
            $request->session()->flash('success', "Xoá thành công".$msg);
            return redirect()->route('topic.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Xoá thất bại !");
            return redirect()->route('topic.index');
        }
    }
}

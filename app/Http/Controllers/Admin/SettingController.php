<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $randomConfig = config('random.test');
        $emailConfig = config('mail');
        $setting = \App\Models\Setting::findOrFail(1);

        return view('admin.setting.index',compact('randomConfig','emailConfig','setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function changeEmailConfig(Request $request)
    {
        try {
            $data = $request->except('_token','_method');
            

            Config::set('mail.username', $data['input'][0]['value']);
            Config::set('mail.password', $data['input'][1]['value']);
            Config::set('mail.from.name', $data['input'][2]['value']);
            Config::set('mail.from.address', $data['input'][0]['value']);
           
            return "Cập nhật thành công !";
        } catch (\Exception $e) {
            return "Cập nhật thất bại !";
        }
       
        
    }

    public function changeRandomConfig(Request $request)
    {
        try {
            $data = $request->except('_token','_method');
           
            $part = $data['input'];
          
            Config::set('random.test.part1', $part[0]['value']);
            Config::set('random.test.part2', $part[1]['value']);
            Config::set('random.test.part3', $part[2]['value']);
            Config::set('random.test.part4', $part[3]['value']);
            Config::set('random.test.part5', $part[4]['value']);
            Config::set('random.test.part6', $part[5]['value']);
            Config::set('random.test.part7', $part[6]['value']);
          
            return "Cập nhật thành công !";
        } catch (\Exception $e) {
            return "Cập nhật thất bại !";
        }
    }


    public function changeInfoConfig(Request $request)
    {
        try {
            $data = $request->except('_token','_method');
            $data = $data['input'];
            $setting = \App\Models\Setting::findOrFail(1);
            $setting->banner =  $data[3]['value'];
            $setting->address = $data[0]['value'];
            $setting->phone = $data[1]['value'];
            $setting->sologan = $data[2]['value'];

            $setting->save();
            return "Cập nhật thành công !";
        } catch (\Exception $e) {
            return "Cập nhật thất bại !";
        }
    }
}

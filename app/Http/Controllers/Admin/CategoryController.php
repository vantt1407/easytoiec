<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArticleCategory;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.article.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try {
            $data = $request->except('_token');
            $data['article_category_parent_id'] = 0;
            $data['article_category_slug'] = Str::slug($data['article_category_name']);
            ArticleCategory::create( $data);
            $request->session()->flash('success', "Thêm danh mục thành công !");
            return redirect()->route('article.index');

        } catch (\Exception $e){
            $request->session()->flash('error', "Thêm danh mục thất bại !");
            return redirect()->route('article.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $category = ArticleCategory::findOrFail($id);
            return  view('admin.article.category.edit')->with([
                'category' =>  $category
            ]);
        } catch (\Exception $e) {
            // throw $e;
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->except('_token', '_method');
            // $data['topic_slug'] = Str::slug($data['topic_name'], '-');
            ArticleCategory::where('article_category_id', $id)
                ->update($data);
            $request->session()->flash('success', "Cập nhật danh mục thành công !");
            return redirect()->route('article.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Cập nhật danh mục thất bại !");
            return redirect()->route('article.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $ids = explode(',' , $request->ids);
            $flagNotDel = false;
            $msg = "";
            foreach ($ids as $k => $id) {
                // Topic::withCount('vocabularies')
                $topic = ArticleCategory::findOrFail($id);
                if($topic->articles->count() >0) {
                    unset($ids[$k]);
                    $flagNotDel = true;
                }
            }
            if($flagNotDel)
            $msg = ". Các danh mục chứa bài viết sẽ không xóa được !";
            ArticleCategory::destroy($ids);
            $request->session()->flash('success', "Xoá danh mục thành công".$msg);
            return redirect()->route('article.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', "Xoá danh mục thất bại !");
            return redirect()->route('article.index');
        }
    }
}

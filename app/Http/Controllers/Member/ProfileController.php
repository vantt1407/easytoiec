<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserProfile;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
  public function profile()
  {
     return view('member.profile.detail');
  }


  public function uploadAvatar(Request $request)
  {
      
        $avatar = $request->file('avatar');
     
        Storage::disk('local')->put('public\photos\shares\user\avatar\\'.$avatar->getClientOriginalName(),file_get_contents($avatar));

        $profile = UserProfile::firstWhere('user_id',Auth::id());
       
        $profile->user_profile_avatar = 'storage/photos/shares/user/avatar/'.$avatar->getClientOriginalName();

        $profile->save();
        
        return redirect()->back();
      }

  public function updateProfile(Request $request)
  {
    try {
      $userId = Auth::id();

      $profile = UserProfile::firstWhere('user_id',$userId);

        if($profile) {

          $profile->user_profile_phone =  $request->phone;
          $profile->user_profile_full_name =  $request->full_name;
          $profile->user_profile_address =  $request->address;
          $profile->save();

        } else {

          $profile = UserProfile::create([
            'user_profile_phone' =>  $request->phone,
            'user_profile_full_name' => $request->full_name,
            'user_profile_address' => $request->address,
            'user_id'=> $userId
          ]);

        }
        $request->session()->flash('success', "Cập nhật thành công !");
        return redirect()->back();
    } catch (\Exception $e) {
      throw $e;
         $request->session()->flash('error', "Cập nhật thất bại !");
      return redirect()->back();
    }
        
  }
}

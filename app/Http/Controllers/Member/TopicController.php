<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Topic;

class TopicController extends Controller
{
    public  function  list(Request $request) {

        $keyword = $request->keyword;

        $list =  Topic::when($keyword, function ($query) use($keyword) {

            $query->where('topic_name','LIKE','%'.$keyword.'%');

        })
        ->paginate(12);
        

        return view('member.topic.list', compact('list'));
    }

}

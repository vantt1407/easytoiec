<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\ArticleCategory;
class ArticleController extends Controller
{


    public function list(Request $request)
    {

        // $list = Article::paginate(10);

        $keyword = $request->keyword;
        $category = $request->category;

        $list =  Article::when($keyword, function ($query) use($keyword) {

            $query->where('article_title','LIKE','%'.$keyword.'%');

        })->when($category, function ($query) use($category) {

            $query->join('article_categories','article_categories.article_category_id','articles.article_category_id')
            ->where('articles.article_category_id',$category);

        })
        ->paginate(4);

        $listNew = Article::where('article_publish',1)->orderBy('created_at', 'desc')->limit(4)->get();
        $listHot =  Article::where('article_publish',1)->orderBy('article_view_count', 'desc')->limit(4)->get();
        $categories = ArticleCategory::all();
        return view('member.article.list',compact('list','categories','listNew','listHot'));
    }
    public function detail($slug)
    {   
      
        $article = Article::firstWhere(['article_slug'=>$slug,'article_publish'=>1]);
        if(!$article) abort(404);
        $listRandom = Article::inRandomOrder()->limit(4)->get();
        $article->increment('article_view_count',1);
        return view('member.article.detail',compact('article','listRandom'));
    }
}

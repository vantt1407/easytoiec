<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\Test;
use App\Models\Part;
use App\Models\QuestionGroup;
use App\Models\Question;
use Illuminate\Support\Facades\Session;
use App\Models\Answer;
use App\Models\UserTest;
use Illuminate\Support\Facades\Auth;
use App\Models\UserTestAnswer;
use Illuminate\Support\Facades\DB;
use App\Models\Level;

class TestController extends Controller
{
    public function create(Request $request)
    {
    }
    public  function list(Request $request)
    {
        $level = $request->level;
        if ($level) {
            $list = Test::where('level_id', $level)->get();
        } else {
            $list = Test::all();
        }

        $levels = Level::all();
        return view('member.test.list', compact('list', 'levels'));
    }

    public function preview(Request $request, $testId)
    {
        try {

            $test = Test::findOrFail($testId);

            return view('member.test.start', compact('test'));
        } catch (\Exception $e) {
            // throw $e;
            abort(404);
        }
    }
    public function detail(Request $request, $testId)
    {
        $part_id =  $request->part;
        $part_id = $part_id ? $part_id : 1;

        $test = Test::with(['question_groups' => function ($query) use ($part_id) {

            $query->where('part_id', $part_id)->orderBy('question_group_id', 'asc');
        }])->firstWhere('test_id', $testId);

        $tree =  [];
        foreach ($test->question_groups as $item) {

            switch ($item->part_id) {
                case 1:
                    $tree['part1'][] = $item;
                    break;
                case 2:
                    $tree['part2'][] = $item;
                    break;
                case 3:
                    $tree['part3'][] = $item;
                    break;
                case 4:
                    $tree['part4'][] = $item;
                    break;
                case 5:
                    $tree['part5'][] = $item;
                    break;
                case 6:
                    $tree['part6'][] = $item;
                    break;
                case 7:
                    $tree['part7'][] = $item;
                    break;
                default:
                    break;
            }
        }
        return view('member.test.test', compact('tree', 'test', 'part_id'));
    }

    public function submit(Request $request, $partId)
    {
        try {
            $data = $request->except('_token');

            if (empty($data['end_test'])) {

                Session::put('data_' . $partId, $data);

                return redirect(route('test_detail', $data['test_id']) . "?part=" . ($partId + 1));
            } else {
                Session::put('data_' . $partId, $data);
                $questions  = [];
                $answers  = [];
                try {
                    $questions = array_merge(
                        Session::get('data_1')['question'],
                        Session::get('data_2')['question'],
                        Session::get('data_3')['question'],
                        Session::get('data_4')['question'],
                        Session::get('data_5')['question'],
                        Session::get('data_6')['question'],
                        Session::get('data_7')['question'],
                    );
                    $answers = array_merge(
                        Session::get('data_1'),
                        Session::get('data_3'),
                        Session::get('data_4'),
                        Session::get('data_5'),
                        Session::get('data_6'),
                        Session::get('data_7'),
                    );
                } catch (\Exception $e) {
                }

                if (count($questions) === 0) {

                    $questionsObj = DB::select("SELECT question_id FROM questions 
                    inner join 
                    question_groups on question_groups.question_group_id = questions.question_group_id
                    inner join test_questions on test_questions.question_group_id = question_groups.question_group_id
                    inner join tests on tests.test_id = test_questions.test_id
                    where tests.test_id = ?", [$data['test_id']]);

                    foreach ($questionsObj as $q) {
                        $questions[] = $q->question_id;
                    }
                }
                $userTest = new UserTest();

                $userTest->test_id = $data['test_id'];
                $userTest->user_id = Auth::id();
                $userTest->scores = 0;
                $userTest->save();

                $qCollection = Question::whereIn('question_id', $questions)->get();

                foreach ($qCollection  as $q) {

                    $userTestAnswer = new UserTestAnswer();

                    $userTestAnswer->user_test_id = $userTest->id;

                    $userTestAnswer->question_id = $q->question_id;

                    try {

                        $idAnswer = $answers[$q->question_id];

                        $answer = Answer::findOrFail($idAnswer);

                        $userTestAnswer->answer = $answer->answer_option;

                        $userTestAnswer->answer_is_correct = $answer->answer_is_correct;
                    } catch (\Exception $e) {

                        $userTestAnswer->answer = "";
                        $userTestAnswer->answer_is_correct = false;
                    }
                    $userTestAnswer->save();
                }
            }
            return redirect()->route('get-result', [$userTest->id]);
        } catch (\Exception $e) {

            throw $e;
        }
    }
    public function getResult($id)
    {
        try {
            $ut = UserTest::findOrFail($id);
            $data = $this->getDataList($ut->test_id);
            $listResult = [];
            foreach ($ut->userTestAnswers as $uta) {
                try {
                    $answer = Answer::findOrFail($uta->answer);
                    $listResult[] = [$uta->question_id => !$answer->answer_is_correct ? $answer->answer_id : 0];
                } catch (\Exception $e) {
                    $listResult[] = [$uta->question_id => 0];
                }
            }
            
            return view('member.test.result')->with([
                'listResult' => $listResult,
                'tree' => $data[0],
                'test' => $data[1],
                'show_result' => true
            ]);
        } catch (\Exception $e) {
            throw $e;
        }
    }
    private function getDataList($testId)
    {

        $test = Test::with(['question_groups' => function ($query) {
            $query->orderBy('question_group_id', 'asc');
        }])->firstWhere('test_id', $testId);
        $tree =  [];

        foreach ($test->question_groups as $item) {

            switch ($item->part_id) {
                case 1:
                    $tree['part1'][] = $item;
                    break;
                case 2:
                    $tree['part2'][] = $item;
                    break;
                case 3:
                    $tree['part3'][] = $item;
                    break;
                case 4:
                    $tree['part4'][] = $item;
                    break;
                case 5:
                    $tree['part5'][] = $item;
                    break;
                case 6:
                    $tree['part6'][] = $item;
                    break;
                case 7:
                    $tree['part7'][] = $item;
                    break;
                default:
                    break;
            }
        }
        return [$tree, $test];
    }


    public function myTest()
    {
        try {
            $userId = Auth::id();

            $list = UserTest::where('user_id', $userId)->paginate(10);

            return view('member.results.my-result', compact('list'));
        } catch (\Exception $e) {
            throw $e;
        }
    }
}

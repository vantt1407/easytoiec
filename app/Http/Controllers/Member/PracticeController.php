<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Part;
use App\Models\Practice;
use Illuminate\Support\Facades\DB;
use App\Models\UserPractice;
use Illuminate\Support\Facades\Auth;
use App\Models\UserPracticeAnswer;
use App\Models\Question;
use App\Models\Level;

class PracticeController extends Controller
{
    public function list(Request $request, $slug)
    {

        $part = $request->part;
        $level = $request->level;
        
        $partList = [];
        $list = [];
        $levels = Level::all();
        try {

            $current = Part::firstWhere('part_slug',$slug);
           
            $sql = 'SELECT distinct practices.practice_slug,practices.practice_id,practices.practice_name,
                parts.part_name 
                FROM practices Inner JOIN  practice_questions
                ON practice_questions.practice_id = practices.practice_id
                Inner JOIN questions ON questions.question_id = practice_questions.question_id
                Inner JOIN parts ON parts.part_id =  questions.part_id
                WHERE parts.part_slug = ? and '. ( $level ?  ('practices.level_id ='. $level ) : 'practices.practice_id > 0') .
                ' order by  practices.practice_id ASC ';
            // dd($sql);

            $list = DB::select($sql, [$slug]);
        } catch (\Exception $e) {
            throw $e;
        }
        return view('member.practice.list', compact('partList', 'slug', 'list','current','levels'));
    }


    public function detail($slug)
    {
        $data = Practice::with('questions')->firstWhere('practice_slug', $slug);
        return view('member.practice.detail', compact('data'));
    }
    public function create(Request $request)
    {
        DB::beginTransaction();
        try {

            $data = $request->except('_token');
            // dd($data);
            $questionIds = $data['question_id'];
            $userPractice = new UserPractice();
            $userPractice->practice_id = $data['practice_id'];
            $userPractice->user_id = Auth::id();
            $userPractice->scores = 0;
            $userPractice->save();
            $i = 2;
            $totalQuestion = count($questionIds);

            $totalCorrect = 0;
            foreach ($questionIds as $k => $i) {
                
                $q = Question::findOrFail($i);
             
                $isCorrect = false;
                foreach ($q->answers as $a) {
                    if (isset($data[$i])) {
                        if ($a->answer_option == $data[$i]) {
                            $isCorrect = $a->answer_is_correct;
                            $totalCorrect+=1;
                        }
                            
                    }
                }
                $userPracticeAnswer = new UserPracticeAnswer();
                $userPracticeAnswer->user_practice_id = $userPractice->id;
                $userPracticeAnswer->answer = isset($data[$i]) ? $data[$i] : null;
                $userPracticeAnswer->answer_is_correct  = $isCorrect;
                $userPracticeAnswer->question_id = $questionIds[$k];
                $userPracticeAnswer->save();
                $i++;
            }
            DB::commit();
            
            $pass =  ($totalCorrect/$totalQuestion)*100;
            
            return redirect()->back()->with(['practice_result' => $userPractice,'data_user'=>$data,'pass'=>$pass]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }


    public function result(Request $request)
    {
    }
}

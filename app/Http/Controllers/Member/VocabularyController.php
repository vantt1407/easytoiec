<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Topic;

class VocabularyController extends Controller
{
    function index(Request $request) {
        $slug = $request->slug;
        $detail = collect();
        if(!empty(  $slug ) ) {
            $detail  = Topic::with('vocabularies')->firstWhere('topic_slug',$slug);
        }
      

        return view('member.topic.vocabulary',compact('detail'));
    }
}

<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Part;
use App\Models\Article;
use App\Models\Topic;
use App\Models\Lesson;

class DashboardController extends Controller
{
    public function index()
    {
        $listPart = Part::all();
        $listArticle = Article::where('article_publish',1)->orderBy('created_at', 'desc')->get();
        $listTopic = Topic::all();
        $listLesson = Lesson::all();
        $banner = \App\Models\Setting::find(1)->banner;
       return view('member.dashhboard',compact('listPart','listArticle','listTopic','listLesson','banner'));
    }
}
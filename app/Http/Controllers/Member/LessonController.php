<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Lesson;
use App\Models\LessonPart;
use App\Models\Level;

class LessonController extends Controller
{

    public function list(Request $request)
    {
        $keyword = $request->keyword;
        $level =  $request->level;

        $list =  Lesson::when($keyword, function ($query) use($keyword) {

            $query->where('lesson_title','LIKE','%'.$keyword.'%');

        })->when($level, function ($query) use($level) {

            $query->where('level_id',$level);

        })
        ->paginate(10);
        
        $levels = Level::all();

        return view('member.lesson.list', compact('list','levels'));
    }
    public function lesson(Request $request)
    {
        $slug =  $request->slug;
        $lesson  = Lesson::with('lessonParts')->firstWhere('lesson_slug',$slug);
        
        return view('member.lesson.lesson',compact('lesson'));
    }


    public function lessonPart($slug,$part)
    {
        

        $partItem = LessonPart::where('lesson_part_slug',$part)->first();

        $lesson = Lesson::where('lesson_slug',$slug)->first();

        $partList =LessonPart::where('lesson_id', $lesson->lesson_id)->get();

        return view('member.lesson.lesson_part',compact('part','partItem','partList','lesson'));
       
    }
}

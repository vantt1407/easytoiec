<?php
use  Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'middleware' => ['check.auth','check.is.admin']], function () {
    Route::get('/', function () {
        return view('layouts.app_admin');
    });

    Route::get('/dashboard','Admin\DashboardController@showView')->name('get_admin_dashboard');
    Route::resource('user', 'Admin\UserController');

   
  
    Route::get('lesson/part/question/list','Admin\LessonController@getListLessonPartQuestion')->name('lesson.part.question.list');

    Route::get('lesson/part/list','Admin\LessonPartController@getListLessonPart')->name('lesson.part.list');
    Route::post('lesson/part/question/random','Admin\LessonPartController@randomQuestion')->name('lesson.part.question.random');
    Route::resource('lesson-part', 'Admin\LessonPartController');
    
    Route::get('lesson/list','Admin\LessonController@getListLesson')->name('lesson.list');

    
    Route::resource('lesson', 'Admin\LessonController');
    Route::resource('topic', 'Admin\TopicController');
    Route::resource('practice', 'Admin\PracticeController');

    Route::get('vocabulary/list', 'Admin\VocabularyController@listVocabulary')->name('vocabulary.list');
    Route::resource('vocabulary', 'Admin\VocabularyController');
    Route::resource('article', 'Admin\ArticleController');
    Route::resource('category', 'Admin\CategoryController');

    
    Route::post('test/random', 'Admin\TestController@randomGroupQuestion')->name('test.random');
    Route::resource('test', 'Admin\TestController');
    Route::resource('part', 'Admin\PartController');
    

    
    Route::get('question/list-l','Admin\QuestionController@getQuestionsAjaxLesson')->name('question.list.l');
    Route::get('question/list-p','Admin\QuestionController@getQuestionsAjaxPractice')->name('question.list.p');
    Route::get('question/list-sv','Admin\QuestionController@getQuestionsAjaxV2')->name('question.list.sv');
    Route::get('question/list','Admin\QuestionController@getQuestionsAjax')->name('question.list');
    Route::get('question/create-group','Admin\QuestionController@createGroup')->name('question.create.group');
    Route::post('question/create-raw','Admin\QuestionController@storeRawDataQuestionGroup')->name('question.create.raw');
    Route::get('question/edit-raw/{index}','Admin\QuestionController@showRawDataQuestionGroup')->name('question.edit.raw');
    Route::post('question/update-raw/{index}','Admin\QuestionController@updateRawDataQuestionGroup')->name('question.update.raw');
    Route::get('question/delete-raw/{index}','Admin\QuestionController@deleteRawQuestion')->name('question.delete.raw');
    
    Route::get('question-group/list','Admin\QuestionGroupController@showList')->name('question-group.list');
    Route::resource('question-group', 'Admin\QuestionGroupController');
    Route::resource('question', 'Admin\QuestionController');

    Route::put('setting/email','Admin\SettingController@changeEmailConfig')->name('setting.email');
    Route::put('setting/random','Admin\SettingController@changeRandomConfig')->name('setting.random');
    Route::put('setting/info','Admin\SettingController@changeInfoConfig')->name('setting.info');

    Route::resource('setting', 'Admin\SettingController');
    Route::get('file', function () {
        return view('admin.file.iframe');
    })->name('dashboard-file');
});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['check.auth','check.is.admin']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});


Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
    ->name('ckfinder_connector');

Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
    ->name('ckfinder_browser');


Route::get('xxx', function() {
    return view('admin.file.ckfinder');
});
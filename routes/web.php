<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('login/{service}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{service}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/', 'HomeController@showHome')->name('home');

Route::get('/dang-xuat', 'Auth\LoginController@logout')->name('put_logout');

Route::get('/dang-nhap', 'Auth\LoginController@showLogin')->name('get_login');

Route::post('/auth', 'Auth\LoginController@login')->name('post_login');

Route::get('/dang-ky', 'Auth\RegisterController@showView')->name('get_register');

Route::post('/user/register', 'Auth\RegisterController@register')->name('post_register');

Route::get('/quen-mat-khau', 'Auth\ForgotPasswordController@showView')->name('get_forgot');

Route::post('/post-forgot', 'Auth\ForgotPasswordController@requestForgotPassword')->name('post_forgot');

Route::get('/xac-thuc-email/{email}', 'Auth\VerificationController@verifyEmail')->name('put_verify');

Route::get('/khoi-phuc-mat-khau/{email}', 'Auth\ResetPasswordController@showView')->name('get_reset');

Route::post('/post-reset-password', 'Auth\ResetPasswordController@reset')->name('post_reset');

Route::group(['middleware' => ['check.auth', 'verified']], function () {

    Route::get('/dashboard', 'Member\DashboardController@index')->name('get_dashboard');

    //article
    Route::get('/bai-viet/{slug}', 'Member\ArticleController@detail')->name('article_detail');
    Route::get('/bai-viet', 'Member\ArticleController@list')->name('article_list');

    //part practice

    Route::get('/phan/{slug}/bai-tap', 'Member\PracticeController@list')->name('part_practices');

    Route::get('/bai-tap/{practice_slug}', 'Member\PracticeController@detail')->name('part_practices_detail');

    Route::post('/bai-tap/nop-bai', 'Member\PracticeController@create')->name('part_practices_create');

    //user
    Route::get('/ho-so', 'Member\ProfileController@profile')->name('user_profile');

    Route::put('/update-profile', 'Member\ProfileController@updateProfile')->name('update_profile');

    Route::put('/update-avatar', 'Member\ProfileController@uploadAvatar')->name('update_avatar');


    //lesson

    //topic
    Route::group(['prefix' => 'chu-de'], function () {
        Route::get('/', 'Member\TopicController@list')->name('topic_list');
        Route::get('/{slug}/tu-vung', 'Member\VocabularyController@index')->name('vocabulary_list');
    });

    Route::group(['prefix' => 'bai-giang'], function () {
        Route::get('/', 'Member\LessonController@list')->name('lesson_list');
        Route::get('{slug}/{part}', 'Member\LessonController@lessonPart')->name('lesson_part_detail');
        Route::get('{slug}', 'Member\LessonController@lesson')->name('lesson_detail');
    });
    Route::group(['prefix' => 'de-thi'], function () {
        
        Route::get('/lan-thi','Member\TestController@myTest')->name('my_test');
        Route::get('/','Member\TestController@list')->name('test_list');
        Route::get('/chi-tiet-dap-an/{id}','Member\TestController@getResult')->name('get-result');

        Route::get('/huong-dan/{id}','Member\TestController@preview')->name('get-preview');
        
        Route::get('thi/{id}','Member\TestController@detail')->name('test_detail');
        
        Route::post('/submit/{part}','Member\TestController@submit')->name('submit_test');
        
       

    });
});


Route::get('crawl-data', 'Crawl\CrawlController@showView');

Route::get('crawl/data', 'Crawl\CrawlController@showViewV2');


Route::post('run-crawl', 'Crawl\CrawlController@handle')->name('run');



Route::get('/init/test', function (){
    
    $questions = \App\Models\QuestionGroup::all();
    // $questions = \App\Models\QuestionGroup::where('question_group_id','>',45)->get();
    // $photoPath = 'easytoiec/public/storage/photos/shares/test/de1/audio';
    // return response()->json($questions );
    foreach($questions as $item) {
             
        if($item->question_group_image) {
            $item->question_group_image = 'https://toeic24.vn'. $item->question_group_image;
            $item->save();
        }
        // $test = new  App\Models\TestQuestion();
        // $test->test_id = 3;
        // $test->question_group_id =$item->question_group_id;
        // $test->save();
        }
    
    echo "done";

});
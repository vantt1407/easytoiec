Cài đặt

Kéo source từ repo về (yêu cầu cài git )

Mở cmd lên gõ lệnh:

# git clone https://gitlab.com/vantt1407/easytoiec.git
mở file .env lên và chỉnh lại

# DB_USERNAME= tên đăng nhập mysql (mặc định là root)
# DB_PASSWORD= password đăng nhập mysql 

Tiến hành cài đặt

Mở folder source vừa clone về trong cmd và gõ lệnh (yêu cầu máy cài composer)

# composer install 

# php artisan  key:generate  

# php artisan  db:create 

# php artisan  migrate 

# php artisan  db:seed 

chạy lần lượt các lệnh kia xong 

# php artisan serve 

truy cập vào url hiển thị trong cmd 

# Laravel development server started: http://127.0.0.1:8000

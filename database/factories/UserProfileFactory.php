<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserProfile;
use App\Models\User;
use Faker\Generator as Faker;


$factory->define(UserProfile::class, function (Faker $faker) {
    
    $list = UserProfile::all();
    $ids = [];
    foreach($list as $p) {
        $ids[] = $p->user_id;
    }
    return 
    [
        'user_profile_avatar' => 'http://demo.foxthemes.net/courseplusv3.3/assets/images/avatars/avatar-2.jpg',
        'user_profile_phone' => $faker->phoneNumber,
        'user_profile_full_name' => $faker->name,
        'user_profile_address' => $faker->address,
        'user_id' => (User::select('user_id')->whereNotIn('user_id',$ids)->first())->user_id
    ];
});



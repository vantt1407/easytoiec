<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePracticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practices', function (Blueprint $table) {
            $table->bigIncrements('practice_id');
            $table->string('practice_name',50)->unique();
            $table->string('practice_slug');
            // $table->unsignedBigInteger('part_id');
            $table->unsignedBigInteger('level_id');
            // $table->foreign('part_id')
            // ->references('part_id')->on('parts');
            $table->foreign('level_id')
            ->references('level_id')->on('levels');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practices');
    }
}

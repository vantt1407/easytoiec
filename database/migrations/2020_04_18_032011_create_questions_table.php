<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('question_id');
            $table->text('question');
            $table->integer('question_scores');
            $table->bigInteger(' question_time');
            $table->string('question_audio')->nullable();
            $table->string('question_image')->nullable();
            $table->unsignedBigInteger('question_group_id')->nullable();

            $table->unsignedBigInteger('part_id');
            $table->foreign('part_id')
            ->references('part_id')->on('parts');
            $table->foreign('question_group_id')
                ->references('question_group_id')->on('question_groups');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}


// '1', 'Mô Tả Hình Ảnh', 'Bạn sẽ xem một bức hình → Nghe 4 lựa chọn A, B, C, D → Chọn một đáp án mô tả chính xác nhất nội dung có trong hình.', 'assets\\images\\giao-tiep-tieng-anh-1.jpg', '1', '2020-05-09 03:41:42', NULL
// '2', 'Hỏi & Đáp', 'Bạn sẽ nghe một câu hỏi hoặc một câu nói → Nghe tiếp 3 câu trả lời / hồi đáp lại câu trên (tương ứng với 3 lựa chọn A, B, C) → Chọn một câu hồi đáp phù hợp nhất cho câu hỏi.', 'assets\\images\\qa.png', '1', '2020-05-09 03:41:42', NULL
// '3', 'Đoạn Hội Thoại', 'Bạn sẽ lắng nghe các đoạn hội thoại ngắn giữa hai người → Với mỗi đoạn sẽ có 3 câu hỏi, mỗi câu hỏi có 4 lựa chọn A, B, C, D. Bạn đọc câu hỏi và chọn câu trả lời phù hợp nhất cho câu hỏi.', 'assets\\images\\giao-tiep-tieng-anh-1.jpg', '1', '2020-05-09 03:41:42', NULL
// '4', 'Bài Nói Chuyện Ngắn', 'Bạn sẽ lắng nghe các bài nói chuyện ngắn (độc thoại) → Với mỗi đoạn sẽ có 3 câu hỏi, mỗi câu hỏi có 4 lựa chọn A, B, C, D. Bạn đọc câu hỏi và chọn câu trả lời phù hợp nhất cho câu hỏi.', 'assets\\images\\phatbieu.png', '1', '2020-05-09 03:41:42', NULL
// '5', 'Điền Vào Câu', 'Bạn sẽ được cho một câu có một chỗ trống → Chọn một đáp án phù hợp nhất để điền vào chỗ trống.', 'assets\\images\\de_luyen_tap_phan_5.webp', '2', '2020-05-09 03:41:42', NULL
// '6', 'Điền Vào Đoạn Văn', 'Bạn sẽ được cho một đoạn văn có nhiều chỗ trống → Chọn một đáp án phù hợp nhất để điền vào chỗ trống.', 'assets\\images\\de_luyen_tap_phan_6.webp', '2', '2020-05-09 03:41:42', NULL
// '7', 'Đọc Hiểu Đoạn Văn', 'Bạn sẽ được cho 10 bài đọc với mỗi bài gồm 1 đoạn văn. Số lượng câu hỏi cho mỗi bài đọc dao động từ 2 đến 4 câu → Bạn đọc câu hỏi và chọn câu trả lời phù hợp nhất cho câu hỏi. /n\n                Bạn sẽ được cho 5 bài đọc với mỗi bài có từ 2 đến 3 đoạn văn. Mỗi bài đọc có 5 câu hỏi. → Bạn đọc câu hỏi và chọn câu trả lời phù hợp nhất cho câu hỏi.', 'assets\\images\\read.jpg', '2', '2020-05-09 03:41:42', NULL

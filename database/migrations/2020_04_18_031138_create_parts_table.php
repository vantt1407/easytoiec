<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts', function (Blueprint $table) {
            $table->bigIncrements('part_id');
            $table->string('part_name',50)->unique();
            $table->string('part_slug',50);
            $table->text('part_description',500);
            $table->string('part_avatar',100);
            $table->unsignedBigInteger('part_type_id');
            $table->foreign('part_type_id')
            ->references('part_type_id')->on('part_types');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_parts', function (Blueprint $table) {
            $table->bigIncrements('lesson_part_id');
            $table->unsignedBigInteger('lesson_id');
            $table->string('lesson_part_title',50)->unique();
            $table->string('lesson_part_slug',50);
            $table->text('lesson_part_content');
            $table->foreign('lesson_id')
                ->references('lesson_id')->on('lessons')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_practices');
    }
}

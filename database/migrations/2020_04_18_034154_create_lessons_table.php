<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->bigIncrements('lesson_id');
            $table->string('lesson_avatar');
            $table->string('lesson_title',100)->unique();
            $table->string('lesson_slug');
            $table->string('lesson_description');
            $table->text('lesson_long_description');
            $table->unsignedBigInteger('level_id');
            $table->foreign('level_id')
            ->references('level_id')->on('levels');
            $table->unsignedBigInteger('created_by');
            $table->foreign('created_by')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}

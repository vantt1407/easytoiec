<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('user_profile_id');
            $table->string('user_profile_avatar')->nullable();
            $table->string('user_profile_phone',10)->nullable();
            $table->string('user_profile_full_name',50)->nullable();
            $table->string('user_profile_address',100)->nullable();
            $table->unsignedBigInteger('user_id')->unique();
            $table->foreign('user_id')
            ->references('user_id')->on('users')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}

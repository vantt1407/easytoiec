<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVocabulariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vocabularies', function (Blueprint $table) {
            $table->bigIncrements('vocabulary_id');
            $table->string('vocabulary_word');
            $table->string('vocabulary_audio')->nullable();
            $table->string('vocabulary_image')->nullable();
            $table->string('vocabulary_pronounce');
            $table->string('vocabulary_vi_translate');
            $table->unsignedBigInteger('antonym_id')->nullable()->default('0');
            $table->unsignedBigInteger('topic_id');
            $table->unsignedBigInteger('vocabulary_type_id');
            $table->foreign('vocabulary_type_id')
                ->references('vocabulary_type_id')->on('vocabulary_types')
                ->onDelete('cascade');
            $table->foreign('topic_id')
                ->references('topic_id')->on('topics')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vocabularies');
    }
}

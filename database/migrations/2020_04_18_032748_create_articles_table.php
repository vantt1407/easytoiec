<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('article_id');
            $table->string('article_avatar');
            $table->string('article_title',100)->unique();
            $table->string('article_slug',100);
            $table->text('article_content');
            $table->tinyInteger('article_publish');
            $table->bigInteger('article_view_count');
            $table->softDeletes();
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('article_category_id');
            $table->foreign('article_category_id')
                ->references('article_category_id')->on('article_categories')
                ->onDelete('cascade');
            $table->foreign('created_by')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}

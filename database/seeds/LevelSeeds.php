<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LevelSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
            [
                'level_name' =>'450-500',
            ],
            [
                'level_name' =>'550-600',
            ],
            [
                'level_name' =>'650-700',
            ]
        ]);
        
    }
}

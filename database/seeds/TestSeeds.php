<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class TestSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       

        for($i=0; $i < 10 ; $i++) {

            $testName = "Đề thi số ".($i+1);
            DB::table('tests')->insert(
                [
                    'test_time' => 120,
                    'test_name' =>$testName,
                    'test_slug' => Str::slug($testName),
                    'level_id' => 1
                ]
            );
        }
    }
}

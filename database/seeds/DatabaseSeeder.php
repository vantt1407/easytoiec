<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(RoleSeeder::class);
        // $this->call(PartTypeSeeder::class);
        // $this->call(Part::class);
        // $this->call(UserSeeder::class);
        // $this->call(UserProfileSeeder::class);
        // $this->call(LevelSeeds::class);
        // $this->call(PracticeSeeds::class);
        // $this->call(TestSeeds::class);
        // $this->call(VocabularyTypeSeed::class);
        $this->call(SettingSeeds::class);
    }
}

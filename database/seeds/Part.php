<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Part extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'part_name', 'part_description', 'part_avatar','part_type_id'
        
        DB::table('parts')->insert([

            [
                'part_name'=>'Mô Tả Hình Ảnh',
                'part_slug' => Str::slug('Mô Tả Hình Ảnh'),
                'part_avatar' => 'assets\images\de_luyen_tap_phan_1.webp',
                'part_type_id'=> '1',
                'part_description' => 'Bạn sẽ xem một bức hình → Nghe 4 lựa chọn A, B, C, D → Chọn một đáp án mô tả chính xác nhất nội dung có trong hình.'
            ],
            [
                'part_name'=>'Hỏi & Đáp',
                'part_slug' => Str::slug('Hỏi & Đáp'),
                'part_avatar' => 'assets\images\de_luyen_tap_phan_2.webp',
                'part_type_id'=> '1',
                'part_description' => 'Bạn sẽ nghe một câu hỏi hoặc một câu nói → Nghe tiếp 3 câu trả lời / hồi đáp lại câu trên (tương ứng với 3 lựa chọn A, B, C) → Chọn một câu hồi đáp phù hợp nhất cho câu hỏi.'
            ],
            [
                'part_name'=>'Đoạn Hội Thoại',
                'part_slug' => Str::slug('Đoạn Hội Thoại'),
                'part_avatar' => 'assets\images\de_luyen_tap_phan_3.webp',
                'part_type_id'=> '1',
                'part_description' => 'Bạn sẽ lắng nghe các đoạn hội thoại ngắn giữa hai người → Với mỗi đoạn sẽ có 3 câu hỏi, mỗi câu hỏi có 4 lựa chọn A, B, C, D. Bạn đọc câu hỏi và chọn câu trả lời phù hợp nhất cho câu hỏi.'
            ],
            [
                'part_name'=>'Bài Nói Chuyện Ngắn',
                'part_slug' => Str::slug('Bài Nói Chuyện Ngắn'),
                'part_avatar' => 'assets\images\de_luyen_tap_phan_4.webp',
                'part_type_id'=> '1',
                'part_description' => 'Bạn sẽ lắng nghe các bài nói chuyện ngắn (độc thoại) → Với mỗi đoạn sẽ có 3 câu hỏi, mỗi câu hỏi có 4 lựa chọn A, B, C, D. Bạn đọc câu hỏi và chọn câu trả lời phù hợp nhất cho câu hỏi.'
            ],
            [
                'part_name'=>'Điền Vào Câu',
                'part_slug' => Str::slug('Điền Vào Câu'),
                'part_avatar' => 'assets\images\de_luyen_tap_phan_5.webp',
                'part_type_id'=> '2',
                'part_description' => 'Bạn sẽ được cho một câu có một chỗ trống → Chọn một đáp án phù hợp nhất để điền vào chỗ trống.'
            ],
            [
                'part_name'=>'Điền Vào Đoạn Văn',
                'part_slug' => Str::slug('Điền Vào Đoạn Văn'),
                'part_avatar' => 'assets\images\de_luyen_tap_phan_6.webp',
                'part_type_id'=> '2',
                'part_description' => 'Bạn sẽ được cho một đoạn văn có nhiều chỗ trống → Chọn một đáp án phù hợp nhất để điền vào chỗ trống.'
            ],
            [
                'part_name'=>'Đọc Hiểu Đoạn Văn',
                'part_slug' => Str::slug('Đọc Hiểu Đoạn Văn'),
                'part_avatar' => 'assets\images\de_luyen_tap_phan_7.webp',
                'part_type_id'=> '2',
                'part_description' => 'Bạn sẽ được cho 10 bài đọc với mỗi bài gồm 1 đoạn văn. Số lượng câu hỏi cho mỗi bài đọc dao động từ 2 đến 4 câu → Bạn đọc câu hỏi và chọn câu trả lời phù hợp nhất cho câu hỏi. /n
                Bạn sẽ được cho 5 bài đọc với mỗi bài có từ 2 đến 3 đoạn văn. Mỗi bài đọc có 5 câu hỏi. → Bạn đọc câu hỏi và chọn câu trả lời phù hợp nhất cho câu hỏi.'
            ]
        ]);
    }
}

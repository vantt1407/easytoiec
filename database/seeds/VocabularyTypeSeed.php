<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VocabularyTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vocabulary_types')->insert([
            [
                'vocabulary_type' =>'Danh từ',
                'vocabulary_symbol'=>'N'
            ],
            [
                'vocabulary_type' =>'Động từ',
                'vocabulary_symbol' => 'V'
            ],
            [
                'vocabulary_type' =>'Tính từ',
                'vocabulary_symbol' =>'Adj'
            ],
            [
                'vocabulary_type' =>'Trạng từ',
                'vocabulary_symbol' =>'Adv'
            ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Providers\AuthServiceProvider;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
           
            [
                'role_name' => AuthServiceProvider::ADMIN_ROLE,
            ],
            [
                'role_name' => AuthServiceProvider::USER_ROLE,
            ]
        ]);
    }
}

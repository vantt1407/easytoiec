<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PracticeSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i < 7 ; $i++) {

            $practiceName = "Bài luyện tập số ".($i+1);

            DB::table('practices')->insert(
                [
                    'practice_name' =>$practiceName,
                    'practice_slug' => Str::slug($practiceName),
                    'level_id' => 1
                ]
            );
        }

        for($i=7; $i < 14 ; $i++) {

            $practiceName = "Bài luyện tập số ".($i+1);

            DB::table('practices')->insert(
                [
                    'practice_name' =>$practiceName,
                    'practice_slug' => Str::slug($practiceName),
                    'level_id' => 1
                ]
            );
        }
    }
}

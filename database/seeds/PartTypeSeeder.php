<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('part_types')->insert([

            [
                'part_type' => 'Nghe',
            ],
            [
                'part_type' => 'Đọc',
            ]
        ]);
    }
}

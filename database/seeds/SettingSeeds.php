<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert(
            [
                'banner' => "",
                'phone' =>"1900636929",
                'address' => "Novaland The Sun Avenue, Tầng 1, Tháp 1, Tòa nhà Số 28, Đường Mai Chí Thọ, P, Quận 2, Thành phố Hồ Chí Minh",
                'slogan' => "Học và luyện thi Toiec chưa bao giờ dễ dàng tới như vậy"
            ]
        );
    }
}

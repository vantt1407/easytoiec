<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicon -->
<link href="{{ asset('favicon.png') }}" rel="icon" type="image/png">
<!-- CSS 
    ================================================== -->

<link  rel="stylesheet" href="{{ asset('assets/admin/css/style.css') }}">

<link rel="stylesheet" href="{{ asset('assets/admin/css/framework.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/common/notify/noty.css') }}">
<link rel="stylesheet" href="{{ asset('assets/common/notify/themes/metroui.css') }}">
<!-- icons
    ================================================== -->

<link rel="stylesheet" href="{{ asset('assets/admin/css/icons.css') }}">




@yield('head')

@yield('css')
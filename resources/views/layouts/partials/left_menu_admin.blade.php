<div class="page-menu">
    <!-- btn close on small devices -->
    <span class="btn-menu-close" uk-toggle="target: #wrapper ; cls: mobile-active"></span>
    <!-- traiger btn -->
    <span class="btn-menu-trigger" uk-toggle="target: .page-menu ; cls: menu-large"></span>

    <!-- logo -->
    <div class="logo uk-visible@s">
        <a href="{{ route('get_admin_dashboard') }}">
            <i class=" uil-graduation-hat"></i>
            <span>Easy Toiec</span>
        </a>
    </div>
    <div class="page-menu-inner" data-simplebar>
        <ul class="mt-0">
            <li><a href="{{ route('get_admin_dashboard') }}"><i class="uil-home-alt"></i> <span> Trang quản trị</span></a> </li>
            <li><a href="{{ route('part.index') }}"><i class="uil-layers"></i> <span> Phần thi </span></a> </li>
            <li><a href="{{ route('practice.index') }}"><i class="uil-file-edit-alt"></i> <span> Luyện tập </span></a> </li>
            <li><a href="{{ route('test.index') }}"><i class="uil-file-edit-alt"></i> <span> Đề thi </span></a> </li>
            <li class="#">
                <a href="#">
                    <i class="icon-feather-help-circle"></i>
                    <span> Câu hỏi</span>
                </a>
                <ul>
                    <li><a href="{{ route('question.index') }}">Câu hỏi đơn</a></li>
                    <li><a href="{{ route('question-group.list') }}">Nhóm câu hỏi</a></li>
                </ul>
            </li>
            <li><a href="{{ route('lesson.index')}}"><i class="uil-youtube-alt"></i> <span> Khoá học</span></a> </li>
            <li><a href="{{ route('user.index') }}"><i class="uil-users-alt"></i> <span> Người dùng</span></a> </li>
            <li><a href="{{ route('article.index') }}"><i class="uil-file-alt"></i> <span> Bài viết</span></a>
            </li>
            <li><a href="{{ route('topic.index') }}"><i class="uil-tag-alt"></i> <span>Chủ đề</span></a> </li>
            <li><a href="{{ route('vocabulary.index') }}"><i class="uil-file-alt"></i> <span>Từ vựng</span></a> </li>
        </ul>

        <ul data-submenu-title="Thiết lập hệ thống">

            <li><a href="{{ route('setting.index') }}"><i class="uil-cog"></i> <span> Thiết lập </span></a> </li>
            <li><a href="{{ route('dashboard-file')}}"><i class=" uil-images"></i> <span> Tệp hệ thống </span></a> </li>
        </ul>

    </div>
</div>

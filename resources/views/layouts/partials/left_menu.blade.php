<div class="side-nav uk-animation-slide-left-medium">


    <div class="side-nav-bg"></div>

    <!-- logo -->
    <div class="logo">
        <a href="{{ route('get_dashboard') }}">
            <i class=" uil-graduation-hat"></i>
        </a>
    </div>

    <ul>
        <li>
            <a href="{{ route('lesson_list') }}"> <i class="uil-play-circle"></i><span class="tooltips">Bài giảng</span>
            </a>

        </li>
        <li>
            <!-- book -->
            <a href="#"> <i class="uil-book-alt"></i></a>
            <div class="side-menu-slide">
                <div class="side-menu-slide-content">
                    <ul data-simplebar>
                        <li style="background-color:#3e416d;">
                            <a  style="color:#fff !important" href="javascript:void()"><i class="uil-book-alt "></i> Luyện tập </a>
                        </li>


                        @foreach ($parts as $item)
                        <li>
                            <a href="{{ route('part_practices', ['slug'=>$item->part_slug]) }}"> 
                                <i class="uil-book-alt "></i> 
                                {{  $item->part_name }}
                            </a>
                        </li>
                        @endforeach
                        
                </div>
            </div>
        </li>
        <li>
            <!-- Paths-->
            <a href="{{ route('test_list') }}"> <i class="icon-feather-bookmark icon-small"></i> <span class="tooltips">
                    Thi thử</span></a>
        </li>
        <li>
            <!-- Paths-->
            <a href="{{ route('topic_list') }}"> <i class=" uil uil-tag-alt icon-small"></i> <span class="tooltips">
                    Chủ đề và từ vựng</span></a>
        </li>
       
        <li>
            <!-- Blog-->
            <a href="{{ route('article_list') }}"> <i class="uil-file-alt"></i> <span class="tooltips"> Bài
                    viết</span></a>
        </li>

    </ul>

</div>
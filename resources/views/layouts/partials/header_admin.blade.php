<header class="header uk-light">

    <div class="container">
        <nav uk-navbar>
            <!-- left Side Content -->
            <div class="uk-navbar-left">
                <!-- menu icon -->
                <span class="mmenu-trigger" uk-toggle="target: #wrapper ; cls: mobile-active">
                    <button class="hamburger hamburger--collapse" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </span>
                <!-- logo -->
                <a href="#" class="logo">
                    <img src="" alt="">
                    <span> Courseplus</span>
                </a>
                <!-- Search box dropdown -->
                <div uk-dropdown="pos: top;mode:click;animation: uk-animation-slide-bottom-small"
                    class="dropdown-search">
                    <div class="erh BR9 MIw"
                        style="top: -26px;position: absolute ; left: 24px;fill: currentColor;height: 24px;pointer-events: none;color: #f5f5f5;">
                        <svg width="22" height="22">
                            <path d="M0 24 L12 12 L24 24"></path>
                        </svg></div>
                    <!-- User menu -->

                </div>
            </div>


            <!--  Right Side Content   -->

            <div class="uk-navbar-right">



                <div class="header-widget">
                    <!-- profile-icon-->

                    <div class="dropdown-user-details">
                        <div class="dropdown-user-avatar">
                            <img src="{{ Auth::user()->profile ? asset(Auth::user()->profile->user_profile_avatar) : ''}}"
                                alt="">
                        </div>
                        <div class="dropdown-user-name">
                            {{ Auth::user()->profile ? Auth::user()->profile->user_profile_full_name : Auth::user()->email }} 
                            <span>Quản trị viên</span>
                        </div>
                    </div>
                    <div uk-dropdown="pos: top-right ;mode:click" class="dropdown-notifications small">

                        <!-- User menu -->

                        <ul class="dropdown-user-menu">
                            
                            <li><a href="profile-edit.html">
                                    <i class="icon-feather-settings"></i>Cài đặt tài khoản</a>
                            </li>
                         
                            <ul class="menu-divider">
                                <li><a href="#" id="logout">
                                        <i class="icon-feather-log-out"></i> Đăng xuất
                                        <form id="logout_form" action="{{ route('put_logout') }}" style="display:none">
                                            @csrf
                                            @method('put')
                                        </form>
                                    </a>
                                </li>
                            </ul>
                    </div>
                </div>
 
                <!-- User icons -->
                <span class="uil-user icon-small uk-hidden@s" uk-toggle="target: .header-widget ; cls: is-active">
                </span>
            </div>
            <!-- End Right Side Content / End -->
        </nav>
    </div>
    <!-- container  / End -->

</header>
<header class="header uk-sticky" uk-sticky="top:20 ; cls-active:header-sticky" style="">

    <div class="container">
        <nav uk-navbar="" class="uk-navbar">

            <!-- left Side Content -->
            <div class="uk-navbar-left">

                <span class="btn-mobile" uk-toggle="target: #wrapper ; cls: mobile-active"></span>



                <!-- logo -->
                <a href="dashboard.html" class="logo">
                    <img src="#" alt="">
                    <span> Courseplus</span>
                </a>

                <!-- breadcrumbs -->
                @yield('breadcrumbs')


            </div>


            <!--  Right Side Content   -->
            @if ( Route::currentRouteName() === "get-preview"  || Route::currentRouteName() === "test_detail" )
            <div class="uk-navbar-right">
                <div class="header-widget" style="display:flex !important">
                    <!-- User icons close mobile-->

                    <a class="test-time" uk-tooltip="title:Thời gian làm bài; pos: bottom-left" style="border:2px solid #3e416d;border-radius:10px">
                        <div style="padding: 5px" id="clock">
                            {{ date('h:i:s',   ($test->test_time ) * 60  ) }}
                        </div>
                    </a>

                    <a href="#"  class="header-widget-icon btn-finish" uk-tooltip="title:Nộp bài; pos: bottom-rigth" title="" aria-expanded="false">
                        <i class="uil-check-circle text-success"></i>

                    </a>
                    <a href="#" class="header-widget-icon part-list" uk-toggle="target: #part-filter" uk-tooltip="title:Tùy chọn; pos: bottom-rigth" title="" aria-expanded="false">
                        <i class="uil-layers"></i>

                    </a>

                    <a href="#" class="header-widget-icon exit" uk-tooltip="title:Hủy bài thi; pos: bottom-left" title="" aria-expanded="false">
                        <i class="uil-times-circle text-danger"></i>

                    </a>
                </div>
            </div>
            @else
            <div class="uk-navbar-right">

                <div class="header-widget">
                    <!-- User icons close mobile-->
                    <span class="icon-feather-x icon-small uk-hidden@s" uk-toggle="target: .header-widget ; cls: is-active"> </span>
                    <!-- Message  -->

                    <a href="#" class="header-widget-icon" uk-tooltip="title: Thông báo ; pos: bottom ;offset:21" title="" aria-expanded="false">
                        <i class="uil-envelope-alt"></i>
                        <span>1</span>
                    </a>

                    <!-- Message  notificiation dropdown -->
                    <div uk-dropdown=" pos: top-right;mode:click" class="dropdown-notifications uk-dropdown">

                        <!-- notivication header -->
                        {{-- <div class="dropdown-notifications-headline">
                            <h4>Lỗ trình học gợi ý</h4>

                        </div> --}}

                        <!-- notification contents -->
                        <div class="dropdown-notifications-content" data-simplebar="init">
                            <div class="simplebar-wrapper" style="margin: 0px;">
                                <div class="simplebar-height-auto-observer-wrapper">
                                    <div class="simplebar-height-auto-observer"></div>
                                </div>
                                <div class="simplebar-mask">
                                    <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                        <div class="simplebar-content" style="padding: 0px; height: 100%; overflow: hidden scroll;">

                                            <!-- notiviation list -->
                                            <ul>
                                                <li class="notifications-not-read">
                                                    <a href="#">
                                                        <span class="notification-avatar">
                                                            <img src="#" alt="">
                                                        </span>
                                                        <div class="notification-text notification-msg-text">
                                                            <strong>Jonathan Madano</strong>
                                                            <p>Okay.. Thanks for The Answer I will be waiting for
                                                                your...
                                                            </p>
                                                            <span class="time-ago"> 2 hours ago </span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span class="notification-avatar">
                                                            <img src="" alt="">
                                                        </span>
                                                        <div class="notification-text notification-msg-text">
                                                            <strong>Stella Johnson</strong>
                                                            <p> Alex will explain you how to keep the HTML structure
                                                                and
                                                                all
                                                                that...</p>
                                                            <span class="time-ago"> 7 hours ago </span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span class="notification-avatar">
                                                            <img src="#" alt="">
                                                        </span>
                                                        <div class="notification-text notification-msg-text">
                                                            <strong>Alex Dolgove</strong>
                                                            <p> Alia Joseph just joined Messenger! Be the first to
                                                                send
                                                                a
                                                                welcome message..</p>
                                                            <span class="time-ago"> 19 hours ago </span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span class="notification-avatar">
                                                            <img src="#" alt="">
                                                        </span>
                                                        <div class="notification-text notification-msg-text">
                                                            <strong>Adrian Mohani</strong>
                                                            <p> Okay.. Thanks for The Answer I will be waiting for
                                                                your...
                                                            </p>
                                                            <span class="time-ago"> Yesterday </span>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="simplebar-placeholder" style="width: 338px; height: 463px;"></div>
                            </div>
                            <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                                <div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); visibility: hidden;"></div>
                            </div>
                            <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
                                <div class="simplebar-scrollbar" style="height: 214px; transform: translate3d(0px, 0px, 0px); visibility: visible;">
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-notifications-footer">
                            <a href="#"><i class="icon-line-awesome-long-arrow-right"></i> </a>
                        </div>
                    </div>


                    <!-- profile-icon-->

                    <a href="#" class="header-widget-icon profile-icon" aria-expanded="false">
                        <img src="{{ empty(Auth::user()->profile->user_profile_avatar) ? asset('assets/images/no-avt.png') :
                        asset(Auth::user()->profile->user_profile_avatar) 
                         }}" class="header-profile-icon">
                    </a>

                    <div uk-dropdown="pos: top-right ;mode:click" class="dropdown-notifications small uk-dropdown">

                        <!-- User Name / Avatar -->
                        <a href="{{ route('user_profile') }}">

                            <div class="dropdown-user-details">
                                <div class="dropdown-user-avatar">
                                    <img src="{{ empty(Auth::user()->profile->user_profile_avatar) ? asset('assets/images/no-avt.png') :
                                    asset(Auth::user()->profile->user_profile_avatar) 
                                     }}" class="header-profile-icon">
                                </div>
                                <div class="dropdown-user-name">
                                    {{ Auth::user()->profile ? Auth::user()->profile->user_profile_full_name : 'Học sinh'}}
                                </div>
                            </div>

                        </a>

                        <!-- User menu -->

                        <ul class="dropdown-user-menu">
                            <li>
                                <a href="{{ route('my_test') }}">
                                    <i class="uil-file-alt"></i>Bài thi của tôi</a>
                            </li>
                            <li><a href="{{ route('user_profile') }}">
                                    <i class="icon-feather-settings"></i>Cập nhật thông tin</a>
                            </li>
                            <li class="menu-divider">

                            <li><a href="#" id="logout">
                                    <i class="icon-feather-log-out"></i> Đăng xuất
                                    <form id="logout_form" action="{{ route('put_logout') }}" style="display:none">
                                        @csrf
                                        @method('put')
                                    </form>
                                </a>
                            </li>
                        </ul>



                    </div>


                </div>

                <!-- User icons -->
                <a href="#" class="uil-user icon-small uk-hidden@s" uk-toggle="target: .header-widget ; cls: is-active">
                </a>

            </div>
            <!-- End Right Side Content / End -->
            @endif

        </nav>

    </div>
    <!-- container  / End -->

</header>

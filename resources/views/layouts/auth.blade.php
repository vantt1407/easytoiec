<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng nhập hệ thống</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/auth/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/auth/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/auth/css/iofrm-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/auth/css/iofrm-theme19.css') }}">
    <style>
     .circle{
        border-radius:500px !important
     }
    </style>
</head>
<body>
    <div class="form-body without-side">
        <div class="website-logo">
            <a href="index.html">
                <div class="logo">
                    <img class="logo-size" src="{{ asset('assets/auth/images/logo-light.svg') }}" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img src="{{ asset('assets/auth/images/graphic3.svg') }}" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('assets/auth/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/auth/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/auth/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/auth/js/main.js') }}"></script>
</body>
</html>
<!doctype html>
<html lang="vi">
<head>
    @include('layouts.partials.head_admin')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    {{-- <div id="fb-root"></div> --}}
    {{-- <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=711546209684490&autoLogAppEvents=1" nonce="ujWICUEp"></script> --}}

    @include('layouts.partials.loading')
    <div id="wrapper" class="admin-panel">

        <!-- menu -->
        @include('layouts.partials.left_menu_admin')
        <!-- Header Container
        ================================================== -->
        @include('layouts.partials.header_admin')

        <!-- content -->
        <div class="page-content">
            <div class="page-content-inner">

                @yield('content')

                <!-- footer
                ================================================== -->


            </div>
            {{-- @include('layouts.partials.footer_admin') --}}
        </div>
        <!-- javaScripts
    ================================================== -->

        <script src="{{ asset('assets/admin/js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('assets/admin/js/framework.js') }}"></script>
        <script src="{{ asset('assets/admin/js/mmenu.js') }}"></script>
        <script src="{{ asset('assets/admin/js/simplebar.js') }}"></script>
        <script src="{{ asset('assets/admin/js/main.js') }}"></script>
        <script src="{{ asset('assets/admin/js/bootstrap-select.min.js') }}"></script>
        <script src="{{ asset('assets/common/notify/noty.min.js') }}"></script>

        <script>
            $(window).on('load', function() {
                $('.wrapper-load').hide();


            });

            $(function() {
                
                $('input[type=number]').on('change keypress', function() {
                    if ($(this).val() <= 0)
                        $(this).val("");
                });

                $(window).keydown(function(event) {
                    if(event.target.name == 'keyword') {
                        return true;
                    }
                    if (event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                    }
                });

            });

        </script>
        @include('component.notify')
        @yield('js')
        @include('admin.file.dashboard')


</body>
</html>

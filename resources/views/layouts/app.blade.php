<!doctype html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    ================================================== -->
    @yield('head')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link href="{{ asset('favicon.png') }}" rel="icon" type="image/png">
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('assets/admin/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/framework.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/common/notify/noty.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/common/notify/themes/metroui.css') }}">
	 <!-- icons
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('assets/admin/css/icons.css') }}">
    @yield('css')
</head>

<body>

    <div id="wrapper">

        <!-- Header Container
        ================================================== -->
        @include('layouts.partials.header')

        <!-- overlay seach on mobile-->
        <div class="nav-overlay uk-navbar-left uk-position-relative uk-flex-1 bg-grey uk-light p-2" hidden
            style="z-index: 10000;">
            <div class="uk-navbar-item uk-width-expand" style="min-height: 60px;">
                <form class="uk-search uk-search-navbar uk-width-1-1">
                    <input class="uk-search-input" type="search" placeholder="Search..." autofocus>
                </form>
            </div>
            <a class="uk-navbar-toggle" uk-close uk-toggle="target: .nav-overlay; animation: uk-animation-fade"
                href="#"></a>
        </div>

        <!-- side nav-->
        @include('layouts.partials.left_menu')

        <div class="page-content">

            @yield('content')

        </div>

    </div>



    <style>
        .flex-wrapper {
            display: flex;
            flex-flow: row nowrap;
        }

        .single-chart {
            width: 33%;
            justify-content: space-around;
        }

        .circular-chart {
            display: block;
            margin: 10px auto;
            max-width: 80%;
            max-height: 250px;
        }

        .circle-bg {
            fill: none;
            stroke: #eee;
            stroke-width: 3.8;
        }

        .circle {
            fill: none;
            stroke-width: 2.8;
            stroke-linecap: round;
            animation: progress 1s ease-out forwards;
        }

        @keyframes progress {
            0% {
                stroke-dasharray: 0 100;
            }
        }

        .circular-chart.orange .circle {
            stroke: #ff9f00;
        }

        .circular-chart.green .circle {
            stroke: #4CC790;
        }

        .circular-chart.blue .circle {
            stroke: #3c9ee5;
        }

        .percentage {
            fill: #666;
            font-family: sans-serif;
            font-size: 0.5em;
            text-anchor: middle;
        }
    </style>

    <!-- For Night mode -->



    <!-- javaScripts
    ================================================== -->
    <script   src="{{ asset('assets/admin/js/jquery-3.3.1.min.js') }}"></script>
	<script  src="{{ asset('assets/admin/js/framework.js') }}"></script>

	{{-- <script async src="{{ asset('assets/admin/js/mmenu.js') }}"></script> --}}
	{{-- <script async src="{{ asset('assets/admin/js/simplebar.js') }}"></script> --}}
	<script async src="{{ asset('assets/admin/js/main.js') }}"></script>
	<script async src="{{ asset('assets/admin/js/bootstrap-select.min.js') }}"></script>
	<script async src="{{ asset('assets/common/notify/noty.min.js') }}"></script>
    {{-- <script src="https://js.pusher.com/5.1/pusher.min.js"></script> --}}
    @yield('js')
    {{-- <script>
        Pusher.logToConsole = true;
        const pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', { 
          cluster: '{{env('PUSHER_APP_CLUSTER')}}',              
          forceTLS: true
        });
        const channel = pusher.subscribe('user-login');
        channel.bind('new-user', data => {

            UIkit.modal('').show();

        });
    </script> --}}

    {{-- @if (isset($isNewUser))


    @endif --}}
    @include('component.notify')
    @stack('handle_js')
    @if ( Session::has('remove_time'))
        <script>
            window.sessionStorage.removeItem('currentTime')
        </script>
    @endif

</body>
</html>

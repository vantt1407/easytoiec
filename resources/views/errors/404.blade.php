<!DOCTYPE html>
<html lang="en-us">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <style type="text/css">
        @charset "UTF-8";
        [ng\:cloak],
        [ng-cloak],
        [data-ng-cloak],
        [x-ng-cloak],
        .ng-cloak,
        .x-ng-cloak,
        .ng-hide:not(.ng-hide-animate) {
            display: none !important;
        }

        ng\:form {
            display: block;
        }

        .ng-animate-shim {
            visibility: hidden;
        }

        .ng-anchor {
            position: absolute;
        }
    </style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Oops, something lost</title>
    <meta name="description" content="Oops, looks like the page is lost. Start your website on the cheap.">
    <link href="{{ asset('assets/common/errors/style.css') }}" rel="stylesheet">
</head>

<body>

    <div class="error" id="error">
            <div class="container">
                <div class="content centered"><img style="width:500px;" src="{{ asset('assets/common/errors/errors.png') }}">
                    <h1>Không tìm thấy trang</h1>
                    <p style="font-size:22px;" class="sub-header text-block-narrow">
                    Quay lại <a href="/">Trang chủ </a>
                    </p>
                </div>
        </div>



</div>
</body>
</html>

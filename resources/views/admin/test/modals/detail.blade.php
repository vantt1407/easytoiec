<div id="modal-question-file-full" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h2 class="uk-modal-title">Danh sách câu hỏi</h2>
        <p>
            <table id="example" class="uk-table uk-table-divider" style="width:100%; margin:20px">
                <thead>
                    <tr>
                        <th> <input class="skin-square all" type="checkbox"> </th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
        
            </table>
        </p>
    </div>
</div>
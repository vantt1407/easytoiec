@extends('layouts.app_admin')
@section('head')
<title>Cập nhật đề thi</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#">Đề thi </a></li>
            <li>Cập nhật</li>
        </ul>
    </nav>
</div>
<div class="card rounded uk-width-1-1@m uk-first-column uk-grid">
    <div class="p-3">
        <h5 class="mb-0"> Đề thi </h5>
    </div>
    <form id="form-test-create" class="uk-width-1-1@m  uk-grid " method="post" action="{{ route('test.update',$test->test_id)}}">
        @csrf
        @method('put')
        <div class="uk-width-1-2@m ">
            <div class="uk-first-column">
                <div class="uk-form-group">
                    <label class="uk-form-label"> Tên đề thi</label>
                    <div class="uk-position-relative w-100">
                        <input value="{{$test->test_name}}" class="uk-input" type="text" name="test_name" placeholder="Tối đa 100 ký tự">
                    </div>
                </div>
            </div>
            <div class="uk-first-column">
                <label class="uk-form-label">Tạo ngẫu nhiên</label>
                <div class="uk-position-relative w-100">
                    <button id="random-all" type="button" class="btn btn-animated btn-primary btn-animated-x uk-first-column">
                        <span class="btn-inner--visible">Tạo toàn bộ</span>
                        <span class="btn-inner--hidden">
                            <i class="uil-arrow-random"></i>
                        </span>
                    </button>
                    <input type="hidden" name="part_1" value="{{json_encode($part1)}}" id="p1">
                    <input type="hidden" name="part_2" value="{{json_encode($part2)}}" id="p2">
                    <input type="hidden" name="part_3" value="{{json_encode($part3)}}" id="p3">
                    <input type="hidden" name="part_4" value="{{json_encode($part4)}}" id="p4">
                    <input type="hidden" name="part_5" value="{{json_encode($part5)}}" id="p5">
                    <input type="hidden" name="part_6" value="{{json_encode($part6)}}" id="p6">
                    <input type="hidden" name="part_7" value="{{json_encode($part7)}}" id="p7">
                </div>
            </div>
        </div>
        <div class="uk-width-1-2@m ">
            <div class="uk-grid-margin uk-first-column">
                <h5 class="uk-text-bold mb-2">Độ khó theo điểm </h5>
                <select class="uk-select" name="level_id">
                    <option disabled selected>Chọn mức điểm</option>
                    @foreach( $levels as $item)
                    <option value="{{ $item->level_id }}" {{ 
                         $test->level_id == $item->level_id ? 'selected' : '' }}>
                        {{ $item->level_name  }}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="uk-first-column uk-grid-margin">
                <div class="uk-form-group">
                    <label class="uk-form-label"> Thời gian thi</label>
                    <div class="uk-position-relative w-100">
                        <select class="uk-select" name="test_time" id="test_time">
                            <option selected disabled value="-1">Chọn thời gian thi</option>
                            <optgroup label="Đề thi đầy đủ">
                                <option value="120" {{  $test->test_time == 120 ? 'selected' : '' }}>120 phút</option>
                            </optgroup>
                            <optgroup label="Đề thi trung bình">
                                <option value="60" {{  $test->test_time == 60 ? 'selected' : '' }}>60 phút</option>
                            </optgroup>
                            <optgroup label="Đề thi ngắn">
                                <option value="10" {{  $test->test_time == 10 ? 'selected' : '' }}>10 phút</option>
                                <option value="5" {{  $test->test_time == 5 ? 'selected' : '' }}>5 phút</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1@m ">
            <div class="uk-first-column">
                <div class="uk-form-group">
                    <div class="uk-position-relative w-100">
                        <table class="uk-table uk-table-divider">
                            <thead class="align-content-center justify-content-between">
                                <tr>
                                    <th>Chọn ngẫu nhiên</th>
                                    <th>Phần thi</th>
                                    <th>Nhóm câu hỏi</th>
                                </tr>
                            </thead>
                            <tbody class="align-content-center justify-content-between">
                                @foreach($parts as $item)
                                <tr>
                                    <td>
                                        <div class="align-content-center justify-content-between">
                                            <button uk-tooltip="Chọn ngẫu nhiên" data-part-id="{{$item->part_id}}" style="margin-top: 0 !important;" type="button" class="btn-random btn btn-primary rounded-circle btn-icon-only uk-margin-small-top uk-first-column">
                                                <div class="load-show" uk-spinner style="display:none;position: absolute;top: 9px;right: 9px;"></div>
                                                <span class="btn-inner--icon">
                                                    <i class="uil-arrow-random"></i>
                                                </span>
                                            </button>
                                        </div>
                                    </td>
                                    <td>

                                        <div class="align-content-center justify-content-between">
                                            <img style="border-radius:50%" src="{{asset($item->part_avatar)}}" width="50" height="50">
                                            <span>{{$item->part_name}}</span>
                                        </div>

                                    </td>
                                    <td id="mapping-part-id-{{$item->part_id}}">
                                        <div class="wrapper-action">
                                            <span>Nhóm câu hỏi phần {{$item->part_id}}</span>
                                            <a href="javascript:void(0)" data-part='{{$item->part_id}}' class="btn btn-icon btn-hover btn-sm btn-circle btn-review" uk-tooltip="Xem chi tiết" title="" aria-expanded="false">
                                                <i class="uil-external-link-alt text-success"></i> </a>
                                            <a href="javascript:void(0)" class="btn-remove-random btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Xóa" title="" aria-expanded="false">
                                                <i class="uil-trash-alt text-danger"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="uk-grid-margin uk-first-column">
                <div class="uk-flex uk-flex-right p-4">
                    <button onclick=" window.location.assign('{{route('part.index')}}')" type="button" class="btn  btn-light mr-2">Hủy
                    </button>
                    <button class="btn btn-default" onclick="document.getElementById('form-part-create').submit()">
                        Lưu
                    </button>
                </div>

            </div>
        </div>

    </form>
</div>

@include('admin.test.modals.group')

@endsection
@section('js')
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>
<script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.uikit.min.js"></script>

<script>
    window.url_data = "{{ route('question-group.index') }}";
    window.create_url = "{{ route('question.create.group') }}";
    window.delete_url = "{{ route('question-group.destroy',0) }}"
    window.part_1 = @php echo json_encode($part1) @endphp;
    window.part_2 = @php echo json_encode($part2) @endphp;
    window.part_3 = @php echo json_encode($part3) @endphp;
    window.part_4 = @php echo json_encode($part4) @endphp;
    window.part_5 = @php echo json_encode($part5) @endphp;
    window.part_6 = @php echo json_encode($part6) @endphp;
    window.part_7 = @php echo json_encode($part7) @endphp;

</script>
<script src="{{ asset('assets/admin/js/test/list_group_random.js') }}"></script>
<script>
    $(function() {
        $.validator.setDefaults({
            ignore: []
        , });
        $("#form-test-create").validate({
            submitHandler: function(form) {
                if (!$('#test_time').val()) {
                    UIkit.modal.alert('Vui lòng chọn thời gian thi !');
                    return;
                }
                if ($('.btn-review').length < 7) {

                    UIkit.modal.alert('Đề thi chưa hoàn chỉnh , vui lòng thêm nhóm câu hỏi đầy đủ 7 phần');
                    return;
                }

                var data = $('#form-test-create').serializeArray().reduce(function(obj, item) {
                    obj[item.name] = item.value;
                    return obj;
                }, {});
                data._method = "put";
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": $(
                            'meta[name="csrf-token"]'
                        ).attr("content")
                    , }
                , });
                $.ajax({
                        url: "{{ route('test.update',$test->test_id)}}"
                        , type: "POST"
                        , data: data
                    })
                    .done(function(response) {
                        new Noty({
                            theme: "bootstrap-v4"
                            , type: "success"
                            , text: `<i class='uil-check'></i>${response}`
                            , timeout: 2000
                        , }).show();
                        setTimeout(function() {
                            window.location.assign("{{route('test.index')}}");
                        },2000)
                       
                    })
                    .fail(function(err) {

                        new Noty({
                            theme: "bootstrap-v4"
                            , type: "error"
                            , text: `<i class='uil-time'></i>${err}`
                            , timeout: 2000
                        , }).show();
                        setTimeout(function() {
                            window.location.assign("{{route('test.index')}}");
                        },2000)
                    });

            }
            , rules: {
                test_name: {
                    required: true
                    , maxlength: 100
                , }
                , level_id: {
                    required: true
                }

            }
            , messages: {

                level_id: {
                    required: "Mức độ điểm không được bỏ trống !"
                }
                , test_name: {
                    required: "Tên bài thi thử không được bỏ trống !"
                    , maxlength: "Tên bài thi thử tối đa 100 kí tự !"
                }

            , }
            , errorElement: "em"
            , errorPlacement: function(error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");
                error.insertAfter(element);
            }
            , highlight: function(element, errorClass, validClass) {
                $(element).addClass("has-error").removeClass("has-success");
            }
            , unhighlight: function(element, errorClass, validClass) {
                $(element).addClass("has-success").removeClass("has-error");
            }
        , });

    });

</script>
<script>
    $(function() {
        $('.btn-random').on('click', function() {
            console.log($('#test_time').val());

            if (!$('#test_time').val()) {

                UIkit.modal.alert('Vui lòng chọn thời gian thi !');
                return;

            }

            $(this).find('.load-show').show();
            let _this = this;
            let idPart = $(this).data('part-id');
            let time = $('#test_time').val();

            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $(
                        'meta[name="csrf-token"]'
                    ).attr("content")
                , }
            , });
            $.ajax({
                type: "POST"
                , url: "{{ route('test.random') }}"
                , data: {
                    'time': time
                    , 'part_id': idPart
                }
                , success: function(data) {
                    window['part_' + idPart] = data;

                    $('#p' + idPart).val(JSON.stringify(data));

                    setTimeout(function() {
                        $(_this).find('.load-show').hide();
                    }, 1000);
                }
                , error: function(err) {
                    setTimeout(function() {
                        $(_this).find('.load-show').hide();
                    }, 1000);
                }
            , });

            $mapingPart = $('#mapping-part-id-' + idPart);
            let tpl = ` <div class="wrapper-action">
                                <span>Nhóm câu hỏi phần ${idPart}</span>
                                <a href="javascript:void(0)" data-part='${idPart}' class="btn btn-icon btn-hover btn-sm btn-circle btn-review" uk-tooltip="Xem chi tiết" title="" aria-expanded="false">
                                    <i class="uil-external-link-alt text-success"></i> </a>
                                <a  href="javascript:void(0)" class="btn-remove-random btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Xóa" title="" aria-expanded="false">
                                    <i class="uil-trash-alt text-danger"></i>
                                </a>
                             </div>`;
            $mapingPart.html(tpl);
        });

        $('body').on('click', '.btn-remove-random', function() {
            $(this).closest('.wrapper-action').remove();
        });
        $('#random-all').on('click', function() {

            console.log($('#test_time').val());

            if (!$('#test_time').val()) {

                UIkit.modal.alert('Vui lòng chọn thời gian thi !');
                return;

            }

            let lg = $('.btn-random').length

            for (let i = 0; i < lg; i++) {
                $($('.btn-random')[i]).click();
            }
        })



        $('body').on('click', '.btn-review', function() {
            var idPart = $(this).data('part');
            window.data_part_select = window['part_' + idPart];
            window.data_part_select = (window.data_part_select).join(',');
            UIkit.modal('#modal-group-question-file-full').show();
        });
    });

</script>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.uikit.min.css">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .dt-buttons {
        float: right;
    }

    .skin-square {
        display: none !important;
    }

</style>
@endsection

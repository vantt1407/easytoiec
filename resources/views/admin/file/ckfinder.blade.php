
<button id="ckfinder-popup-1">xxxx</button>
<input type="text" id="ckfinder-input-1">
<script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
<script>
    CKFinder.config({
        connectorPath: '/ckfinder/connector'
    });

</script>

<script>
    var button1 = document.getElementById('ckfinder-popup-1');
    var button2 = document.getElementById('ckfinder-popup-2');

    button1.onclick = function() {
        selectFileWithCKFinder('ckfinder-input-1');
    };
    button2.onclick = function() {
        selectFileWithCKFinder('ckfinder-input-2');
    };

    function selectFileWithCKFinder(elementId) {
        CKFinder.modal({
            chooseFiles: true
            , width: 800
            , height: 600
            , onInit: function(finder) {
                finder.on('files:choose', function(evt) {
                    var file = evt.data.files.first();
                    var output = document.getElementById(elementId);
                    output.value = file.getUrl();
                });

                finder.on('file:choose:resizedImage', function(evt) {
                    var output = document.getElementById(elementId);
                    output.value = evt.data.resizedUrl;
                });
            }
        });
    }

</script>

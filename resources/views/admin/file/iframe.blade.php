@extends('layouts.app_admin')

@section('head')
<title>Quản lý file</title>
@endsection

@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#"> Quản lý file </a></li>
        </ul>
    </nav>
</div>
<div style="margin-bottom:20px">
<iframe src="/filemanager?field_name=mceu_52-inp&amp;type=image" tabindex="-1" style="width:100%;height:100vh;border: 1px solid #3e416d">
</iframe>

</div>
@endsection

@section('js')
@endsection

@section('css')

@endsection

@extends('layouts.app_admin')
@section('head')
<title>Tạo mới các phần bài giảng</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#">Phần bài giảng </a></li>
            <li>Tạo mới</li>
        </ul>
    </nav>
</div>
<div class="card rounded uk-width-1-1@m uk-first-column uk-grid">

    <div class="p-3">
        <h5 class="mb-0">Phần Bài giảng </h5>
    </div>
    <form id="form-lesson-part-create" class="uk-width-1-1@m  uk-grid " method="post" action="{{ route('lesson-part.store')}}">
        @csrf
        <input name="lesson_id" type="hidden" value="{{$lesson_id}}">
        <div class="uk-width-1-1@m ">
            <div class="uk-first-column">
                <div class="uk-form-group">
                    <label class="uk-form-label"> Tên bài giảng</label>
                    <div class="uk-position-relative w-100">
                        <input class="uk-input" type="text" name="lesson_part_title" placeholder="Tối đa 100 ký tự">
                        @error('topic_name')
                        <span class="text-right text-danger text-error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="uk-first-column">
                <div class="uk-form-group">
                    <label class="uk-form-label">Video bài giảng</label>
                    <div class="uk-position-relative w-100" uk-form-custom>
                     <input  type="hidden" id="lesson_video" name="lesson_video">
                        <button data-path-name="path-name" data-audio="image-preview" data-input="lesson_video" id="choice-photo" class="uk-button uk-button-default" type="button" tabindex="-1">
                            <i class="uil-image-upload text-danger"></i>
                            Chọn video
                        </button>
                        <span id="path-name"></span>

                    </div>
                </div>
            </div>
            <div class="uk-grid-margin uk-first-column">
                <div class="uk-form-group">
                    <label class="uk-form-label">Xem trước video</label>
                    <div class="uk-position-relative w-100">
                        <div class="wrapper-img" id="image-preview">
        
                            <video class="image-preview"  controls preload="auto" width="100%" height="100%" type="video/mp4" >
                                <source src="#" />
                
                                <p class="vjs-no-js">
                                    To view this video please enable JavaScript, and consider upgrading to a
                                    web browser that
                                    <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                </p>
                            </video>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="uk-width-1-1@m ">
            <div class="uk-grid-margin uk-first-column">
                <div class="uk-form-group">
                    <label class="uk-form-label">Nội dung</label>
                    <div class="uk-position-relative w-100">
                        <textarea id="editer" class="uk-textarea" rows="10" name="lesson_part_content"></textarea>
                        @error('topic_description')
                        <span class="text-right text-danger text-error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="uk-grid-margin uk-first-column">
                <div class="uk-form-group">
                    <label class="uk-form-label">Câu hỏi luyện tập</label>
                    <div class="uk-position-relative">
                        <div class="uk-gird uk-width-1-1@m" uk-gird>


                            <article class="uk-card uk-card-default uk-card-small uk-card-body uk-width-auto@m uk-first-column">
                                <button uk-toggle="target: #modal-question-file-full" style="margin-top:20px" class="btn btn-default mb-4" type="button">Chọn câu hỏi</button>
                                <h3 class="uk-card-title">Câu hỏi</h3>
                                <ul id="list-question" class="uk-list-divider uk-list-large uk-accordion" uk-accordion>
                                   
                                </ul>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-grid-margin uk-first-column">
                <div class="uk-flex uk-flex-right p-4">
                    <button onclick=" window.location.assign('{{route('lesson.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                    <button class="btn btn-default" type="submit" >Lưu</button>
                </div>

            </div>
        </div>
    </form>
</div>
@include('admin.lesson.lesson_part_question.question_modal')
@endsection
@section('js')
<script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
<script src="{{ asset('assets/admin/js/lesson/lesson_part/create.js') }}"></script>
<script src="{{ asset('assets/admin/js/lesson/random.question.js?v=1') }}"></script>
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>
<script src="{{ asset('assets/common/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/common/tinymce/init.js') }}"></script>
<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js?v=1') }}"></script>


<script src=" https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.uikit.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>

<script>
    $(function() {
        $('#choice-photo').filemanager();
    })
</script>

<script>
    window.random_url = "{{ route('lesson.part.question.random') }}"
    window.url_data = "{{ route('question.list.l') }}";
</script>

<script  src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script  src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>


<script src="{{ asset('assets/admin/js/lesson/lesson_part/question.js') }}"></script>

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
.skin-square {
display: none !important;
}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.uikit.min.css">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .dt-buttons {
        float: right;
    }

    .skin-square {
        display: none !important;
    }
    .has-error {
            border-color: #f0506e !important;
        }

</style>
@endsection

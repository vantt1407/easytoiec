@extends('layouts.app_admin')
@section('head')
    <title>Tạo mới bài giảng</title>
@endsection
@section('content')
    <div class="d-flex">
        <nav id="breadcrumbs" class="mb-3">
            <ul>
                <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
                <li><a href="#">Bài giảng </a></li>
                <li>Tạo mới</li>
            </ul>
        </nav>
    </div>
    <div class="card rounded uk-width-1-1@m uk-first-column uk-grid">

        <div class="p-3">
            <h5 class="mb-0"> Bài giảng </h5>
        </div>
        <form id="form-lesson-create" class="uk-width-1-1@m  uk-grid " method="post"
              action="{{ route('lesson.store')}}">
            @csrf
            <div class="uk-width-1-2@m " >

                <div class="uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Ảnh đại diện</label>
                        <div class="uk-position-relative w-100" uk-form-custom>
                        <input type="hidden" id="lesson_avatar" name="lesson_avatar">
                            <button data-path-name="path-name" data-preview="image-preview"
                                    data-input="lesson_avatar" id="choice-photo" class="uk-button uk-button-default"
                                    type="button" tabindex="-1">
                                <i class="uil-image-upload text-danger"></i>
                                Chọn ảnh
                            </button>
                            <span id="path-name"></span>

                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Xem trước ảnh đại diện</label>
                        <div class="uk-position-relative w-100">
                            <div class="wrapper-img" id="image-preview">
                                <img class="image-preview" src="{{ asset('assets/nophoto.png') }}">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="uk-width-1-2@m ">
                <div class="uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label"> Tên bài giảng</label>
                        <div class="uk-position-relative w-100">
                        <input class="uk-input" type="text" name="lesson_title" placeholder="Tối đa 100 ký tự">
                            @error('topic_name')
                            <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <h5 class="uk-text-bold mb-2">Mức điểm </h5>
                    <select class="uk-select" name="level_id">
                        <option disabled selected>Chọn mức điểm</option>
                        @foreach( $levels as $item)
                            <option value="{{ $item->level_id }}">
                                {{ $item->level_name  }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Mô tả</label>
                        <div class="uk-position-relative w-100">
                                <textarea class="uk-textarea" rows="6" name="lesson_description"
                                          placeholder="Tối đa 1500 ký tự"></textarea>
                            @error('topic_description')
                            <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-1@m ">
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Kết qủa đạt được</label>
                        <div class="uk-position-relative w-100">
                         <textarea id="editer" class="uk-textarea" rows="10" name="lesson_long_description"
                                          placeholder="Tối đa 1500 ký tự"></textarea>
                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-flex uk-flex-right p-4">
                        <button onclick=" window.location.assign('{{route('lesson.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                        <button class="btn btn-default" type="submit" >Lưu</button>
                    </div>

                </div>
            </div>
        </form>
    </div>
@endsection
@section('js')

<script  src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js?v=1') }}"></script>
<script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
<script  src="{{ asset('assets/admin/js/lesson/create.js?v=1') }}"></script>
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="{{ asset('assets/common/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/common/tinymce/init.js') }}"></script>
@endsection

@section('css')

<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .skin-square {
    display: none !important;
}
   
    .has-error {
            border-color: #f0506e !important;
        }
</style>


@endsection

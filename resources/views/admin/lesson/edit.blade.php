@extends('layouts.app_admin')
@section('head')
    <title>Cập nhật bài giảng</title>
@endsection
@section('content')
    <div class="d-flex">
        <nav id="breadcrumbs" class="mb-3">
            <ul>
                <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
                <li><a href="#">Bài giảng </a></li>
                <li>Cập nhật</li>
            </ul>
        </nav>
    </div>
    <div class="card rounded uk-width-1-1@m uk-first-column uk-grid">

        <div class="p-3">
            <h5 class="mb-0"> Bài giảng </h5>
        </div>
        <form id="form-lesson-update" class="uk-width-1-1@m  uk-grid " method="post"
              action="{{ route('lesson.update',$lesson->lesson_id)}}">
            @csrf
            @method('put')
            <div class="uk-width-1-2@m " >

                <div class="uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Ảnh đại diện</label>
                        <div class="uk-position-relative w-100" uk-form-custom>
                        <input type="hidden" id="lesson_avatar" name="lesson_avatar" value="{{$lesson->lesson_avatar}}">
                            <button data-path-name="path-name" data-preview="image-preview"
                                    data-input="lesson_avatar" id="choice-photo" class="uk-button uk-button-default"
                                    type="button" tabindex="-1">
                                <i class="uil-image-upload text-danger"></i>
                                Chọn ảnh
                            </button>
                            <span id="path-name">{{$lesson->lesson_avatar}}</span>

                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Xem trước ảnh đại diện</label>
                        <div class="uk-position-relative w-100">
                            <div class="wrapper-img" id="image-preview">
                                <img class="image-preview" src="{{ asset($lesson->lesson_avatar) }}">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="uk-width-1-2@m ">
                <div class="uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label"> Tên bài giảng</label>
                        <div class="uk-position-relative w-100">
                        <input  value="{{$lesson->lesson_title}}" class="uk-input" type="text" name="lesson_title" placeholder="Tối đa 100 ký tự">
                            @error('topic_name')
                            <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <h5 class="uk-text-bold mb-2">Mức điểm </h5>
                    <select class="uk-select" name="level_id">
                        <option disabled selected>Chọn mức điểm</option>
                        @foreach( $levels as $item)
                            <option {{ $item->level_id ==  $lesson->level_id ? 'selected' : '' }} value="{{ $item->level_id }}">
                                {{ $item->level_name  }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Mô tả</label>
                        <div class="uk-position-relative w-100">
                                <textarea class="uk-textarea" rows="6" name="lesson_description"
                                          placeholder="Tối đa 1500 ký tự">{{$lesson->lesson_description}}</textarea>
                            @error('topic_description')
                            <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="uk-width-1-1@m ">
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Kết qủa đạt được</label>
                        <div class="uk-position-relative w-100">
                         <textarea id="editer" class="uk-textarea" rows="10" name="lesson_long_description"
                        placeholder="Tối đa 1500 ký tự">{{$lesson->lesson_long_description}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-flex uk-flex-right p-4">
                        <button onclick=" window.location.assign('{{route('lesson.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                        <button class="btn btn-default" type="submit" >Lưu</button>
                    </div>

                </div>
            </div>
        </form>

      <hr />
      <div class="p-3">
        <h5 class="mb-0">Danh sách các phần </h5>
    </div>
            <table id="example" class="uk-table uk-table-divider" style="width:100% !important;">
                <thead>
                    <tr>
                        <th>  <input class="skin-square all" type="checkbox"> </th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
    
            </table>
    
    
    </div>
@endsection
@section('js')

<script src=" https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.uikit.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="{{ asset('assets/common/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/common/tinymce/init.js') }}"></script>
<script  src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js?v=1') }}"></script>
<script>
    window.url_data = "{{ route('lesson.part.list') }}"+"?lesson={{$lesson->lesson_id}}";
    window.create_url = "{{ route('lesson-part.create') }}"+"?lesson={{$lesson->lesson_id}}";
</script>
<script  src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script  src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>
<script src="{{ asset('assets/admin/js/lesson/list_part.js?v=13') }}"></script>

@endsection

@section('css')

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.uikit.min.css">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .dt-buttons {
        float: right;
    }
    .skin-square {
        display: none !important;
    }
    .has-error {
            border-color: #f0506e !important;
        }
</style>

@endsection

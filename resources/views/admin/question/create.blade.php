@extends('layouts.app_admin')
@section('head')
<title>Tạo mới câu hỏi</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#"> Câu hỏi </a></li>
            <li> Tạo mới câu hỏi</li>
        </ul>
    </nav>
</div>

<form action="{{ route('question.store') }}" method="post" id="form-create-single">
    @csrf
    <div uk-grid="" class="uk-grid">

        <div class="uk-width-1-4@m uk-flex-last@m">
            <nav class="responsive-tab style-3 setting-menu card uk-sticky"
                uk-sticky="top:30 ; offset:100; media:@m ;bottom:true; animation: uk-animation-slide-top" style="">
                <div class="p-3">
                    <h5 class="mb-0">Phần thi</h5>

                </div>
                <hr class="m-0">
                <ul>
                    @foreach ($partList as $item)
                    <li class="uk-active">
                        <a href="#">
                            <input value="{{$item->part_id}}" class="skin-square" type="radio" name="part_id">
                            {{$item->part_name}}
                        </a>
                    </li>
                    @endforeach

                </ul>
            </nav>
            <div class="uk-sticky-placeholder" style="height: 522px; margin: 0px 0px 20px;" hidden=""></div>

        </div>
        <div class="uk-width-3-4@m uk-first-column">

            <div class="card rounded">
                <div class="p-3">
                    <h5 class="mb-0"> Câu hỏi </h5>
                </div>
                <hr class="m-0">
                <ul uk-tab uk-switcher="animation: uk-animation-slide-left-medium, uk-animation-slide-right-medium">
                    <li class="uk-active"><a href="#">Thêm mới</a></li>
                    <li>
                        <a href="#">Tải lên </a>
                    </li>
                </ul>

                <ul class="uk-switcher uk-margin">
                    <li>
                        <div class=" uk-child-width-2-3@m uk-grid-small p-4 uk-grid" uk-grid="">
                            <div class="uk-first-column">
                                <input  style="display:none;" {{  isset($createGroup) ? 'checked': ''  }} id="change-type-add-question"  class="skin-square" type="checkbox" > Nhóm câu hỏi test
                            </div>
                            @if( isset($createGroup) )
                                @include('admin.question.question.group')
                            @else
                                @include('admin.question.tab.create')
                            @endif
                        </div>

                    </li>
                    <li>
                        @include('admin.question.tab.upload')
                    </li>
                </ul>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')

<script  src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js?v=1') }}"></script>
<script  src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
<script>
    window.url_create = "{{route('question.create')}}";
    window.url_create_group = "{{route('question.create.group')}}";
    $(function () {
        window.initIcheck = function() {
            var iBoxes = $('.skin-square');
            $.each(iBoxes,function (index,item) {
                let $dom = $(item);
                if($dom.attr('type')==='radio') {
                    $dom.iCheck({
                        radioClass: 'iradio_square-purple'
                        , increaseArea: '20%'
                    });
                } else {
                    $dom.iCheck({
                        checkboxClass: 'icheckbox_square-purple',
                        increaseArea: '20%'
                    });
                }
            });
            $('#change-type-add-question').on('ifChecked ifUnchecked',function () {
                if(this.checked) {
                    window.location.assign(window.url_create_group);
                } else {
                    window.location.assign(window.url_create);
                }
            });
        }
        window.initIcheck();
    });
</script>
@if( isset($createGroup) )
    @if (isset($index))
        @include('admin.question.question.modal_edit')
        <script>
            $(function() {
                UIkit.modal('#popup-create-question',{stack: true}).show();
            })
        </script>
    @else    
         @include('admin.question.question.modal_create')
    @endif
    <script src="{{asset('assets/admin/js/question/create_group.js?v=1')}}"></script>
    <script src="{{ asset('assets/common/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/common/tinymce/init.js') }}"></script>
    <script >
        $(function() {
            $('#choice-audio').filemanager('image');
            $('#choice-photo').filemanager('image');
           
        });
    </script>
   
@else
    <script src="{{asset('assets/admin/js/question/createv2.js?v=1')}}"></script>
 @endif
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .audio-preview{
        margin-top: 40%;
    transform: translateY(-50%);
    }
</style>
@endsection




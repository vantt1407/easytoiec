<div class=" uk-child-width-2-3@m uk-grid-small p-4 uk-grid" uk-grid="">

    <div class="uk-first-column  js-upload" uk-form-custom>
        <h5 class="uk-text-bold mb-2">Tải lên file excel hoặc csv</h5>
     
        <button  id="choice-file" class="uk-button uk-button-default" type="button" tabindex="-1">
            <i class="uil-file-upload text-danger"></i>
            <span id="uploading"> Chọn file</span>
            <input type="file" id="upload_file" name="upload_file">
            <span id="progress-value"></span>
        </button>
        <span id="path-name"></span>
    
            <progress   id="js-progressbar" class="uk-progress" value="10" max="100">
            
            </progress>
      
        @error('topic_avatar')
        <span class="text-right text-danger text-error" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        
    </div>
    
</div>
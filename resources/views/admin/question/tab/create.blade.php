<div id="sigle-input">
    <div class="uk-first-column">
        <h5 class="uk-text-bold mb-2">Tên câu hỏi</h5>
        <input name="question" type="text" class="uk-input" placeholder="Tối đa 100 ký tự">
    </div>
    <div class="uk-first-column">
        <h5 class="uk-text-bold mb-2">Số điểm</h5>
        <input name="question_scores" type="number" class="uk-input">
    </div>
    <div uk-gird class="uk-grid">
        <div class="uk-width-1-2@m uk-first-column">
            <div class="uk-first-column">
                <h5 class="uk-text-bold mb-2">Ảnh mô tả </h5>
                <input type="hidden" id="question_image" name="question_image">
                <button data-path-name="path-name-photo" data-preview="image-preview" data-input="question_image"
                    id="question-image" class="uk-button uk-button-default" type="button" tabindex="-1">
                    <i class="uil-image-upload text-danger"></i>
                    Chọn ảnh</button>
                <span id="path-name-photo"></span>
            </div>
            <div class="uk-first-column">
                <div class="wrapper-img" id="image-preview">
                    <img class="image-preview" src="{{ asset('assets/nophoto.png') }}">
                </div>
            </div>

        </div>
        <div class="uk-width-1-2@m uk-first-column">
            <div class="uk-first-column">
                <h5 class="uk-text-bold mb-2">Âm thanh mô tả </h5>
                <input type="hidden" id="question_audio" name="question_audio">
                <button data-path-name="path-name-audio" data-audio="audio-preview" data-input="question_audio"
                    id="question-audio" class="uk-button uk-button-default" type="button" tabindex="-1">
                    <i class="uil-file-upload text-danger"></i>
                    Chọn âm</button>
                <span id="path-name-audio"></span>
            </div>
            <div class="uk-first-column">
                <div class="wrapper-img">
                    <div class="image-preview" style="text-align: center">
                        <audio controls class="audio-preview">
                            <source src="#">
                        </audio>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="uk-first-column">
        <h5 class="uk-text-bold mb-2">Đáp án</h5>
        <div id="no-answer" style="color:#f0506e ">
            <i  class="error help-block">Vui lòng thêm câu trả lời !</i>
        </div>
        <button id="btn-add-question" type="button" class="btn btn-animated btn-success btn-animated-x ">
            <span class="btn-inner--visible">Thêm đáp án</span>
            <span class="btn-inner--hidden">
                <i class="uil-plus"></i>
            </span>
        </button>

        <div id="question-container" uk-sortable="group:sortable-group">

        </div>
    </div>
    <div class="uk-flex uk-flex-right p-4">
        <button onclick="window.location.assign(' {{ route('question.index') }} ')"
            class="btn btn-outline-danger uk-first-column mr-2">
            Huỷ
        </button>

        <button class="btn btn-outline-success btn-create-single-question" type="submit">
            Tạo mới</button>
    </div>
</div>

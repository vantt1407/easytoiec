<div id="popup-create-question" uk-modal class="uk-modal-container"  bg-close="false">
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title"></h2>
        <div id="sigle-input-modal">
            <form id="form-create-modal" method="post" action="{{ route('question.update.raw',$index) }}">
                @csrf
            <div class="uk-first-column">
                <h5 class="uk-text-bold mb-2">Tên câu hỏi</h5>
            <input value="{{$q['modal_question']}}"  name="modal_question" type="text" class="uk-input" placeholder="Tối đa 200 ký tự">
            </div>
            <div class="uk-first-column">
                <h5 class="uk-text-bold mb-2">Số điểm</h5>
                <input value="{{$q['modal_question_scores']}}" id="modal_question_scores"  name="modal_question_scores" type="number" class="uk-input">
            </div>
            <div uk-gird class="uk-grid">
                <div class="uk-width-1-2@m uk-first-column">
                    <div class="uk-first-column">
                        <h5 class="uk-text-bold mb-2">Ảnh mô tả </h5>
                        <input value="{{$q['modal_question_image']}}" type="hidden" id="modal_question_photo" name="modal_question_image">
                        <button data-path-name="modal-path-name" data-preview="modal-image-preview" data-input="modal_question_photo"
                                id="modal-choice-photo" class="uk-button uk-button-default" type="button" tabindex="-1">
                            <i class="uil-image-upload text-danger"></i>
                            Chọn ảnh</button>
                        <span id="modal-path-name">{{$q['modal_question_image']}}</span>
                    </div>
                    <div class="uk-first-column">
                        <div class="wrapper-img" id="modal-image-preview">
                            <img class="image-preview" src="{{ asset($q['modal_question_image']) }}">
                        </div>
                    </div>

                </div>
                <div class="uk-width-1-2@m uk-first-column">
                    <div class="uk-first-column">
                        <h5 class="uk-text-bold mb-2">Âm thanh mô tả </h5>
                    <input value="{{$q['modal_question_audio']}}" type="hidden" id="modal_question_audio" name="modal_question_audio">
                        <button data-path-name="modal-path-name-audio" data-audio="modal-audio-preview" data-input="modal_question_audio"
                                id="modal-choice-audio" class="uk-button uk-button-default" type="button" tabindex="-1">
                            <i class="uil-file-upload text-danger"></i>
                            Chọn âm thanh</button>
                        <span id="modal-path-name-audio">{{$q['modal_question_audio']}}</span>
                    </div>
                    <div class="uk-first-column">
                        <div class="wrapper-img">
                            <div class="image-preview" style="text-align: center">
                                <audio style="margin-top: 25%" controls class="modal-audio-preview">
                                    <source src="{{$q['modal_question_audio']}}">
                                </audio>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-first-column">
                <h5 class="uk-text-bold mb-2">Đáp án</h5>
                <div id="no-answer" style="color:#f0506e ">
                    <i  class="error help-block">Vui lòng thêm câu trả lời !</i>
                </div>
                <button id="btn-add-answer-modal" type="button" class="btn btn-animated btn-success btn-animated-x ">
                    <span class="btn-inner--visible">Thêm đáp án</span>
                    <span class="btn-inner--hidden">
                <i class="uil-plus"></i>
            </span>
                </button>
                <div id="question-container-modal" uk-sortable="group:sortable-group">
                    @foreach ($q['answer_option_modal'] as $a)
                        <div class="uk-margin wrapper-question">
                            <div class="uk-card uk-card-default uk-card-body uk-card-small">
                            <input value="{{$a}}" name="answer_option_modal[]" type="text" class="uk-input uk-grid-small answer-option-modal"  placeholder="Nhập đáp án">
                                <div class="wp" style="float:right">
                                    <input {{$a == $q['is_correct_modal'] ? " value=".$a." checked " :" value=".$a." "}}  class="skin-square is-correct-modal" type="radio" name="is_correct_modal"> Đáp án đúng
                                    <button type="button" class="btn btn-icon btn-hover btn-circle btn-delete-question" uk-tooltip="Xóa" title="" aria-expanded="false">
                                        <i class="uil-trash-alt text-danger"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if ($a == $q['is_correct_modal'])
                        <script>
                            $(function() {
                                $('input[value={{$a}}]').iCheck('check'); 
                            })
                        </script>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="uk-flex uk-flex-right p-4">
                <button onclick="window.location.assign('{{ route('question.create.group') }}')" class="btn btn-outline-danger uk-first-column mr-2 uk-modal-close">
                    Huỷ
                </button>
                <button id="btn-add-question-modal" class="btn btn-outline-success" type="submit">
                   Cập nhật
                </button>
            </div>
            </form>
        </div>
    </div>
</div>


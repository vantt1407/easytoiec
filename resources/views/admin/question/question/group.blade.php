<div id="group-input">
    <div class="uk-first-column  mb-2">
        <h5 class="uk-text-bold mb-2">Nhóm câu hỏi</h5>
        <textarea required id="editer" name="question_group_question" type="text" class="uk-textarea mb-2" placeholder="Tối đa 500 ký tự"></textarea>
    </div>
    <div class="uk-first-column mb-2">
        <h5 class="uk-text-bold mb-2">Hình ảnh</h5>
        <input value="" type="hidden" id="question_group_image" name="question_group_image">
        <button data-path-name="path-name" data-preview="image-preview" data-input="question_group_image" id="choice-photo" class="uk-button uk-button-default" type="button" tabindex="-1">
            <i class="uil-file-upload text-danger"></i>
            Chọn hình ảnh
        </button>
        <span id="path-name"></span>
        <div class="uk-first-column">
            <div class="wrapper-img">
                <div id="image-preview" class="image-preview" style="text-align: center">
                    <img class="image-preview" src="{{ asset('assets/nophoto.png') }}">
                </div>

            </div>
        </div>
    </div>
    <div class="uk-first-column mb-2">
        <h5 class="uk-text-bold mb-2">Âm thanh</h5>
        <input required type="hidden" id="question_group_audio" name="question_group_audio">
        <button data-path-name="path-name" data-audio="audio-preview" data-input="question_group_audio"
                id="choice-audio" class="uk-button uk-button-default" type="button" tabindex="-1">
            <i class="uil-file-upload text-danger"></i>
            Chọn âm thanh
        </button>
        <span id="path-name"></span>
        <div class="uk-first-column">
            <div class="wrapper-img">
                <div class="image-preview" style="text-align: center">
                    <audio style="margin-top: 25%" controls class="audio-preview">
                        <source src="#">
                    </audio>
                </div>

            </div>
        </div>
    </div>
    <div class="uk-first-column mb-2">
        <h5 class="uk-text-bold mb-2">Danh sách câu hỏi</h5>
        <table class="uk-table uk-table-divider">
            <thead class="text-center">
            <tr>
                <th>Câu hỏi</th>
                <th>
                    <a id="create-new" href="void:javascript(0)" class="btn btn-sm btn-success" uk-tooltip="Thêm" title="Thêm">
                        <i class="uil-file-plus "></i>
                    </a>
                </th>

            </tr>
            </thead>
            <tbody class="text-center">
                @foreach ($listRawQuestion as $key => $item)
                    <tr>
                    <td>{{$item['modal_question']}}</td>
                        <td>
                            <a href="{{ route('question.edit.raw', $key) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa" title="Chỉnh sửa">
                                <i class="uil-pen text-warning"></i>
                            </a>
                            <a href="{{ route('question.delete.raw', $key) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Xóa" title="Xóa">
                                <i class="uil-trash-alt text-danger"></i>
                            </a>
        
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @if (empty($listRawQuestion) )
        Không có dữ liệu !
        @endif
    </div>
    <div class="uk-flex uk-flex-right p-4">
        <button onclick="window.location.assign(' {{ route('question.index') }} ')"
            class="btn btn-outline-danger uk-first-column mr-2">
            Huỷ
        </button>

        <button class="btn btn-outline-success btn-create-single-question" type="submit">
            Tạo mới</button>
    </div>
</div>

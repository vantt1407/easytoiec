@extends('layouts.app_admin')
@section('head')
<title>Tạo mới bài luyện tập</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#">Luyện tập </a></li>
            <li>Tạo mới</li>
        </ul>
    </nav>
</div>
<div class="card rounded uk-width-1-1@m uk-first-column uk-grid">

    <div class="p-3">
        <h5 class="mb-0">Luyện tập</h5>
    </div>
    <form id="form-practice-create" class="uk-width-1-1@m  uk-grid " method="post" action="{{ route('practice.store')}}">
        @csrf
        
        <div class="uk-width-1-1@m ">
            <div class="uk-first-column">
                <div class="uk-form-group">
                    <label class="uk-form-label"> Tên bài luyện tập</label>
                    <div class="uk-position-relative w-100">
                        <input class="uk-input" type="text" name="practice_name" placeholder="Tối đa 100 ký tự">
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1@m ">
            <div class="uk-grid-margin uk-first-column">
                <h5 class="uk-text-bold mb-2">Độ khó theo điểm </h5>
                <select class="uk-select" name="level_id">
                    <option disabled selected>Chọn mức điểm</option>
                    @foreach( $levels as $item)
                        <option value="{{ $item->level_id }}">
                            {{ $item->level_name  }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="uk-grid-margin uk-first-column">
                <div class="uk-form-group">
                    <label class="uk-form-label">Câu hỏi luyện tập</label>
                    <div class="uk-position-relative">
                        <div class="uk-gird uk-width-1-1@m" uk-gird>
                            <article class="uk-card uk-card-default uk-card-small uk-card-body uk-width-auto@m uk-first-column">
                                <button uk-toggle="target: #modal-question-file-full" style="margin-top:20px" class="btn btn-default mb-4" type="button">Chọn câu hỏi</button>
                                <button uk-toggle="target: #modal-question-file-full" style="margin-top:20px" class="btn btn-default mb-4" type="button">Chọn nhóm câu hỏi</button>
                                <h3 class="uk-card-title">Câu hỏi</h3>
                                <ul id="list-question" class="uk-list-divider uk-list-large uk-accordion" uk-accordion>
                                </ul>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-grid-margin uk-first-column">
                <div class="uk-flex uk-flex-right p-4">
                    <button onclick=" window.location.assign('{{route('practice.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                    <button class="btn btn-default" type="submit" >Lưu</button>
                </div>

            </div>
        </div>
    </form>
</div>
@include('admin.lesson.lesson_part_question.question_modal')
@endsection
@section('js')
<script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js?v=1') }}"></script>
<script src="{{ asset('assets/admin/js/practice/create.js') }}"></script>
<script src="{{ asset('assets/admin/js/lesson/random.question.js?v=1') }}"></script>
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>


<script src=" https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.uikit.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>



<script>
    window.random_url = "{{ route('lesson.part.question.random') }}"
    window.url_data = "{{ route('question.list.p') }}";
</script>

<script  src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script  src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>


<script src="{{ asset('assets/admin/js/practice/question.js') }}"></script>
<script>
    $(function () {

        $('body').on('click','.delete-question', function() {
            $(this).closest('li').remove();
        });
    });
</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
.skin-square {
display: none !important;
}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.uikit.min.css">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .dt-buttons {
        float: right;
    }

    .skin-square {
        display: none !important;
    }
    .has-error {
            border-color: #f0506e !important;
        }

</style>
@endsection

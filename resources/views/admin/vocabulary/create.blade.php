@extends('layouts.app_admin')
@section('head')
    <title>Tạo mới từ vựng</title>
@endsection
@section('content')
    <div class="d-flex">
        <nav id="breadcrumbs" class="mb-3">
            <ul>
                <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
                <li>
                    <a href="{{ route('vocabulary.index'                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ) }}">
                        Từ vựng </a>
                </li>
                <li> Tạo mới từ vựng</li>
            </ul>
        </nav>
    </div>

    <form novalidate action="{{ route('vocabulary.store') }}" method="post" id="form-create-vocabulary">
        @csrf
        <div uk-grid="" class="uk-grid">
            <div class="uk-width-1-4@m uk-flex-last@m">
                <nav class="responsive-tab style-3 setting-menu card uk-sticky"
                     uk-sticky="top:30 ; offset:100; media:@m ;bottom:true; animation: uk-animation-slide-top" style="">
                    <div class="p-3">
                        <h5 class="mb-0"> Chủ đề </h5>
                    </div>
                    <hr class="m-0">
                    <ul>
                        @foreach ($categories as $item)
                            <li class="uk-active">
                                <a href="#">
                                    <input value="{{$item->topic_id}}" class="skin-square" type="radio" name="topic_id">
                                    {{$item->topic_name}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </nav>
                <div class="uk-sticky-placeholder" style="height: 522px; margin: 0px 0px 20px;" hidden=""></div>
            </div>
            <div class="uk-width-3-4@m uk-first-column">
                <div class="card rounded">
                    <div class="p-3">
                        <h5 class="mb-0">Tạo mới từ vựng</h5>
                    </div>
                    <hr class="m-0">
                    <ul id="csw" uk-tab uk-switcher="animation: uk-animation-slide-left-medium, uk-animation-slide-right-medium">
                        <li id="tv" class="uk-active"><a href="#"> Từ vựng</a></li>
                        <li id="tn">
                            <a href="#">Từ trái nghĩa </a>
                        </li>
                    </ul>
                    <ul class="uk-switcher uk-margin">
                        <li class="tab-create">
                            <div class=" uk-child-width-2-3@m uk-grid-small p-4 uk-grid" uk-grid="">
                                @include('admin.vocabulary.form.tab1')
                            </div>
                        </li>
                        <li class="tab-create">
                            <div class=" uk-child-width-2-3@m uk-grid-small p-4 uk-grid" uk-grid="">
                                @include('admin.vocabulary.form.tab2')
                            </div>
                        </li>
                    </ul>
                    <div class="uk-flex uk-flex-right p-4">
                        <button onclick="window.location.assign('{{route('vocabulary.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                        <button class="btn btn-default">Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js?v=3') }}"></script>
    <script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
    <script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
    <script src="{{asset('assets/admin/js/vocabulary/create.js')}}"></script>
    <script>
        $(function () {
            var radioBoxes = $('.skin-square');
            radioBoxes.iCheck({
                radioClass: 'iradio_square-purple'
                , increaseArea: '20%'
            });
            $('#choice-photo').filemanager('image');
            $('#choice-audio').filemanager('image');
            $('#choice-photo-1').filemanager('image');
            $('#choice-audio-1').filemanager('image');

        })

    </script>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
    <style>
        .audio-preview,.audio-preview-1{
            margin-top: 40%;
            transform: translateY(-50%);
        }
        .has-error {
            border-color: #f0506e !important;
        }
    </style>
@endsection

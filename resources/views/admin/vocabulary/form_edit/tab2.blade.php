<div class="uk-child-width-1-2@s uk-grid-small p-4 uk-grid" uk-grid="">
    <div class="uk-first-column">
        <h5 class="uk-text-bold mb-2"> Từ vựng </h5>
        <input value="{{ $av ? $av->vocabulary_word :'' }}" name="_word" type="text" class="uk-input" placeholder="Tối đa 100 kí tự">
    </div>
    <div>
        <h5 class="uk-text-bold mb-2"> Nghĩa tiếng việt </h5>
        <input value="{{ $av ? $av->vocabulary_vi_translate :'' }}" name="_vi_translate" type="text" class="uk-input" placeholder="Tối đa 100 kí tự">
    </div>
    <div class="uk-grid-margin uk-first-column">
        <h5 class="uk-text-bold mb-2"> Loại từ </h5>
        <select class="uk-select" name="_type_id" >

            <option disabled selected>Chọn loại</option>
            @foreach( $types as $item)
                <option 
                {{
                  ( $av  && $av->vocabulary_type_id == $item->vocabulary_type_id ) ? 'selected' : ''
                }}

                value="{{ $item->vocabulary_type_id }}">{{ $item->vocabulary_type  }}
                    ({{ $item->vocabulary_symbol }})
                </option>
            @endforeach
        </select>
    </div>
    <div class="uk-grid-margin">
        <h5 class="uk-text-bold mb-2"> Phiên âm </h5>
        <input value="{{ $av ? $av->vocabulary_pronounce :'' }}"  name="_pronounce" type="text" class="uk-input" placeholder="">
    </div>
</div>
<div uk-gird class="uk-child-width-1-2@s uk-grid-small p-4 uk-grid">
    <div class="uk-width-1-2@m uk-first-column">
        <div class="uk-first-column">
            <h5 class="uk-text-bold mb-2">Ảnh mô tả </h5>
            <input type="hidden" id="article_avatar_1" name="_image" value="{{ $av ? $av->vocabulary_image :'' }}" >
            <button data-path-name="path-name-1" data-preview="image-preview-1"
                    data-input="article_avatar_1"
                    id="choice-photo-1" class="uk-button uk-button-default" type="button"
                    tabindex="-1">
                <i class="uil-image-upload text-danger"></i>
                Chọn ảnh
            </button>
            <span id="path-name-1">{{ $av ? $av->vocabulary_image :'' }}</span>
        
        </div>
        <div class="uk-first-column">
            <div class="wrapper-img" id="image-preview-1">
                <img class="image-preview" src="{{ $av ? asset($av->vocabulary_image) : asset('assets/nophoto.png')  }}">
            </div>
        </div>

    </div>
    <div class="uk-width-1-2@m uk-first-column">
        <div class="uk-first-column">
            <h5 class="uk-text-bold mb-2">Âm thanh mô tả </h5>
            <input type="hidden" id="vocabulary_audio_1" name="_audio" value="{{ $av ? $av->vocabulary_audio :'' }}">
            <button data-path-name="path-audio-name-1" data-preview="image-preview-1"
                    data-input="vocabulary_audio_1" data-audio="audio-preview-1"
                    id="choice-audio-1" class="uk-button uk-button-default" type="button"
                    tabindex="-1">
                <i class="uil-file-upload text-danger"></i>
                Chọn âm thanh
            </button>
            <span id="path-audio-name-1">{{ $av ? $av->vocabulary_audio :'' }}</span>
        </div>
        <div class="uk-first-column">
            <div class="wrapper-img">
                <div class="image-preview" style="text-align: center">
                    <audio controls class="audio-preview-1" autoplay>
                        <source src="{{ $av ? asset($av->vocabulary_audio) : '#' }}">
                    </audio>
                </div>

            </div>
        </div>
    </div>
</div>

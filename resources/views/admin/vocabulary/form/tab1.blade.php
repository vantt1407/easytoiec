<div class="uk-child-width-1-2@s uk-grid-small p-4 uk-grid" uk-grid="">
    <div class="uk-first-column">
        <h5 class="uk-text-bold mb-2"> Từ vựng </h5>
        <input name="vocabulary_word" type="text" class="uk-input" placeholder="Tối đa 10 kí tự">
    </div>
    <div>
        <h5 class="uk-text-bold mb-2"> Nghĩa tiếng việt </h5>
        <input name="vocabulary_vi_translate" type="text" class="uk-input" placeholder="Tối đa 300 kí tự">
    </div>
    <div class="uk-grid-margin uk-first-column">
        <h5 class="uk-text-bold mb-2"> Loại từ </h5>
        <select class="uk-select" name="vocabulary_type_id">

            <option disabled selected>Chọn loại</option>
            @foreach( $types as $item)
                <option value="{{ $item->vocabulary_type_id }}">{{ $item->vocabulary_type  }}
                    ({{ $item->vocabulary_symbol }})
                </option>
            @endforeach
        </select>
    </div>
    <div class="uk-grid-margin">
        <h5 class="uk-text-bold mb-2"> Phiên âm </h5>
        <input name="vocabulary_pronounce" type="text" class="uk-input" placeholder="">
    </div>
</div>
<div uk-gird class="uk-child-width-1-2@s uk-grid-small p-4 uk-grid">
    <div class="uk-width-1-2@m uk-first-column">
        <div class="uk-first-column">
            <h5 class="uk-text-bold mb-2">Ảnh mô tả </h5>
            <input type="hidden" id="article_avatar" name="vocabulary_image">
            <button data-path-name="path-name" data-preview="image-preview"
                    data-input="article_avatar"
                    id="choice-photo" class="uk-button uk-button-default" type="button"
                    tabindex="-1">
                <i class="uil-image-upload text-danger"></i>
                Chọn ảnh
            </button>
            <span id="path-name"></span>
            @error('topic_avatar')
            <span class="text-right text-danger text-error" role="alert">
                                  <strong>{{ $message }}</strong>
                                 </span>
            @enderror
        </div>
        <div class="uk-first-column">
            <div class="wrapper-img" id="image-preview">
                <img class="image-preview" src="{{ asset('assets/nophoto.png') }}">
            </div>
        </div>

    </div>
    <div class="uk-width-1-2@m uk-first-column">
        <div class="uk-first-column">
            <h5 class="uk-text-bold mb-2">Âm thanh mô tả </h5>
            <input type="hidden" id="vocabulary_audio" name="vocabulary_audio">
            <button data-path-name="path-audio-name" data-preview="image-preview"
                    data-input="vocabulary_audio" data-audio="audio-preview"
                    id="choice-audio" class="uk-button uk-button-default" type="button"
                    tabindex="-1">
                <i class="uil-file-upload text-danger"></i>
                Chọn âm thanh
            </button>
            <span id="path-audio-name"></span>
        </div>
        <div class="uk-first-column">
            <div class="wrapper-img">
                <div class="image-preview" style="text-align: center">
                    <audio controls class="audio-preview" autoplay>
                        <source src="#">
                    </audio>
                </div>

            </div>
        </div>
    </div>
</div>

@extends('layouts.app_admin')
@section('head')
<title>Quản lý chủ đề</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#"> Chủ đề </a></li>
            <li>Danh sách</li>
        </ul>
    </nav>
</div>
<div class="card">
    <!-- Card header -->
    <div class="card-header actions-toolbar border-0">
        <div class="d-flex justify-content-between align-items-center">
            <h4 class="d-inline-block mb-0">Chủ đề</h4>
            <div class="d-flex">

                <a href="#" class="btn btn-icon btn-hover  btn-circle" uk-tooltip="Tìm kiếm">
                    <i class="uil-search"></i>
                </a>
                <div class="uk-drop" uk-drop="mode: click; pos: left-center; offset: 0">
                    <form class="uk-search uk-search-navbar uk-width-1-1" method="get" action="{{ route('topic.index') }}">
                        <input name="keyword" class="uk-search-input shadow-0 uk-form-small" type="search" placeholder="Tìm kiếm"
                            autofocus>
                    </form>
                </div>
                <a  href="{{ route('topic.create') }}" class="btn btn-icon btn-hover  btn-circle " uk-tooltip="Thêm mới">
                    <i class="uil-file-plus text-success"></i>
                </a>
                <button  id="delete-topic" class="btn btn-icon btn-hover  btn-circle " uk-tooltip="Xoá">
                    <i class="uil-trash-alt text-danger"></i>
                    <form method="POST" style="display:node" id="form-delete-topic" action="{{ route('topic.destroy',0)}}">
                        @method("delete")
                        @csrf
                    </form>
                </button>
            </div>
        </div>
    </div>
    <!-- Table -->
    <div class="table-responsive">
        <table class="table align-items-center">
            <thead>
                <tr>
                    <th>
                        <input  style="display: none"  class="skin-square all" type="checkbox">
                    </th>
                    <th scope="col">Tên chủ đề</th>
                    <th scope="col">Số từ vựng</th>
                    <th scope="col">Ngày tạo</th>
                    <th scope="col"> </th>
                </tr>
            </thead>
            <tbody class="list">
                @foreach ($topicList as $item)
                <tr>
                    <th scope="row">
                        <input style="display: none" value="{{ $item->topic_id }}" class="skin-square check" type="checkbox">
                    </th>
                    <th scope="row">
                        <div class="media align-items-center">
                            <div>
                                <div class="avatar-parent-child" style="width: max-content">
                                    <img alt="Image placeholder" src="{{ asset($item->topic_avatar) }}" class="avatar rounded-circle">
                                </div>
                            </div>
                            <div class="media-body ml-4">
                                <a href="{{ route('topic.show', $item->topic_id) }}" class="name h6 mb-0 text-sm">{{$item->topic_name}}</a>
                                <small class="d-block font-weight-bold">#{{$item->topic_id}}</small>
                            </div>
                        </div>
                    </th>
                    <td scope="row">
                        <span class="uk-label uk-label-success">{{ $item->vocabularies->count()}}</span>
                    </td>
                    <td>{{$item->created_at}}</td>
                    <td class="text-right">
                        <!-- Actions -->
                        <div class="actions ml-3">
                            <a href="{{ route('topic.show', $item->topic_id) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Xem chi tiết">
                                <i class="uil-external-link-alt text-info"></i> </a>
                            <a href="{{ route('topic.edit', $item->topic_id) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa">
                                <i class="uil-pen text-warning"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if ($topicList->isEmpty())
        <div class="empty-data">Không có dữ liệu</div>
        @else
        <div class="text-center" style="float:right; ">
            {{ $topicList->links('vendor.pagination.default') }}
        </div>
        @endif
    </div>
</div>

@endsection
@section('js')
<script   src="{{ asset('assets/admin/js/topic/list.js') }}"></script>
<script  src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
@if(isset($showFormCreate))
@include('admin.topic.create')
<script  src="{{ asset('assets/admin/js/topic/create.js') }}"></script>
@endif
@if(isset($showFormView))
@include('admin.topic.edit')
<script  src="{{ asset('assets/admin/js/topic/edit.js') }}"></script>
@endif
@if(!isset($showFormCreate) && !isset($showFormView) )
<script  src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script   src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>
@endif
@endsection

@section('css')
@if(!isset($showFormCreate) && !isset($showFormView) )
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
@endif
@endsection

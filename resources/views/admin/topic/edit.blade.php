
<div id="modal-topic-show-edit" class="uk-modal-container" uk-modal bg-close="false">
    <div class="uk-modal-dialog"> <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title"> {{ isset($disableInput) ? "Chi tiết chủ đề": "Chỉnh sửa chủ đề" }} </h2>
        </div>
        <div class="uk-modal-body">
            <form id="form-update-topic" class="uk-child-width-1-1 uk-grid-small uk-grid uk-grid-stack" method="post" action="{{ route('topic.update',$topic->topic_id)}}" enctype="multipart/form-data">
                @csrf
                @method("put")
                <div class="uk-width-1-2@m">

                    @if(!isset($disableInput))
                    <div class="uk-first-column">
                        <div class="uk-form-group">
                            <label class="uk-form-label">Ảnh đại diện</label>
                            <div class="uk-position-relative w-100" uk-form-custom>
                                <input value="{{$topic->topic_avatar}}" type="hidden"  id="topic_avatar" name="topic_avatar">
                                <button  id="choice-photo" data-path-name="path-name" data-preview="image-preview" data-input="topic_avatar" class="uk-button uk-button-default" type="button">
                                    <i class="uil-image-upload text-danger"></i>
                                    Chọn ảnh</button>
                                <span id="path-name">{{$topic->topic_avatar}}</span>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="uk-grid-margin uk-first-column">
                        <div class="uk-form-group">
                        <label class="uk-form-label">{{ isset($disableInput) ? "Ảnh đại diện" : "Xem trước ảnh đại diện" }}</label>
                            <div class="uk-position-relative w-100">
                                <div class="wrapper-img" id="image-preview">
                                    <img class="image-preview" src="{{ asset($topic->topic_avatar) }}" style='{{ isset($disableInput) ? "height: 380px;" : "" }}' >
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="uk-width-1-2@m">
                    <div class="uk-first-column">
                        <div class="uk-form-group">
                            <label class="uk-form-label"> Tên chủ để</label>
                            <div class="uk-position-relative w-100">
                                <input value="{{$topic->topic_name}}" class="uk-input" type="text" name="topic_name" {{ isset($disableInput) ? "disabled" : "" }}>
                                @error('topic_name')
                                <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                        <div class="uk-form-group">
                            <label class="uk-form-label">Mô tả ngắn</label>
                            <div class="uk-position-relative w-100">
                            <textarea class="uk-textarea" rows="10" name="topic_description" {{ isset($disableInput) ? "disabled" : "" }}>{{$topic->topic_description}}</textarea>
                            @error('topic_description')
                            <span class="text-right text-danger text-error" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <div class="uk-flex uk-flex-right p-4">

            @if (isset($disableInput) )
                <button onclick=" window.location.assign('{{route('topic.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                <button class="btn btn-default"   onclick=" window.location.assign('{{route('topic.edit',$topic->topic_id)}}')" >Sửa</button>

            @else
                <button onclick=" window.location.assign('{{route('topic.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                <button class="btn btn-default"  onclick="document.getElementById('form-update-topic').submit()" >Lưu</button>
            @endif


            </div>
        </div>
    </div>
</div>


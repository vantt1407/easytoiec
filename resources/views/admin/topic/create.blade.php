
<div id="modal-topic-create" class="uk-modal-container" uk-modal bg-close="false">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Tạo mới chủ đề</h2>
        </div>
        <div class="uk-modal-body">
            <form id="form-topic-create" class="uk-child-width-1-1 uk-grid-small uk-grid uk-grid-stack" method="post" action="{{ route('topic.store')}}">
                @csrf
                <div class="uk-width-1-2@m">

                    <div class="uk-first-column">
                        <div class="uk-form-group">
                            <label class="uk-form-label">Ảnh đại diện</label>
                            <div class="uk-position-relative w-100" uk-form-custom>
                                <input type="hidden"  id="topic_avatar" name="topic_avatar">
                                <button data-path-name="path-name" data-preview="image-preview" data-input="topic_avatar" id="choice-photo" class="uk-button uk-button-default" type="button" tabindex="-1">
                                    <i class="uil-image-upload text-danger"></i>
                                    Chọn ảnh</button>
                                <span id="path-name"></span>
                                @error('topic_avatar')
                                <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                        <div class="uk-form-group">
                            <label class="uk-form-label">Xem trước ảnh đại diện</label>
                            <div class="uk-position-relative w-100">
                                <div class="wrapper-img" id="image-preview">
                                    <img class="image-preview" src="{{ asset('assets/nophoto.png') }}">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="uk-width-1-2@m">
                    <div class="uk-first-column">
                        <div class="uk-form-group">
                            <label class="uk-form-label"> Tên chủ để</label>
                            <div class="uk-position-relative w-100">
                                <input class="uk-input" type="text" name="topic_name"  placeholder="Tối đa 100 ký tự">
                                @error('topic_name')
                                <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                        <div class="uk-form-group">
                            <label class="uk-form-label">Mô tả ngắn</label>
                            <div class="uk-position-relative w-100">
                                <textarea class="uk-textarea" rows="10" name="topic_description" placeholder="Tối đa 255 ký tự"></textarea>
                                @error('topic_description')
                                <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <div class="uk-flex uk-flex-right p-4">
                <button onclick=" window.location.assign('{{route('topic.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                <button class="btn btn-default"  onclick="document.getElementById('form-topic-create').submit()" >Lưu</button>
            </div>

        </div>
    </div>
</div>


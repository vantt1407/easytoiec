<div id="popup-create-question" uk-modal class="uk-modal-container" bg-close="false">
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title"></h2>
        <div id="sigle-input-modal">
            <form id="form-create-modal" method="post" action="{{ route('question.store') }}">
                @csrf
                <input type="hidden" name="callback" value="callback">
                <input type="hidden" value="{{$part_id}}" name="part_id" >
                <div class="uk-first-column">
                    <h5 class="uk-text-bold mb-2">Tên câu hỏi</h5>
                    <input name="question_question" type="text" class="uk-input" placeholder="Tối đa 100 ký tự">
                </div>
                <div class="uk-first-column">
                    <h5 class="uk-text-bold mb-2">Số điểm</h5>
                    <input id="question_scores" name="question_scores" type="number" class="uk-input">
                </div>
                <div uk-gird class="uk-grid">
                    <div class="uk-width-1-2@m uk-first-column">
                        <div class="uk-first-column">
                            <h5 class="uk-text-bold mb-2">Ảnh mô tả </h5>
                            <input type="hidden" id="question_photo" name="question_image">
                            <button data-path-name="modal-path-name" data-preview="modal-image-preview" data-input="question_photo" id="modal-choice-photo" class="uk-button uk-button-default" type="button" tabindex="-1">
                                <i class="uil-image-upload text-danger"></i>
                                Chọn ảnh</button>
                            <span id="modal-path-name"></span>
                        </div>
                        <div class="uk-first-column">
                            <div class="wrapper-img" id="modal-image-preview">
                                <img class="image-preview" src="{{ asset('assets/nophoto.png') }}">
                            </div>
                        </div>

                    </div>
                    <div class="uk-width-1-2@m uk-first-column">
                        <div class="uk-first-column">
                            <h5 class="uk-text-bold mb-2">Âm thanh mô tả </h5>
                            <input type="hidden" id="question_audio" name="question_audio">
                            <button data-path-name="modal-path-name-audio" data-audio="modal-audio-preview" data-input="question_audio" id="modal-choice-audio" class="uk-button uk-button-default" type="button" tabindex="-1">
                                <i class="uil-file-upload text-danger"></i>
                                Chọn âm thanh</button>
                            <span id="modal-path-name-audio"></span>
                        </div>
                        <div class="uk-first-column">
                            <div class="wrapper-img">
                                <div class="image-preview" style="text-align: center">
                                    <audio style="margin-top: 25%" controls class="modal-audio-preview">
                                        <source src="#">
                                    </audio>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-first-column">
                    <h5 class="uk-text-bold mb-2">Đáp án</h5>
                    <div id="no-answer" style="color:#f0506e ">
                        <i class="error help-block">Vui lòng thêm câu trả lời !</i>
                    </div>
                    <button id="btn-add-answer-modal" type="button" class="btn btn-animated btn-success btn-animated-x ">
                        <span class="btn-inner--visible">Thêm đáp án</span>
                        <span class="btn-inner--hidden">
                            <i class="uil-plus"></i>
                        </span>
                    </button>
                    <div id="question-container-modal" uk-sortable="group:sortable-group">

                    </div>
                </div>
                <div class="uk-flex uk-flex-right p-4">
                    <button  type="button" onclick="window.location.reload()" class="btn btn-outline-danger uk-first-column mr-2 uk-modal-close">
                        Huỷ
                    </button>
                    <button class="btn btn-outline-success" type="submit">
                        Tạo mới</button>
                </div>
            </form>
        </div>
    </div>
</div>

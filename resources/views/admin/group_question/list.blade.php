@extends('layouts.app_admin')

@section('head')

@endsection

@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#"> Nhóm Câu hỏi </a></li>
            <li>Danh sách</li>
        </ul>
    </nav>
</div>
    <div  style="padding: 20px;" class="card">
        <table id="example" class="uk-table uk-table-divider">
            {{-- <thead>
                <tr>
                    <th>  <input class="skin-square all" type="checkbox"> </th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead> --}}

        </table>
</div>

@endsection

@section('js')
<script src=" https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.uikit.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script>
    window.url_data = "{{ route('question-group.index') }}";
    window.create_url = "{{ route('question.create.group') }}";
    window.delete_url = "{{ route('question-group.destroy',0) }}"
</script>



<script  src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script  src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>


<script src="{{ asset('assets/admin/js/group_question/list.js') }}"></script>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.uikit.min.css">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .dt-buttons {
        float: right;
    }
    .skin-square {
        display: none !important;
    }
</style>
@endsection

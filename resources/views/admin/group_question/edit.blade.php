@extends('layouts.app_admin')
@section('head')
<title>Cập nhật nhóm câu hỏi</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#">Nhóm Câu hỏi </a></li>
            <li> Cập nhật nhóm câu hỏi</li>
        </ul>
    </nav>
</div>

<form action="{{ route('question-group.update',$group->question_group_id) }}" method="post" id="form-create-single">
    @csrf
    @method('put')
    <div uk-grid="" class="uk-grid">


        <div class="uk-width-1-1@m uk-first-column">

            <div class="card rounded">
                <div class="p-3">
                    <h5 class="mb-0">Nhóm Câu hỏi </h5>
                </div>
                <hr class="m-0">
                <ul uk-tab uk-switcher="animation: uk-animation-slide-left-medium, uk-animation-slide-right-medium">
                    <li class="uk-active"><a href="#">Cập nhật</a></li>

                </ul>

                <ul class="uk-switcher uk-margin">
                    <li>
                        <div class=" uk-child-width-2-3@m uk-grid-small p-4 uk-grid" uk-grid="">

                            <div id="group-input">
                                <div class="uk-first-column  mb-2">
                                    <h5 class="uk-text-bold mb-2">Nhóm câu hỏi</h5>
                                    <textarea id="editer" name="question_group_question" type="text" class="uk-textarea mb-2" placeholder="Tối đa 500 ký tự">{{$group->question_group_question}}</textarea>
                                </div>
                                <div class="uk-first-column mb-2">
                                    <h5 class="uk-text-bold mb-2">Hình ảnh</h5>
                                    <input value="{{$group->question_group_image}}" type="hidden" id="question_group_image" name="question_group_image">
                                    <button data-preview="image-preview" data-path-name="path-name-photo"  data-input="question_group_image" id="choice-photo" class="uk-button uk-button-default" type="button" tabindex="-1">
                                        <i class="uil-file-upload text-danger"></i>
                                        Chọn hình ảnh
                                    </button>
                                    <span id="path-name-photo">{{$group->question_group_image}}</span>
                                    <div class="uk-first-column">
                                        <div class="wrapper-img">
                                            <div id="image-preview" class="image-preview" style="text-align: center">
                                                <img class="image-preview" src="{{ $group->question_group_image
                                                ? asset($group->question_group_image)
                                                : asset('assets/nophoto.png') }}">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="uk-first-column mb-2">
                                    <h5 class="uk-text-bold mb-2">Âm thanh</h5>
                                    <input value="{{$group->question_group_audio}}" type="hidden" id="question_group_audio" name="question_group_audio">
                                    <button data-path-name="path-name" data-audio="audio-preview" data-input="question_group_audio" id="choice-audio" class="uk-button uk-button-default" type="button" tabindex="-1">
                                        <i class="uil-file-upload text-danger"></i>
                                        Chọn âm thanh
                                    </button>
                                    <span id="path-name">{{$group->question_group_audio}}</span>
                                    <div class="uk-first-column">
                                        <div class="wrapper-img">
                                            <div class="image-preview" style="text-align: center">
                                                <audio style="margin-top: 15%" controls class="audio-preview">
                                                    <source src="{{ asset($group->question_group_audio) }}">
                                                </audio>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="uk-first-column mb-2">
                                    <h5 class="uk-text-bold mb-2">Danh sách câu hỏi</h5>
                                    <table class="uk-table uk-table-divider">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Câu hỏi</th>
                                                <th>
                                                    <a id="create-new" href="void:javascript(0)" class="btn btn-sm btn-success" uk-tooltip="Thêm" title="Thêm">
                                                        <i class="uil-file-plus "></i>
                                                    </a>
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody class="text-center">
                                            @foreach ($group->questions as $item)
                                            @php
                                            $part_id = $item->part_id;
                                            @endphp
                                            <tr>
                                                <td>{{ $item->question }}</td>
                                                <td>
                                                    <a href="{{ route('question.edit',  $item->question_id )."?group=". $group->question_group_id }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa" title="Chỉnh sửa">
                                                        <i class="uil-pen text-warning"></i>
                                                    </a>
                                                    <a href="{{ route('question.destroy',$item->question_id)."?ids=".$item->question_id }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Xóa" title="Xóa">
                                                        <i class="uil-trash-alt text-danger"></i>
                                                    </a>

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                    @if (empty($group->questions) )
                                    Không có dữ liệu !
                                    @endif
                                </div>
                                <div class="uk-flex uk-flex-right p-4">
                                    <button type="button" onclick="window.location.assign(' {{ route('question-group.list') }} ')" class="btn btn-outline-danger uk-first-column mr-2">
                                        Huỷ
                                    </button>

                                    <button class="btn btn-outline-success btn-create-single-question" type="submit">
                                        Lưu </button>
                                </div>
                            </div>


                        </div>

                    </li>

                </ul>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')

<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js?v=1') }}"></script>
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
<script src="{{ asset('assets/common/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/common/tinymce/init.js') }}"></script>
<script>
    window.url_create_group = "{{route('question.create.group')}}";
    

</script>

@include('admin.group_question.create_modal')
<script>
    $(function() {

        $('#choice-photo').filemanager();
        $('#choice-audio').filemanager();

        $('#create-new').on('click', function() {
            UIkit.modal('#popup-create-question').show();
        });
    });

</script>
<script>
    $(function() {
        $('#create-new').on('click', function() {
            UIkit.modal('#popup-create-question', {
                stack: true
            }).show();
        });

        $('#popup-create-question').on('shown', function() {
            $('#modal_question_photo').on('change', function() {
                $(this).removeClass('has-error');
                $('#article_avatar-error').remove();
            });
            $('#modal_question_audio').on('change', function() {
                $(this).removeClass('has-error');
                $('#vocabulary_audio-error').remove();
            });
            $('#no-answer').hide();
            $('#modal-choice-photo').filemanager();
            $('#modal-choice-audio').filemanager();
            $.validator.setDefaults({
                ignore: []
            , });
            $.validator.addMethod("string", function(value, element) {
                return /^[a-zA-Z0-9.\-$* ]{1,}$/.test(value);
            });
            $("#form-create-modal").validate({
                submitHandler: function(form) {
                    if ($(document).find('#question-container-modal').hasClass('uk-sortable-empty')) {
                        $('#no-answer').show();
                        return;
                    }
                    $(form)[0].submit();
                }
                , rules: {
                    question_question: {
                        required: true
                        , minlength: 2
                        , maxlength: 100
                    }
                    , question_scores: {
                        required: true
                        , number: true
                    }
                    , "answer_option_modal[]": {
                        required: true
                    , }
                    , is_correct_modal: {
                        required: true
                    }

                }
                , messages: {
                    question_question: {
                        required: "Câu hỏi không được bỏ trống !"
                        , minlength: "Tối thiểu 2 ký tự !"
                        , maxlength: "Tối đa 15 ký tự !"
                    }
                    , question_scores: {
                        required: "Điểm không được bỏ trống !"
                        , number: "Điểm phải là số !"
                    }
                    , "answer_option_modal[]": {
                        required: "Câu trả lời không được bỏ trống!"
                    }
                    , is_correct_modal: {
                        required: "Không có đáp án nào được chọn đúng !"
                    }
                }
                , errorElement: "em"
                , errorPlacement: function(error, element) {



                    // Add the `help-block` class to the error element
                    error.addClass("help-block");

                    if (element.prop("type") === "radio") {
                        UIkit.modal
                            .alert(error.text(), {
                                stack: true
                            })

                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                }
                , highlight: function(element, errorClass, validClass) {
                    $(element).addClass("has-error").removeClass("has-success");
                }
                , unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("has-success").removeClass("has-error");
                }
            });
            $('body').on('click', '#btn-add-question-modal', function() {
                if ($(document).find('#question-container-modal').hasClass('uk-sortable-empty')) {
                    $('#no-answer').show();
                }
            });
            $('body').on('keypress', '.answer-option-modal', function() {

                $(this).next().children('.iradio_square-purple').children('.is-correct-modal').val($(this).val());
            });

            let initIcheckModal = function() {
                var iBoxes = $('.skin-square');
                $.each(iBoxes, function(index, item) {
                    let $dom = $(item);
                    if ($dom.attr('type') === 'radio') {
                        $dom.iCheck({
                            radioClass: 'iradio_square-purple'
                            , increaseArea: '20%'
                        });
                    } else {
                        $dom.iCheck({
                            checkboxClass: 'icheckbox_square-purple'
                            , increaseArea: '20%'
                        });
                    }
                });
                $('#change-type-add-question').on('ifChecked ifUnchecked', function() {
                    if (this.checked) {

                        $('#sigle-input').hide();
                        $('#group-input').show();
                    } else {
                        $('#sigle-input').show();
                        $('#group-input').hide();
                    }
                });
            }
            $('#btn-add-answer-modal').on('click', function() {
                $('#no-answer').hide();
                if ($(document).find('#question-container-modal').children('div').length === 4) {
                    return;
                }
                let tpl = ` <div class="uk-margin wrapper-question">
                            <div class="uk-card uk-card-default uk-card-body uk-card-small">
                                <input name="answer_option_modal[]" type="text" class="uk-input uk-grid-small answer-option-modal"  placeholder="Nhập đáp án">
                                <div class="wp" style="float:right">
                                    <input  class="skin-square is-correct-modal" type="radio" name="is_correct_modal"> Đáp án đúng
                                    <button type="button" class="btn btn-icon btn-hover btn-circle btn-delete-question" uk-tooltip="Xóa" title="" aria-expanded="false">
                                        <i class="uil-trash-alt text-danger"></i>
                                    </button>
                                </div>
                            </div>
                        </div>`;

                $('#question-container-modal').append($(tpl));
                initIcheckModal();
            });
        });

    });

</script>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .audio-preview {
        margin-top: 12%;
        transform: translateY(-50%);
    }

</style>
@endsection

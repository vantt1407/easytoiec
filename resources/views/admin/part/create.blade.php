@extends('layouts.app_admin')
@section('head')
    <title>Quản lý phần thi</title>
@endsection
@section('content')
    <div class="d-flex">
        <nav id="breadcrumbs" class="mb-3">
            <ul>
                <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
                <li><a href="#">Phần thi </a></li>
                <li>Thêm mới</li>
            </ul>
        </nav>
    </div>
    <div class="card rounded uk-width-1-1@m uk-first-column uk-grid">

        <div class="p-3">
            <h5 class="mb-0"> Phần thi </h5>
        </div>
        <form id="form-part-create" class="uk-width-1-1@m  uk-grid " method="post"
              action="{{ route('part.store')}}">
            @csrf

            <div class="uk-width-1-2@m " >

                <div class="uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Ảnh đại diện</label>
                        <div class="uk-position-relative w-100" uk-form-custom>
                            <input type="hidden" id="part_avatar" name="part_avatar">
                            <button data-path-name="path-name" data-preview="image-preview"
                                    data-input="part_avatar" id="choice-photo" class="uk-button uk-button-default"
                                    type="button" tabindex="-1">
                                <i class="uil-image-upload text-danger"></i>
                                Chọn ảnh
                            </button>
                            <span id="path-name"></span>

                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Xem trước ảnh đại diện</label>
                        <div class="uk-position-relative w-100">
                            <div class="wrapper-img" id="image-preview">
                                <img class="image-preview" src="{{ asset('assets/nophoto.png') }}">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="uk-width-1-2@m ">
                <div class="uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label"> Tên phần thi</label>
                        <div class="uk-position-relative w-100">
                            <input  value="" class="uk-input" type="text" name="part_name" placeholder="Tối đa 100 ký tự">
                            @error('topic_name')
                            <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <h5 class="uk-text-bold mb-2"> Loại phần thi </h5>
                    <select class="uk-select" name="part_type_id">
            
                        <option disabled selected>Chọn loại</option>
                        @foreach( $types as $item)
                            <option value="{{ $item->part_type_id }}">
                                {{ $item->part_type  }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label">Mô tả</label>
                        <div class="uk-position-relative w-100">
                                <textarea class="uk-textarea" rows="6" name="part_description"
                                          placeholder="Tối đa 1500 ký tự"></textarea>
                            @error('topic_description')
                            <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="uk-grid-margin uk-first-column">
                    <div class="uk-flex uk-flex-right p-4">
                        <button onclick=" window.location.assign('{{route('part.index')}}')" type="button" class="btn  btn-light mr-2">Hủy</button>
                        <button class="btn btn-default" type="submit">Lưu</button>
                    </div>

                </div>
            </div>
        </form>
    </div>
@endsection
@section('js')
<script  src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js?v=1') }}"></script>
<script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
<script src="{{ asset('assets/admin/js/part/create.js?v=1') }}"></script>
@endsection

@section('css')
<style>
    .has-error {
            border-color: #f0506e !important;
        }
</style>
@endsection

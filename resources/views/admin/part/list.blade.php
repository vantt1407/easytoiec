@extends('layouts.app_admin')
@section('head')
<title>Quản lý phần thi</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#">Phần thi </a></li>
            <li>Danh sách</li>
        </ul>
    </nav>
</div>
<div class="card">
    <!-- Card header -->
    <div class="card-header actions-toolbar border-0">
        <div class="d-flex justify-content-between align-items-center">
            <h4 class="d-inline-block mb-0">Phần thi</h4>
            <div class="d-flex">
                <a  href="{{ route('part.create') }}" class="btn btn-icon btn-hover  btn-circle " uk-tooltip="Thêm mới">
                    <i class="uil-file-plus text-success"></i>
                </a>
                <button  id="delete-part" class="btn btn-icon btn-hover  btn-circle " uk-tooltip="Xoá">
                    <i class="uil-trash-alt text-danger"></i>
                    <form method="POST" style="display:node" id="form-delete-part" action="{{ route('part.destroy',0)}}">
                        @method("delete")
                        @csrf
                    </form>
                </button>
            </div>
        </div>
    </div>
    <!-- Table -->
    <div class="table-responsive">
        <table class="table align-items-center">
            <thead>
                <tr>
                    <th>
                        <input class="skin-square all" type="checkbox">
                    </th>
                    <th scope="col">Tên phần</th>
                    <th scope="col">Loại</th>
                    <th scope="col">Ngày tạo</th>
                    <th scope="col"> </th>
                </tr>
            </thead>
            <tbody class="list">
                @if ($partList->isNotEmpty())
                @foreach ($partList as $item)
                <tr>
                    <th scope="row">
                        <input value="{{ $item->part_id }}" class="skin-square check" type="checkbox">
                    </th>
                    <th scope="row">
                        <div class="media align-items-center">
                            <div>
                                <div class="avatar-parent-child" style="width: max-content">
                                    <img alt="Image placeholder" src="{{ asset($item->part_avatar) }}" class="avatar rounded-circle">
                                </div>
                            </div>
                            <div class="media-body ml-4">
                                <a href="{{ route('part.edit', $item->part_id) }}" class="name h6 mb-0 text-sm">{{$item->part_name}}</a>
                                <small class="d-block font-weight-bold">Phần {{$item->part_id}}</small>
                            </div>
                        </div>
                    </th>
                    <td scope="row">
                    <span class="uk-label text-center" style="width:58.02px;background-color:#3e416d !important">
                    {{ $item->partType->part_type}}
                    </span>
                    </td>
                    <td>{{$item->created_at}}</td>
                    <td class="text-right">
                        <!-- Actions -->
                        <div class="actions ml-3">
                            <a href="{{ route('part.edit', $item->part_id) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa">
                                <i class="uil-pen text-warning"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        @if ($partList->isEmpty())
        <div class="empty-data">Không có dữ liệu</div>
        @else
        <div class="text-center" style="float:right; ">
            {{ $partList->links('vendor.pagination.default') }}
        </div>
        @endif
    </div>
</div>

@endsection
@section('js')
<script async src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script async src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>
<script async src="{{ asset('assets/admin/js/part/list.js') }}"></script>
<script>
    $(function () {
        

    });
</script>

@endsection

@section('css')

<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
     .skin-square {
        display: none !important;
    }
</style>
@endsection
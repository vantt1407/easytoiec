@extends('layouts.app_admin')

@section('head')
    <title>Chi tiết người dùng</title>
@endsection


@section('content')
<h4>Thông tin</h4>

<div uk-grid>

    <div class="uk-width-2-5@m uk-flex-last@m">

        <div class="uk-card-default rounded text-center p-4">
            <div class="uk-position-relative my-4">

                    <div class="user-profile-photo  m-auto">
                    <img src="{{ asset($user->profile->user_profile_avatar) }}" alt="">
                    </div>
            </div>

        </div>
    </div>
    <div class="uk-width-expand@m">

        <div class="uk-card-default rounded">

            <hr class="m-0">
            <form class="uk-child-width-1-2@s uk-grid-small p-4" uk-grid>
                <div>
                    <h5 class="uk-text-bold mb-2"> Họ tên </h5>
                <input  value="{{$user->profile->user_profile_full_name}}" type="text" class="uk-input" readonly>
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Số điện thoại </h5>
                    <input value="{{$user->profile->user_profile_phone}}" type="text" class="uk-input" readonly>
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Email </h5>
                    <input value="{{$user->email}}" type="text" class="uk-input" readonly>
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Địa chỉ </h5>
                    <input value="{{$user->profile->user_profile_address}}" type="text" class="uk-input" readonly>
                </div>
            </form>

            {{-- <div class="uk-flex uk-flex-right p-4">
                <button class="btn btn-default grey">Xoá</button>
            </div> --}}
        </div>
    </div>

</div>
@endsection

@section('css')
    
@endsection

@section('js')
    
@endsection
@extends('layouts.app_admin')
@section('head')
<title>Quản lý người dùng</title>

@endsection

@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#"> Người dùng </a></li>
            <li>Danh sách</li>
        </ul>
    </nav>
</div>




<div class="card">
    <!-- Card header -->
    <div class="card-header actions-toolbar border-0">
        <div class="d-flex justify-content-between align-items-center">
            <h4 class="d-inline-block mb-0">Người dùng</h4>
            <div class="d-flex">

                <a href="#" class="btn btn-icon btn-hover  btn-circle" uk-tooltip="Tìm kiếm">
                    <i class="uil-search"></i>
                </a>
                <div class="uk-drop" uk-drop="mode: click; pos: left-center; offset: 0">
                <form method="GET" class="uk-search uk-search-navbar uk-width-1-1" action="{{route('user.index')}}">
                        <input name="keyword" class="uk-search-input shadow-0 uk-form-small" type="text" placeholder="Tìm kiếm"
                            autofocus>
                    </form>
                </div>
                <button id="delete-user" class="btn btn-icon btn-hover  btn-circle " uk-tooltip="Xoá">
                    <i class="uil-trash-alt text-danger"></i>
                    <form method="POST" style="display:node" id="form-delete-user" action="{{ route('user.destroy',0)}}">
                        @method("delete")
                        @csrf
                    </form>
                </button>

            </div>
        </div>
    </div>
    <!-- Table -->
    <div class="table-responsive">
        <table class="table align-items-center">
            <thead>
                <tr>
                    <th>
                        <input class="skin-square all" type="checkbox"  style="display:none">
                    </th>
                    <th scope="col">Tên người dùng</th>
                    <th scope="col">Ngày đăng ký</th>
                    <th scope="col">Số lần thi thử</th>
                    <th scope="col"> </th>
                </tr>
            </thead>
            <tbody class="list">
                @foreach ($list as $item)
                <tr>
                    <th scope="row">
                        <input style="display:none" value="{{ $item->user_id }}" class="skin-square check" type="checkbox">
                    </th>
                    <th scope="row">
                        <div class="media align-items-center">
                            <div>
                                <div class="avatar-parent-child" style="width: max-content">
                                    <img alt="Image placeholder" src="{{ $item->profile ? asset($item->profile->user_profile_avatar) :   asset('assets/images/no-avt.png') }}"
                                        class="avatar  rounded-circle">

                                </div>
                            </div>
                            <div class="media-body ml-4">
                                <a href="#" class="name h6 mb-0 text-sm">{{ $item->profile ?  $item->profile->user_profile_full_name : ''}}</a>
                                <small class="d-block font-weight-bold">#{{$item->user_id}}</small>
                            </div>
                        </div>
                    </th>
                    <td>{{$item->created_at}}</td>
                    <td class="text-center" >
                    <span class="uk-label uk-label-danger">{{
                        $item->userTests->count()
                        }}</span>
                    </td>

                    <td class="text-right">
                        <!-- Actions -->
                        <div class="actions ml-3">
                            <a href="{{ route('user.show', $item->user_id) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Xem chi tiết">
                                <i class="uil-external-link-alt text-info"></i> </a>

                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
        @if($list->isEmpty())
            <div class="text-center">
                Không có bản ghi nào 
            </div>
        @endif
        <div class="text-center" style="float:right;padding: 20px;">
            {{ $list->links('vendor.pagination.default') }}
        </div>
    </div>
</div>

@endsection
@section('js')
<script  src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script  src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>
<script src="{{ asset('assets/admin/js/user/list-user.js') }}"></script>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
@endsection

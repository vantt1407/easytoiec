@extends('layouts.app_admin')

@section('head')
<title>Thiết lập hệ thống </title>
@endsection

@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li id="info"><a href="#"> Thiết lập hệ thống </a></li>
        </ul>
    </nav>
</div>
<div uk-grid>
    <div class="uk-width-1-4@m uk-flex-last@m">

        <nav class="responsive-tab style-3 setting-menu card" uk-sticky="top:30 ; offset:100; media:@m ;bottom:true; animation: uk-animation-slide-top">

            <ul>
                <li class="uk-lm uk-active"><a class="uk-am" href="#info"> <i class="uil-cog"></i>Thông tin chung</a></li>
                {{-- <li class="uk-lm"><a class="uk-am" href="#random-update-form"> <i class="uil-bolt"></i></i> Random nhóm câu hỏi </a></li> --}}
                <li class="uk-lm"><a class="uk-am" href="#app-update-form"> <i class="uil-shield-check"></i>Ứng dụng</a></li>
            </ul>
        </nav>

    </div>

    <div class="uk-width-2-3@m">

        <div class="card rounded">
            <div class="p-3">
                <h5 class="mb-0"> Thông tin chung </h5>
            </div>
            <hr class="m-0">
            <form id="info-form" class="" >
                <div class="uk-child-width-1-2@s uk-grid-small p-4" uk-grid>
                <div>
                    <h5 class="uk-text-bold mb-2"> Địa chỉ </h5>
                    <input value="{{$setting->address}}" type="text" class="uk-input" name="address">
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Số điện thoại </h5>
                    <input value="{{$setting->phone}}"  type="text" class="uk-input" name="phone">
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Slogan </h5>
                    <input  value="{{$setting->sologan}}"  type="text" class="uk-input" name="sologan">
                </div>
            
             </div>
                <div class="uk-grid-margin  uk-first-column p-4" >
                    <div class="wrapper-img" id="image-preview">
                        <img class="image-preview" src="{{ asset($setting->banner) }}">
                    </div>
                    <input value="{{$setting->banner}}"  name="banner" id="banner-path" type="hidden">
                    <button data-path-name="path-name" data-preview="image-preview" data-input="banner-path" id="choice-photo" class="uk-button uk-button-default" type="button" tabindex="-1">
                        <i class="uil-image-upload text-danger"></i>
                        Chọn ảnh
                    </button>
                    <span id="path-name">{{$setting->banner}}</span>
                </div>
                
                
            </form>

            <div class="uk-flex uk-flex-right p-4">
                <button class="btn btn-default" id="update-info">Lưu</button>
            </div>
        </div>

        {{-- <div class="card rounded mt-4">
            <div class="p-3">
                <h5 class="mb-0"> Random nhóm câu hỏi </h4>
                    <span>(Số nhóm câu hỏi lấy ra khi tạo đề thi thử ngẫu nhiên)</span>
            </div>
            <hr class="m-0">
            <form id="random-update-form" class="uk-child-width-1-2@s uk-grid-small p-4" uk-grid>
                <div>
                    <h5 class="uk-text-bold mb-2"> Phần 1 </h5>
                    <input name="part[]" type="number" class="uk-input" value="{{$randomConfig['part1']}}">
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Phần 2 </h5>
                    <input  name="part[]" type="number" class="uk-input" value="{{$randomConfig['part2']}}">
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Phần 3 </h5>
                    <input  name="part[]" type="number" class="uk-input" value="{{$randomConfig['part3']}}">
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Phần 4 </h5>
                    <input  name="part[]" type="number" class="uk-input" value="{{$randomConfig['part4']}}">
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Phần 5 </h5>
                    <input  name="part[]" type="number" class="uk-input" value="{{$randomConfig['part5']}}">
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Phần 6 </h5>
                    <input  name="part[]" type="number" class="uk-input" value="{{$randomConfig['part6']}}">
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Phần 7 </h5>

                    <input  name="part[]" type="number" class="uk-input" value="{{$randomConfig['part7']}}">
                </div>

            </form>
            <div class="uk-flex uk-flex-right p-4">

                <button id="random-update" class="btn btn-default">Lưu</button>
            </div>
        </div> --}}
        <div class="card rounded mt-4">
            <div class="p-3">
                <h5 class="mb-0"> Ứng dụng </h4>
            </div>
            <hr class="m-0">
            <form class="uk-child-width-1-2@s uk-grid-small p-4" uk-grid id="app-update-form">
                <div>
                    <h5 class="uk-text-bold mb-2"> Email </h5>
                    <input name="username" value="{{ $emailConfig['username'] }}" type="text" class="uk-input" >
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Mật khẩu </h5>
                    <input name="password" id="pass-mail"  value="{{ $emailConfig['password']}}"  type="password" class="uk-input" >
                </div>
                <div>
                    <h5 class="uk-text-bold mb-2"> Tên </h5>
                    <input name="from.name" value="{{ $emailConfig['from']['name']}}" type="text" class="uk-input" >
                </div>
            </form>
            <div class="uk-flex uk-flex-right p-4">
                <button id="app-update" class="btn btn-default">Lưu</button>
            </div>
        </div>
       
    </div>


</div>
@endsection

@section('js')
<script>
    window.app_update = "{{route('setting.email')}}";
    window.random_update = "{{route('setting.random')}}";
    window.info_update = "{{route('setting.info')}}";
</script>
<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script src="{{ asset('assets/admin/js/setting.js') }}"></script>
<script>
    $(function() {
        $('#choice-photo').filemanager();
        $('.uk-am').on('click', function() {

            $('.uk-lm').removeClass('uk-active');
            $(this).parents('.uk-lm').addClass('uk-active');

        });

    });

</script>
@endsection

@section('css')

@endsection

@extends('layouts.app_admin')

@section('head')
<title>Trang quản trị</title>
@endsection
@section('content')

<div class="ro" uk-grid>
    <div class="uk-width-expand@m">


        <div class="section-small">

            <h3>Xin chào {{ isset(Auth::user()->profile->user_profile_full_name) ? Auth::user()->profile->user_profile_full_name : "Admin !"}}</h3>

            <div class="uk-position-relative uk-visible-toggle " uk-slider="finite: true">

                <ul class="uk-slider-items uk-child-width-1-2@m uk-child-width-1-2@s uk-grid">

                    <li>
                        <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                            <h3 class="uk-card-title">Tổng số người dùng</h3>
                            <h4>{{$totalUser}}</h4>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
                            <h3 class="uk-card-title">Tổng số bài luyện tập </h3>
                            <h4>{{$totalPractice}}</h4>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-secondary uk-card-hover uk-card-body uk-light">
                            <h3 class="uk-card-title">Tổng số bài viết</h3>
                            <h4>{{$totalArticle}}</h4>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                            <h3 class="uk-card-title">Tổng số chủ đề</h3>
                            <h4>{{$totalTopic}}</h4>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
                            <h3 class="uk-card-title">Tổng số bài test </h3>
                            <h4>{{$totalTest}}</h4>
                        </div>
                    </li>

                </ul>


                <ul class="uk-slider-nav uk-dotnav uk-flex-center mt-3"></ul>

            </div>
            <hr/>
            <div class="uk-position-relative uk-visible-toggle " uk-slider="finite: true">

                <ul class="uk-slider-items uk-child-width-1-2@m uk-child-width-1-2@s uk-grid">

                    <li>
                        <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                            <h3 class="uk-card-title">Truy cập trong ngày</h3>
                            <h4>{{$userDaily ? $userDaily : 0}}</h4>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
                            <h3 class="uk-card-title">Truy cập trong tuần </h3>
                            <h4>{{$userWeekly ? $userWeekly  : 0}}</h4>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-secondary uk-card-hover uk-card-body uk-light">
                            <h3 class="uk-card-title">Truy cập trong tháng</h3>
                            <h4>{{$userMonthly ? $userMonthly : 0}}</h4>
                        </div>
                    </li>
                    <li>
                        <div class="uk-card uk-card-secondary uk-card-hover uk-card-body uk-light">
                            <h3 class="uk-card-title">Tổng truy cập</h3>
                            <h4>{{$userMonthly ? $userMonthly : 0}}</h4>
                        </div>
                    </li>
                </ul>
                <ul class="uk-slider-nav uk-dotnav uk-flex-center mt-3"></ul>

            </div>
        </div>

    </div>

</div>

@endsection

@section('css')

@endsection

@section('js')

@endsection

@extends('layouts.app_admin')
@section('head')
<title>Tạo mới bài viết</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#"> Bài viết </a></li>
            <li> Tạo mới bài viết</li>
        </ul>
    </nav>
</div>
{{-- [
        'article_avatar',
        'article_title',
        'article_slug',
        'article_content',
        'article_category_id',
        'created_by'
    ] --}}
<form id="form-article-create" action="{{ route('article.store') }}" method="post">
    @csrf
    <div uk-grid="" class="uk-grid">

        <div class="uk-width-1-4@m uk-flex-last@m">
            <nav class="responsive-tab style-3 setting-menu card uk-sticky" uk-sticky="top:30 ; offset:100; media:@m ;bottom:true; animation: uk-animation-slide-top" style="">
                <div class="p-3">
                    <h5 class="mb-0"> Danh mục </h5>
                </div>
                <hr class="m-0">
                <ul>
                    @foreach ($categories as $item)
                    <li class="uk-active">
                        <a href="#">
                            <input value="{{$item->article_category_id}}" class="skin-square" type="radio" name="article_category_id">
                            {{$item->article_category_name}}
                        </a>
                    </li>
                    @endforeach

                </ul>
            </nav>
            <div class="uk-sticky-placeholder" style="height: 522px; margin: 0px 0px 20px;" hidden=""></div>

        </div>
        <div class="uk-width-3-4@m uk-first-column">
            <div class="card rounded">
                <div class="p-3">
                    <h5 class="mb-0"> Bài viết </h5>
                </div>
                <hr class="m-0">
                <div class="uk-child-width-2-3@m uk-grid-small p-4 uk-grid" uk-grid="">
                    <div class="uk-first-column">
                        <h5 class="uk-text-bold mb-2">Tiêu đề </h5>
                        <input name="article_title" type="text" class="uk-input" placeholder="Tối đa 100 ký tự">
                    </div>
                    <div class="uk-grid-margin  uk-first-column">
                        <h5 class="uk-text-bold mb-2">Ảnh đại diện </h5>
                        <input type="hidden" id="article_avatar" name="article_avatar">
                        <button data-path-name="path-name" data-preview="image-preview" data-input="article_avatar" id="choice-photo" class="uk-button uk-button-default" type="button" tabindex="-1">
                            <i class="uil-image-upload text-danger"></i>
                            Chọn ảnh</button>
                        <span id="path-name"></span>
                        @error('topic_avatar')
                        <span class="text-right text-danger text-error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="uk-grid-margin  uk-first-column">
                        <div class="wrapper-img" id="image-preview">
                            <img class="image-preview" src="{{ asset('assets/nophoto.png') }}">
                        </div>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                        <h5 class="uk-text-bold mb-2">Công khai bài viết</h5>
                        <select  class="uk-select" name="article_publish">
                            <option value="1">Công khai</option>
                            <option value="0">Ẩn</option>
                        </select>
                    </div>
                    <div class="uk-grid-margin  uk-first-column">
                        <h5 class="uk-text-bold mb-2">Nội dung</h5>
                        <textarea name="article_content" id="editer" class="uk-input" placeholder="Tối đa 50000 ký tự"></textarea>
                    </div>


                    <div class="uk-flex uk-flex-right p-4">
                        <button  onclick="window.location.assign(' {{ route('article.index') }} ')" type="button"
                                class="btn  btn-light mr-2">Hủy
                        </button>
                        <button class="btn btn-default" type="submit" >
                            Lưu
                        </button>
                        
                    </div>
                </div>
            </div>
        </div>
</form>
@endsection 

@section('js')
<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="{{ asset('assets/common/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/common/tinymce/init.js') }}"></script>
<script src="{{asset('assets/common/jqueryvalidate/jquery.validate.js')}}"></script>
<script src="{{ asset('assets/admin/js/article/create.js') }}"></script>
<script>
    $(function() {
        var radioBoxes = $('.skin-square');
        radioBoxes.iCheck({
            radioClass: 'iradio_square-purple'
            , increaseArea: '20%'
        });
        $('#choice-photo').filemanager('image');
    })

</script>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .has-error {
         border-color: #f0506e !important;
     }
</style>
@endsection

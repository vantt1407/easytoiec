@extends('layouts.app_admin')
@section('head')
<title>Thêm mới danh mục</title>
@endsection
@section('content')
<div id="modal-category-create" uk-modal bg-close="false">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Tạo mới danh mục</h2>
        </div>
        <div class="uk-modal-body">
            <form id="form-category-create" class="uk-child-width-1-1 uk-grid-small uk-grid uk-grid-stack" method="post" action="{{ route('category.store')}}">
                @csrf
                    <div class="uk-first-column">
                        <div class="uk-form-group">
                            <label class="uk-form-label"> Tên danh mục</label>
                            <div class="uk-position-relative w-100">
                                <input class="uk-input" type="text" name="article_category_name"  placeholder="Tối đa 100 ký tự">
                                @error('topic_name')
                                <span class="text-right text-danger text-error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
            </form>
        </div>
        <div class="uk-modal-footer uk-text-right"> 
            <button onclick=" window.location.assign('{{route('article.index')}}')" class="btn btn-outline-danger uk-first-column">
                Huỷ
            </button>
         
             <button class="btn btn-outline-success" type="button" onclick="document.getElementById('form-category-create').submit()" >
                Tạo mới</button>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(function() {
        UIkit.modal("#modal-category-create").show();
    });
</script>
@endsection

@section('css')
@endsection
@extends('layouts.app_admin')
@section('head')
<title>Cập nhật danh mục</title>
@endsection
@section('content')

<div id="modal-category-edit" uk-modal bg-close="false">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Cập nhật danh mục</h2>
        </div>
        <div class="uk-modal-body">
            <form id="form-category-update" class="uk-child-width-1-1 uk-grid-small uk-grid uk-grid-stack" method="post" action="{{ route('category.update',$category->article_category_id) }}">
                @csrf
                @method("put")
                <div class="uk-first-column">
                    <div class="uk-form-group">
                        <label class="uk-form-label"> Tên danh mục</label>
                        <div class="uk-position-relative w-100">
                            <input value="{{$category->article_category_name}}" class="uk-input" type="text" name="article_category_name" placeholder="Tối đa 100 ký tự">
                            @error('topic_name')
                            <span class="text-right text-danger text-error" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button onclick=" window.location.assign('{{route('article.index')}}')" class="btn btn-outline-danger uk-first-column">
                Huỷ
            </button>

            <button class="btn btn-outline-success" type="button" onclick="document.getElementById('form-category-update').submit()">
                Cập nhật</button>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(function() {
        UIkit.modal("#modal-category-edit").show();
    });
</script>
@endsection

@section('css')
@endsection

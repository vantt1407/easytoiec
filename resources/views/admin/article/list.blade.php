@extends('layouts.app_admin')
@section('head')
<title>Quản lý bài viết</title>
@endsection
@section('content')
<div class="d-flex">
    <nav id="breadcrumbs" class="mb-3">
        <ul>
            <li><a href="#"> <i class="uil-home-alt"></i> </a></li>
            <li><a href="#"> Bài viết </a></li>
            <li> Danh mục và Bài viết</li>
        </ul>
    </nav>
</div>
<div uk-grid="" class="uk-grid">
    <div class="uk-width-1-3@m uk-first-column">
        <nav class="responsive-tab style-3 setting-menu card uk-sticky" uk-sticky="top:30 ; offset:100; media:@m ;bottom:true; animation: uk-animation-slide-top" style="">
            <div class="card-header actions-toolbar border-0">
                <div class="d-flex justify-content-between align-items-center">
                    <h4 class="d-inline-block mb-0">Danh mục</h4>
                    <div class="d-flex">
                        <button onclick=" window.location.assign('{{route('category.create')}}')" class="btn btn-icon btn-hover  btn-circle " uk-tooltip="Thêm mới">
                            <i class="uil-file-plus text-success"></i>
                        </button>
                        <button class="btn btn-icon btn-hover  btn-circle btn-update" uk-tooltip="Cập nhật">
                            <i class="uil-pen text-warning"></i>
                        </button>

                        <button class="btn btn-icon btn-hover  btn-circle btn-delete" uk-tooltip="Xoá">
                            <i class="uil-trash-alt text-danger"></i>
                        </button>
                    </div>
                </div>
            </div>
            <hr class="m-0 uk-visible@m">
            <ul>
                <li id="all" >
                    <a href="{{ route('article.index')}}" class="text-center">
                       Tất cả
                    </a>
                </li>
                @foreach ($categories as $item)
                <li id="{{$item->article_category_id}}">
                    <a href="{{ route('article.index', ['category_id'=> $item->article_category_id]) }}">
                        <input  style="display: none"  value="{{$item->article_category_id}}" class="skin-square category-choice" type="checkbox">
                        {{ $item->article_category_name}}
                        <span class="badge badge-light ml-2 badge-sm">{{$item->articles->count()}}</span>
                    </a>
                </li>
                @endforeach

            </ul>
        </nav>
        <div class="uk-sticky-placeholder" style="height: 734px; margin: 0px 0px 20px;" hidden=""></div>

    </div>

    <div class="uk-width-2-3@m">

        <div class="card rounded">
            <!-- Card header -->
            <div class="card-header actions-toolbar border-0">
                <div class="d-flex justify-content-between align-items-center">
                    <h4 class="d-inline-block mb-0">Bài viết</h4>
                    <div class="d-flex">
                        <a href="#" class="btn btn-icon btn-hover  btn-circle" uk-tooltip="Tìm kiếm bài viết">
                            <i class="uil-search"></i>
                        </a>
                        <div class="uk-drop" uk-drop="mode: click; pos: left-center; offset: 0">
                            <form class="uk-search uk-search-navbar uk-width-1-1" action="{{ route('article.index') }}">
                                <input class="uk-search-input shadow-0 uk-form-small" name="keyword" type="search" placeholder="Tìm kiếm" autofocus>
                            </form>
                        </div>
                        <a href="{{ route('article.create') }}" class="btn btn-icon btn-hover  btn-circle " uk-tooltip="Thêm mới">
                            <i class="uil-file-plus text-success"></i>
                        </a>
                        <button id="delete-article" class="btn btn-icon btn-hover  btn-circle " uk-tooltip="Xoá">
                            <i class="uil-trash-alt text-danger"></i>
                            <form id="form-article-delete" action="{{ route('article.destroy',0) }}" method="post" style="display:node">
                                @csrf
                                @method("delete")
                            </form>
                        </button>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead>
                        <tr>
                            <th>
                                <input  style="display: none"  class="skin-square all" type="checkbox">
                            </th>
                            <th scope="col">Tiêu đề</th>
                            <th scope="col">Công khai</th>
                            {{-- <th scope="col">Ngày tạo</th> --}}
                            <th scope="col"> </th>
                        </tr>
                    </thead>
                    <tbody class="list">
                       
                        @foreach ($articleList as $item)
                        <tr>
                            <th scope="row">
                                <input  style="display: none"  value="{{ $item->article_id }}" class="skin-square check" type="checkbox">
                            </th>
                            <th scope="row">
                                <div class="media align-items-center">
                                    <div>
                                        <div class="avatar-parent-child" style="width: max-content">
                                            <img style="object-fit: cover;" src="{{ asset($item->article_avatar) }}" class="avatar rounded-circle">
                                        </div>
                                    </div>
                                    <div class="media-body ml-4">
                                        <a href="{{ route('article.edit', $item->article_id) }}" class="name h6 mb-0 text-sm">{{$item->article_title}}</a>
                                        <small class="d-block font-weight-bold">#{{$item->article_id}}</small>
                                    </div>
                                </div>
                            </th>
                            <td scope="row">
                            <span class="uk-label {{
                                $item->article_publish == 1 ? 'uk-label-success' : 'uk-label-error'
                                }} ">{{
                                $item->article_publish == 1 ? 'Công khai' : 'Ẩn'
                                }}</span>
                            </td>
                            {{-- <td>{{$item->created_at}}</td> --}}
                            <td class="text-right">
                                <!-- Actions -->
                                <div class="actions ml-3">
                                    {{-- <a href="{{ route('topic.show', $item->topic_id) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Xem chi tiết">
                                        <i class="uil-external-link-alt text-info"></i> </a> --}}
                                    <a href="{{ route('article.edit', $item->article_id) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chỉnh sửa">
                                        <i class="uil-pen text-warning"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                       
                    </tbody>
                </table>
                @if ($articleList->isEmpty())
                <div class="empty-data">Không có dữ liệu</div>
                @else
                <div class="text-center" style="float:right; ">
                    {{ $articleList->links('vendor.pagination.default') }}
                </div>
                @endif
            </div>

        </div>

    </div>


</div>
@endsection

@section('js')
<script async src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script async src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script async src="{{ asset('assets/admin/js/custom-icheck.js') }}"></script>
<script>
    $(function() {

       (function() {
            var url_string = window.location.href; 
           
            var url = new URL(url_string);
            var paramValue = url.searchParams.get("category_id");
           
            if(!paramValue) {
                $('#all').addClass('uk-active');
            } else {
                $('.uk-search').append(`<input name='category_id' type='hidden' value='${paramValue}' />`)
                $(`#${paramValue}`).addClass('uk-active');
            }
        })();
        $('.btn-update').on('click', function() {
            var check_parrent = $('.checked');
            var count_item = check_parrent.length;
            if (count_item === 0) {
                UIkit.modal.alert("Không có danh mục nào được chọn !", {
                    labels: {
                        ok: "Đóng"
                    }
                });
            } else {
                var id = $(check_parrent[0]).children('input').val();
                window.location.assign(`category/${id}/edit`);
            }
        });

        $('.btn-delete').on('click', function() {
            var check_parrent = $('.checked');
            var count_item = check_parrent.length;
            if (count_item === 0) {
                UIkit.modal.alert("Không có danh mục nào được chọn !", {
                    labels: {
                        ok: "Đóng"
                    }
                });
            } else {
                var ids = [];
                var from = $('#form-category-delete');
                $.each(check_parrent.children('input'), function(i, item) {
                    ids.push($(item).val());
                });
                ids = ids.join(',');
                from.attr('action', function(i, value) {
                    return `${value}?ids=${ids}`;
                });
                from.submit();
            }

        });
        $("#delete-article").on("click", function (e) {
        $check = $(".checked");
        if ($check.length === 0 ) {
            UIkit.modal.alert("Không có bài viết nào được chọn !", {
                    labels: {
                        ok: "Đóng"
                    }
                });
        }
        var ids = [];
        $.each($check, function (key, value) {
            var id = $(value).children("input").val();
            id != 'on' ?  ids.push( id  ) : '';
        });
        $("#form-article-delete").attr("action", function (index, value) {
            ids = ids.join(",");
            return `${value}?ids=${ids}`;
        });
        if(!ids.length) { return; }
        UIkit.modal
            .confirm("Bạn có chắc muốn xoá !", {
                labels: { ok: "Xoá", cancel: "Huỷ" },
            })
            .then(
                function () {
                    $("#form-article-delete").submit();
                },
                function (err) {}
            );
    });

    });

</script>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .empty-data {
        text-align: center;
        padding: 20px;
        padding-top: 0;
    }

    .image-preview {
        height: 282px;
        width: 100%;
        object-fit: cover;
        border: 1px solid #eceff5;
        border-radius: 7px;
        box-shadow: 0 0.010416667in 0.25pc 0in rgba(0, 0, 0, .12);

    }

    .color-purge {
        background-color: #3e416d !important
    }

    .wrapper-img {
        padding-top: 6px;
        padding-bottom: 4px;
        vertical-align: top;
        position: relative;
    }

</style>
@endsection

@extends('layouts.app')
@section('head')
<title>Danh sách bài viết</title>
@endsection
@section('breadcrumbs')
<nav id="breadcrumbs">
    <ul>
        <li><a href="/"> Trang chủ </a></li>
        <li><a href="{{ route('article_list') }}">Bài viết</a></li>
    </ul>
</nav>
@endsection

@section('content')
<div class="container">
    <h1> Danh sách bài viết </h1>
    <div uk-toggle="cls: uk-flex uk-flex-between@m uk-flex-middle; mode: media; media: @m" class="uk-flex uk-flex-between@m uk-flex-middle">

        <div class="section-header-right">
            <a href="javascript:void(0)" uk-toggle="target: #course-filter" title="" aria-expanded="false" type="button" class="btn btn-icon-label btn-outline-dark">
                <span class="btn-inner--icon">
                    <i class="icon-feather-filter"></i>
                </span>
                <span class="btn-inner--text">Bộ lọc</span>
            </a>
        </div>
    </div>
    <div class="uk-grid-large uk-grid">
        <div class="uk-width-1-3@m uk-first-column">
            <div class="uk-card-default rounded mt-4">

                <ul class="uk-child-width-expand uk-tab" uk-switcher="animation: uk-animation-fade">
                    <li class="uk-active"><a href="#" aria-expanded="true">Bài viết mới</a></li>
                    <li><a href="#" aria-expanded="false">Bài viết hay</a></li>
                </ul>

                <ul class="uk-switcher" style="touch-action: pan-y pinch-zoom;">
                    <!-- tab 1 -->
                    <li class="uk-active">
                        <div class="py-3 px-4">
                            @foreach ( $listNew as $item)
                            <div class="uk-grid-small uk-grid" uk-grid="">
                                <div class="uk-width-expand uk-first-column">
                                    <a href="{{ route('article_detail',$item->article_slug) }}">
                                        <p>{{ $item->article_title}}</p>
                                        
                                    </a>
                                </div>
                                <div class="uk-width-1-3">
                                    <img src="{{ asset($item->article_avatar) }}" class="rounded-sm">
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </li>

                    <!-- tab 2 -->
                    <li>
                        <div class="py-3 px-4">

                            @foreach ( $listHot as $item)
                            <div class="uk-grid-small uk-grid" uk-grid="">
                                <div class="uk-width-expand uk-first-column">
                                    <a href="{{ route('article_detail',$item->article_slug) }}">
                                        <p>{{ $item->article_title}}</p>
                                        
                                    </a>
                                </div>
                                <div class="uk-width-1-3">
                                    <img src="{{ asset($item->article_avatar) }}" class="rounded-sm">
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </li>
                </ul>

            </div>
        </div>
        <div class="uk-width-expand@m">

            @foreach ( $list as $item)
                <div class="blog-article">
                    <a href="{{ route('article_detail',$item->article_slug) }}">
                        <h2>
                            {{ $item->article_title }}
                        </h2>
                    </a>
                    <p class="blog-articl-meta">
                        <strong> {{$item->category->article_category_name }} </strong>
                        <i> {{ date("Y-m-d",strtotime($item->created_at))}} </i>
                       
                       <span class="uil-eye"></span>  {{$item->article_view_count}}
                    </p>
                    <a href="{{ route('article_detail',$item->article_slug) }}">
                        <div class="blog-article-thumbnail">
                            <div class="blog-article-thumbnail-inner">
                                <img src="{{ asset($item->article_avatar) }}" />
                            </div>
                        </div>
                    </a>
                    <p class="blog-article-content">
                    
                        {!! substr(strip_tags($item->article_content),0,204) !!}....
                        
                        <a href="{{ route('article_detail',$item->article_slug) }}">Đọc thêm</a>
                    </p>
                </div>
            @endforeach
            <div style="float:right; ">
                {{ empty($list)? "Không có dữ liệu" : $list->links('vendor.pagination.default') }}
            </div>
        </div>

        <div id="course-filter" uk-offcanvas="flip: true; overlay: true">
            <div class="uk-offcanvas-bar">
                <!-- close button -->
                <button class="uk-offcanvas-close" type="button" uk-close></button>
                <div>
                    <div class="sidebar-filter-contents">
                        <h4> Lọc theo </h4>
                        <form method="get" action="{{ route('article_list')}}">
                            <ul class="sidebar-filter-list" uk-accordion="multiple: true">

                                <li class="uk-open">
                                    <a class="uk-accordion-title" href="#"> Tên bài viết</a>
                                    <div class="uk-accordion-content">
                                        <div class="uk-form-controls">
                                            <input name="keyword" type="text" class="uk-input">
                                        </div>
                                    </div>
                                </li>
                                <li class="uk-open">
                                    <a class="uk-accordion-title" href="#">Danh mục</a>
                                    <div class="uk-accordion-content">
                                        <div class="uk-form-controls">
                                            @foreach ($categories as $item)
                                            <label>
                                                <input value="{{ $item->article_category_id }}" class="uk-radio level_name" type="radio" name="category">
                                                <span class="test">{{ $item->article_category_name}}</span>
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                <li class="uk-margin uk-open">

                                    <div class="uk-accordion-content">
                                        <div class="uk-form-controls">
                                            <button id="filter" class="btn btn-default" type="submit">Lọc</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')

@endsection

@section('js')

@endsection

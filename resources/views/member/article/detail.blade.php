@extends('layouts.app')
@section('head')
@endsection
@section('breadcrumbs')
<nav id="breadcrumbs">
    <ul>
        <li><a href="/"> Trang chủ </a></li>
        <li><a href="{{ route('article_list') }}">Bài viết</a></li>
        <li>{{ $article->article_title}}</li>
    </ul>
</nav>
@endsection
@section('content')
<div class="container">
    <div uk-grid="" class="uk-grid">
        <div class="uk-width-3-4@m uk-first-column">
            <div class="blog-post single-post">
                <!-- Blog Post Thumbnail -->
                <div class="blog-post-thumbnail">
                    <div class="blog-post-thumbnail-inner">
                        <span class="blog-item-tag">{{$article->category->article_category_name}}</span>
                        <img src="{{ asset($article->article_avatar) }}" alt="">
                    </div>
                </div>
                <!-- Blog Post Content -->
                <div class="blog-post-content">

                    <h3> {{ $article->article_title}}</h3>
                    <div class="mb-2">
                        <a href="#" class="blog-post-info">
                            <span class="uil-watch"></span>{{ date("Y-m-d",strtotime($article->created_at))}}
                            <span class="uil-eye"></span>  {{$article->article_view_count}}
                        </a>

                    </div>
                    {!!$article->article_content !!}
                </div>
            </div>
            <h4>Tác giả </h4>
            <div class="uk-card-default rounded px-3 pb-md-3 uk-flex uk-flex-between@m uk-flex-middle" uk-toggle="cls: uk-flex uk-flex-between@m uk-flex-middle; mode: media; media: @m">
                <div class="user-details-card">
                    <div class="user-details-card-avatar">
                        <img src="{{ asset($article->author->profile->user_profile_avatar) }}" alt="">
                    </div>
                    <div class="user-details-card-name">
                        {{$article->author->profile->user_profile_full_name}}
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-1-4@m">
            <h4> Bài viết gợi ý</h4>
            @foreach ($listRandom as $item)
            <a href="{{ route('article_detail',$item->article_slug) }}">
                <div class="uk-card-default mb-4 rounded animate-this uk-inline-clip">
                    <img src="{{ $item->article_avatar}}" alt="">
                    <div class="p-3">
                        <p class="mb-2"> {{ $item->article_title}} </p>
                    </div>
                </div>
            </a>
            @endforeach
        </div>

    </div>
</div>

@endsection


@section('css')

@endsection

@section('js')

@endsection

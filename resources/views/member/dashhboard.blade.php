@extends('layouts.app')

@section('head')
<title>Luyện tập và thi thử toiec cùng Easy Toiec</title>
@endsection

@section('content')


<div class="home-hero" data-src="{{ asset($banner) }}" uk-img>
    <div class="uk-width-1-1">
        <div class="page-content-inner uk-position-z-index">
            <h1>Luyện tập và thi thử toiec</h1>
            <h4 class="my-lg-4">Easy Toiect Chúc bạn đạt được kết quả thật tốt !</h4>
            <a href="{{ route('test_list') }}" class="btn btn-default">Thi thử ngay </a>
        </div>
    </div>
</div>

<div class="container">


    <!-- course card resume sliders  -->
    <div class="section-small">

        <div uk-slider="finite: true" class="course-grid-slider">

            <div class="grid-slider-header">
                <div>
                    <h4 class="uk-text-truncate">Luyện tập</a>
                    </h4>
                </div>
                <div class="grid-slider-header-link">

                    <a href="#" class="slide-nav-prev" uk-slider-item="previous"></a>
                    <a href="#" class="slide-nav-next" uk-slider-item="next"></a>

                </div>
            </div>

            <ul class="uk-slider-items uk-child-width-1-3 uk-child-width-1-2@s uk-child-width-1-4@m uk-grid">
                @foreach ($listPart as $item)
                <li>
                    <a href="{{ route('part_practices', ['slug'=>$item->part_slug]) }}">
                        <div class="course-card-resume">
                            <div class="course-card-resume-thumbnail text-center" style="top: 20px">
                                <img src="{{ asset( $item->part_avatar) }}">
                            </div>
                            <div class="course-card-resume-body text-center">
                                <h5>{{  $item->part_name }}</h5>
                                {{-- <span class="number">{{ $item->questions->practice->count() }} đề luyện tập </span> --}}
                                <div class="course-progressbar">
                                    <div class="course-progressbar-filler" style="width:100%"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>

        </div>

    </div>


    <div class="section-small pt-0">

        <div class="course-grid-slider" uk-slider="finite: true">

            <div class="grid-slider-header">
                <div>
                    <h4 class="uk-text-truncate"> Bài giảng</h4>
                </div>
                <div class="grid-slider-header-link">

                    <a href="{{ route('lesson_list') }}" class="button transparent uk-visible@m">Xem tất cả</a>
                    <a href="#" class="slide-nav-prev" uk-slider-item="previous"></a>
                    <a href="#" class="slide-nav-next" uk-slider-item="next"></a>

                </div>
            </div>

            <ul class="uk-slider-items uk-child-width-1-4@m uk-child-width-1-3@s uk-grid">
              
                @foreach ($listLesson as $item)
                <li>
                    <a href="{{ route('lesson_detail', ['slug'=>$item->lesson_slug]) }}">
                        <div class="course-card">
                            <div class="course-card-thumbnail ">
                                <img src="{{ asset($item->lesson_avatar) }}">
                                <span class="play-button-trigger"></span>
                            </div>
                            <div class="course-card-body">
                                <div class="course-card-info">
                                    <div>
                                        <span class="catagroy">{{$item->lessonParts->count()}} bài học</span>
                                        </div>
                                    <div>
                                        <i class="uil-play-circle icon-small"></i>
                                    </div>
                                </div>

                                <h4 uk-tooltip="{{$item->lesson_title}}">{{$item->lesson_title}}</h4>
                            </div>

                        </div>
                    </a>
                </li>
                @endforeach

            </ul>

        </div>

    </div>


    <div class="section-small pt-0">

        <div class="course-grid-slider" uk-slider="finite: true">

            <div class="grid-slider-header">
                <div>
                    <h4 class="uk-text-truncate">Từ vựng theo chủ đề</h4>
                </div>
                <div class="grid-slider-header-link">

                    <a href="{{ route('topic_list') }}" class="button transparent uk-visible@m"> Xem tất cả </a>
                    <a href="#" class="slide-nav-prev" uk-slider-item="previous"></a>
                    <a href="#" class="slide-nav-next" uk-slider-item="next"></a>

                </div>
            </div>

            <ul class="uk-slider-items uk-child-width-1-4@m uk-child-width-1-3@s uk-grid">
            
                @foreach ($listTopic as $item)
                <li>
                    <a href="{{route('vocabulary_list',['slug'=> $item->topic_slug])}}">
                        <div class="course-card episode-card animate-this">
                            <div class="course-card-thumbnail ">


                            <img src="{{ asset($item->topic_avatar) }}">
                                <span class="play-button-trigger"></span>
                            </div>
                            <div class="course-card-body">
                                <h4 class="mb-0">{{$item->topic_name}} </h4>
                            </div>
                        </div>
                    </a>
                </li>

                @endforeach
            </ul>

        </div>

    </div>


    <div class="section-small pt-0">

        <div class="course-grid-slider" uk-slider="finite: true">

            <div class="grid-slider-header">
                <div>
                    <h4 class="uk-text-truncate">Bài viết</h4>
                </div>
                <div class="grid-slider-header-link">

                    <a href="{{ route('article_list') }}" class="button transparent uk-visible@m"> Xem tất cả </a>
                    <a href="#" class="slide-nav-prev" uk-slider-item="previous"></a>
                    <a href="#" class="slide-nav-next" uk-slider-item="next"></a>

                </div>
            </div>

            <ul class="uk-slider-items uk-child-width-1-4@m uk-child-width-1-3@s uk-grid">
                @foreach ($listArticle as $item)
                <li>
                    <a href="{{ route('article_detail', ['slug'=>$item->article_slug]) }}">
                        <div class="course-card">
                            <div class="course-card-thumbnail ">
                                <img src="{{ asset( $item->article_avatar) }}">
                                <span class="play-button-trigger"></span>
                            </div>
                            <div class="course-card-body">
                                <div class="course-card-info">
                                    <div>
                                    <span class="catagroy">{{$item->category->article_category_name}}</span>
                                    </div>
                                    <div>
                                        {{$item->article_view_count}}
                                        <span class="uil-eye"></span>
                                    </div>
                                </div>

                                <h4 uk-tooltip="{{$item->article_title}}">{{$item->article_title}}</h4>
                            </div>

                        </div>
                    </a>

                </li>
                @endforeach
            </ul>

        </div>

    </div>


</div>
@include('component.modals.user.new_member');
@endsection

@section('css')
<link
rel="stylesheet"
href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
/>
<style>
    .hollow-dots-spinner, .hollow-dots-spinner * {
      box-sizing: border-box;
    }

    .hollow-dots-spinner {
      height: 15px;
      margin: 0 auto;
      width: calc(30px * 3);
    }

    .hollow-dots-spinner .dot {
      width: 15px;
      height: 15px;
      margin: 0 calc(15px / 2);
      border: calc(15px / 5) solid #3e416d;
      border-radius: 50%;
      float: left;
      transform: scale(0);
      animation: hollow-dots-spinner-animation 1000ms ease infinite 0ms;
    }

    .hollow-dots-spinner .dot:nth-child(1) {
      animation-delay: calc(300ms * 1);
    }

    .hollow-dots-spinner .dot:nth-child(2) {
      animation-delay: calc(300ms * 2);
    }

    .hollow-dots-spinner .dot:nth-child(3) {
      animation-delay: calc(300ms * 3);

    }

    @keyframes hollow-dots-spinner-animation {
      50% {
        transform: scale(1);
        opacity: 1;
      }
      100% {
        opacity: 0;
      }
    }
</style>
@endsection
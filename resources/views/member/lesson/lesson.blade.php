@extends('layouts.app')
@section('head')
    
@endsection
@section('content')
<div class="container">

    <div class="mt-lg-4 uk-grid" uk-grid="">
        <div class="uk-width-1-4@m uk-first-column">
            <img src="{{ asset( $lesson->lesson_avatar) }}" alt="" class="rounded shadow">
        </div>
        <div class="uk-width-expand">
            <p class="my-0 uk-text-small">Tên bài giảng</p>
            <h3 class="mt-0">{{ $lesson->lesson_title }}</h3>
            <p>
                {{ $lesson->lesson_description }}
                 <br/>
            
            </p>
        </div>
    </div>

    <div class="course-path-info my-4 my-lg-5">
        <h4 class="uk-text-bold">Với khóa học này bạn sẽ học được </h4>
        {!!  $lesson->lesson_long_description !!}
    </div>

    <ul class="course-path-level uk-accordion" uk-accordion="">

        <li class="uk-open">
            <a class="uk-accordion-title" href="#">Nội dung các phần của bài giảng</a>
            <div class="uk-accordion-content" aria-hidden="false">
            
                <div class="path-wrap">

                    <div class="course-grid-slider uk-slider uk-slider-container" uk-slider="finite: true">

                        <ul class="uk-slider-items uk-child-width-1-3@m uk-child-width-1-5@m uk-grid-match uk-grid" style="transform: translate3d(-257px, 0px, 0px);">
                          @foreach ($lesson->lessonParts as $item)
                                <li tabindex="-1" class="">
                                    <div class="course-card completed">
                                        <div class="course-card-thumbnail">
                                            <img src="{{ asset( $lesson->lesson_avatar) }}">
                                            <a href="{{ route('lesson_part_detail',['slug'=>$lesson->lesson_slug,'part'=>$item->lesson_part_slug]) }}" class="play-button-trigger show"> </a>
                                            {{-- <span class="duration">5:39</span> --}}
                                        </div>
                                        <div class="course-card-body">
                                            <span class="completed-text"> Hoàn thành </span>
                                            <h4> Xem ngay </h4>
                                            <p>{{ $item->lesson_part_title}}</p>
                                        </div>
                                    </div>
                                </li>  
                          @endforeach
                           
                        </ul>

                        <a class="uk-position-center-left uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                        <a class="uk-position-center-right uk-position-small uk-hidden-hover slidenav-next uk-invisible" href="#" uk-slider-item="next"></a>

                    </div>

                </div>

            </div>
        </li>

        

    </ul>

</div>
@endsection

@section('js')
    
@endsection

@section('css')
    
@endsection
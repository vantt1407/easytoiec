@extends('layouts.app')
@section('head')
    
@endsection
@section('breadcrumbs')
<nav id="breadcrumbs">
    <ul>
        <li><a href="/"> Trang chủ </a></li>
        <li><a href="{{ route('lesson_list') }}">Bài giảng</a></li>
    </ul>
</nav>
@endsection
@section('content')
<div class="container">
    <h1>Bài giảng</h1>
    <div class="section-header pb-0 mt-5">
        <div class="section-header-left">
            <h4> Danh sách bài giảng </h4>
        </div>
        <div class="section-header-right">
            <a href="javascript:void(0)" uk-toggle="target: #course-filter" title="" aria-expanded="false" type="button" class="btn btn-icon-label btn-outline-dark">
                <span class="btn-inner--icon">
                    <i class="icon-feather-filter"></i>
                </span>
                <span class="btn-inner--text">Bộ lọc</span>
            </a>
        </div>
    </div>

    <div class="section-small">

        <div class="uk-child-width-1-4@m uk-child-width-1-3@s course-card-grid uk-grid-match" uk-grid>
            @foreach ($list as $item)
            <div>
                <a href="{{ route('lesson_detail', ['slug'=>$item->lesson_slug]) }}">
                    <div class="course-card">
                        <div class="course-card-thumbnail ">
                            <img src="{{ asset($item->lesson_avatar) }}">
                            <span class="play-button-trigger"></span>
                        </div>
                        <div class="course-card-body">
                            <div class="course-card-info">
                                <div>
                                <span class="catagroy">{{$item->lessonParts->count()}} bài học</span>
                                    </div>
                                <div>
                                    <i class="uil-play-circle icon-small"></i>
                                </div>
                            </div>

                            <h4 uk-tooltip="{{$item->lesson_title}}">{{$item->lesson_title}}</h4>
                        </div>

                    </div>
                </a>
            </div>
            @endforeach
          
            
        </div>

    </div>


    <!-- pagination menu -->
        @if ($list->isEmpty())
        <div class="empty-data">Không có dữ liệu</div>
        @else
        <div class="text-center" style="float:right">
            {{ $list->links('vendor.pagination.default') }}
        </div>
        @endif

    <div id="course-filter" uk-offcanvas="flip: true; overlay: true">
        <div class="uk-offcanvas-bar">

            <!-- close button -->
            <button class="uk-offcanvas-close" type="button" uk-close></button>

            <div>

                <div class="sidebar-filter-contents">


                    <h4> Lọc theo </h4>
                    <form method="get" action="{{ route('lesson_list')}}">
                    <ul class="sidebar-filter-list" uk-accordion="multiple: true">

                        <li class="uk-open">
                            <a class="uk-accordion-title" href="#"> Mức điểm </a>
                            <div class="uk-accordion-content">
                                <div class="uk-form-controls">
                                    @foreach ($levels as $item)
                                    <label>
                                    <input value="{{ $item->level_id }}" class="uk-radio level_name" type="radio" name="level">
                                        <span class="test">{{ $item->level_name}} Điểm </span>
                                    </label>
                                     @endforeach
                                    
                                </div>
                            </div>
                        </li>

                        <li class="uk-open">
                            <a class="uk-accordion-title" href="#"> Tên bài giảng</a>
                            <div class="uk-accordion-content">
                                <div class="uk-form-controls">
                                        <input name="keyword" type="text" class="uk-input">
                                </div>
                            </div>
                        </li>

                        <li class="uk-margin uk-open">
                            
                            <div class="uk-accordion-content">
                                <div class="uk-form-controls">
                                    <button id="filter" class="btn btn-default" type="submit">Lọc</button>
                                </div>
                            </div>
                           
                        </li>


                    </ul>
                    </form>


                </div>

            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
    
@endsection

@section('css')
    
@endsection
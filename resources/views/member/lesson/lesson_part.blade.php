@extends('layouts.app')
@section('head')
@endsection
@section('content')
<div class="uk-grid-collapse uk-grid" uk-grid="">
    <div class="uk-width-3-4@m bg-white uk-first-column">

        <div class="embed-video">

            @if ($partItem->lesson_video)
            <video controls preload="auto" width="100%" height="100%" data-setup="{}">
                <source src="{{ asset($partItem->lesson_video) }}" />

                <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a
                    web browser that
                    <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                </p>
            </video>
            @else
            {!! $partItem->lesson_part_content !!}
            @endif
        </div>
        <!-- video description contents -->
        <div class="p-lg-5 p-3">
            <h2>{{ $partItem->lesson_part_title}}</h2>

            <div class="uk-grid-small uk-grid" uk-grid="">
                <div class="uk-width-auto uk-first-column">
                    <span>
                        {{ date("Y-m-d",strtotime($partItem->created_at) )}}
                    </span>
                </div>
            </div>

            <hr class="my-2" />
            @if ($partItem->lesson_video)
            <h4 class="mt-4">Nội dung</h4>

            <p class="lead">
                {!! $partItem->lesson_part_content !!}
            </p>

            @endif
            <h4 class="mt-4">Bài tập thực hành</h4>

            <p class="lead">
                @if ( $partItem->questionGroups->count() > 0)
                    @foreach ($partItem->questionGroups as $p)
                    <div class="uk-form-group">
                        <div class="uk-position-relative w-100" style="display: {{ !empty($p->question_group_image) ? "block" :"none"}}">
                            <div class="text-center">
                                <img src="{{ !empty($p->question_group_image) ? asset($p->question_group_image) : '#'}}" class="w-100" />
                            </div>
                            <div class="text-center" style="display: {{ !empty($p->question_group_audio) ? "block" :"none"}}">
                                <audio class="w-100">
                                    <source src="{{ !empty($p->question_group_audio) ? asset($p->question_group_audio) : '#'}}">
                                </audio>

                            </div>
                        </div>
                        <div class="uk-position-relative w-100">
                            <label class="uk-form-label">
                                {!! $p->question_group_question !!}
                            </label>
                        </div>
                    </div>

                        @foreach ( $p->questions as $q)

                        <div class="uk-position-relative w-100">
                            <label class="uk-form-label"> {{ $q->question }}</label>
                            <input type="hidden" name="question[]" value="{{$q->question_id}}" />
                            <div class="uk-position-relative uk-first-column">
                                @foreach ($q->answers as $ia => $a)
                                <input data-identity="{{$a->answer_id}}" data-check=" {{ $a->answer_is_correct ? 1 : 0 }}" class="skin-square awnser" type="radio" name="{{$q->question_id}}" value="{{$a->answer_id}}">
                                <span id="{{$a->answer_id}}">
                                    {{ $a->answer_option }}
                                </span> <br/>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    @endforeach
                @else

                    @foreach ( $partItem->questions as $q)
                    <div class="uk-position-relative w-100" style="display: {{ !empty($q->question_image) ? "block" :"none"}}">
                        <div class="text-center">
                            <img src="{{ !empty($p->question_group_image) ? asset($q->question_image) : '#'}}" class="w-100" />
                        </div>
                        <div class="text-center" style="display: {{ !empty($q->question_audio) ? "block" :"none"}}">
                            <audio class="w-100">
                                <source src="{{ !empty($p->question_audio) ? asset($p->question_audio) : '#'}}">
                            </audio>

                        </div>
                    </div>
                    <div class="uk-position-relative w-100">
                        <label class="uk-form-label">{{ $q->question }}</label>
                        <input type="hidden" name="question[]" value="{{$q->question_id}}" />
                        <div class="uk-position-relative uk-first-column">
                            @foreach ($q->answers as $ia => $a)
                            <input data-identity="{{$a->answer_id}}" data-check="{{ $a->answer_is_correct ? 1 : 0 }}" class="skin-square awnser" type="radio" name="{{$q->question_id}}" value="{{$a->answer_id}}">
                            <span id="{{$a->answer_id}}">
                                {{ $a->answer_option }}
                            </span>  <br/>
                           
                            @endforeach
                        </div>
                    </div>
                    @endforeach

                @endif

              
            </p>
            <hr/>
            <button id="show-answer" class="btn btn-sm btn-default">Xem kết quả</button>
            <button onclick="window.location.reload()" id="reload" class="btn btn-sm btn-default hide">Làm lại</button>
        </div>
    </div>

    <!-- sidebar -->
    <div class="uk-width-1-4@m ">
        <div uk-sticky="" class="uk-sticky uk-sticky-fixed" style="position: fixed; top: 0px; width: 339px;">
            <h5 class="bg-gradient-grey text-white py-4 p-3 mb-0">
                {{$lesson->lesson_title}}
            </h5>

            <ul class="uk-switcher uk-overflow-hidden" style="touch-action: pan-y pinch-zoom;">
                <!-- first tab -->

                <li class="uk-active">
                    <div class="vidlist-3-content" data-simplebar="init">
                        <div class="simplebar-wrapper" style="margin: 0px;">
                            <div class="simplebar-height-auto-observer-wrapper">
                                <div class="simplebar-height-auto-observer"></div>
                            </div>
                            <div class="simplebar-mask">
                                <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                    <div class="simplebar-content" style="
                                            padding: 0px;
                                            height: 100%;
                                            overflow: hidden scroll;
                                        ">
                                        <ul class="vidlist-3-section uk-accordion" uk-accordion="">
                                            <!--  section 1  -->
                                            <li class=" uk-open">
                                                <a class="uk-accordion-title" href="#">
                                                    Danh sách các phần
                                                </a>
                                                <div class="uk-accordion-content" aria-hidden="true" hidden="">
                                                    <!-- vidlist -->
                                                    <ul class="vidlist-3">
                                                        @foreach ($partList as $item)
                                                        <li class="{{$part == $item->lesson_part_slug ? 'uk-active':'' }}">
                                                            <a href="{{ route('lesson_part_detail',['slug'=>$lesson->lesson_slug,'part'=>$item->lesson_part_slug]) }}" aria-expanded="true">{{ $item->lesson_part_title }}</a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="simplebar-placeholder" style="width: 339px; height: 397px;"></div>
                        </div>
                        <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                            <div class="simplebar-scrollbar" style="
                                    transform: translate3d(0px, 0px, 0px);
                                    visibility: hidden;
                                "></div>
                        </div>
                        <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
                            <div class="simplebar-scrollbar" style="
                                    transform: translate3d(0px, 0px, 0px);
                                    visibility: visible;
                                    height: 204px;
                                "></div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="uk-sticky-placeholder" style="height: 409px; margin: 0px;"></div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/admin/css/step.css') }}">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .iradio_square-purple {
        margin-right: 10px;
    }

</style>
@endsection
@section('js')
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script>
    $(function() {
        $('#reload').hide();
        var radioBoxes = $('.skin-square');
        radioBoxes.iCheck({
            radioClass: 'iradio_square-purple'
            , increaseArea: '20%'
        });

       $('#show-answer').on('click',function() {

        var trueIcon = '<i class="uil-check"></i>';

            $.each($('input'), function(index,item) {

                if($(item).is(':checked')) {
                    let id = $(item).data('identity');
                    let isTrue = $(item).data('check');

                    if(isTrue == 1) {
                        $('#'+id).css('color','green');
                        $('#'+id).append($(trueIcon));
                    } else {
                        $('#'+id).css('color','red');
                        $('#'+id).append($('<i class="uil-times"></i>'));
                    }
                } else {
                    let id = $(item).data('identity');
                    let isTrue = $(item).data('check');

                    if(isTrue == 1) {
                        $('#'+id).css('color','green');
                        $('#'+id).append($(trueIcon));
                    }
                }
            })
            $('#reload').show();
            $('#show-answer').hide();
       });
    });

</script>
@endsection

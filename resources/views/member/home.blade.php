<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Easy Toiec - Học và thi toiec mọi lúc mọi nơi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="images/logo.png">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    {{-- <link rel="shortcut icon" sizes="196x196" href="images/logo.png"> --}}
    <link rel="stylesheet" href="{{ asset('assets/member/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/member/style.css') }}" type="text/css" />
    <style>
        .photo {
            text-align: center;
        }

    </style>
    <style>
        html,
        body {
            font-family: 'Roboto', sans-serif !important;
        }

        .nav-wrapper {
            transition: 0.5s;
        }

        .p-88 {
            padding-top: 88px;
        }

        .fixed-header {
            position: fixed;
            width: 100%;
            z-index: 9999999;
            background-color: rgba(0, 0, 0, 0.5);
        }

    </style>
</head>

<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=616136309021537&autoLogAppEvents=1" nonce="G651cujh"></script>
    <div class="app" id="app" style="background: #33d0ff;">

        <!-- ############ LAYOUT START-->

        <div class="p-a black nav-wrapper">
            <div class="navbar">
                <a data-toggle="collapse" data-target="#navbar" class="navbar-item pull-right hidden-md-up m-a-0 m-l">
                    <i class="ion-android-menu"></i>
                </a>
                <!-- brand -->
                <a href="index.html" class="navbar-brand">
                    <div data-ui-include="'images/logo.svg'"></div>
                    <img src="images/logo.png" alt="." class="hide">
                    <span class="hidden-folded inline">Easy Toiec</span>
                </a>
                <!-- / brand -->

                <!-- navbar collapse -->
                <div class="collapse navbar-toggleable-sm pull-right pull-none-xs" id="navbar">
                    <!-- link and dropdown -->
                    <ul class="nav navbar-nav text-info-hover" data-ui-nav>

                        <li class="nav-item">
                            <a href="{{ route('get_register') }}" class="nav-link">
                                <span class="nav-text text-info">
                                    Đăng ký
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('get_login') }}" class="nav-link">
                                <span class="btn btn-md rounded info">
                                    Đăng nhập
                                </span>
                            </a>
                        </li>
                    </ul>
                    <!-- / link and dropdown -->
                </div>
                <!-- / navbar collapse -->
            </div>
        </div>

        <!-- content -->
        <div id="content" class="app-content" role="main">
            <div class="app-body">
                <!-- ############ PAGE START-->
                <div class="container-fluid">
                    <div class="row black">
                        <div class="col-sm-3 "></div>
                        <div class="col-sm-6 text-center ">
                            <div class="p-t-lg">
                                <h1 class="m-y-md text-white"> {{ $config->slogan}} </h1>
                                {{-- <h6 class="text-muted m-b-lg">Học mọi lúc mọi nơi</h6>  --}}
                                <a href="{{ route('get_login') }}" class="btn btn-lg rounded success p-x-md m-x">Thử ngay</a>

                            </div>
                        </div>
                        <div class="col-sm-3 "></div>
                        <div class="col-md-12 col-lg-12" style="background: url(' {{ asset('/assets/member/imgs/couple_bg.webp') }} ') no-repeat;background-size: cover;background-color: #9ec268">
                            <img src="{{ asset('/assets/member/imgs/boy.webp') }}">
                            <img src="{{ asset('/assets/member/imgs/girl.webp') }}">
                            <img src="{{ asset('/assets/member/imgs/couple.webp') }} ">
                        </div>

                    </div>
                </div>
                <div class="p-y-lg b-b box-shadow-z0 dark-white" id="features">
                    <div class="container p-y-md">
                        <h4 class="text-center m-b-lg">EASY TOEIC có tất cả mọi thứ bạn cần
                            để luyện thi TOEIC hiệu quả</h4>
                        <div class="row">
                            <div class="col-sm-4 m-y-md text-center">

                                <h6 class="_600 m-y">Lộ trình học giúp bạn tăng điểm nhanh</h6>
                                <div class="photo">
                                    <img src="{{ asset('assets/images/icon_lo_trinh_hoc.webp')}}" />
                                </div>
                            </div>
                            <div class="col-sm-4 m-y-md text-center ">
                                <span class="text-muted">
                                    <i class="ion-ios-paper-outline text-3x"></i>
                                </span>
                                <h6 class="_600 m-y">600 từ vựng TOEIC
                                    thiết yếu</h6>
                                <div class="photo">
                                    <img src="{{ asset('assets/images/icon_600_tu_vung.webp')}}" />
                                </div>
                            </div>
                            <div class="col-sm-4 m-y-md text-center">
                                <span class="text-muted">
                                    <i class="ion-ios-pie-outline text-3x"></i>
                                </span>
                                <h6 class="_600 m-y">50 chủ điểm ngữ pháp quan trọng</h6>
                                <div class="photo">
                                    <img src="{{ asset('assets/images/icon_17_chu_diem_ngu_phap.webp')}}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 m-y-md text-center">

                                <h6 class="_600 m-y">Mẹo làm bài TOEIC</h6>
                                <div class="photo">
                                    <img src="{{ asset('assets/images/icon_meo_lam_bai_tap.webp')}}" />
                                </div>
                            </div>
                            <div class="col-sm-4 m-y-md text-center">
                                <span class="text-muted">
                                    <i class="ion-social-html5-outline text-3x"></i>
                                </span>
                                <h6 class="_600 m-y">Hỗ trợ người học
                                    nhiệt tình</h6>
                                <div class="photo">
                                    <img src="{{ asset('assets/images/icon_ho_tro_nguoi_hoc_nhiet_tinh.webp')}}" />
                                </div>
                            </div>
                            <div class="col-sm-4 m-y-md text-center">
                                <span class="text-muted">
                                    <i class="ion-social-javascript-outline text-3x"></i>
                                </span>
                                <h6 class="_600 m-y">Học chăm có thưởng</h6>
                                <div class="photo">
                                    <img src="{{ asset('assets/images/icon_hoc_cham_co_thuong.webp')}}" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- ############ PAGE END-->

            </div>
        </div>
        <div class="footer black text-info-hover" style="background-color:#3e416d !important">
            <div class="container">
                <div class="row p-y-lg">
                    <div class="col-md-5">

                        <h6 class="text-sm text-u-c m-b text-muted">Về chúng tôi</h6>
                        <ul class="nav l-h-2x">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Địa chỉ: {{ $config->address}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Số điện thoại:{{ $config->phone}} </a>
                            </li>
                        </ul>

                        <div class="text-muted m-y-lg">
                            <h2 class="text-muted _600">
                                <span class="text-muted">EASY TOEIC</span>

                            </h2>
                            <p>
                                {{ $config->slogan}}
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <h6 class="text-sm text-u-c m-b text-muted">Menu</h6>
                        <ul class="nav l-h-2x">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('get_login') }}">Đăng nhập </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('get_register') }}">Đăng ký </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <h6 class="text-sm text-u-c m-b text-muted">Fanpage</h6>
                        <div class="fb-page" data-href="https://www.facebook.com/Easy-Toeic-113772147088040/?modal=admin_todo_tour" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/Easy-Toeic-113772147088040/?modal=admin_todo_tour" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Easy-Toeic-113772147088040/?modal=admin_todo_tour">Easy Toeic</a></blockquote>
                        </div>
                    </div>
                </div>
                <div class="p-y-lg">
                    <div class="m-b-lg text-sm">

                        <div class="text-muted pull-right pull-none-xs m-b">
                            <span class="text-muted">&copy; Copyright. All rights reserved.</span>
                        </div>
                        <div>
                            <a href="https://www.facebook.com/Easy-Toeic-113772147088040" class="btn btn-sm btn-icon btn-social rounded lt" title="Facebook">
                                <i class="fa fa-facebook"></i>
                                <i class="fa fa-facebook indigo"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-icon btn-social rounded lt" title="Twitter">
                                <i class="fa fa-twitter"></i>
                                <i class="fa fa-twitter light-blue"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-icon btn-social rounded lt" title="Google+">
                                <i class="fa fa-google-plus"></i>
                                <i class="fa fa-google-plus red-600"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ############ LAYOUT END-->
    </div>
    <!-- build:js scripts/app.min.js -->
    <!-- jQuery -->
    <script src="{{ asset('assets/admin/js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            $(window).scroll(function() {
                if ($(window).scrollTop() > 1) {
                    $('.nav-wrapper').addClass('fixed-header');
                    $('.app-content').addClass('p-88');
                } else {
                    $('.nav-wrapper').removeClass('fixed-header');
                    $('.app-content').removeClass('p-88');
                }
            });
        });

    </script>
</body>

</html>

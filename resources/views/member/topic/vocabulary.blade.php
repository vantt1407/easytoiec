@extends('layouts.app')
@section('head')
    <title>Từ vựng theo chủ đề: {{ $detail->topic_name}}</title>
@endsection
@section('breadcrumbs')
    <nav id="breadcrumbs">
        <ul>
            <li><a href="/"> Trang chủ </a></li>
            <li><a href="#">Từ vựng theo chủ đề</a></li>
            <li>{{ $detail->topic_name}}</li>
        </ul>
    </nav>
@endsection
@section('content')
    <div class="page-content-inner">
        <div class="uk-width-1-1@m">

            <div class="card rounded">
                <div class="p-3 d-flex justify-content-between">
                    <div class="uk-width-1-2@m">
                        <h2 class="mb-0"><b>Chủ đề:</b> {{ $detail->topic_name}} </h2>
                        <p style="padding: 23px 30px 0 0; font-size:18px">
                            {{ $detail->topic_description}}
                        </p>
                    </div>
                    <div class="uk-width-1-2@m">
                        <div class="card ">
                            <img src="{{asset($detail->topic_avatar)}}">
                        </div>
                    </div>

                </div>
                <hr class="m-0">
                <div class="uk-grid-small p-4 uk-grid justify-content-between align-content-between" >
                    <div class="uk-width-1-2@m uk-first-column uk-grid text-center">
                        <h3>Từ vựng</h3>
                    </div>
                </div>
                <hr class="m-0">


                <div class="course-grid-slider uk-slider uk-slider-container" uk-slider="finite: true">

                    <ul class="uk-slider-items uk-child-width-1-1@m  uk-grid-match uk-grid" style="transform: translate3d(-257px, 0px, 0px);">
                      @foreach ($detail->vocabularies  as $item)
                            <li tabindex="-1"  class="uk-width-1-1@s" style="width:100% !important">
                                <div class="uk-grid-small p-4 uk-grid " >
                                    <div class="uk-width-1-2@m uk-first-column uk-grid">
                                        <div class="uk-width-1-2@m">
                                            <img src="{{asset($item->vocabulary_image)}}" class="  rounded" alt="">
                                        </div>
                                        <div class="uk-width-expand@m">
                                            <h4 class="mb-2"> {{$item->vocabulary_word}} <i>({{$item->dataType->vocabulary_type}})</i> </h4>
                                            <h5 class="uk-text-small mb-2">
                                                <span style="cursor: pointer" class="play-audio mr-3 bg-light p-2 mt-1"> {{$item->vocabulary_pronounce}}
                                                       <i class="icon-play icon-feather-play-circle"></i>
                                                        <audio style="display: none" class="audio-hidden">
                                                            <source src="{{asset($item->vocabulary_audio)}}">
                                                        </audio>
                                                </span>
                                            </h5>
                                            <h5 class="mb-0 uk-text-small mt-3">
                                           <span>
                                                {{$item->vocabulary_vi_translate}}
                                           </span>
                                            </h5>
            
                                        </div>
                                    </div>
                                    @if(!empty($item->antonym))
                                        <div class="uk-width-1-2@m uk-first-column uk-grid" style="margin-top: 0 !important;">
                                            <div class="uk-width-1-2@m ">
                                                <img src="{{asset($item->antonym->vocabulary_image)}}" class="  rounded" alt="">
                                            </div>
                                            <div class="uk-width-expand@m ">
                                                <h4 class="mb-2"> {{$item->antonym->vocabulary_word}} <i>({{$item->antonym->dataType->vocabulary_type}})</i> </h4>
                                                <h5 class="uk-text-small mb-2">
                                                <span style="cursor: pointer" class="play-audio mr-3 bg-light p-2 mt-1"> {{$item->antonym->vocabulary_pronounce}}
                                                      <i class="icon-play icon-feather-play-circle"></i>
                                                         <audio style="display: none" class="audio-hidden">
                                                            <source src="{{asset($item->antonym->vocabulary_audio)}}">
                                                        </audio>
                                                </span>
            
                                                </h5>
                                                <h5 class="mb-0 uk-text-small mt-3">
                                           <span>
                                                {{$item->antonym->vocabulary_vi_translate}}
                                           </span>
                                                </h5>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </li>  
                      @endforeach
                       
                    </ul>

                    <a class="uk-position-center-left uk-position-small uk-hidden-hover slidenav-prev" href="#" uk-slider-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover slidenav-next uk-invisible" href="#" uk-slider-item="next"></a>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(function () {
            let pause = 'icon-feather-pause-circle';
            let play = 'icon-feather-play-circle';

            $('.play-audio').on('click',function () {
                let $audio =  $(this).children('.audio-hidden');
                let iconPlay =  $(this).children('.icon-play');

                $audio[0].play();
                iconPlay.removeClass(play);
                iconPlay.addClass(pause);
                $audio.on('ended',function () {
                    iconPlay.removeClass(pause);
                    iconPlay.addClass(play);
                });
            });
        });
    </script>
@endsection

@section('css')

@endsection

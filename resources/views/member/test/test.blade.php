@extends('layouts.app')
@section('head')
<title>{{$test->test_name}}</title>
@endsection
@section('breadcrumbs')
<nav id="breadcrumbs">
    <ul>

        <li>
            <h3 class="mb-0">{{ $test->test_name }}</h3>
        </li>
    </ul>
</nav>
@endsection
@section('content')
<div class="blog-article-single" data-src="{{ asset('storage/photos/shares/test/5ebdb4f900016.jpg') }}" uk-img>
    <div class="container-small">
        <p class="blog-article-meta mb-3">
            <h1><strong> {{ $test->test_name }} </strong></h1>
            <h1><strong> Thời gian làm bài: {{ $test->test_time }} phút </strong></h1>
        </p>
        {{-- <div class="blog-article-auther">
            <button type="button" class="btn btn-danger btn-start">
                Bắt đầu làm bài
            </button>
        </div> --}}
    </div>
</div>
@php 
$rt = \Request::getPathInfo();
@endphp
<div class="container p-0">
    @include('component.modals.user.result_test')
    <!-- Blog Post Content -->
    <form method="post" action="{{ route('submit_test', \Request::get('part') ? \Request::get('part') : 1  ) }}" id="form_submit">
        @csrf
        <div class="container-small blog-article-content-read" id="part1-anchor">
                @php
                $lm = ['I','II','III','IV','V','VI','VII'];
                @endphp
            <input type="hidden" name="test_id" value="{{$test->test_id }}">
            @switch($part_id)
                @case(1)
                     @include('member.test.part.part1',['part1' =>$tree['part1'][0] ]) 
                    
                     <button class="btn btn-sm btn-default" type="submit">Trang sau</a>
                     @break
                @case(2)
                    @include('member.test.part.part2',['part2' => $tree['part2'][0] ])

                    <button class="btn btn-sm btn-default" type="submit">Trang sau</a>
                    @break
                @case(3)
                    @include('member.test.part.part3',['part3' => $tree['part3'] ])

                    <button class="btn btn-sm btn-default" type="submit">Trang sau</a>
                    @break
                @case(4)
                    @include('member.test.part.part4',['part4' => $tree['part4'] ])

                    <button class="btn btn-sm btn-default" type="submit">Trang sau</a>
                    @break
                @case(5)
                    @include('member.test.part.part5',['part5' => $tree['part5'] ])
                   
                    <button class="btn btn-sm btn-default" type="submit">Trang sau</a>
                    @break
                @case(6)
                    @include('member.test.part.part6',['part6' => $tree['part6'] ])
                    
                    <button class="btn btn-sm btn-default" type="submit">Trang sau</a>
                    @break
                @case(7)
                    @include('member.test.part.part7',['part7' => $tree['part7'] ])

                     <input type="hidden" name="end_test" value="end_test">
                     <button class="btn btn-sm btn-default" type="submit">Nộp bài</a>
                    @break
                @default
            @endswitch
        </div>
    </form>
    <!-----config------>
    <div id="part-filter" uk-offcanvas="flip: true; overlay: true">
        <div class="uk-offcanvas-bar">
            <!-- close button -->
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <div>
                <div class="sidebar-filter-contents">
                    <ul class="sidebar-filter-list" uk-accordion="multiple: true">

                        <li class="uk-open">
                            <a class="uk-accordion-title" href="#">
                                <h4>Phần thi</h4>
                            </a>
                            <div class="uk-accordion-content">
                                <div class="uk-form-controls">
                                    @foreach ($parts as $k => $item)
                                    <label>
                                        <a href="{{url()->current()."?part=$item->part_id"}}">
                                        <span class="test"><b>{{$lm[$k]}}</b>.{{$item->part_name}}</span>
                                        </a>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src="{{ asset('js/enjoyhint.min.js') }}"></script>
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>
<script>
    $(function() {


        // if (navigator.userAgent.match(/Android/i) ||
        //     navigator.userAgent.match(/webOS/i) ||
        //     navigator.userAgent.match(/iPhone/i) ||
        //     navigator.userAgent.match(/iPad/i) ||
        //     navigator.userAgent.match(/iPod/i) ||
        //     navigator.userAgent.match(/BlackBerry/i) ||
        //     navigator.userAgent.match(/Windows Phone/i)) {} else {
        //     if (!window.sessionStorage.getItem('isClick')) {
        //         var enjoyhint_instance = new EnjoyHint({});
        //         var enjoyhint_script_steps = [{
        //                 'next .test-time': 'Tổng thời gian làm bài thi'
        //                 , 'nextButton': {
        //                     className: "myNext"
        //                     , text: "Đã hiểu"
        //                 }
        //                 , 'skipButton': {
        //                     className: "mySkip"
        //                     , text: "Dừng!"
        //                 }
        //             }, {
        //                 'next .btn-finish': 'Nộp bài thi'
        //                 , 'nextButton': {
        //                     className: "myNext"
        //                     , text: "Đã hiểu"
        //                 }
        //                 , 'skipButton': {
        //                     className: "mySkip"
        //                     , text: "Dừng!"
        //                 }
        //             }, {
        //                 'next .part-list': 'Danh sách các part trong bài thi, <br>có thể chọn từng part khi đang thi'
        //                 , 'nextButton': {
        //                     className: "myNext"
        //                     , text: "Đã hiểu"
        //                 }
        //                 , 'skipButton': {
        //                     className: "mySkip"
        //                     , text: "Dừng!"
        //                 }
        //             }, {
        //                 'next .exit': 'Thoát bài thi'
        //                 , 'nextButton': {
        //                     className: "myNext"
        //                     , text: "Đã hiểu"
        //                 }
        //                 , 'skipButton': {
        //                     className: "mySkip"
        //                     , text: "Dừng!"
        //                 }
        //             }
        //             , {
        //                 'next .btn-start': 'Bạn đã sẵn sàng. Click vô đây để bắt đầu đếm giờ làm bài'
        //                 , 'nextButton': {
        //                     className: "myNext"
        //                     , text: "OK ! Hiểu "
        //                 }
        //                 , 'skipButton': {
        //                     className: "mySkip"
        //                     , text: "Dừng!"
        //                 }
        //             }
        //         ];
        //         window.sessionStorage.setItem('isClick', true);
        //         enjoyhint_instance.set(enjoyhint_script_steps);
        //         enjoyhint_instance.run();
        //     }
        // }

        var radioBoxes = $('.skin-square');
        radioBoxes.iCheck({
            radioClass: 'iradio_square-purple'
            , increaseArea: '20%'
        });

        $('.exit').on('click', function() {

            UIkit.modal.confirm('Bạn có chắc muốn huỷ bài thi thử !', {
                labels: {
                    cancel: 'Đóng'
                    , ok: 'Huỷ bài '
                }
            }).then(function() {
                window.location.assign("{{route('test_list')}}");
            }, function() {
                console.log('Rejected.')
            });

        });
        $('.btn-finish').on('click', function() {

            UIkit.modal.confirm('Bạn có chắc muốn nộp bài thi thử !', {
                labels: {
                    cancel: 'Đóng'
                    , ok: 'Nộp bài '
                }
            }).then(function() {
                let partId = {{$part_id}}
                if(partId != 7) {
                    $('#form_submit').append('<input type="hidden" name="end_test" value="end_test">');
                }
                $('#form_submit').submit();
            }, function() {
                console.log('Rejected.')
            });

        });
            
           var totalTime =  sessionStorage.getItem('currentTime')

            if(sessionStorage.getItem('currentTime'))
            { 
                totalTime =  parseInt(totalTime);

             } else {

                var testTime = {{ $test->test_time * 60000 }};

                 totalTime = new Date().getTime() + testTime;

                 sessionStorage.setItem('currentTime',totalTime);
             }

            $('div#clock').countdown(totalTime)
                .on('update.countdown', function(event) {
                    var $this = $(this);
                    $this.html(event.strftime('%H:%M:%S'));
                })
                .on('finish.countdown', function() {
                    UIkit.modal.alert("Hết thời gian làm bài !");
                    let partId = {{$part_id}}
                    if(partId != 7) {
                        $('#form_submit').append('<input type="hidden" name="end_test" value="end_test">');
                    }
                    $('#form_submit').submit();
                });

    });

</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/admin/css/step.css') }}">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .iradio_square-purple {
        margin-right: 10px;
    }

</style>
@endsection

@extends('layouts.app')
@section('head')
<title>{{$test->test_name}}</title>
@endsection
@section('breadcrumbs')
<nav id="breadcrumbs">
    <ul>

        <li>
            <h3 class="mb-0">{{ $test->test_name }}</h3>
        </li>
    </ul>
</nav>
@endsection
@section('content')
<div class="blog-article-single" data-src="{{ asset('storage/photos/shares/test/5ebdb4f900016.jpg') }}" uk-img>
    <div class="container-small">
        <p class="blog-article-meta mb-3">
            <h1><strong> {{ $test->test_name }} </strong></h1>
            <h1><strong> Thời gian làm bài: {{ $test->test_time }} phút </strong></h1>
        </p>
        <div class="blog-article-auther">
            <a href="{{ route('test_detail', ['id'=>$test->test_id]) }}"  class="btn btn-danger btn-start">
                Bắt đầu làm bài
            </a>
        </div>
    </div>
</div>


@endsection
@section('js')
<script src="{{ asset('js/enjoyhint.min.js') }}"></script>
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>
<script>
    $(function() {


        if (navigator.userAgent.match(/Android/i) ||
            navigator.userAgent.match(/webOS/i) ||
            navigator.userAgent.match(/iPhone/i) ||
            navigator.userAgent.match(/iPad/i) ||
            navigator.userAgent.match(/iPod/i) ||
            navigator.userAgent.match(/BlackBerry/i) ||
            navigator.userAgent.match(/Windows Phone/i)) {} else {
            if (!window.sessionStorage.getItem('isClick')) {
                var enjoyhint_instance = new EnjoyHint({});
                var enjoyhint_script_steps = [{
                        'next .test-time': 'Tổng thời gian làm bài thi'
                        , 'nextButton': {
                            className: "myNext"
                            , text: "Đã hiểu"
                        }
                        , 'skipButton': {
                            className: "mySkip"
                            , text: "Dừng!"
                        }
                    }, {
                        'next .btn-finish': 'Nộp bài thi'
                        , 'nextButton': {
                            className: "myNext"
                            , text: "Đã hiểu"
                        }
                        , 'skipButton': {
                            className: "mySkip"
                            , text: "Dừng!"
                        }
                    }, {
                        'next .part-list': 'Danh sách các part trong bài thi, <br>có thể chọn từng part khi đang thi'
                        , 'nextButton': {
                            className: "myNext"
                            , text: "Đã hiểu"
                        }
                        , 'skipButton': {
                            className: "mySkip"
                            , text: "Dừng!"
                        }
                    }, {
                        'next .exit': 'Thoát bài thi'
                        , 'nextButton': {
                            className: "myNext"
                            , text: "Đã hiểu"
                        }
                        , 'skipButton': {
                            className: "mySkip"
                            , text: "Dừng!"
                        }
                    }
                    , {
                        'next .btn-start': 'Bạn đã sẵn sàng. Click vô đây để bắt đầu đếm giờ làm bài'
                        , 'nextButton': {
                            className: "myNext"
                            , text: "OK ! Hiểu "
                        }
                        , 'skipButton': {
                            className: "mySkip"
                            , text: "Dừng!"
                        }
                    }
                ];
                window.sessionStorage.setItem('isClick', true);
                enjoyhint_instance.set(enjoyhint_script_steps);
                enjoyhint_instance.run();
            }
        }

        // $('.exit').on('click', function() {

        //     UIkit.modal.confirm('Bạn có chắc muốn huỷ bài thi thử !', {
        //         labels: {
        //             cancel: 'Đóng'
        //             , ok: 'Huỷ bài '
        //         }
        //     }).then(function() {
        //         window.location.assign("{{route('test_list')}}");
        //     }, function() {
        //         console.log('Rejected.')
        //     });

        // });
        // $('.btn-finish').on('click', function() {

        //     UIkit.modal.confirm('Bạn có chắc muốn nộp bài thi thử !', {
        //         labels: {
        //             cancel: 'Đóng'
        //             , ok: 'Nộp bài '
        //         }
        //     }).then(function() {
        //         $('#form_submit').submit();
        //     }, function() {
        //         console.log('Rejected.')
        //     });

        // });

        $('.btn-start').on('click', function() {

        });
          
    });

</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/admin/css/step.css') }}">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .iradio_square-purple {
        margin-right: 10px;
    }

</style>
@endsection

@extends('layouts.app')
@section('head')

@endsection
@section('content')
<div class="container">
    <div>
        <h1 class="mb-2"> ONLINE TOEIC TEST - Thi Thử TOEIC Online Miễn Phí </h1>

        <p style="font-size: 18px">TOEIC (viết tắt của Test of English for International Communication – Bài kiểm tra tiếng Anh giao tiếp quốc tế)
            là một chứng chỉ được sử dụng phổ biến nhằm đánh giá trình độ sử dụng tiếng Anh trong môi trường giao tiếp quốc
            tế. Ở Việt Nam những năm gần đây, chứng chỉ TOEIC nổi lên như một tiêu chuẩn phổ biến để đánh giá trình độ thông
            thạo tiếng Anh của người lao động được nhiều doanh nghiệp, tổ chức lựa chọn. Ngoài ra TOEIC còn là chuẩn đầu ra
            tiếng Anh của nhiều trường đại học cho sinh viên khi tốt nghiệp</p>

        <p style="font-size: 18px">Để thuận tiện cho các bạn ôn luyện và thi lấy chứng chỉ TOEIC, EasyToeic cung cấp các bài thi thử TOEIC online
            miễn phí. Với các đề thi TOEIC được biên soạn bài bản tương tự các đề thi thực tế giúp mọi người dễ dàng làm
            quen và đánh giá trình độ hiện tại. Từ đó có kế hoạch luyện thi TOEIC phù hợp</p>

        <p style="font-size: 18px">Đề thi thử TOEIC được thực hiện theo format mới, có chấm điểm và giải thích chi tiết giúp bạn đánh giá chính xác
            điểm TOEIC hiện tại đặc biệt website dễ sử dụng và hoàn toàn miễn phí phù hợp với tất cả mọi người</p>
    </div>
    <h2 class="mb-2"> Đề thi thử </h2>
    <a href="javascript:void(0)" uk-toggle="target: #course-filter" title="" aria-expanded="false" type="button" class="btn btn-icon-label btn-outline-dark">
        <span class="btn-inner--icon">
            <i class="icon-feather-filter"></i>
        </span>
        <span class="btn-inner--text">Bộ lọc</span>
    </a>
    <div class="uk-child-width-1-3@s uk-child-width-1-4@m uk-grid">

        @foreach ($list as $item)
        <div class="uk-first-column uk-grid-margin">
            <a href="{{ route('get-preview', ['id'=>$item->test_id]) }}" class="skill-card">
                <img width="50px" height="50px" style="margin-right:5px" src="{{ asset('storage/photos/shares/test/5ebfed9d659d5.png') }}" alt="">
                <div>
                    <h2 class="skill-card-title">{{$item->test_name}}</h2>
                    <p class="skill-card-subtitle">
                        {{$item->test_time }} phút
                    </p>
                </div>
            </a>
        </div>
        @endforeach
        @if ($list->isEmpty())
        <p>Không có dữ liệu</p> 
        @endif
    </div>
    <div id="course-filter" uk-offcanvas="flip: true; overlay: true">
        <div class="uk-offcanvas-bar">
            <!-- close button -->
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <div>
                <div class="sidebar-filter-contents">
                    <h4> Lọc theo</h4>
                    <form method="get" action="{{ route('test_list')  }}">
                        <ul class="sidebar-filter-list" uk-accordion="multiple: true">
                            <li class="uk-open">
                                <a class="uk-accordion-title" href="#">Độ khó theo mức điểm</a>
                                <div class="uk-accordion-content">
                                    <div class="uk-form-controls">
                                        @foreach ($levels as $item)
                                        <label>
                                            <input value="{{ $item->level_id }}" class="uk-radio level_name" type="radio" name="level">
                                            <span class="test">{{ $item->level_name}} Điểm </span>
                                        </label>
                                        @endforeach
                                    </div>
                                </div>
                            </li>
                          
                            <li class="uk-open">
                                <button id="filter" class="btn btn-default" type="submit">Lọc</button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')

@endsection

@section('js')

@endsection

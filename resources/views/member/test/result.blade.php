@extends('layouts.app')
@section('head')

@endsection
@section('breadcrumbs')
<nav id="breadcrumbs">
    <ul>
        <li>
        <h3 class="mb-0">{{ $test->test_name }}</h3>
        </li>
    </ul>
</nav>
@endsection
@section('content')
<div class="blog-article-single" src="{{ asset('storage/photos/shares/test/5ebdb4f900016.jpg') }}" uk-img>
    <div class="container-small">
        <p class="blog-article-meta mb-3">
            <h1><strong> {{ $test->test_name }} </strong></h1>
            <h1><strong> Thời gian làm bài: {{ $test->test_time }} phút </strong></h1>
        </p>
    </div>
</div>

<div class="container p-0" style="margin-top: 150px">
    @include('component.modals.user.result_test')
    <!-- Blog Post Content -->
    <form id="form_submit">
        @csrf
        <div class="container-small blog-article-content-read" id="part1-anchor">
            <input type="hidden" name="test_id" value="{{$test->test_id }}">
            @include('member.results.part1',['part1' =>$tree['part1'][0] ])
            @include('member.results.part2',['part2' => $tree['part2'][0] ])
            @include('member.results.part3',['part3' => $tree['part3'] ])
            @include('member.results.part4',['part4' => $tree['part4'] ])
            @include('member.results.part5',['part5' => $tree['part5'] ])
            @include('member.results.part6',['part6' => $tree['part6'] ])
            @include('member.results.part7',['part7' => $tree['part7'] ])
        </div>
    </form>
    <!-----config------>
</div>

@endsection
@section('js')

<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script>
    $(function() {
        var radioBoxes = $('.skin-square');
        radioBoxes.iCheck({
            radioClass: 'iradio_square-purple'
            , increaseArea: '20%'
        });

        @isset($show_result) 
          UIkit.modal('#modal-group-1').show();
        @endisset
    });

</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/admin/css/step.css') }}">
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .iradio_square-purple {
        margin-right: 10px;
    }

</style>
@endsection

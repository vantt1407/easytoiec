<div id="part4">
    <h3>Part IV: Short Talks</h3>
    <h5> <span style="font-weight: 800;">Directions</span>:
        You will hear some talks given by a single speaker. You will be asked to answer three questions
        about what the speaker says in each talk. Select the best response to each question and mark the letter (A),
        (B), (C) or (D) on your answer sheet. The talks will not be printed in your test book and will be spoken
        only one time.</h5>
    @php
    $total = \Session::get('total');
    $results =  \Session::get('data_4');
    @endphp
    @foreach ($part4 as $p)
    @if ($p->part_id == 4)
    <div class="uk-form-group">
        <div class="uk-position-relative w-100">
            <label class="uk-form-label">
                {!! $p->question_group_question !!}
            </label>
        </div>
        <div class="uk-position-relative w-100">
            <span>
                Audio
            </span>
            <div class="text-center">
                <audio controls class="w-100"  preload="none">
                    <source src="{{ !empty($p->question_group_audio) ? asset($p->question_group_audio) : '#' }}">
                </audio>
            </div>

        </div>
    </div>
    @foreach ( $p->questions as $q)
    {{-- <input type="hidden" name="part_4[]" value="{{$q->question_id}}"> --}}
    <div class="uk-position-relative w-100">
        <label class="uk-form-label">Question {{++$total}}: {{ $q->question }}</label>
        <input type="hidden" name="question[]"  value="{{$q->question_id}}" />
        <div class="uk-position-relative uk-first-column">
            @foreach ($q->answers as $ia => $a)
            <input 
            {{
                ( isset($results[$q->question_id]) && $results[$q->question_id] == $a->answer_id) ? 'checked' : ''
             }}
            class="skin-square awnser" type="radio" type="text" name="{{$q->question_id}}" value="{{$a->answer_id}}">
            {{substr($a->answer_option,0,1)}}<br />
            @endforeach
        </div>
    </div>
    @endforeach
    <hr />
    @php
    \Session::put('total',$total);
    @endphp
    @endif

    @endforeach
</div>

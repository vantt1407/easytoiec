<div id="part2">
    <h3>Part II: Question - Response</h3>
    <h5> <span style="font-weight: 800;">Directions</span>: In this part of the test, you will hear a question or statement spoken in English,
        followed by three responses, also spoken in English. The question or statement and the responses
        will be spoken just one time. They will not be printed in your test book, so you must listen
        carefully. You are to choose the best response to each question or statement. Now listen to a sample
        question.
    </h5>
 
    <div class="uk-form-group">
        <div class="uk-position-relative w-100">
            <span>
                Audio
            </span>
            <div class="text-center">
                <audio controls class="w-100"  preload="none">
                    <source src="{{ !empty($part2->question_group_audio) ? asset($part2->question_group_audio) : '#' }}">
                </audio>
            </div>

        </div>

    </div>
    @php
      $total = \Session::get('total');
      $results =  \Session::get('data_2');
    @endphp
    <div class="uk-grid-column-small uk-grid-row-large uk-child-width-1-4@s  uk-text-center" uk-grid>
        @foreach ( $part2->questions as $q)
        {{-- <input type="hidden" name="part_2[]" value="{{$q->question_id}}"> --}}
        <div class="wp-question">
        <label class="uk-form-label">Question {{ ++$total}} : </label>
        <input type="hidden" name="question[]"  value="{{$q->question_id}}" />
            <div class="uk-position-relative uk-first-column">
                @foreach ($q->answers as $ia => $a)
                <input 
                {{
                    ( isset($results[$q->question_id]) && $results[$q->question_id] == $a->answer_id) ? 'checked' : ''
                 }}
                class="skin-square awnser" type="radio" type="text" name="{{$q->question_id}}" value="{{$a->answer_id}}">
                {{substr($a->answer_option,0,1)}}<br />
                @endforeach

            </div>
        </div>
       
        @endforeach
        @php
        \Session::put('total',$total);
        @endphp
    </div>
    <hr/> 
</div>

<div id="part6">
    <h3>Part VI: Text Completion</h3>
    <h5> <span style="font-weight: 800;">Directions</span>: Read the texts that follow. A word or pharse is missing in some of the sentences. Four answer
    choices are given below each of the sentences. Select the best answer to complete the text. Then mark the
    letter (A), (B), (C) or (D) on your answer sheet.</h5>
    @php
    $total = \Session::get('total');
    $results =  \Session::get('data_6');
    @endphp
    @foreach ($part6 as $p)
    <div class="uk-form-group">
        <div class="uk-position-relative w-100">
            <label class="uk-form-label">
                {!! strip_tags( $p->question_group_question ) !!}
            </label>
        </div>
    </div>
    @foreach ( $p->questions as $q)
    <div class="uk-position-relative w-100">
        {{-- <input type="hidden" name="part_5[]" value="{{$q->question_id}}"> --}}
        <label class="uk-form-label">Question {{++$total}}: {{ $q->question }}</label>
        <input type="hidden" name="question[]"  value="{{$q->question_id}}" />
        <div class="uk-position-relative uk-first-column">
            @foreach ($q->answers as $ia => $a)
            <input 
            {{
                ( isset($results[$q->question_id]) && $results[$q->question_id] == $a->answer_id) ? 'checked' : ''
             }}
            class="skin-square awnser" type="radio" type="text" name="{{$q->question_id}}" value="{{$a->answer_id}}">
            {{$a->answer_option}}<br />
            @endforeach
        </div>
    </div>
    @endforeach
    <hr />
    @php
    \Session::put('total',$total);
    @endphp
    @endforeach
</div>
<div id="part7">
    <h3>Part VII: Reading Comprehension</h3>
    <h5> <span style="font-weight: 800;">Directions</span>: In this part you will read a selection of texts, such as magezine and newspaper articles,
        letters, and advertisements. Each text is followed by several questions. Select the best answer for each
        question and mark the letter (A), (B), (C) or (D) on your answer sheet.
    </h5>
    @php
    $total = \Session::get('total');
    $results =  \Session::get('data_7');
    @endphp
    @foreach ($part7 as $p)
    <div class="uk-form-group">
        <div class="uk-position-relative w-100" style="display: {{ !empty($p->question_group_image) ? "block" :"none"}}">
            <div class="text-center">
                <img src="{{ !empty($p->question_group_image) ? asset($p->question_group_image) : '#'}}" class="w-100" />

            </div>
        </div>
        <div class="uk-position-relative w-100">
            <label class="uk-form-label">
                {!! $p->question_group_question !!}
            </label>
        </div>
    </div>

    @foreach ( $p->questions as $q)
    {{-- <input type="hidden" name="part_7[]" value="{{$q->question_id}}"> --}}
    <div class="uk-position-relative w-100">
        <label class="uk-form-label">Question {{++$total}}: {{ $q->question }}</label>
        <input type="hidden" name="question[]"  value="{{$q->question_id}}" />
        <div class="uk-position-relative uk-first-column">
            @foreach ($q->answers as $ia => $a)
            <input 
            {{
                ( isset($results[$q->question_id]) && $results[$q->question_id] == $a->answer_id) ? 'checked' : ''
             }}
            class="skin-square awnser" type="radio" type="text" name="{{$q->question_id}}" value="{{$a->answer_id}}">
            {{$a->answer_option}}<br />
            @endforeach
        </div>
    </div>
    @endforeach
    <hr />
    @php
    \Session::put('total',$total);
    @endphp
    @endforeach
</div>

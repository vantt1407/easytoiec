@extends('layouts.app')
@section('head')

@endsection
@section('breadcrumbs')
<nav id="breadcrumbs">
    <ul>
        <li>
            <h3 class="mb-0">{{$data->practice_name}}</h3>
        </li>
    </ul>
</nav>
@endsection
@section('content')
<div class="blog-article-single" data-src="{{ asset('storage/photos/shares/test/5ebdb4f900016.jpg') }}" uk-img>
    <div class="container-small">
        <p class="blog-article-meta mb-3">
            <h1>
                <strong>
                    {{$data->practice_name}}
                </strong>
            </h1>
        </p>
    </div>
</div>
@php
$an = ['A','B','C','D'];
@endphp
<div class="container p-0" style="margin-top: 150px">
    <!-- Blog Post Content -->
    <div class="container-small blog-article-content-read">
        <form action="{{ \Session::has('data_user') ? '#' : route('part_practices_create') }}" method="POST">
            @csrf

            <input type="hidden" name="practice_id" value="{{$data->practice_id}}">

            @foreach ($data->questions as $k => $item)
            <div>
                @if (!empty($item->question_audio))
                <div class="uk-form-group">
                    <div class="uk-position-relative w-100">
                        <span>
                            Audio
                        </span>
                        <div class="text-center">
                            <audio controls class="w-100">
                                <source src="{{ asset($item->question_audio) }}">
                            </audio>
                        </div>
                    </div>
                </div>
                @endif
                @if (!empty($item->question_image))
                <div class="uk-form-group">
                    <div class="uk-position-relative w-100 text-center">
                        <img src="{{ asset($item->question_image) }}">
                    </div>
                </div>
                @endif
                <div class="uk-form-group parrent_wp">
                    <label class="uk-form-label">Câu {{$k+1}} : {{$item->question}}   <a href="javascript:void(0)" class="ideal"><i class="uil uil-lamp"></i> Gợi ý</a></label>
                    <input type="hidden" name="question_id[]" value="{{$item->question_id}}">
                    <div class="uk-position-relative w-100">
                        @foreach ($item->answers as $k =>$i)

                        @if ( \Session::has('data_user') )
                        <input required value="{{$i->answer_option }}" style="display:none" id="awnser-{{$i->answer_id}}" class="skin-square awnser" type="radio" type="text" name="{{$item->question_id}}" {{  $i->answer_is_correct ? 'checked':'' }} disabled />
                        @if (\Session::get('data_user')[$item->question_id] == $i->answer_option )
                        <span style=" {{  !$i->answer_is_correct ? 'color:red':'' }}">
                            {{ $i->answer_option }}{!! !$i->answer_is_correct ? '<i class="uil-times"></i>':'' !!}
                        </span>
                        @else
                        <span style=" {{  $i->answer_is_correct ? 'color:green':'' }}">
                            {{ $i->answer_option }}{!! $i->answer_is_correct ? '<i class="uil-check"></i>':'' !!}
                        </span>
                        @endif

                        @else
                        <input required value="{{$i->answer_option }}" style="display:none" id="awnser-{{$i->answer_id}}" class="skin-square awnser" type="radio" type="text" name="{{$item->question_id}}" />
                        <span class="show-answer" >
                            {{ $an[$k] }} 
                        </span>
                        <span class="hidden-answer" style="display:none;"> 
                            {{$i->answer_option }}
                        </span>
                        @endif

                        <br />
                        @endforeach
                    </div>
                </div>
            </div>
            @endforeach
            @if(\Session::has('data_user'))
            <div class="uk-form-group">
                <button onclick="window.location.assign('/phan/mo-ta-hinh-anh/bai-tap')" type="button" class="btn btn-default" style="float: right">Trở lại</button>
                <div style="clear: both;"></div>
            </div>
            @else
            <div class="uk-form-group">
                <button type="submit" class="btn btn-default" style="float: right">Nộp bài</button>
                <div style="clear: both;"></div>
            </div>
            @endif
        </form>
    </div>

</div>

@endsection
@section('js')
<script src="{{ asset('assets/common/icheck-1.x/icheck.min.js') }}"></script>
<script>
    $(function() {

        @if( \Session::has('pass'))
            UIkit.modal.alert("Bạn vừa hoàn thành xong bài tập <br/> Tỉ lệ đúng là : {{ \Session::get('pass') }}%. Sau đây là đáp án chi tiết :");
        @endisset
        
        var radioBoxes = $('.skin-square');
        radioBoxes.iCheck({
            radioClass: 'iradio_square-purple'
            , increaseArea: '20%'
        });


        $('.ideal').on('click', function() {
            console.log($(this).parent('div'));
            $(this).closest('.parrent_wp').find('.show-answer').css('display','none');

            $(this).closest('.parrent_wp').find('.hidden-answer').css('display','inline-block');
        });
    });

</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/common/icheck-1.x/skins/all.css') }}">
<style>
    .iradio_square-purple {
        margin-right: 10px;
    }

</style>
@endsection

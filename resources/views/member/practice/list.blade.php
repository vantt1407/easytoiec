@extends('layouts.app')
@section('head')

@endsection
@section('content')
<div class="container">

    <h1>Danh sách đề luyện tập </h1>

    <nav class="responsive-tab style-2">
        <ul>
            @foreach ($parts as $key => $item)
            <li class="{{ $slug == $item->part_slug  ? 'uk-active': '' }}">
                <a href="{{ route('part_practices', ['slug'=>$item->part_slug]) }}"
                    uk-tooltip="title:{{ $item->part_name}}; pos: bottom">
                    Phần {{ $item->part_id }}
                </a></li>
            @endforeach


        </ul>
    </nav>


    <div class="section-header pb-0 mt-5">
        <div class="section-header-left">
        <h4>{{$current->part_name}}</h4>
        </div>
        <div class="section-header-right">

            {{-- <a href="#" class="btn-filter btn btn-outline-primary uk-first-column" uk-tooltip="title: Bộ lọc ; pos:  top-right"
                uk-toggle="target: #course-filter" title="" aria-expanded="false">
                <i class="icon-feather-filter"></i>
            </a> --}}
            <a href="javascript:void(0)" uk-toggle="target: #course-filter" title="" aria-expanded="false" type="button"
                class="btn btn-icon-label btn-outline-dark">
                <span class="btn-inner--icon">
                    <i class="icon-feather-filter"></i>
                </span>
                <span class="btn-inner--text">Bộ lọc</span>
            </a>
        </div>
    </div>

    <div class="section-small">

        <div class="uk-child-width-1-4@m uk-child-width-1-3@s course-card-grid uk-grid-match uk-grid" uk-grid="">
            @foreach ($list as $item)
            @if ( !empty($item->practice_slug))
            <div class="uk-first-column">
                        <a href="{{ route('part_practices_detail',$item->practice_slug)}}">
                            <div class="course-card">
                                <div class="course-card-thumbnail ">
                                    <img src="{{ asset('storage/photos/shares/part/exercise/5ebda716be2c2.jpg') }}">
                                    <span class="play-button-trigger"></span>
                                </div>
                                <div class="course-card-body">
                                    <div class="course-card-info">
                                        <div>
                                            <span class="catagroy">{{$item->part_name}}</span>
                                        </div>
                                        <div>
                                            <i class="icon-feather-bookmark icon-small"></i>
                                        </div>
                                    </div>

                                    <h4>{{$item->practice_name}} </h4>

                                    <div class="course-card-footer">
                                        {{-- <h5> <i class="uil-file-edit-alt"></i> {{$item->total_question}} Câu hỏi </h5> --}}
                                    </div>
                                </div>

                            </div>
                        </a>   
                    @endif
                </div>
                @endforeach
             @if (empty($list[0]->practice_slug))
                 Không có bài luyện tập nào 
             @endif
           

        </div>

    </div>

    <div id="course-filter" uk-offcanvas="flip: true; overlay: true">
        <div class="uk-offcanvas-bar">
            <!-- close button -->
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <div >
                <div class="sidebar-filter-contents">
                    <h4> Lọc theo</h4>
                    <ul class="sidebar-filter-list" uk-accordion="multiple: true">

                        <li class="uk-open">
                            <a class="uk-accordion-title" href="#">Độ khó theo mức điểm</a>
                            <div class="uk-accordion-content">
                                <div class="uk-form-controls">
                                    @foreach ($levels as $item)
                                        <label>
                                        <input value="{{ $item->level_id }}" class="uk-radio level_name" type="radio" name="level">
                                            <span class="test">{{ $item->level_name}} Điểm </span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </li>

                        <li class="uk-open">
                            <a class="uk-accordion-title" href="#">Theo phần thi </a>
                            <div class="uk-accordion-content">
                                <div class="uk-form-controls">
                                    @foreach ($parts as $item)
                                    <label>
                                        <input value="{{ $item->part_slug}}" class="uk-radio part_name" type="radio" name="topic">
                                        <span class="test">{{ $item->part_name}}</span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                        </li>

                        <li class="uk-open">
                         
                            <button id="filter" class="btn btn-default" type="submit">Lọc</button>
                        </li>


                    </ul>



                </div>

            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
    <script>

        $(function () {

            $('#filter').on('click', function () {
                var level = 0;
                var part = "";
               

                $.each($('.level_name'),function(index,item) {
                    
                    if($(item).is(':checked')) { 
                        level = $(item).val();
                     }

                });
                $.each($('.part_name'),function(index,item) {
                    
                    if($(item).is(':checked')) { 
                        part = $(item).val();
                     }

                });
                if(!part && !level ) return;
                var fullPath =window.location.origin + window.location.pathname;

                fullPath = `${fullPath}?part=${part}&level=${level}`;

                window.location.assign(fullPath);
                

            })

        });

    </script>
@endsection

@section('css')

@endsection
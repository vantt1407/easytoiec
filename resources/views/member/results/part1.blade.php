<div id="part1">
    <h3>Part I: Picture Description</h3>
    <h5><span style="font-weight: 800;">Directions</span>:
        For each question, you will see a picture and you will hear four short statements. The
        statements will be spoken just one time. They will not be printed in your test book so you must
        listen carefully to understand what the speaker says. When you hear the four statements, look at the
        picture and choose the statement that best describes what you see in the picture. Choose the best
        answer A, B, C or D
    </h5>

    <div class="uk-form-group">
        <div class="uk-position-relative w-100">
            <span>
                Audio
            </span>
            <div class="text-center">
                <audio controls class="w-100" preload="auto">
                    <source  src="{{ $part1->question_group_audio ? asset($part1->question_group_audio) : '#' }}">
                </audio>
            </div>
        </div>
    </div>
    @php
       \Session::put('total',0);
    @endphp
    @foreach ($part1->questions as $key => $q )
    <input type="hidden" name="part_1[]" value="{{$q->question_id}}">
    <div class="wp-question">
        <div class="uk-form-group">
            <div class="uk-position-relative w-100">
                <img class="w-100" src="{{ asset($q->question_image) }}" alt="">
            </div>
        </div>
        <div class="uk-form-group">
            <label class="uk-form-label">Question {{$key+=1}}: {{$q->question}}</label>
            <input type="hidden" name="question[]"  value="{{$q->question_id}}"/>
            <div class="uk-position-relative w-100">
                @foreach ($q->answers as $a)
                <input 
                {{ $a->answer_is_correct ? 'checked' : ''}}
                class="skin-square awnser" type="radio" type="text" name="{{$q->question_id}}" value="{{$a->answer_id}}" />
                @if (   $listResult[$key] != 0 && $listResult[$key] == $a->answer_id )
                <span style=" {{  !$a->answer_is_correct ? 'color:red':'' }}">
                    {{ $a->answer_option }}{!! !$a->answer_is_correct ? '<i class="uil-times"></i>':'' !!}
                </span>
                @else
                <span style=" {{  $a->answer_is_correct ? 'color:green':'' }}">
                    {{ $a->answer_option }}{!! $a->answer_is_correct ? '<i class="uil-check"></i>':'' !!}
                </span>
                @endif
                <br />
                @endforeach
            </div>
        </div>
    </div>
    <hr/>
        @php
            \Session::put('total',$key);
        @endphp
    @endforeach

</div>


@extends('layouts.app')
@section('head')
@endsection
@section('content')
<div class="container">

    <h4>Danh sách các lần thi </h4>

    <div uk-grid="" class="uk-grid">
        <div class="uk-width-expand@m uk-first-column">
            <div class="uk-card-default rounded">
                <div class="p-3">
                    <h5 class="mb-0"> Danh sách </h5>
                </div>
                <div class="uk-grid-small p-4 uk-grid">
                    <table class="table align-items-center">
                        <thead>
                            <tr>
                            
                                <th scope="col">Đề thi</th>
                                <th scope="col">Số điểm</th>
                                <th scope="col">Ngày tạo</th>
                                <th scope="col"> </th>
                            </tr>
                        </thead>
                        <tbody class="list">
                           
                            @foreach ($list as $item)
                            <tr>
        
                                <td scope="row">
                                    <div class="media align-items-center">
                                        <div class="media-body ml-4">
                                            <a href="{{ route('test_detail', $item->test->test_id) }}" class="name h6 mb-0 text-sm">
                                                {{
                                                    $item->test->test_name
                                                }}
                                            </a>
                                        </div>
                                    </div>
                                </th>
                                <td scope="row">
                                <span class="uk-label">
                                    {{ $item->scores}}
                                </span>
                                </td>
                                <td>{{$item->created_at}}</td>
                                <td class="text-right">
                                    <!-- Actions -->
                                    <div class="actions ml-3">
                                       
                                        <a href="{{ route('get-result', $item->id) }}" class="btn btn-icon btn-hover btn-sm btn-circle" uk-tooltip="Chi tiết">
                                            <i class="uil-eye text-success"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                           
                        </tbody>
                    </table>
                    @if ($list->isEmpty())
                    <div class="empty-data">Không có dữ liệu</div>
                    @else
                    <div class="text-center" style="float:right; ">
                        {{ $list->links('vendor.pagination.default') }}
                    </div>
                    @endif
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
   
@endsection


@section('css')
    
@endsection
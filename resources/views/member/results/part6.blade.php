<div id="part6">
    <h3>Part VI: Text Completion</h3>
    <h5> <span style="font-weight: 800;">Directions</span>: Read the texts that follow. A word or pharse is missing in some of the sentences. Four answer
    choices are given below each of the sentences. Select the best answer to complete the text. Then mark the
    letter (A), (B), (C) or (D) on your answer sheet.</h5>
    @php
    $total = \Session::get('total');
    @endphp
    @foreach ($part6 as $p)
    <div class="uk-form-group">
        <div class="uk-position-relative w-100">
            <label class="uk-form-label">
                {!! strip_tags( $p->question_group_question ) !!}
            </label>
        </div>
    </div>
    <div class="uk-position-relative w-100" style="display: {{$p->question_group_audio ? "block" :"none"}}">
            <span>
                Audio
            </span>
            <div class="text-center">
                <audio controls class="w-100" preload="auto">
                    <source src="{{ $p->question_group_audio  ? asset($p->question_group_audio ) : '#' }}">
                </audio>
            </div>
    </div>
    @foreach ( $p->questions as $kk => $q)
    <div class="uk-position-relative w-100">
        {{-- <input type="hidden" name="part_5[]" value="{{$q->question_id}}"> --}}
        <label class="uk-form-label">Question {{++$total}}: {{ $q->question }}</label>
        <input type="hidden" name="question[]"  value="{{$q->question_id}}" />
        <div class="uk-position-relative uk-first-column">
            @foreach ($q->answers as $ia => $a)
            <input 
            {{ $a->answer_is_correct ? 'checked' : ''}}
            class="skin-square awnser" type="radio" type="text" name="{{$q->question_id}}" value="{{$a->answer_id}}">
            @if (   $listResult[$kk] != 0 && $listResult[$kk] == $a->answer_id )
                <span style=" {{  !$a->answer_is_correct ? 'color:red':'' }}">
                    {{ $a->answer_option }}{!! !$a->answer_is_correct ? '<i class="uil-times"></i>':'' !!}
                </span>
            @else
            <span style=" {{  $a->answer_is_correct ? 'color:green':'' }}">
                {{ $a->answer_option }}{!! $a->answer_is_correct ? '<i class="uil-check"></i>':'' !!}
            </span>
            @endif
            <br />
            @endforeach
        </div>
    </div>
    @endforeach
    <hr />
    @php
    \Session::put('total',$total);
    @endphp
    @endforeach
</div>
<div id="part7">
    <h3>Part VII: Reading Comprehension</h3>
    <h5> <span style="font-weight: 800;">Directions</span>: In this part you will read a selection of texts, such as magezine and newspaper articles,
        letters, and advertisements. Each text is followed by several questions. Select the best answer for each
        question and mark the letter (A), (B), (C) or (D) on your answer sheet.
    </h5>
    @php
    $total = \Session::get('total');
    @endphp
    @foreach ($part7 as $p)
    <div class="uk-form-group">
        <div class="uk-position-relative w-100" style="display: {{$p->question_group_image ? "block" :"none"}}">
            <div class="text-center">
                <img src="{{$p->question_group_image ? asset($p->question_group_image) : '#'}}" class="w-100" />

            </div>
        </div>
        <div class="uk-position-relative w-100">
            <label class="uk-form-label">
                {!! $p->question_group_question !!}
            </label>
        </div>
    </div>


    <div class="uk-position-relative w-100" style="display: {{$p->question_group_audio ? "block" :"none"}}">
        <span>
            Audio
        </span>
        <div class="text-center">
            <audio controls class="w-100" preload="auto">
                <source src="{{ $p->question_group_audio ? asset($p->question_group_audio) : '#' }}">
            </audio>
        </div>
</div>

    @foreach ( $p->questions as $kk => $q)
    {{-- <input type="hidden" name="part_7[]" value="{{$q->question_id}}"> --}}
    <div class="uk-position-relative w-100">
        <label class="uk-form-label">Question {{++$total}}: {{ $q->question }}</label>
        <input type="hidden" name="question[]"  value="{{$q->question_id}}" />
        <div class="uk-position-relative uk-first-column">
            @foreach ($q->answers as $ia => $a)
            <input 
            {{ $a->answer_is_correct ? 'checked' : ''}}
            class="skin-square awnser" type="radio" type="text" name="{{$q->question_id}}" value="{{$a->answer_id}}">
            @if (   $listResult[$kk] != 0 && $listResult[$kk] == $a->answer_id )
                <span style=" {{  !$a->answer_is_correct ? 'color:red':'' }}">
                    {{ $a->answer_option }}{!! !$a->answer_is_correct ? '<i class="uil-times"></i>':'' !!}
                </span>
            @else
            <span style=" {{  $a->answer_is_correct ? 'color:green':'' }}">
                {{ $a->answer_option }}{!! $a->answer_is_correct ? '<i class="uil-check"></i>':'' !!}
            </span>
            @endif
            <br />
            @endforeach
        </div>
    </div>
    @endforeach
    <hr />
    @php
    \Session::put('total',$total);
    @endphp
    @endforeach
</div>

@extends('layouts.app')
@section('head')
@endsection
@section('content')
<div class="container">

    <h4>Thông tin cá nhân</h4>

    <div uk-grid="" class="uk-grid">

        <div class="uk-width-2-5@m uk-flex-last@m">
            <form id="update-avatar-form" action="{{ route('update_avatar') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method("put")
            <div class="uk-card-default rounded text-center p-4">
                <div class="uk-position-relative my-4">
                        <div class="user-profile-photo  m-auto">
                        <img id="img_avt" src="{{ empty(Auth::user()->profile->user_profile_avatar) ? asset('assets/images/no-avt.png') :
                        asset(Auth::user()->profile->user_profile_avatar) 
                         }}" alt="">
                        </div>
                    <div class="uk-position-center">
                        <div uk-form-custom="" class="uk-form-custom">
                            <input type="file" id="avatar" name="avatar">
                            <span class="uk-link icon-feather-camera icon-medium text-white"> </span>
                        </div>

                    </div>
                </div>

                <button id="update-avatar" class="btn btn-default grey " type="button">Lưu Ảnh</button>

            </div>
            </form>
        </div>
        <div class="uk-width-expand@m uk-first-column">

            <div class="uk-card-default rounded">
                <div class="p-3">
                    <h5 class="mb-0"> Cập nhật thông tin </h5>
                </div>
                <hr class="m-0">
                <form id="form_update_profile" class="uk-child-width-1-2@s uk-grid-small p-4 uk-grid" action="{{ route('update_profile') }}" method="POST">
                    @csrf
                    @method("put")
                    <div class="uk-first-column">
                        <h5 class="uk-text-bold mb-2"> Họ và tên </h5>
                        <input name="full_name" value="{{Auth::user()->profile ? Auth::user()->profile->user_profile_full_name : ''}}" type="text" class="uk-input" >
                    </div>
                    <div>
                        <h5 class="uk-text-bold mb-2"> Địa chỉ Email </h5>
                        <input type="text" class="uk-input"  value="{{Auth::user()->email}}" disabled>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                        <h5 class="uk-text-bold mb-2"> Số điện thoại </h5>
                        <input name="phone" type="text" class="uk-input"  value="{{Auth::user()->profile ? Auth::user()->profile->user_profile_phone: ''}} ">
                    </div>
                    <div class="uk-grid-margin">
                        <h5 class="uk-text-bold mb-2"> Địa chỉ </h5>
                        <input name="address" type="text" class="uk-input" value="{{Auth::user()->profile ? Auth::user()->profile->user_profile_address : ''}}" >
                    </div>
                </form>

                <div class="uk-flex uk-flex-right p-4">
                    <button class="btn btn-link mr-2">Hủy</button>
                    <button class="btn btn-default grey" id="update_profile">Lưu</button>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/common/notify/noty.min.js') }}"></script>
<script>
    $(function() {
        $('#update_profile').on('click', function() {
            $('#form_update_profile').submit();
        });

        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#img_avt').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]); 
        }
        }

        $("#avatar").change(function() {
           readURL(this);
        });

        $('#update-avatar').on('click',function(e) {
        
            if( document.getElementById("avatar").files.length != 0 ){
                 $('#update-avatar-form').submit();
            }
        })
    });
</script>
@endsection


@section('css')
@endsection

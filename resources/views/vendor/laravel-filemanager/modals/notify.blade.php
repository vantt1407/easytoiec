<div id="notify" class="uk-flex-top" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical"> 
      <button class="uk-modal-close-default"
        type="button" uk-close></button>
        <h2 class="uk-modal-title content"></h2>
      <p class="uk-text-right" style="margin-top:20px">
        
        <button class="uk-button uk-button-default uk-modal-close"
        type="button" uk-close>{{ trans('laravel-filemanager::lfm.btn-close') }}</button>
      
        <button type="button" class=" btn btn-outline-danger  uk-modal-bind"
          type="button">{{ trans('laravel-filemanager::lfm.btn-confirm') }}</button>
      </p>
    </div>
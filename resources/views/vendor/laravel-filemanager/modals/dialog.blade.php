
<div id="dialog" class="uk-flex-top" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical"> 
      <button class="uk-modal-close-default"
        type="button" uk-close></button>
        <h2 class="uk-modal-title content"></h2>
        <input type="text" class="form-control msg" >
      <p class="uk-text-right" style="margin-top:20px">
        <button type="button" class="uk-button uk-button-default uk-modal-close">{{ trans('laravel-filemanager::lfm.btn-close') }}</button>
        <button type="button" class="btn-action btn btn-outline-danger  uk-modal-close">{{ trans('laravel-filemanager::lfm.btn-confirm') }}</button>
      </p>
    </div>
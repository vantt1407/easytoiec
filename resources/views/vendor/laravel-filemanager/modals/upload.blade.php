

<div id="uploadModal" class="uk-flex-top" uk-modal>
  <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical"> 
    <button class="uk-modal-close-default"
      type="button" uk-close></button>
      <h2 class="uk-modal-title">{{ trans('laravel-filemanager::lfm.title-upload') }}</h2>
      <form action="{{ route('unisharp.lfm.upload') }}" role='form' id='uploadForm' name='uploadForm' method='post'
      enctype='multipart/form-data' class="dropzone">
      <div class="form-group" id="attachment">
        <div class="controls text-center">
          <div class="input-group w-100">
            <button id="upload-button" class="uk-button uk-button-primary"
              type="button">{{ trans('laravel-filemanager::lfm.message-choose') }}</button>
            {{-- <a class="btn btn-primary w-100 text-white"></a> --}}
          </div>
        </div>
      </div>
      <input type='hidden' name='working_dir' id='working_dir'>
      <input type='hidden' name='type' id='type' value='{{ request("type") }}'>
      <input type='hidden' name='_token' value='{{csrf_token()}}'>
    </form>
    <p class="uk-text-right" style="margin-top:20px">
      <button class="uk-button uk-button-default uk-modal-close"
        type="button">{{ trans('laravel-filemanager::lfm.btn-close') }}</button>
    </p>
  </div>
</div>
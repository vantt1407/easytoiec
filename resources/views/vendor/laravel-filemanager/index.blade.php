<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content="#333844">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#333844">
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-status-bar-style" content="#333844">

  <title>{{ trans('laravel-filemanager::lfm.title-page') }}</title>
  <link rel="shortcut icon" type="image/png" href="{{ asset('vendor/laravel-filemanager/img/72px color.png') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/css/framework.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/admin/css/icons.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/cropper.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/dropzone.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/mime-icons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/vendor/laravel-filemanager/css/lfm.css') }}">
  <style>
    .uk-navbar-nav > li > a {
     font-size : 0.8em !important;
     padding: 0rem 0.5rem !important;
    }
    .disable-after:after {
                content: '' !important;
               
          
            }
  </style>
</head>
<body>
  
  @include('vendor.laravel-filemanager.nav.nav')
  <div class="d-flex flex-row">
    <div id="tree"></div>

    <div id="main">
      <div id="alerts"></div>

      <nav aria-label="breadcrumb" class="d-none d-lg-block" id="breadcrumbs">
        <ol class="breadcrumb">
          <li class="breadcrumb-item invisible">Home</li>
        </ol>
      </nav>

      <div id="empty" class="d-none">
        <i class="far fa-folder-open"></i>
        {{ trans('laravel-filemanager::lfm.message-empty') }}
      </div>

      <div id="content"></div>

      <a id="item-template" class="d-none">
        <div class="square"></div>

        <div class="info">
          <div class="item_name text-truncate"></div>
          <time class="text-muted font-weight-light text-truncate"></time>
        </div>
      </a>
    </div>

    <div id="fab"></div>
  </div>
  @include('vendor.laravel-filemanager.carousel.list')
 
  @include('vendor.laravel-filemanager.modals.upload')
 
  @include('vendor.laravel-filemanager.modals.notify')

  @include('vendor.laravel-filemanager.modals.dialog')
  
 
  <script>
    window.lang = {!! json_encode(trans('laravel-filemanager::lfm')) !!};
    window.acceptedFiles = "{{ implode(',', $helper->availableMimeTypes()) }}"
 </script>
  <script src="{{ asset('assets/admin/js/framework.js') }}"></script>
  <script src="{{ asset('assets/admin/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('vendor/laravel-filemanager/js/cropper.min.js') }}"></script>
  <script src="{{ asset('vendor/laravel-filemanager/js/dropzone.min.js') }}"></script>
  <script src="{{ asset('vendor/laravel-filemanager/js/script.js') }}"></script>
  <script src="{{ asset('vendor/laravel-filemanager/js/init.js') }}"></script>

</body>
</html>

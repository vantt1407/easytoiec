<nav   uk-navbar class="uk-navbar-container navbar sticky-top navbar-expand-lg navbar-dark" id="nav">
    <a class="navbar-brand invisible-lg d-none d-lg-inline" id="to-previous">
        <i class="fas fa-arrow-left fa-fw"></i>
        <span class="d-none d-lg-inline">{{ trans('laravel-filemanager::lfm.nav-back') }}</span>
    </a>
    <a class="navbar-brand d-block d-lg-none" id="show_tree">
        <i class="fas fa-bars fa-fw"></i>
    </a>
    <a class="navbar-brand d-block d-lg-none" id="current_folder"></a>
    <a id="loading" class="navbar-brand"><i class="fas fa-spinner fa-spin"></i></a>
    {{-- <div class="ml-auto px-2">
        <a class="navbar-link d-none" >
            
           
        </a>
    </div> --}}
    <div class="ml-auto px-2 flex-grow-0">
        <ul class="uk-navbar-nav">
            <li>
                <a class="nav-link disable-after" data-display="list" id="multi_selection_toggle">
                    <i class="uil-check-square"></i>
                <span class="d-none d-lg-inline">{{ trans('laravel-filemanager::lfm.menu-multiple') }}</span>
                </a>
                
            </li>
        </ul>
    </div>
    <a class="navbar-toggler collapsed border-0 px-1 py-2 m-0" data-toggle="collapse" data-target="#nav-buttons">
        <i class="fas fa-cog fa-fw"></i>
    </a>
    {{-- <div class="collapse navbar-collapse flex-grow-0" >
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-display="grid">
                    <i class="fas fa-th-large fa-fw"></i>
                    <span>{{ trans('laravel-filemanager::lfm.nav-thumbnails') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-display="list">
                    <i class="fas fa-list-ul fa-fw"></i>
                    <span>{{ trans('laravel-filemanager::lfm.nav-list') }}</span>
                </a>
            </li>
            <li class="nav-item dropdown">
              
            </li>
        </ul>
    </div> --}}

    {{-- <nav  class="uk-navbar-container " uk-navbar> --}}
        <div class="= flex-grow-0" id="nav-buttons">
            <ul class="uk-navbar-nav">
                <li>
                    <a class="nav-link disable-after" data-display="gird">
                    <i class="fas fa-th-large fa-fw"></i>
                    <span>{{ trans('laravel-filemanager::lfm.nav-thumbnails') }}</span>
                    </a>
                    
                </li>
                <li>
                    <a class="nav-link disable-after" data-display="list">
                        <i class="fas fa-list-ul fa-fw"></i>
                        <span>{{ trans('laravel-filemanager::lfm.nav-list') }}</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" >
                        <i class="fas fa-sort fa-fw"></i>{{ trans('laravel-filemanager::lfm.nav-sort') }}
                    </a>
                    <div class="uk-navbar-dropdown">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                           
                        </ul>
                    </div>
                    
                </li>
            </ul>
        </div>
        
    {{-- </nav> --}}

</nav>

{{-- <nav class="bg-light fixed-bottom border-top d-none" >
    
    
    
</nav> --}}

<nav id="actions" class="uk-navbar-container  fixed-bottom" uk-navbar>
    <div class="uk-navbar-center">
    <ul class="uk-navbar-nav">
        <li class="uk-active">
            <a data-action="open" data-multiple="false" class="disable-after  ">
                <i class="uil-folder-open"></i>{{ trans('laravel-filemanager::lfm.btn-open') }}</a>
            
        </li>
        <li>
            <a data-action="preview" data-multiple="true"  class="disable-after text-primary"><i
                    class=" uil-images"></i>{{ trans('laravel-filemanager::lfm.menu-view') }}</a>
        </li>
        <li>
            <a data-action="use" data-multiple="true"  class="disable-after text-success">
                <i class="uil-check"></i>{{ trans('laravel-filemanager::lfm.btn-confirm') }}</a>
        </li>
    </ul>
    </div>
</nav>
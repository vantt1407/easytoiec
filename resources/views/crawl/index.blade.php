<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

    <div class="container">
        <h2>Crawl Data</h2>
        <form action="" method="POST">
            @csrf
            <div class="form-group">
                <label> Phần </label>
                <select id="part_id">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                </select>
            </div>
            <div class="form-group">
                <label> Dữ liệu html </label>
                <textarea rows="30" id="data-raw" class="form-control" type="text" class="form-control" name="data_html"></textarea>
            </div>

            <button type="button" id="parse" class="btn btn-default">Parse</button>
        </form>
    </div>
    <script>
        $(function() {
            $('#parse').on('click', function() {

                var data = $('#data-raw').val();

                $dom = $(data);
                $question_group = $dom.find('.question-content').children('p').text();
                $question_group = $.trim($question_group);
                //  $img_src = 

                // console.log($question_group);

                let question_group = {
                    part_id: $('#part_id').val()
                    , question: $question_group
                    , image: ''
                    , audio: ''
                };
                let dataJson = {
                    question_group: question_group
                }
                // console.log(dataJson);

                console.log(listAudio($dom));

            });


            function listImgQuestion($dom) {

                $img = $dom.find('img');

                $list = [];

                $.each($img, function(index,item)  {

                    $list.push( $(item).attr('src') )
                });

                return $list;
            }

            function listAudio($dom) {

                $audio = $dom.find('audio');

                $list = [];

                $.each( $audio, function(index,item){

                    $list.push( $(item).children('source').attr('src') )

                });
                return $list;
            }

        });

    </script>
</body>
</html>

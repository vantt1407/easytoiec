@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Easy Toiec !',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')
    <h2>
        Chào mừng bạn đến với Easy Toiec.
        Truy cập trang sau để đổi mật khẩu !
    </h2>
    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
        'title' => 'Đổi mật khẩu',
        'link' => $url
    ])

@stop
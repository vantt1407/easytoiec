@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Easy Toiec !',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')
    <h2>
        Chào mừng bạn đến với Easy Toiec.
        Bạn có thể dùng tài khoản mới để truy cập và sử dụng các sản phẩm, ứng dụng và dịch vụ của Easy Toiec
    </h2>
    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
        'title' => 'Xác thực tài khoản !',
        'link' => $url
    ])

@stop
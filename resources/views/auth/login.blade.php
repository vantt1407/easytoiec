@extends('layouts.auth')
@section('title')
    <title>Đăng</title>
@endsection
@section('content')
<div class="form-items">
    <h3>Đăng nhập</h3>
    <p>Học tiếng anh thật dễ dàng !</p>
    @error('auth_fail')
    <div class="alert alert-danger alert-dismissible fade show with-icon" role="alert">
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @enderror
    @if (session('register_success'))
    <div class="alert alert-success alert-dismissible fade show with-icon" role="alert">
        {{session('register_success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @endif
    @if (session('reset_success'))
    <div class="alert alert-success alert-dismissible fade show with-icon" role="alert">
        {{session('reset_success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @endif
    @if (session('email_verified_success'))
    <div class="alert alert-success alert-dismissible fade show with-icon" role="alert">
        {{session('email_verified_success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @endif
    @if (session('email_verified'))
    <div class="alert alert-warning alert-dismissible fade show with-icon" role="alert">
        {{session('email_verified')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    @endif
    <form action="{{ route('post_login') }}" method="POST">
        @csrf
        <input class="circle form-control @error('email') is-invalid @enderror" type="text" name="email"
            placeholder="Địa chỉ E-mail" value="{{ old('email') }}">
        @error('email')
        <span class="invalid-feedback text-right" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <input class="circle form-control @error('password') is-invalid @enderror" type="password" name="password"
            placeholder="Mật khẩu" value="{{ old('password') }}">
        @error('password')
        <span class="invalid-feedback text-right" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
            <input class="form-control" type="checkbox" id="chk1"  name="remember">
            <label for="chk1">Ghi nhớ đăng nhập</label>
        <div class="form-button full-width text-center">
            <button id="submit" type="submit" class="ibtn circle" style="background:#2296f3 !important">Đăng nhập</button>
            <a href="{{ route('get_forgot') }}">Quên mật khẩu ?</a>
        </div>
    </form>
    <div class="other-links" style="margin-top:20px">
        <div class="text">Đăng nhập với</div>
        <a href="{{url('login/facebook')}}">
            <i class="fab fa-facebook-f"></i>Facebook</a>
        <a href="{{url('login/google')}}"><i class="fab fa-google"></i>Google</a>
        <a href="{{url('login/linkedin')}}"><i class="fab fa-linkedin-in"></i>Linkedin</a>
    </div>
    <div class="page-links">
        <a href="{{ route('get_register') }}">Đăng ký tài khoản</a>
    </div>
</div>
@endsection
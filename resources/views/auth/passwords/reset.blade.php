@extends('layouts.auth')

@section('content')
<div class="form-items">
    <h3>Tạo mật khẩu mới !</h3>
    <form action="{{ route('post_reset') }}" method="POST">
        @csrf
        <input value="{{ $email }}" name="email" type="hidden">
        <input value="{{ $token }}" name="token" type="hidden">
        <input value="{{ old('password') }}" class="circle form-control @error('password') is-invalid @enderror" type="password" name="password" placeholder="Mật khẩu" >
        @error('password')
        <span class="invalid-feedback text-right" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <input  value="{{ old('confirm_password') }}" class="circle form-control @error('confirm_password') is-invalid @enderror" type="password" name="confirm_password" placeholder="Nhập lại mật khẩu" >
        @error('confirm_password')
        <span class="invalid-feedback text-right" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="form-button full-width">
            <button id="submit" type="submit" class="ibtn circle">Gửi</button>
        </div>
    </form>
</div>
@endsection

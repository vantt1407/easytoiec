@extends('layouts.auth')

@section('content')
<div class="form-content">
<div class="form-items {{ session('request_success') ? 'hide-it' : 'show-it' }}">
        <h3>Quên mật khẩu !</h3>
        <p>Nhập địa chỉ E-mail đăng ký tài khoản </p>
        @error('email')
        <div class="alert alert-danger alert-dismissible fade show with-icon" role="alert">
            {{$message}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        @enderror
        <form method="post" action="{{ route('post_forgot')}}">
            @csrf
            <input class="circle form-control" type="text" name="email" placeholder="Địa chỉ E-mail" required>
            <div class="form-button full-width text-center">
                <button  style="background:#2296f3 !important" id="submit" type="submit" class="circle ibtn btn-forget">Lấy lại mật khẩu</button>
                <a href="{{ route('get_login') }}">Quay lại đăng nhập</a>
            </div>
        </form>
    </div>
    <div class="form-sent {{ session('request_success') ? 'show-it' : 'hide-it' }}">
        <div class="tick-holder">
            <div class="tick-icon"></div>
        </div>
        <h3>Yêu cầu lấy lại mật khẩu thành công</h3>
        <p>Kiểm tra hộp thư E-mail của bạn !</p>
        
    </div>
</div>

@endsection

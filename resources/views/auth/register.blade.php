@extends('layouts.auth')

@section('content')
<div class="form-items">
    <h3>Đăng ký tài khoản</h3>
    <p>Mọi thứ hoàn toàn miễn phí !</p>
    <form action="{{ route('post_register') }}" method="POST">
        @csrf
        <input value="{{ old('email') }}" class="circle form-control @error('email') is-invalid @enderror" type="email" name="email" placeholder="Địa chỉ E-mail" >
        @error('email')
        <span class="invalid-feedback text-right" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <input value="{{ old('password') }}" class="circle form-control @error('password') is-invalid @enderror" type="password" name="password" placeholder="Mật khẩu" >
        @error('password')
        <span class="invalid-feedback text-right" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <input  value="{{ old('confirm_password') }}" class="circle form-control @error('confirm_password') is-invalid @enderror" type="password" name="confirm_password" placeholder="Nhập lại mật khẩu" >
        @error('confirm_password')
        <span class="invalid-feedback text-right" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="form-button full-width">
            <button id="submit" type="submit" class="ibtn circle">Đăng ký</button>
        </div>
    </form>
    <div class="other-links" style="margin-top:20px">
        <div class="text">Đăng nhập với</div>
        <a href="#"><i class="fab fa-facebook-f"></i>Facebook</a>
        <a href="#"><i class="fab fa-google"></i>Google</a>
        <a href="#"><i class="fab fa-linkedin-in"></i>Linkedin</a>
    </div>
    <div class="page-links">
        <a href="{{ route('get_login') }}">Đã có tài khoản ?</a>
    </div>
</div>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Crawl Data</h2>
  <form action="{{ route('run') }}" method="POST">
    @csrf
  
    <div class="form-group">
      <label >Part 1</label>
      <input type="text" class="form-control" name="part1">
    </div>
    <div class="form-group">
        <label >Part 2</label>
        <input type="text" class="form-control" name="part2">
      </div>
      <div class="form-group">
        <label >Part 3</label>
        <input type="text" class="form-control" name="part3">
      </div>
      <div class="form-group">
        <label >Part 4</label>
        <input type="text" class="form-control" name="part4">
      </div>
      <div class="form-group">
        <label >Part 5</label>
        <input type="text" class="form-control" name="part5">
      </div>
      <div class="form-group">
        <label >Part 6</label>
        <input type="text" class="form-control" name="part6">
      </div>
      <div class="form-group">
        <label >Part 7</label>
        <input type="text" class="form-control" name="part7">
      </div>
    <button type="submit" class="btn btn-default">Crawl</button>
  </form>
</div>

</body>
</html>
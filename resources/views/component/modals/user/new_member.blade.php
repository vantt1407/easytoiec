<div id="modal-wellcome" class="uk-modal-full" uk-modal>
    <div class="uk-modal-dialog"> <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
            <div class="uk-background-cover" style="background-size:contain;background-image: url('http://localhost:8000/assets/auth/images/graphic3.svg');" uk-height-viewport></div>
            <div class="uk-padding-large">
                <h1 class="title_run animate__animated animate__fadeInRight animate__delay-2s">Chào mừng bạn đến với Easy Toeic</h1>
                <p class="title_run animate__animated animate__fadeInRight animate__delay-2s">
                    Chúng ta làm một bài khảo sát nho nhỏ nhé !
                </p>
                <button id="start_run" type="button" class="btn btn-animated btn-success btn-animated-x
                 animate__animated animate__fadeInUp animate__delay-3s">
                    <span class="btn-inner--visible">Bắt đầu nào</span>
                    <span class="btn-inner--hidden">
                        <i class="uil-arrow-right"></i>
                    </span>
                </button>
                <div id="wpp" style="display:none !important" class="animate__animated ">
                    <h1>Điểm số Toeic của bạn hiện tại đang ở mức nào ?</h1>
                    <form class="uk-child-width-1-2@s uk-grid-small p-4 uk-grid animate__animated" uk-grid="">
                        <div class=" uk-first-column">
                            <h5 class="uk-text-bold mb-2"> Số điểm Toeic hiện tại </h5>
                            <input type="number" class="uk-input" min="0" max="990">
                        </div>
                        <div class="">
                            <h5 class="uk-text-bold mb-2"> Số điểm Toeic mong muốn </h5>
                            <input min="0" max="990" type="number" class="uk-input">
                        </div>
                        <div class="uk-grid-margin uk-first-column">
                            <button id="send_run" class="btn btn-default grey" type="button">Gửi</button>
                        </div>
                    </form>
                </div>


                <div id="load_run" style="display:none">
                    <div class="hollow-dots-spinner" :style="spinnerStyle">
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                    </div>
                </div>


                <div id="result" style="display:none">
                    <h1>Cảm ơn bạn đã điền thông tin !</h1>
                    <p>
                        Sau đây là lộ trình gợi ý của Easy Toeic, bạn có thể tham khảo qua.<br/>
                        Chúc bạn học tập thật tốt <3 ! <br/>

                        <button id="close_run" type="button" class="btn btn-animated btn-success btn-animated-x
                        ">
                            <span class="btn-inner--visible">Xem ngay</span>
                            <span class="btn-inner--hidden">
                                <i class="uil-arrow-right"></i>
                            </span>
                        </button>

                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@push('handle_js')
@if (isset($newMember) )
<script>
    $(function() {
    
        UIkit.modal('#modal-wellcome').show();
        
        $('#start_run').on('click', function() {
            $(this).removeClass('animate__fadeInUp');
            $(this).removeClass('animate__delay-3s');
            $(this).addClass('animate__fadeOutDown');

            $('.title_run').removeClass('animate__fadeInRight');
            $('.title_run').removeClass('animate__delay-2s');
            $('.title_run').addClass('animate__fadeOutRight');

            setTimeout(() => {
                $(this).css('display', 'none');
                $('.title_run').css('display', 'none');
            }, 500);

            setTimeout(() => {
                $('#wpp').addClass('animate__fadeInRight');
                $('#wpp').css('display', 'block');
            }, 1000);

        });

        $('#send_run').on('click', function() {
            $('#wpp').removeClass('animate__fadeInRight');
            $('#wpp').addClass('animate__fadeOutRight');
            setTimeout(() => {
                $("#wpp").css('display', 'none');
                $("#load_run").css('display', 'block');

                setTimeout(() => {
                    $("#load_run").css('display', 'none');
                    $('#result').css('display', 'block');
                },1000);
            }, 500);
        });

        $('#close_run').on('click', function() {
            UIkit.modal('#modal-wellcome').hide();
        });

    })

</script> 
@endif
@endpush

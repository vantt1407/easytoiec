<div id="modal-group-1" uk-modal>
    <div class="uk-modal-dialog"> <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">{{$test->test_name}}</h2>
        </div>
        <div class="uk-modal-body">
            <div class="question-content">
                <i>Cảm ơn bạn đã thi thử trên Easy Toeic !</i>
                <br>
                <table class="table-question" style="border: none">
                    <thead>
                        <tr>
                            <th colspan="2" style="color: #D93425">
                                Listening: 0/100
                            </th>
                            <th class="right-answer" style="color: #D93425">Điểm nghe: 5</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="td-quest">Part I: Picture Description <b>(0/10)</b></td>
                        </tr>
                        <tr>
                            <td class="td-quest">Part II: Question - Response <b>(0/30)</b></td>
                        </tr>
                        <tr>
                            <td class="td-quest">Part III: Short Conversations <b>(0/30)</b></td>
                        </tr>
                        <tr>
                            <td class="td-quest">Part IV: Short Talks <b>(0/30)</b></td>
                        </tr>
                    </tbody>
                </table>

                <table class="table-question" style="border: none">
                    <thead>
                        <tr>
                            <th colspan="2" style="color: #D93425">
                                Reading: 0/100
                            </th>
                            <th class="right-answer" style="color: #D93425">Điểm đọc: 5</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="td-quest">Part V: Incomplete Sentences <b>(0/40)</b></td>
                            
                        </tr>
                        <tr>
                            <td class="td-quest">Part VI: Text Completion <b>(0/12)</b></td>
                           
                        </tr>
                        <tr>
                            <td class="td-quest">Part VII: Reading Comprehension <b>(0/48)</b></td>
                           
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="uk-modal-footer uk-text-right"> 
            <button class="uk-button uk-button-default uk-modal-close" type="button">Thoát</button> 
            <a href="#modal-group-1" class="uk-button uk-button-primary" uk-toggle>Chi tiếp đáp án</a> </div>
    </div>
</div>

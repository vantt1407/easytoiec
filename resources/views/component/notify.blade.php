<script>
    function showNotificationError(content) {
        new Noty({
            theme: 'bootstrap-v4'
            , type: 'error'
            , text: `<i class='uil-time'></i>${content}`
            , timeout: 2000
        }).show();
    }

    function showNotificationSuccess(content) {

        new Noty({
            theme: 'bootstrap-v4'
            , type: 'success'
            , text: `<i class='uil-check'></i>${content}`
            , timeout: 2000
        }).show();
    }
    @if(session('success'))
    showNotificationSuccess("{{session('success')}}");
    @endif
    @if(session('error'))
    showNotificationError("{{session('error')}}");
    @endif
    @if(session('total_error'))
    showNotificationError("{{session('total_error')}}");
    @endif
</script>

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' => [
        'client_id' => '711546209684490',         // Your Facebook Client ID
        'client_secret' => 'f7ac898c6baa8f378ded7a7b78e84884', // Your Facebook Client Secret
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],
    'google' => [
        'client_id' => '739894489428-8jta1k0f74fcrj8b8162m8lp3tmpqrmc.apps.googleusercontent.com',         // Your google Client ID
        'client_secret' => 'b3MqMSCTH9m0pNpDiKJXwMtI', // Your google Client Secret
        'redirect' => 'http://localhost:8000/login/google/callback',
    ],

    'linkedin' => [
        'client_id' => '864m70pz38bi4w',         // Your twitter Client ID
        'client_secret' => 'qt5xyXKj9cXEkCnD', // Your twitter Client Secret
        'redirect' => 'http://localhost:8000/login/linkedin/callback',
    ]

];

<?php


return [
    'test' => [
        '120' => [
            'part1' => 1,
            'part2' => 1,
            'part3' => 7,
            'part4' => 10,
            'part5' => 1,
            'part6' => 4,
            'part7' => 16
        ],
        '60' => [
            'part1' => 1,
            'part2' => 1,
            'part3' => 4,
            'part4' => 5,
            'part5' => 1,
            'part6' => 2,
            'part7' => 8
        ],
        '10' => [
            'part1' => 1,
            'part2' => 1,
            'part3' => 1,
            'part4' => 1,
            'part5' => 1,
            'part6' => 1,
            'part7' => 1
        ],
        '5' => [
            'part1' => 1,
            'part2' => 1,
            'part3' => 1,
            'part4' => 1,
            'part5' => 1,
            'part6' => 1,
            'part7' => 1
        ]
    ],
    'practice' => 10,
    'lesson_part_question' => 7
];



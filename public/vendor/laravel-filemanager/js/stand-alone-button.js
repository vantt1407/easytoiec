(function ($) {
    $.fn.filemanager = function (type, options) {
        type = type || "file";

        this.on("click", function (e) {
            let target_input = $("#" + $(this).data("input"));
            let target_preview = $("#" + $(this).data("preview"));
            let classAudio =  $(this).data("audio")
            let $audio = classAudio ? $('.'+classAudio) : classAudio;
            
            let path_name = $("#"+ $(this).data("path-name"));
            let modal = UIkit.modal("#modal-file-full", { stack: true });
            let $element = "#modal-file-full";
            let $wrapper = $('#wrapper-iframe');

            let $iframe =`<iframe id="iframe" style="width:100%;height:100vh" src="/filemanager?type=image" frameborder="0"></iframe>`;

            $wrapper.html($iframe);

            let dropIframe = function() {
              $wrapper.html('');
            }
            let handleIframe = function () {
                let iframe = document.getElementById("iframe");
                let iframeWindow =
                    iframe.contentWindow || iframe.contentDocument;
                iframeWindow.modal = modal;
                // console.log(iframeWindow);
                let openModal = { el: $element };
                openModal.SetUrl = function (items) {
                    console.log(items);
                    let file_path = items
                        .map(function (item) {
                          return (new URL(item.url)).pathname;
                        })
                        .join(",");

                    // set the value of the desired input to image url
                    target_input.val("").val(file_path).trigger("change");
                    path_name.html("").html(items[0].name).trigger("change");
                    if( $audio ) {
                        $audio.children('source').attr('src',file_path);
                        $audio[0].load();
                    } else {
                        // clear previous preview
                        target_preview.html("");
                        // set or change the preview image src
                        items.forEach(function (item) {

                            target_preview.append(
                                $("<img>").addClass('image-preview').attr("src", item.thumb_url)
                            );
                        });
                        // trigger change event
                        target_preview.trigger("change");
                    }

                };
                iframeWindow.openModal = openModal;
            };
            $('#iframe').on('load', function () {
                handleIframe();
                modal.show();
            });

            $(document).on("hidden", $element,dropIframe );

        });
    };
})(jQuery);

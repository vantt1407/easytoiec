

var acceptedFiles = window.acceptedFiles;
Dropzone.options.uploadForm = {
    paramName: "upload[]",
    uploadMultiple: false,
    parallelUploads: 5,
    timeout: 0,
    clickable: "#upload-button",
    dictDefaultMessage:"Kéo thả file vào đây",
    init: function () {
        this.on("success", function (response) {
            var status = response.status;
            if (status === "success") {
                loadFolders();
            } else {
                this.defaultOptions.error(response, JSON.parse(status));
            }
        });
    },
    headers: {
        Authorization: "Bearer " + getUrlParam("token"),
    },
    acceptedFiles: acceptedFiles,
    maxFilesize: 100,
};



// 

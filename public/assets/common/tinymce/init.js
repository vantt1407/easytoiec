$(function() {
    tinymce.init({
        selector: '#editer'
       , height: 300
       , language: 'vi'
       , plugins: [
           'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker'
           , 'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking'
           , 'table emoticons template paste help'
       ]
       , image_description: true
       , relative_urls: true
       , toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
           'bullist numlist outdent indent | link image | print preview media fullpage | ' +
           'forecolor backcolor emoticons | help'
       , menubar: 'favs file edit view insert format tools table help'
       , file_browser_callback: function(field_name, url, type, win) {
           var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
           var y = 800;
           var cmsURL = '/filemanager?field_name=' + field_name;
           if (type == 'image') {
               cmsURL = cmsURL + "&type=image";
           } else {
               cmsURL = cmsURL + "&type=file";
           }
           tinyMCE.activeEditor.windowManager.open({
                file: cmsURL
               , title: 'Quản lý hình ảnh'
               , width: x * 0.8
               , height: y * 0.8
               , resizable: "yes"
               , close_previous: "no"
           });
       }
   });
})
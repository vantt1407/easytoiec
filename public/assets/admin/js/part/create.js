$(function () {
    $('#part_avatar').on('change',function () {
        $(this).removeClass('has-error');
        $('#part_avatar-error').remove();
    });
    $.validator.setDefaults({
        ignore: [],
    });
    $("#form-part-create").validate({
        submitHandler: function (form) {
            $(form)[0].submit();
        },
        rules: {
            part_avatar: {
                required: true,
            },
            part_type_id: {
                required: true,
            },
            part_name: {
                required: true,
                maxlength: 100
            },
            part_description: {
                required: true,
                maxlength: 1500
            },
        },
        messages: {
            part_avatar: {
                required: "Ảnh đại diện không được bỏ trống !",
            },
            part_type_id: {
                required: "Loại phần không được bỏ trống !",
            },
            part_name: {
                required: "Tên phần không được bỏ trống !",
                maxlength: "Tên phần tối đa 100 kí tự !"
            },
            part_description: {
                required: "Mô tả không được bỏ trống !",
                maxlength: "Mô tả tối đa 1500 kí tự !"
            },
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            error.insertAfter(element);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("has-success").removeClass("has-error");
        },
    });

    $("#choice-photo").filemanager("image");
});

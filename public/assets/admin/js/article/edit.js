$(function (){

    $('#article_avatar').on('change',function () {
        $(this).removeClass('has-error');
        $('#article_avatar-error').remove();
    });
    $.validator.setDefaults({
        ignore: [],
    });
    $("#form-article-edit").validate({
        submitHandler: function (form) {
            $(form)[0].submit();
        },
        rules: {
            article_category_id:{
                required: true,
            },
            article_avatar: {
                required: true,
            },
            article_publish: {
                required: true,
            },
            article_title: {
                required: true,
                maxlength: 100
            },
            article_content: {
                required: true,
                maxlength:50000
            }
        }, 
        messages: {
            article_category_id:{
                required: () =>{  UIkit.modal.alert('Vui lòng chọn danh mục !') ; return ''},
            },
            article_avatar: {
                required: "Ảnh đại diện không được bỏ trống !",
            },
            article_publish: {
                required: "Công khai bài viết không được bỏ trống !",
            },
            article_title: {
                required: "Tên bài viết không được bỏ trống !",
                maxlength: "Tên bài viết tối đa 50000 kí tự !"
            },
            article_content: {
                required: "Nội dung không được bỏ trống !",
                maxlength: "Nội dung tối đa 2000 kí tự !"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            error.insertAfter(element);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("has-success").removeClass("has-error");
        },
    });


    $('#choice-photo').filemanager();
});
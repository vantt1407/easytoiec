function format(d) {
    
    return (
        `
         <div>
         ${d.lesson_part_content}
         </div>
        `
    );

    return '';
}

$(function () {
    var table = $("#example").DataTable({
        processing: true,
        serverSide: true,
        ajax: window.url_data,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Vietnamese.json"
        },
        columns: [
            {
                orderable: false,
                data: null,
                searchable: false,
                defaultContent: ' <input class="skin-square check" type="checkbox">',

            },
            {
                className: "details-control",
                orderable: false,
                searchable: false,
                data: null,
                defaultContent:
                    '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="plus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="9.5" y1="5" x2="9.5" y2="14"></line><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>',
            },
            {data: "lesson_part_title", title: "<b style='color: #29303b;'>Tiêu đề</b>"},
            {data: "action", title: "<b style='color: #29303b;'>Thao tác</b>",orderable: false,searchable: false},

        ],
        dom: 'Bfrtip',
        buttons: [

            {
                extend: 'copyHtml5',
                text: '<i class="uil-file-plus text-success"></i>',
                className: 'btn btn-icon btn-hover btn-circle',
                attr: {
                    'uk-tooltip': "Thêm mới",
                    id: 'add-question'
                },
                action: function () {
                    window.location.assign(window.create_url)
                }
            },
            {
                extend: 'copyHtml5',

                text: '<i class="uil-trash-alt text-danger"></i>',
                className: 'btn btn-icon btn-hover  btn-circle',
                attr: {
                    'uk-tooltip': "Xoá",
                    id: 'delete-question'
                }
            }
        ],
        "drawCallback": function (settings) {
            $(function () {
                var checkAll = $('input.all');
                var checkboxes = $('input.check');
                var parentTr = checkboxes.closest('tr');
                var bgClass = 'check-bg';

                $('.skin-square').iCheck({
                    checkboxClass: 'icheckbox_square-purple',
                    increaseArea: '20%'
                });
                checkAll.on('ifChecked ifUnchecked', function (event) {
                    if (event.type == 'ifChecked') {
                        checkboxes.iCheck('check');
                        parentTr.addClass(bgClass);
                    } else {
                        if (checkboxes.filter(':checked').length == checkboxes.length) {
                            checkboxes.iCheck('uncheck');
                            parentTr.removeClass(bgClass);
                        }
                    }
                });
                checkboxes.on('ifChecked ifUnchecked', function (event) {
                    if (event.type == 'ifChecked') {

                        $(event.target).closest('tr').addClass(bgClass);

                    } else {
                        console.log();
                        $(event.target).closest('tr').removeClass(bgClass);
                    }

                    if (checkboxes.filter(':checked').length == checkboxes.length) {
                        checkAll.prop('checked', 'checked');
                        checkAll.iCheck('update');
                    } else {
                        checkAll.iCheck('uncheck');
                    }
                });
            });
        }
    });

    // Add event listener for opening and closing details
    $("#example tbody").on("click", "td.details-control", function () {
        var tr = $(this).closest("tr");
        var row = table.row(tr);
        var td = $(this);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass("shown");
            td.html(
                '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="plus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="9.5" y1="5" x2="9.5" y2="14"></line><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>'
            );
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass("shown");

            td.html(
                '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="minus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>'
            );
        }
    });
});

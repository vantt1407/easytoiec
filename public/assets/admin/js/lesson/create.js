$(function (){

    $('#lesson_avatar').on('change',function () {
        $(this).removeClass('has-error');
        $('#lesson_avatar-error').remove();
    });
    $.validator.setDefaults({
        ignore: [],
    });
    $("#form-lesson-create").validate({
        submitHandler: function (form) {
            $(form)[0].submit();
        },
        rules: {
            lesson_avatar: {
                required: true,
            },
            level_id: {
                required: true,
            },
            lesson_title: {
                required: true,
                maxlength: 100
            },
            lesson_description: {
                required: true,
                maxlength: 1500
            },
            lesson_long_description: {
                required: true,
                maxlength: 50000
            },
        },
        messages: {
            lesson_avatar: {
                required: "Ảnh đại diện không được bỏ trống !",
            },
            level_id: {
                required: "Mức độ điểm không được bỏ trống !",
            },
            lesson_title: {
                required: "Tên bài giảng không được bỏ trống !",
                maxlength: "Tên bài giảng tối đa 100 kí tự !"
            },
            lesson_description: {
                required: "Mô tả không được bỏ trống !",
                maxlength: "Mô tả tối đa 1500 kí tự !"
            },
            lesson_long_description: {
                required: "Kết quả đạt được không được bỏ trống !",
                maxlength: "Kết quả đạt được tối đa 50000 kí tự !"
            },
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            error.insertAfter(element);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("has-success").removeClass("has-error");
        },
    });


    $('#choice-photo').filemanager();
});
$(function () {
    $("#choice-photo").filemanager();

    $('#lesson_part_avatar').on('change',function () {
        $(this).removeClass('has-error');
        $('#lesson_part_avatar-error').remove();
    });
    $.validator.setDefaults({
        ignore: [],
    });
    $("#form-lesson-part-create").validate({
        submitHandler: function (form) {
            $(form)[0].submit();
        },
        rules: {
            lesson_part_avatar: {
                required: true,
            },
            lesson_part_title: {
                required: true,
                maxlength: 100
            },
            lesson_part_content: {
                required: true,
                maxlength: 50000
            }
        },
        messages: {
            lesson_part_avatar: {
                required: "Ảnh đại diện không được bỏ trống !",
            },
            lesson_part_title: {
                required: "Tên bài giảng không được bỏ trống !",
                maxlength: "Tên bài giảng tối đa 100 kí tự !"
            },
            lesson_part_content: {
                required: "Nội dung không được bỏ trống !",
                maxlength: "Nội dung tối đa 50000 kí tự !"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            error.insertAfter(element);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("has-success").removeClass("has-error");
        },
    });
});

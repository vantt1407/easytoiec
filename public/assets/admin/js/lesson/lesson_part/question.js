function format(d) {
    var tpl = "";
    var listAnwsers = d.answers;
    listAnwsers.forEach((element) => {
        tpl += `<div style="margin-bottom:15px" class=" ${
            element.answer_is_correct == 1 ? "text-success" : ""
        }">
                ${element.answer_option}${
            element.answer_is_correct == 1 ? '<i class="uil-check"></i>' : ""
        }
                </div>`;
    });
    return `<div  style="display:inline-block">

        <h4>Câu trả lời</h4>

        ${tpl}
        </div>
        <img style="${
            d.question_image ? "display:inline-block" : "display:none"
        };margin-top: -149px;margin-left: 20px;" width="250" src="${
        d.question_image
    }"/>
        <audio controls style="${
            d.question_audio ? "display:inline-block" : "display:none"
        };margin-left: 20px; margin-top: -144px;">
        <source  src="${d.question_audio}"/>
        </audio>
        `;
}

$(function () {
    var table = "";
    $(document).on("shown", "#modal-question-file-full", function () {
        table = $("#example").DataTable({
            ajax: window.url_data,
            processing: true,
            serverSide: true,
            language: {
                url:
                    "//cdn.datatables.net/plug-ins/1.10.21/i18n/Vietnamese.json",
            },
            columns: [
                {
                    orderable: false,
                    data: "check",
                    sortable: false,
                    searchable: false,
                },
                {
                    className: "details-control",
                    orderable: false,
                    sortable: false,
                    searchable: false,
                    data: null,
                    defaultContent:
                        '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="plus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="9.5" y1="5" x2="9.5" y2="14"></line><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>',
                },
                {
                    data: "question",
                    title: "<b style='color: #29303b;'>Câu hỏi</b>",
                },
                {
                    data: "part.part_name",
                    title: "<b style='color: #29303b;'>Phần</b>",
                }
               
            ],
            order: [[1, "asc"]],
            dom: "Bfrtip",
            buttons: [
                {
                    extend: "copyHtml5",
                    text: '<i class="uil-file-plus text-success"></i>',
                    className: "btn btn-icon btn-hover btn-circle",
                    attr: {
                        "uk-tooltip": "Chọn",
                        id: "add-question",
                    },
                    action: function () {
                        let qIds = $(".check:checked")
                            .map(function () {
                                return $(this).val();
                            })
                            .toArray();

                        $.ajaxSetup({
                            headers: {
                                "X-CSRF-TOKEN": $(
                                    'meta[name="csrf-token"]'
                                ).attr("content"),
                            },
                        });
                        $.ajax({
                            type: "POST",
                            url: window.random_url,
                            data: { ids: JSON.stringify(qIds) },
                            success: function (data) {
                                $.each(data, function (index, item) {
                                    let answers = item.answers;
                                    let tplAnswers = "";
                                    $.each(answers, function (index, item) {
                                        if (item.answer_is_correct) {
                                            tplAnswers += `<p class=" text-success">
                                                            ${item.answer_option}
                                                            <i class="uil-check"></i>
                                                        </p>`;
                                        }
                                        tplAnswers += `<p>${item.answer_option}</p>`;
                                    });

                                    $("#list-question").append(
                                        $(`
                                   
                                   <li> 
                                   
                                   <input class="question_data" type="hidden"  name="question_id[]" value="${
                                       item.question_id
                                   }" />
                                   <a class="uk-accordion-title" href="#">
                                   ${item.question}  (${item.part.part_name})
                                   </a>
                                   <a class="delete-question btn btn-icon btn-hover btn-circle" type="button" uk-tooltip="Xoá"  title="" >
                                    <span>
                                    <i class="uil-trash-alt text-danger"></i>
                                    </span>
                                   </a>
                                        <div class="uk-accordion-content">
                                        <div class="uk-child-width-expand@s" uk-grid>
                                        <div>
                                            <div class="uk-card uk-card-default uk-card-body">
                                            ${tplAnswers}
                                            </div>
                                        </div>
                                        <div>
                                            <div style="${
                                                item.question_image
                                                    ? ""
                                                    : "display:none"
                                            }"  class="uk-card uk-card-default uk-card-body">
                                            <img style="${
                                                item.question_image
                                                    ? "display:inline-block"
                                                    : "display:none"
                                            };" width="250" src="${
                                            item.question_image
                                        }"/>
                                            </div>
                                        </div>
                                        <div>
                                            <div style="${
                                                item.question_audio
                                                    ? ""
                                                    : "display:none"
                                            }" class="uk-card uk-card-default uk-card-body">
                                            <audio style="  width:250px !important; " controls >
                                            <source  src="${
                                                item.question_audio
                                            }"/>
                                            </audio>
                                            </div>
                                        </div>
                                    </div>
                                           
                                          
               
                                        </div>
                                    </li>
                                   
                                   `)
                                    );
                                });
                                UIkit.modal("#modal-question-file-full").hide();
                            },
                            error: function (err) {
                                UIkit.modal("#modal-question-file-full").hide();
                            },
                        });
                    },
                },
            ],
            drawCallback: function (settings) {
                $(function () {
                    var checkAll = $("input.all");
                    var checkboxes = $("input.check");
                    var parentTr = checkboxes.closest("tr");
                    var bgClass = "check-bg";

                    $(".skin-square").iCheck({
                        checkboxClass: "icheckbox_square-purple",
                        increaseArea: "20%",
                    });
                    checkAll.on("ifChecked ifUnchecked", function (event) {
                        if (event.type == "ifChecked") {
                            checkboxes.iCheck("check");
                            parentTr.addClass(bgClass);
                        } else {
                            if (
                                checkboxes.filter(":checked").length ==
                                checkboxes.length
                            ) {
                                checkboxes.iCheck("uncheck");
                                parentTr.removeClass(bgClass);
                            }
                        }
                    });
                    checkboxes.on("ifChecked ifUnchecked", function (event) {
                        if (event.type == "ifChecked") {
                            $(event.target).closest("tr").addClass(bgClass);
                        } else {
                            console.log();
                            $(event.target).closest("tr").removeClass(bgClass);
                        }

                        if (
                            checkboxes.filter(":checked").length ==
                            checkboxes.length
                        ) {
                            checkAll.prop("checked", "checked");
                            checkAll.iCheck("update");
                        } else {
                            checkAll.iCheck("uncheck");
                        }
                    });
                });
            },
        });

        // Add event listener for opening and closing details
        $("#example tbody").on("click", "td.details-control", function () {
            var tr = $(this).closest("tr");
            var row = table.row(tr);
            var td = $(this);
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass("shown");
                td.html(
                    '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="plus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="9.5" y1="5" x2="9.5" y2="14"></line><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>'
                );
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass("shown");

                td.html(
                    '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="minus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>'
                );
            }
        });
    });

    $(document).on("hidden", "#modal-question-file-full", function () {
        table.clear();
        table.destroy();
    });
});

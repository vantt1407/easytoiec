$(function () {
    $('#create-new').on('click',function () {
        UIkit.modal('#popup-create-question',{stack: true}).show();
    });

    $('#popup-create-question').on('shown', function() {
        $('#modal_question_photo').on('change',function () {
            $(this).removeClass('has-error');
            $('#article_avatar-error').remove();
        });
        $('#modal_question_audio').on('change',function () {
            $(this).removeClass('has-error');
            $('#vocabulary_audio-error').remove();
        });
        $('#no-answer').hide();
        $('#modal-choice-photo').filemanager();
        $('#modal-choice-audio').filemanager();
        $.validator.setDefaults({
            ignore: [],
        });
        $.validator.addMethod("string", function (value, element) {
            return /^[a-zA-Z0-9.\-$* ]{1,}$/.test(value);
        });
        $("#form-create-modal").validate({
            submitHandler: function(form) {
                if($(document).find('#question-container-modal').hasClass('uk-sortable-empty')) {
                    $('#no-answer').show();
                    return;
                }
                 $(form)[0].submit();
            },
            rules: {
                modal_question: {
                    required: true,
                    minlength: 2,
                    maxlength: 100,
                    string: true,
                },
                modal_question_scores: {
                    required: true,
                    minlength: 2,
                    number: true
                },
                modal_question_time:{
                    required: true,
                    number: true
                },
                modal_question_image: {
                    required: true,
                    // minlength: 2,
                    // maxlength: 20,
                    // string: true,
                },
                modal_question_audio: {
                    required: true,
                    // string: true,
                },
                "answer_option_modal[]":
                    {
                        required: true,
                        string: true,
                    },
                is_correct_modal: {
                    required:true
                }
                // topic_id: "required",
            },
            messages: {
                modal_question: {
                    required: "Câu hỏi không được bỏ trống !",
                    minlength: "Tối thiểu 2 ký tự !",
                    maxlength: "Tối đa 100 ký tự !",
                    string: "Câu hỏi phải là chữ không dấu !"
                },
                modal_question_scores: {
                    required: "Điểm không được bỏ trống !",
                    minlength: "Tối thiểu 2 ký tự !",
                    // maxlength: "Tối đa 50 ký tự !",
                    number: "Điểm phải là số !"
                },
                modal_question_time: {
                    required: "Thời gian không được bỏ trống !",
                },
                modal_question_image: {
                    required: "Hình ảnh không được bỏ trống !"
                },
                modal_question_audio: {
                    required: "Âm thanh không được bỏ trống !",

                },
                "answer_option_modal[]": {
                    required: "Câu trả lời không được bỏ trống!",
                },
                is_correct_modal: {
                    required: "Không có đáp án nào được chọn đúng !",
                }
            },
            errorElement: "em",
            errorPlacement: function (error, element) {



                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "radio") {
                    UIkit.modal
                        .alert(error.text(),{stack: true})

                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).addClass("has-success").removeClass("has-error");
            }
        });
        $('body').on('click', '#btn-add-question-modal',function () {
            if($(document).find('#question-container-modal').hasClass('uk-sortable-empty')) {
                $('#no-answer').show();
            }
        });
        $('body').on('keypress','.answer-option-modal',function(){

            $(this).next().children('.iradio_square-purple').children('.is-correct-modal').val(  $(this).val() );
        });

        let initIcheckModal = function() {
            var iBoxes = $('.skin-square');
            $.each(iBoxes,function (index,item) {
                let $dom = $(item);
                if($dom.attr('type')==='radio') {
                    $dom.iCheck({
                        radioClass: 'iradio_square-purple'
                        , increaseArea: '20%'
                    });
                } else {
                    $dom.iCheck({
                        checkboxClass: 'icheckbox_square-purple',
                        increaseArea: '20%'
                    });
                }
            });
            $('#change-type-add-question').on('ifChecked ifUnchecked',function () {
                if(this.checked) {

                    $('#sigle-input').hide();
                    $('#group-input').show();
                } else {
                    $('#sigle-input').show();
                    $('#group-input').hide();
                }
            });
        }
        $('#btn-add-answer-modal').on('click',function() {
            $('#no-answer').hide();
            if($(document).find('#question-container-modal').children('div').length === 4) {
                return;
            }
            let tpl = ` <div class="uk-margin wrapper-question">
                            <div class="uk-card uk-card-default uk-card-body uk-card-small">
                                <input name="answer_option_modal[]" type="text" class="uk-input uk-grid-small answer-option-modal"  placeholder="Nhập đáp án">
                                <div class="wp" style="float:right">
                                    <input  class="skin-square is-correct-modal" type="radio" name="is_correct_modal"> Đáp án đúng
                                    <button type="button" class="btn btn-icon btn-hover btn-circle btn-delete-question" uk-tooltip="Xóa" title="" aria-expanded="false">
                                        <i class="uil-trash-alt text-danger"></i>
                                    </button>
                                </div>
                            </div>
                        </div>`;

            $('#question-container-modal').append($(tpl));
            initIcheckModal();
        });
    });

});

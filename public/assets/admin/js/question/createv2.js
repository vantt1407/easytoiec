$(function() {
    $.validator.setDefaults({
        ignore: [],
    });
    $.validator.addMethod("string", function (value, element) {
        return /^[a-zA-Z0-9.\-$* ]{1,}$/.test(value);
    });
    $("#form-create-single").validate({
        submitHandler: function(form) {
            $("#form-create-single")[0].submit();
        },
        rules: {
            question: {
                required: true,
          //      minlength: 2,
                maxlength: 100,
                // string: true,
            },
            question_scores: {
                required: true,
                // minlength: 2,
                number: true
            },
            question_time:{
                required: true,
                number: true
            },
            question_image: {
                required: true,
                // minlength: 2,
                // maxlength: 20,
                // string: true,
            },
            question_audio: {
                required: true,
                // string: true,
            },
            "answer_option[]":
                {
                    required: true,
                    string: true,
                },
            is_corect: {
                required:true
            },
            part_id: "required",
        },
        messages: {
            question: {
                required: "Câu hỏi không được bỏ trống !",
           //     minlength: "Tối thiểu 2 ký tự !",
                maxlength: "Tối đa 100 ký tự !",
                // string: "Câu hỏi phải là chữ không dấu !"
            },
            question_scores: {
                required: "Điểm không được bỏ trống !",
                // minlength: "Tối thiểu 2 ký tự !",
                // maxlength: "Tối đa 50 ký tự !",
                number: "Điểm phải là số !"
            },
            question_time: {
                required: "Thời gian không được bỏ trống !",
            },
            question_image: {
                required: "Hình ảnh không được bỏ trống !"
            },
            question_audio: {
                required: "Âm thanh không được bỏ trống !",

            },
            "answer_option[]": {
                required: "Câu trả lời không được bỏ trống!",
            },
            is_corect: {
                required: "Không có đáp án nào được chọn đúng !",
            },
            part_id:  {
                required: "Không có phần thi nào được chọn !",
            },
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "radio") {
                UIkit.modal
                    .alert(error.text(),{stack: true})

                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("has-success").removeClass("has-error");
        }
    });




    //#################################################################################################
    //#################################################################################################
    //#################################################################################################
    // $('#group-input').hide();

    $('#question-image').filemanager('image');
    $('#question-audio').filemanager('image');
    var bar = document.getElementById('js-progressbar');
    $('#uploading').text('Đang tải lên: ');
    var animate = setInterval(function () {
        bar.value += 1;
        $('#progress-value').text(`${bar.value}%`)
        if (bar.value >= bar.max) {
            clearInterval(animate);
        }
    }, 100);
    $('#btn-add-question').on('click',function() {
        let tpl = ` <div class="uk-margin wrapper-question">
                            <div class="uk-card uk-card-default uk-card-body uk-card-small">
                                <input name="answer_option[]" type="text" class="uk-input uk-grid-small answer-option"  placeholder="Nhập đáp án">
                                <div class="wp" style="float:right">
                                    <input  class="skin-square is-corect" type="radio" name="is_corect"> Đáp án đúng
                                    <button type="button" class="btn btn-icon btn-hover btn-circle btn-delete-question" uk-tooltip="Xóa" title="" aria-expanded="false">
                                        <i class="uil-trash-alt text-danger"></i>
                                    </button>
                                </div>
                            </div>
                        </div>`;

        $('#question-container').append($(tpl));
        window.initIcheck();
    })


    $('body').on('keypress','.answer-option',function(){

        $(this).next().children('.iradio_square-purple').children('.is-corect').val(  $(this).val() );
    });
    $('body').on('click','.btn-delete-question', function() {
        $(this).closest('.wrapper-question').remove();
    });
    $('body').on('click', '.btn-create-single-question',function () {
        if($(document).find('#question-container').hasClass('uk-sortable-empty')) {
            $('#no-answer').show();
        }
    });
    $('#question_image').on('change',function () {
        $(this).removeClass('has-error');
        $('#question_image-error').remove();
    });
    $('#question_audio').on('change',function () {
        $(this).removeClass('has-error');
        $('#question_audio-error').remove();
    });
    $('#no-answer').hide();
    $('#modal-choice-photo').filemanager();
    $('#modal-choice-audio').filemanager();

});

// window.db = new Dexie("db_tmp");
// window.db.version(1).stores({
//     question_tmp: '++id,' +
//         'modal_question,' +
//         'modal_question_scores,' +
//         'modal_question_time,' +
//         'modal_question_image,' +
//         'modal_question_audio,' +
//         'answer'
// });
// window.db.question_tmp.each(item => console.log(item));

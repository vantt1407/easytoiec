$(function () {

    function callApi(url,data,key) {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $(
                    'meta[name="csrf-token"]'
                ).attr("content"),
            },
        });
        $.ajax({
            url:url,
            type: "POST",
            data: {
                _method: "put",
                key: key,
                input:data
            },
        })
            .done(function (response) {
                new Noty({
                    theme: "bootstrap-v4",
                    type: "success",
                    text: `<i class='uil-check'></i>${response}`,
                    timeout: 2000,
                }).show();
                
            })
            .fail(function (err) {
                new Noty({
                    theme: "bootstrap-v4",
                    type: "error",
                    text: `<i class='uil-time'></i>${err}`,
                    timeout: 2000,
                }).show();
                table.ajax.reload();
            });
    }

    $('#update-info').on('click', function() {
        $data = $('#info-form').serializeArray();
        callApi(window.info_update,$data,'info');
    });

    $('#app-update').on('click', function() {
        $data = $('#app-update-form').serializeArray();

        callApi(window.app_update,$data,'mail');
       
    });

    $('#random-update').on('click', function() {

        $data = $('#random-update-form').serializeArray();
        callApi(window.random_update,$data,'random');
    });

 
    
    
    $('#pass-mail').on('focus',function() {
        $(this).attr('type','text');
    });

    $('#pass-mail').on( "focusout", function() {
        $(this).attr('type','password');
    } );
});
$(function () {
   
    $.validator.setDefaults({
        ignore: [],
    });
    $("#form-practice-edit").validate({
        submitHandler: function (form) {
            
            if($('.question_data').length <= 0) {
                UIkit.modal.alert("Câu hỏi không được bỏ trống");
                return;
            }
            $(form)[0].submit();
        },
        rules: {
            level_id: {
                required: true,
            },
            practice_name: {
                required: true,
                maxlength: 50
            }
        },
        messages: {
            
            level_id: {
                required: "Độ khó không được bỏ trống !",
            },
            practice_name: {
                required: "Tên bài luyện tập không được bỏ trống !",
                maxlength: "Tên bài luyện tập tối đa 50 kí tự !"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            error.insertAfter(element);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("has-success").removeClass("has-error");
        },
    });
});

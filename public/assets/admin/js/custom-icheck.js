$(function () {
    var checkAll = $('input.all');
    var checkboxes = $('input.check');
    var parentTr = checkboxes.closest('tr');
    var bgClass = 'check-bg';

    $('.skin-square').iCheck({
                checkboxClass: 'icheckbox_square-purple',
                increaseArea: '20%'
    });
    checkAll.on('ifChecked ifUnchecked', function(event) {        
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
            parentTr.addClass(bgClass);
        } else {
            if(checkboxes.filter(':checked').length == checkboxes.length) {
                checkboxes.iCheck('uncheck');
                parentTr.removeClass(bgClass);
             } 
        }
    });
    checkboxes.on('ifChecked ifUnchecked', function(event) {
        if (event.type == 'ifChecked') {
           
            $(event.target).closest('tr').addClass(bgClass);
            
        } else {
            console.log();
            $(event.target).closest('tr').removeClass(bgClass);
        }

        if(checkboxes.filter(':checked').length == checkboxes.length) {
            checkAll.prop('checked', 'checked');
            checkAll.iCheck('update');
        } else {
            checkAll.iCheck('uncheck');
        }
    });
});
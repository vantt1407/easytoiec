



$(function () {
    $("#delete-user").on("click", function (e) {
        $check = $(".checked");
        if ($check.length === 0 ) {
            return;
        }
        var ids = [];
        $.each($check, function (key, value) {
            var id = $(value).children("input").val();
            id != 'on' ?  ids.push( id  ) : '';
        });
        $("#form-delete-user").attr("action", function (index, value) {
            ids = ids.join(",");
            return `${value}?ids=${ids}`;
        });
        if(!ids.length) { return; }
        UIkit.modal
            .confirm("Bạn có chắc muốn xoá !", {
                labels: { ok: "Xoá", cancel: "Huỷ" },
            })
            .then(
                function () {
                    $("#form-delete-user").submit();
                },
                function (err) {}
            );
    });
});

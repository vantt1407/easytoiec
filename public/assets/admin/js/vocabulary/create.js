$(document).ready(function () {
    $('#article_avatar').on('change',function () {
        $(this).removeClass('has-error');
        $('#article_avatar-error').remove();
    });
    $('#vocabulary_audio').on('change',function () {
        $(this).removeClass('has-error');
        $('#vocabulary_audio-error').remove();
    });
    $.validator.setDefaults({
        ignore: [],
    });
    $.validator.addMethod("string", function (value, element) {
        return /^[a-zA-Z]{1,}$/.test(value);
    });
    // $.validator.addMethod("pronounce", function (value, element) {
    //     return /^[]$/.test(value);
    // });
    $("#form-create-vocabulary").validate({
        rules: {
            topic_id:{
                required: true,
            },
            vocabulary_word: {
                required: true,
                minlength: 2,
                maxlength: 100,
                string: true,
            },
            vocabulary_vi_translate: {
                required: true,
                minlength: 2,
                maxlength: 100,
                // string: true,
            },
            vocabulary_type_id: "required",
            vocabulary_pronounce: {
                required: true,
                minlength: 2,
                maxlength: 100,
                // pronounce: true,
            },
            vocabulary_image: {
                required: true,
            },
            vocabulary_audio: {
                required: true,
            },
            topic_id: "required",
        },
        messages: {
            topic_id:{
                required: () =>{  UIkit.modal.alert('Vui lòng chọn chủ đề !') ; return ''},
            },
            vocabulary_word: {
                required: "Từ vựng không được bỏ trống !",
                minlength: "Tối thiểu 2 ký tự !",
                maxlength: "Tối đa 100 ký tự !",
                string: "Từ vựng phải là chữ !"
            },
            vocabulary_vi_translate: {
                required: "Nghĩa tiếng việt không được bỏ trống !",
                minlength: "Tối thiểu 2 ký tự !",
                maxlength: "Tối đa 100 ký tự !",
                // string: "Nghĩa tiếng việt phải là chữ !"
            },
            vocabulary_type_id: "Loại từ không được bỏ trống !",
            vocabulary_pronounce: {
                required: "Phiên âm không được bỏ trống !",
                minlength: "Tối thiểu 2 ký tự !",
                maxlength: "Tối đa 100 ký tự !",
                // pronounce: "Phiên âm không hợp lệ !"
            },
            vocabulary_image: {
                required: "Hình ảnh mô tả không được bỏ trống !",
            },
            vocabulary_audio:{
                required: "Âm thanh mô tả không được bỏ trống !",
            },
            topic_id: "Chủ đề không được bỏ trống !",
        },
        errorElement: "em",
        errorPlacement: function (error, element) {

            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "radio") {
                UIkit.modal
                    .alert(error.text())

                error.insertAfter(element.parent("label"));
            } else {

                UIkit.tab('#csw').show(element.closest('.tab-create').index());
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("has-success").removeClass("has-error");
        }
    })
});



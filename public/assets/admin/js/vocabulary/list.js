function format(d) {
    return `<div  style="display:block">

           <img style="display:inline-block;margin-left: 20px;" width="250" src="${d.vocabulary_image}"/>
            <audio controls style="display:inline-block;margin-left: 20px;">
            <source  src="${d.vocabulary_audio}"/>
            </audio>

        </div>

        `;
}

$(function () {
    var table = $("#example").DataTable({
        processing: true,
        serverSide: true,
        ajax: window.url_data,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Vietnamese.json",
        },
        columns: [
            {
                orderable: false,
                data: "vocabulary_id",
                render: function (data, type, row) {
                    return `<input value="${data}" class="skin-square check" type="checkbox">`;
                },
                defaultContent: "",
            },
            {
                className: "details-control",
                orderable: false,
                data: null,
                defaultContent:
                    '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="plus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="9.5" y1="5" x2="9.5" y2="14"></line><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>',
            },
            {
                data: "vocabulary_word",
                title: "<b style='color: #29303b;'>Từ</b>",
            },
            {
                data: "vocabulary_vi_translate",
                title: "<b style='color: #29303b;'>Nghĩa tiếng việt</b>",
            },
            {
                data: "vocabulary_pronounce",
                title: "<b style='color: #29303b;'>Phiên âm</b>",
            },
            {
                sortable: false,
                orderable: false,
                data: "topic.topic_name",
                title: "<b style='color: #29303b;'>Chủ đề</b>",
            },
            { data: "action", sortable: false, orderable: false },
        ],
        dom: "Bfrtip",
        buttons: [
            {
                extend: "copyHtml5",
                text: '<i class="uil-file-plus text-success"></i>',
                className: "btn btn-icon btn-hover btn-circle",
                attr: {
                    "uk-tooltip": "Thêm mới",
                    id: "add-question",
                },
                action: function () {
                    window.location.assign(window.create_url);
                },
            },
            {
                extend: "copyHtml5",

                text: '<i class="uil-trash-alt text-danger"></i>',
                className: "btn btn-icon btn-hover  btn-circle",
                attr: {
                    "uk-tooltip": "Xoá",
                    id: "delete-question",
                },
                action: function () {
                    var dic = $(".check:checked")
                        .map(function () {
                            return $(this).val();
                        })
                        .toArray();
                    if (dic.length <= 0) {
                        UIkit.modal.alert("Không có từ nào được chọn");
                    } else {
                        UIkit.modal
                            .confirm("Bạn có chắc muốn xoá !", {
                                labels: { ok: "Xoá", cancel: "Huỷ" },
                            })
                            .then(
                                function () {
                                    $.ajaxSetup({
                                        headers: {
                                            "X-CSRF-TOKEN": $(
                                                'meta[name="csrf-token"]'
                                            ).attr("content"),
                                        },
                                    });
                                    $.ajax({
                                        url:
                                            window.delete_url +
                                            "?ids=" +
                                            dic.toString(),
                                        type: "POST",
                                        data: {
                                            _method: "delete",
                                        },
                                    })
                                        .done(function (response) {
                                            new Noty({
                                                theme: "bootstrap-v4",
                                                type: "success",
                                                text: `<i class='uil-check'></i>${response}`,
                                                timeout: 2000,
                                            }).show();
                                            table.ajax.reload();
                                        })
                                        .fail(function (err) {
                                            new Noty({
                                                theme: "bootstrap-v4",
                                                type: "error",
                                                text: `<i class='uil-time'></i>${err}`,
                                                timeout: 2000,
                                            }).show();
                                            table.ajax.reload();
                                        });
                                },
                                function (err) {}
                            );
                    }
                },
            },
        ],
        drawCallback: function (settings) {
            $(function () {
                var checkAll = $("input.all");
                var checkboxes = $("input.check");
                var parentTr = checkboxes.closest("tr");
                var bgClass = "check-bg";

                $(".skin-square").iCheck({
                    checkboxClass: "icheckbox_square-purple",
                    increaseArea: "20%",
                });
                checkAll.on("ifChecked ifUnchecked", function (event) {
                    if (event.type == "ifChecked") {
                        checkboxes.iCheck("check");
                        parentTr.addClass(bgClass);
                    } else {
                        if (
                            checkboxes.filter(":checked").length ==
                            checkboxes.length
                        ) {
                            checkboxes.iCheck("uncheck");
                            parentTr.removeClass(bgClass);
                        }
                    }
                });
                checkboxes.on("ifChecked ifUnchecked", function (event) {
                    if (event.type == "ifChecked") {
                        $(event.target).closest("tr").addClass(bgClass);
                    } else {
                        $(event.target).closest("tr").removeClass(bgClass);
                    }

                    if (
                        checkboxes.filter(":checked").length ==
                        checkboxes.length
                    ) {
                        checkAll.prop("checked", "checked");
                        checkAll.iCheck("update");
                    } else {
                        checkAll.iCheck("uncheck");
                    }
                });
            });
        },
    });

    // Add event listener for opening and closing details
    $("#example tbody").on("click", "td.details-control", function () {
        var tr = $(this).closest("tr");
        var row = table.row(tr);
        var td = $(this);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass("shown");
            td.html(
                '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="plus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="9.5" y1="5" x2="9.5" y2="14"></line><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>'
            );
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass("shown");

            td.html(
                '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="minus-circle"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9.5" cy="9.5" r="9"></circle><line fill="none" stroke="#000" x1="5" y1="9.5" x2="14" y2="9.5"></line></svg>'
            );
        }
    });
});



/* ----------------- Start Document ----------------- */
(function ($) {
    "use strict";

    $(document).ready(function () {
        /*--------------------------------------------------*/
        /*  Mobile Menu - mmenu.js
		/*--------------------------------------------------*/
     

        /*----------------------------------------------------*/
        /*  Sidebar Nav Submenus
		/*----------------------------------------------------*/

        $(".page-menu ul li a").on("click", function (e) {
            if ($(this).closest("li").children("ul").length) {
                if ($(this).closest("li").is(".active-submenu")) {
                    $(".page-menu ul li").removeClass("active-submenu");
                } else {
                    $(".page-menu ul li").removeClass("active-submenu");
                    $(this).parent("li").addClass("active-submenu");
                }
                e.preventDefault();
            }
        });

        /*----------------------------------------------------*/
        /*  Back to Top
		/*----------------------------------------------------*/

        // Button
        function backToTop() {
            $("body").append('<div id="backtotop"><a href="#"></a></div>');
        }
        backToTop();

        // Showing Button
        var pxShow = 100; // height on which the button will show
        var scrollSpeed = 500; // how slow / fast you want the button to scroll to top.

        $(window).scroll(function () {
            if ($(window).scrollTop() >= pxShow) {
                $("#backtotop").addClass("visible uk-animation-slide-bottom");
            } else {
                $("#backtotop").removeClass(
                    "visible uk-animation-slide-bottom"
                );
            }
        });

        $("#backtotop a").on("click", function () {
            $("html, body").animate(
                {
                    scrollTop: 0,
                },
                scrollSpeed
            );
            return false;
        });
        $("#logout").on("click", function () {
            $("#logout_form").submit();
        });
        // ------------------ End Document ------------------ //
    });
})(this.jQuery);


